// _arsenalNames = [];
// _arsenalDataLocal = [];
// _arsenalData = profilenamespace getvariable ["bis_fnc_saveInventory_data",[]];

// for "_i" from 0 to (count _arsenalData - 1) step 2 do {
    // _name = _arsenalData select _i;
    // _arsenalDataLocal = _arsenalDataLocal + [_name,_arsenalData select (_i + 1)];
    // _nul = _arsenalNames pushBack ( format[ "missionnamespace:%1", _name ] );
// };
// missionnamespace setvariable ["bis_fnc_saveInventory_data",_arsenalDataLocal];
// [player,_arsenalNames] call bis_fnc_setrespawninventory;

#include "defines.h"

dfk_captiveStatus =
{
	private _dude = _this select 0;
	private _string = _this select 1;
	private _trg = _this select 2;
	
	_trg setpos getpos _dude;
	
	private _change = false;
	
	//_suspiciousGear = [];
	
	//systemchat format ["dfk_captiveStatus1 %1 %2 %3 %4 %5 %6",_string,_dude,primaryweapon _dude,secondaryweapon _dude,handgunweapon _dude,currentweapon _dude];
	//systemchat format ["dfk_captiveStatus2 %1 %2 %3 %4",headgear _dude,vest _dude,backpack _dude,uniform _dude];
	
	if (!_change && triggeractivated _trg) then {_change = true;};
	if (!_change && !(primaryweapon _dude == "")) then {_change = true;};
	if (!_change && !(secondaryweapon _dude == "")) then {_change = true;};
	if (!_change && !(handgunweapon _dude == "")) then {_change = true;};
	if (!_change && !(currentweapon _dude == "")) then {_change = true;};
	if (!_change && (headgear _dude in dfk_captive_contraband)) then {_change = true;};
	if (!_change && (vest _dude in dfk_captive_contraband)) then {_change = true;};
	if (!_change && (backpack _dude in dfk_captive_contraband)) then {_change = true;};
	if (!_change && (!(vehicle _dude == _dude ) && isonroad (vehicle _dude))) then {_change = true;};
	
	private _arr = _dude nearentities ["SoldierEB",20];
	if (!_change && count _arr > 0) then {_change = true};
	
	//if (!_change && (uniform _dude in dfk_captive_contraband)) then {_change = true;};
	
	if (_change) then {_dude setcaptive false;} else {_dude setcaptive true;_dude groupchat format ["%1 could now pass for a civilian",name _dude]};
	
	//if (!_change && )
};

["InitializePlayer", [player, true]] call BIS_fnc_dynamicGroups;

group player setcombatmode "RED";
group player setbehaviour "COMBAT";

private _trg = createTrigger ["EmptyDetector", getpos player];
_trg setTriggerArea [10, 10, 0, false];
_trg setTriggerActivation ["EAST", "PRESENT", true];
//_trg setTriggerStatements ["this", "[thisTrigger] spawn dfk_roadBlockSpawnFunction", "[thisTrigger] spawn dfk_roadBlockDeSpawnFunction"];
_trg setTriggerStatements ["this", "", ""];

player addEventHandler ["InventoryClosed", {[_this select 0,"inv_closed",_trg] call dfk_captiveStatus}];

_dfk_unit_specialtraits = ["dfk_unit_specialtraits"] call BIS_fnc_getParamValue;
if (_dfk_unit_specialtraits == 0) then
{
	player setunittrait ["engineer",true];
	player setunittrait ["explosiveSpecialist",true];
	player setunittrait ["medic",true];
	player setunittrait ["UAVHacker",true];
	player setunittrait ["loadcoef",-2];

	// player addEventHandler ["MPRespawn",{
											// _this select 0 setunittrait ["engineer",true];
											// _this select 0 setunittrait ["explosiveSpecialist",true];
											// _this select 0 setunittrait ["medic",true];
											// _this select 0 setunittrait ["UAVHacker",true];
											// _this select 0 setunittrait ["loadcoef",-2];
										// }];
};
//player addEventHandler ["Take", {[_this select 0,"take"] call dfk_captiveStatus}];

sleep 10;

_fps = 0;
_lastfps = 0;
_fpslimit = 15;
_fpsceil = 24;

_fog = 0;
_fog_incr = 0.05;
_fog_max = 1;
_viewdistance = 1200;
_viewdistance_incr = 100;

_change = false;

private _lastpurge = time;

player setvariable ["dfk_ai_kills",0];



while {true} do
{
	_trg setpos getpos player;
	if (triggeractivated _trg && captive player) then
	{
		player setcaptive false;
	};
	
	if (!(vehicle player == player) && captive player) then
	{
		if (!(isonroad (vehicle player))) then
		{
			player setcaptive false;
		};
	};
	
	//_fog < _fog_max or 
	if (diag_fps <= _fpslimit && (_viewdistance > 100)) then
	{
		//[] remoteexec ["systemchat"];
		//if (_fog < 1) then {_fog = _fog + _fog_incr};
		if (_viewdistance > 200) then {_viewdistance = _viewdistance - _viewdistance_incr};
		_change = true;
	};
	
	//_fog > 0 or 
	if (diag_fps >= _fpsceil && (_viewdistance < 1200)) then
	{
		// if (_fog > 0) then {_fog = _fog - _fog_incr};
		if (_viewdistance < 1200) then {_viewdistance = _viewdistance + _viewdistance_incr};
		_change = true;		
	};
	
	// if (_fog < 0) then {_fog = 0};
	// if (_fog > _fog_max) then {_fog = _fog_max};
	
	if (_viewdistance < 200) then {_viewdistance = 200};
	if (_viewdistance > 1200) then {_viewdistance = 1200};
	
	if (_change) then
	{
		// 10 setfog _fog;
		setviewdistance _viewdistance;
		//diag_log format ["changing fog %1 viewdist %2 | %3",fog,viewdistance,diag_fps];
		//[format ["%4 changing fog %1 viewdist %2 | %3",fog,viewdistance,diag_fps,name player]] remoteexec ["systemchat"];		
		systemchat format ["Viewdistance %1",_viewdistance];
		sleep 1;
		_change = false;
	};
	
	if (isserver && diag_fps <= 15 && viewdistance <= 600 && _lastpurge + 10 > time) then
	{
		_lastpurge = time;
		//[] remoteexec ["dfk_purgeDeadUnits",0];
		private _count = count alldead;
		diag_log format ["initserver purge (initlocal) %1 %2",_count,alldead];
		if (_count > 0) then
		{
			{
				//[_x] call dfk_deletevehicle
				deletevehicle _x;
			} foreach alldead;
			
			[format ["Removed %1 dead things",_count]] remoteexec ["systemchat"];
		};
	};		
	
	//[format ["%1 %2 %3",diag_fps,_fog,_viewdistance]] remoteexec ["systemchat"];
	sleep 1;
};
