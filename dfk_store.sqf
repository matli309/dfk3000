vipstorearray =
[
	["hgun_Pistol_heavy_01_F",10000],
	["SMG_03C_TR_black", 25000],
	
	["arifle_SDAR_F",20000],
	["arifle_ARX_blk_F",80000],			

	["MMG_01_tan_F", 300000],
	["srifle_EBR_F",60000],			
	["srifle_DMR_02_F",100000],
	["launch_MRAWS_green_F",150000],
	["launch_O_Vorona_green_F",200000],		
	["srifle_GM6_F", 300000],	
	["launch_NLAW_F", 250000],	
	["launch_I_Titan_F", 300000],	
	["launch_I_Titan_short_F", 300000],	
	
	["acc_pointer_IR",5000],
	["I_UavTerminal",10000],

	["optic_MRD", 3000],
	["optic_Hamr",20000],	
	["optic_Arco",20000],
	["optic_MRCO",20000],
	["optic_DMS", 20000],	
	["optic_AMS",40000],	
	["optic_NVS", 50000],
	["optic_Nightstalker", 50000],
	["optic_tws", 50000],
	["optic_tws_mg", 50000],
	["optic_LRPS", 50000],
	["optic_KHS_blk", 50000],

	["muzzle_snds_H_snd_F", 5000],
	["muzzle_snds_m_snd_F", 5000],
	["muzzle_snds_58_blk_F", 5000],
	["muzzle_snds_B_snd_F", 5000],
	["muzzle_snds_65_TI_blk_F", 5000],
	["muzzle_snds_H", 5000],
	["muzzle_snds_L", 5000],
	["muzzle_snds_M", 5000],
	["muzzle_snds_B", 5000],
	["muzzle_snds_H_MG", 5000],
	["muzzle_snds_H_SW", 5000],
	["muzzle_snds_acp", 5000],
	["muzzle_snds_570", 5000],

	["H_HelmetB_light",5000],
	["H_HelmetB",2500],
	["H_HelmetB_camo",4000],
	["H_HelmetSpecB_blk",5000],
	["H_HelmetIA",2000],
	["H_HelmetB_light_black",5000],
		
	["U_I_CombatUniform",2000],
	["U_I_CombatUniform_tshirt",2000],
	["U_I_CombatUniform_shortsleeve",2000],		
		
	["V_PlateCarrier1_rgr_noflag_F", 20000],
	["V_PlateCarrier2_rgr_noflag_F", 20000],
	["V_TacChestrig_grn_F", 20000],
	["V_PlateCarrierL_CTRG", 30000],
	["V_PlateCarrierH_CTRG", 30000],
	["V_PlateCarrierIAGL_oli", 30000],
	["V_PlateCarrierSpec_rgr", 30000],
	["U_I_FullGhillie_ard", 40000],
	["B_ViperHarness_blk_F",20000],
	["B_ViperLightHarness_blk_F",15000],
	
	["B_Kitbag_mcamo",10000],		
	["B_Carryall_oli",15000],
	["I_UAV_01_backpack_F", 35000],
	["I_GMG_01_A_weapon_F", 50000],
	["I_HMG_01_A_weapon_F", 50000],
	["I_HMG_01_weapon_F", 50000],
	["I_GMG_01_weapon_F", 50000],
	["I_Mortar_01_support_F", 50000],
	["I_Mortar_01_weapon_F", 50000],
	["I_HMG_01_support_F", 50000],
	["I_HMG_01_support_high_F", 50000],
	["I_AA_01_weapon_F", 100000],
	["I_AT_01_weapon_F", 100000],

	["APERSMine_Range_Mag", 10000],
	["APERSBoundingMine_Range_Mag", 10000],
	["SLAMDirectionalMine_Wire_Mag", 10000],	
	["DemoCharge_Remote_Mag", 15000],	
	["ATMine_Range_Mag", 30000],	
	["SatchelCharge_Remote_Mag", 30000],
	["IEDUrbanBig_Remote_Mag", 20000],
	["IEDLandBig_Remote_Mag", 20000]
];

{vipstorearray pushbackunique _x;} foreach (
												selectrandom 
												[
													[["arifle_CTAR_blk_F", 40000],["arifle_CTAR_GL_blk_F", 50000],["arifle_CTARS_blk_F", 100000]],
													[["arifle_MXC_Black_F", 45000],["arifle_MX_Black_F", 50000],["arifle_MXM_Black_F", 60000],["arifle_MX_GL_Black_F", 60000],["arifle_MX_SW_Black_F", 160000]],	
													[["arifle_SPAR_01_blk_F", 45000],["arifle_SPAR_03_blk_F", 65000],["arifle_SPAR_01_GL_blk_F", 55000],["arifle_SPAR_02_blk_F", 180000]]
												]
											);

storearray =
[
	["Smokes",500],
	["Grenades",5000],
	
	["bnae_saa_virtual",500],
	["CUP_hgun_TaurusTracker455",700],	
	["bnae_l35_virtual",1000],
	["hgun_Pistol_01_F",3000],	
	["hgun_ACPC2_F",5000],
	["hgun_P07_F",6200],
	["hgun_Pistol_heavy_02_F",7300],	
	["hgun_Rook40_F",8000],		
	
	["CUP_sgun_M1014",8000],
	["CUP_sgun_Saiga12K",10000],
	["bnae_mk1_t_virtual",7000],
	["bnae_scope_v3_virtual",7000],
	["CUP_srifle_CZ550",15000],
	
	["CUP_hgun_SA61",3000],
	["hgun_PDW2000_F",8000],
	["SMG_05_F",9000],
	["SMG_01_F",10000],
	["SMG_02_F",15000],

	["arifle_AKS_F",24000],
	["arifle_Mk20C_plain_F",31800],
	["arifle_TRG20_F",41800],
	["arifle_Mk20_plain_F",34000],	
	["arifle_TRG21_F",36000],	
	["arifle_Mk20_GL_ACO_F",37000],
	["arifle_TRG21_GL_F",40000],
	["srifle_DMR_01_F",40000],
	
	["arifle_AKM_F",44000],		
	["srifle_DMR_06_olive_F",45000],
	["arifle_AK12_F",45000],
	["arifle_AK12_GL_F",50000],

	["LMG_03_F",75000],		
	["LMG_Mk200_F",80000],
	["LMG_Zafir_F",95000],	
	["launch_RPG7_F",70000],	
	["launch_MRAWS_green_rail_F",100000],

	["Binocular",500],	
	["bipod_03_F_blk",1000],	
	["Rangefinder",3000],	
	["ItemGPS",5000],	
	["MineDetector",10000],
	["Medikit",12000],
	["ToolKit",8000],
	["NVGoggles_INDEP",25000],	

	["optic_Yorris", 3000],	
	["optic_Aco",8000],
	["optic_Holosight",12000],	
	["optic_SOS",17000],	
	["optic_KHS_old",30000],	
	["muzzle_snds_acp",5000],
	["muzzle_snds_L",5000],
	
	["H_Construction_headset_black_F",150],
	["H_RacingHelmet_1_black_F",500],	
	["H_Helmet_Skate",1000],	
	["H_PASGT_basic_blue_F",2000],	

	["U_I_C_Soldier_Para_1_F",1000],
	["U_I_C_Soldier_Para_2_F",1000],
	["U_I_C_Soldier_Para_3_F",1000],
	["U_I_C_Soldier_Para_4_F",1000],
	["U_I_C_Soldier_Para_5_F",1000],
	["U_I_Wetsuit",4000],
	["U_I_GhillieSuit",25000],	
	
	["V_Press_F",1000],	
	["V_Pocketed_black_F",2000],	
	["V_EOD_coyote_F", 3000],		
	["V_Chestrig_rgr",5000],
	["V_HarnessO_brn",6500],		
	["V_TacVest_camo",8000],	

	["B_Messenger_Coyote_F",500],	
	["B_LegStrapBag_black_F",1000],	
	["B_AssaultPack_blk",7000],	
	["B_FieldPack_oli",8000],
	["B_TacticalPack_blk",9000],
	
	["V_RebreatherIA",10000],
	["B_Parachute",10000],

	["ClaymoreDirectionalMine_Remote_Mag", 6000],
	["APERSTripMine_Wire_Mag", 5000],
	["IEDUrbanSmall_Remote_Mag",10000],
	["IEDLandSmall_Remote_Mag",10000]
];

gunlootarray = [];

publicvariable "storearray";
publicvariable "vipstorearray";
publicvariable "gunlootarray";