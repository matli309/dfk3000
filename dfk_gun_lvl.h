dfk_gun_lvl1 = [
	"bnae_m97_s_virtual",
	"SmokeShellPurple",
	"B_LegStrapBag_black_F"
];

dfk_gun_lvl2 = [
	"SmokeShellGreen",
	"bnae_l35_virtual",	
	"V_Press_F"
];

dfk_gun_lvl3 = [
	"SmokeShellBlue",
	"bnae_m97_virtual",
	"H_RacingHelmet_1_black_F"
];

dfk_gun_lvl4 = [
	"bnae_mk1_virtual",
	"SmokeShellOrange",
	"hgun_Pistol_01_F",
	"Binocular",
	"optic_Yorris",
	"V_BandollierB_rgr"
];

dfk_gun_lvl5 = [
	"SmokeShellYellow",
	"hgun_ACPC2_F",
	"hgun_PDW2000_F",
	"V_EOD_coyote_F"
];

dfk_gun_lvl6 = [
	"SmokeShellRed",
	(selectrandom ["srifle_DMR_06_olive_F","srifle_DMR_03_woodland_F","srifle_DMR_01_F"]),
	"H_Helmet_Skate",
	"MineDetector",	
	"B_AssaultPack_blk"
];

dfk_gun_lvl7 = [
	"SmokeShell",
	"hgun_Pistol_heavy_02_F",
	(selectrandom ["optic_Aco_smg","optic_Aco_grn_smg","optic_Holosight_smg"]),	
	(selectrandom ["optic_Aco","optic_Aco_grn","optic_Holosight"]),	
	"V_Chestrig_rgr",
	"HandGrenade",
	"MiniGrenade",
	(selectrandom ["SMG_01_F","SMG_05_F","SMG_02_F"])
];

dfk_gun_lvl8 = [
	"arifle_AKS_F",
	"U_I_C_Soldier_Para_1_F",
	"U_I_C_Soldier_Para_2_F",
	"U_I_C_Soldier_Para_3_F",
	"U_I_C_Soldier_Para_4_F",
	"U_I_C_Soldier_Para_5_F",
	"ItemGPS",
	"APERSTripMine_Wire_Mag",
	"IEDUrbanSmall_Remote_Mag",
	"IEDLandSmall_Remote_Mag"		
];

dfk_gun_lvl9 = [
	(selectrandom ["arifle_AKM_F","bnae_rk95_virtual"]),
	"bnae_mk1_t_virtual",
	"bnae_scope_v3_virtual",
	"V_HarnessO_brn",
	"H_PASGT_basic_blue_F",
	"hgun_P07_F",
	"bipod_03_F_blk",
	"optic_KHS_old",	
	"APERSMine_Range_Mag",
	"B_FieldPack_oli"
];

dfk_gun_lvl10 = [
	"V_TacVest_camo",
	(selectrandom ["arifle_TRG20_F","arifle_Mk20C_plain_F"]),
	"hgun_Rook40_F",
	"U_I_Wetsuit",
	"B_TacticalPack_blk",
	"V_RebreatherIA",
	"optic_MRCO",
	"arifle_SDAR_F",
	"V_TacChestrig_grn_F",
	"ClaymoreDirectionalMine_Remote_Mag"
];

dfk_gun_lvl11 = [
	(selectrandom ["arifle_Mk20_plain_F","arifle_TRG21_F"]),
	(selectrandom ["arifle_AK12_F","bnae_rk95r_virtual"]),
	"hgun_Pistol_heavy_02_F",
	(selectrandom ["LMG_Mk200_F","LMG_Zafir_F","LMG_03_F"]),
	"Rangefinder",
	"optic_SOS",
	"muzzle_snds_acp",
	"optic_MRD",
	"B_Kitbag_mcamo",
	"APERSBoundingMine_Range_Mag"
];

dfk_gun_lvl12 = [
	(selectrandom ["arifle_Mk20_GL_F","arifle_TRG21_GL_F"]),
	"arifle_AK12_GL_F",
	"launch_RPG7_F",
	"NVGoggles_INDEP",
	"acc_pointer_IR",
	"I_UavTerminal",
	(selectrandom ["optic_Arco_blk","optic_ERCO_blk_f","optic_Hamr"]),
	"H_HelmetB_camo",
	"muzzle_snds_L",
	"DemoCharge_Remote_Mag",
	"SLAMDirectionalMine_Wire_Mag",
	"IEDUrbanBig_Remote_Mag",
	"IEDLandBig_Remote_Mag"
];

dfk_gun_lvl13 = [
	"bnae_trg42_f_virtual",
	"I_IR_Grenade",
	"launch_MRAWS_green_rail_F",
	"hgun_Pistol_heavy_01_F",
	"SMG_03C_TR_black",
	(selectrandom ["srifle_EBR_F","srifle_DMR_02_F"]),
	"optic_LRPS",
	"U_I_GhillieSuit",
	"B_Parachute",
	"U_I_CombatUniform",
	"U_I_CombatUniform_tshirt",
	"U_I_CombatUniform_shortsleeve",
	"muzzle_snds_H_snd_F",
	"muzzle_snds_m_snd_F",
	"muzzle_snds_58_blk_F",
	"muzzle_snds_B_snd_F",
	"muzzle_snds_65_TI_blk_F",
	"muzzle_snds_H",
	"muzzle_snds_L",
	"muzzle_snds_M",
	"muzzle_snds_B",
	"muzzle_snds_H_MG",
	"muzzle_snds_H_SW",
	"muzzle_snds_acp",
	"muzzle_snds_570",
	"H_HelmetB",
	"B_Carryall_oli",
	"I_HMG_01_support_F",
	"I_HMG_01_support_high_F",
	"I_HMG_01_weapon_F",
	"ATMine_Range_Mag",
	"V_PlateCarrier1_rgr_noflag_F",
	"V_PlateCarrier2_rgr_noflag_F",	
	(selectrandom ["arifle_CTAR_blk_F","arifle_MXC_Black_F","arifle_SPAR_01_blk_F"])
];

 dfk_gun_lvl14 = [
	"arifle_ARX_blk_F",
	"launch_NLAW_F",
	(selectrandom ["optic_DMS","optic_AMS"]),
	"V_PlateCarrierIAGL_oli",
	"V_PlateCarrierSpec_rgr",
	"H_HelmetB_light",
	"I_UAV_01_backpack_F",
	"I_GMG_01_weapon_F",
	"SatchelCharge_Remote_Mag",
	(selectrandom ["arifle_CTAR_GL_blk_F","arifle_SPAR_01_GL_blk_F","arifle_MX_GL_Black_F"])
];

dfk_gun_lvl15 = [
	"MMG_01_tan_F",
	"launch_MRAWS_green_F",
	"optic_NVS",
	"V_PlateCarrierL_CTRG",
	"V_PlateCarrierH_CTRG",
	"U_I_FullGhillie_sard",
	"H_HelmetSpecB_blk",
	"I_Mortar_01_support_F",
	"I_Mortar_01_weapon_F",
	(selectrandom ["arifle_SPAR_03_blk_F","arifle_CTARS_blk_F","arifle_MX_SW_Black_F","arifle_SPAR_02_blk_F"])
];

dfk_gun_lvl16 = [
	"launch_O_Vorona_green_F",
	"srifle_GM6_F",
	"optic_KHS_blk",
	"B_ViperHarness_blk_F",
	"B_ViperLightHarness_blk_F",
	"H_HelmetIA",
	"I_AA_01_weapon_F",
	"I_AT_01_weapon_F"
];

dfk_gun_lvl17 = [
	"launch_I_Titan_F",
	"launch_I_Titan_short_F",
	"optic_tws",
	"optic_tws_mg",
	"H_HelmetB_light_black",
	"I_HMG_01_A_weapon_F"
];

dfk_gun_lvl18 = [
	"I_GMG_01_A_weapon_F"
];

dfk_gun_lvl19 = [];
