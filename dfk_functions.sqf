//function defines
#include "dfk_equipment_functions.sqf";
#include "dfk_merc_gear.sqf";
#include "defines.h";

#include "dfk_publicvars.h"

dfk_RobinMode = ["dfk_RobinMode"] call BIS_fnc_getParamValue;

//reference array with different levels of enemy importance, grunt -> general
_killedeastarray = ["KILLEDEAST","KILLEDEAST_level1","KILLEDEAST_level2","KILLEDEAST_level3","KILLEDEAST_level4"];

// #ifndef sanctionedmagsarray
	// #define sanctionedmagsarray sanctionedmagsarray_def
// #endif

//lit de parad
//opfor killed your friends
dfk_litDeParad =
{
	private _townposition = _this select 0;

	//systemchat "dfk_litDeParad";
	
	private _square = ObjNull;
	_square = [_townposition,0,100,10,0,0.1,0,10,"find square"] call dfk_find_fuken_safe_spawnpoint;
	//systemchat format ["%1",_square];
	_deadDudes = 9;
	
	//create tarp?
	createcenter civilian;
	waituntil {typename _square == "ARRAY"};
	
	private _arr = [];
	
	private _stuffarr = [];
	
	if (count _square > 0) then
	{
		_deadgroup = creategroup civilian;
		
		_tarp = createVehicle ["Land_MedicalTent_01_floor_light_F", _square, [], 0, "NONE"];
		//player setpos (_tarp getpos [12,180]);
		_tarp setdir 0;
		
		//[getpos _tarp,"colorblack","","hd_destroy",1] call dfk_generate_marker;
		
		_stuffarr pushback _tarp;
		
		_bbr = boundingBoxReal _tarp;
		_p1 = _bbr select 0;
		_p2 = _bbr select 1;
		_maxWidth = abs ((_p2 select 0) - (_p1 select 0));
		_maxLength = abs ((_p2 select 1) - (_p1 select 1));
		_maxHeight = abs ((_p2 select 2) - (_p1 select 2));		
		
		private _derp_x = 0;
		
		for "_dD" from 0 to _deadDudes do
		{
			_dude = _deadgroup createUnit [selectrandom dfk_deadGuysKindArray, _square, [], 0, "NONE"];
			_stuffarr pushbackunique _dude;
			_dude disableai "ALL";
			removeallweapons _dude;
			_dude unlinkItem "NVGoggles_INDEP";
			//_dude setdamage [1,false];
			
			private _tarppos = getpos _tarp;
			private _y = 0;
			
			if (_derp_x > 6) then {_derp_x = 0};
						
			if (_dD > 3 && _dD < 7) then {_y = 2};
			if (_dD > 6 && _dD < 10) then {_y = 4};
			
			_dude setpos [(_tarppos select 0) - (_maxLength/2) + _derp_x + 2,(_tarppos select 1) - (_maxHeight/2) + _y + 1,0];
			_dude setdir random 360;
			_derp_x = _derp_x + 2;
			_arr pushbackunique _dude;
		};
		
		private _gunsarr = [];
		{
			//_x setvelocity [0,0,1];
			_x setpos [getpos _x select 0,getpos _x select 1,2];
			_x setVectorDirAndUp [[0,0,1],[0,1,0]];
			_x setdamage [1,false];
			
			private _guns = getArray (configFile >> "CfgVehicles" >> (typeof _x) >> "Weapons");
			
			{
				if (!(_x iskindof ["Throw", configFile >> "CfgWeapons"]) && !(_x iskindof ["Put", configFile >> "CfgWeapons"])) then
				{
					_gunsarr pushback _x;
				};
			} foreach _guns;
		} foreach _arr;
		
		//systemchat format ["%1",_gunsarr];
		
		_weapontarppos = getpos _tarp;
		
		{
			private _gunholder = "groundweaponholder" createVehicle [0,0,1000];
			_gunholder setpos [(_weapontarppos select 0) - (_maxLength/2) + 1.5 + (0.5 * _foreachindex),(_weapontarppos select 1) + (_maxHeight/2) - 0.7,0];
			_gunholder addWeaponCargo [_x,1];
			_gunholder setdir 90;		
			_stuffarr pushbackunique _gunholder;
		} foreach _gunsarr;
	}
	else
	{
		//systemchat "FAILZlololololo";
	};
	
	_stuffarr;
};

//simulates sabotage
//or lighting a vehicle on fire;
dfk_setFireToTheVehicle =
{
	_vehicle = _this select 0;
	_killer = _this select 1;
	
	diag_log format ["dfk_setFireToTheVehicle %1"];
	
	["derpfunction"] remoteexec ["systemchat"];
	
	//insert cool burning efx here
	//faux random damage until destruction

	//smallwrecksmoke
	//smallfirebarrel
	
	//old: ObjectDestructionFire1Smallx
	
	
	if (isServer) then
	{
		[format ["FIREFIREFIRE %1 %2 %3",_vehicle,_killer,player]] remoteexec ["systemchat",0];
	
		_source01 = "#particlesource" createVehicle position _vehicle;
		_source01 setParticleClass "smallfirebarrel";
		_source01 attachTo [_vehicle,[random 2,random 2,random 2],"palivo"];	
		
		_source02 = "#particlesource" createVehicle position _vehicle;
		_source02 setParticleClass "smallwrecksmoke";
		_source02 attachTo [_vehicle,[random 2,random 2,random 2],"palivo"];		

		while {alive _vehicle} do
		{
			_vehicle setdamage ((damage _vehicle) + 0.1);
		
			sleep 4;
		};
		
		deletevehicle _source01;
		deletevehicle _source02;
		_killer addscore 2;
	};
};

dfk_addRandomEquipmentToBox =
{
	private _box = _this select 0;
	private _array = _this select 1;
	private _n = _this select 2;
	
	//diag_log format ["dfk_addRandomEquipmentToBox %1 %2 %3",_box,_array,_n];
	
	private _gunloot_thingie = _array;
	
	if (_gunloot_thingie iskindof ["Rifle", configFile >> "CfgWeapons"] or _gunloot_thingie iskindof ["Pistol", configFile >> "CfgWeapons"] or _gunloot_thingie iskindof ["Launcher", configFile >> "CfgWeapons"] or _gunloot_thingie iskindof ["Mgun", configFile >> "CfgWeapons"]) then
	{
		private _guns_count = ceil _n;
		private _mags_count = _guns_count * 10;
		private _mags_array = [];
		
		_box addWeaponCargoGlobal [_gunloot_thingie,_guns_count];
		
		_mags_array = getarray(configfile >> "CfgWeapons" >> _gunloot_thingie >> "magazines");
		private _sanctionedmags = [];
		
		_mz = getarray(configfile >> "CfgWeapons" >> _gunloot_thingie >> "muzzles");
		if (count _mz >= 2) then
		{
			if ("EGLM" in _mz or "GL_3GL_F" in _mz) then
			{
				_derp = [
							"1Rnd_HE_Grenade_shell",
							"1Rnd_Smoke_Grenade_shell",
							"1Rnd_SmokeRed_Grenade_shell",
							"1Rnd_SmokeGreen_Grenade_shell",
							"1Rnd_SmokeYellow_Grenade_shell",
							"1Rnd_SmokePurple_Grenade_shell",
							"1Rnd_SmokeBlue_Grenade_shell",
							"1Rnd_SmokeOrange_Grenade_shell",
							"UGL_FlareWhite_F",
							"UGL_FlareGreen_F",
							"UGL_FlareRed_F",
							"UGL_FlareYellow_F",
							"UGL_FlareCIR_F",
							"FlareWhite_F",
							"FlareGreen_F",
							"FlareRed_F",
							"FlareYellow_F"							
						];
						
				_sanctionedmags append _derp;
			};
			
			if ("Secondary" in _mz) then
			{
				_derp = [
								"10Rnd_50BW_Mag_F"
						];
						
				_sanctionedmags append _derp;
			};			
		};	

		{
			if (_x in sanctionedmagsarray) then 
			{
				_box addMagazineCargoGlobal [_x,_mags_count];
			};
		} foreach _mags_array;
	};	

	_derparr = ["MineDetector","Binocular","NVGoggles_INDEP","Rangefinder"];
	if (_gunloot_thingie iskindof ["ItemCore", configFile >> "CfgWeapons"] or _gunloot_thingie in _derparr) then
	{
		private _guns_count = ceil random 10;
		_box addItemCargoGlobal [_gunloot_thingie,_guns_count];
	};			
	
	if (_gunloot_thingie iskindof "Bag_Base") then
	{
		private _guns_count = ceil random 10;
		_box addBackpackCargoGlobal [_gunloot_thingie,_guns_count];
	};				
	
	_bombsarr = ["ATMine_Range_Mag","APERSMine_Range_Mag","APERSBoundingMine_Range_Mag","SLAMDirectionalMine_Wire_Mag","APERSTripMine_Wire_Mag","ClaymoreDirectionalMine_Remote_Mag","SatchelCharge_Remote_Mag","DemoCharge_Remote_Mag","IEDUrbanBig_Remote_Mag","IEDLandBig_Remote_Mag"];
	if (_gunloot_thingie in _bombsarr) then
	{
		private _guns_count = ceil random 10;	
		_box addMagazineCargoGlobal [_gunloot_thingie,_guns_count];
	};	
};

dfk_aiPreCalArtyFireMission2 =
{
	private _grp = _this select 0;
	private _mode = _this select 1;
	
	private _targetArr = leader _grp neartargets viewdistance;
	
	
	private _answer = true;
	private _airstrike = false;
	private _availablearty = [];
	
	//diag_log format ["dfk_aiPreCalArtyFireMission2 0 targetarr %1",_targetArr];
	
	{
		if ((_x select 4) iskindof "Thing") then
		{
			_targetArr deleteat _foreachindex;
		};
	} foreach _targetArr;
	
	//sort targetarray
	//remove civ targets
	if (sidemorale > 0) then
	{
		{
			if (_x select 2 != civilian) then
			{
				_targetArr deleteat _foreachindex;
			};
		} foreach _targetArr;
	};
	
	//remove own targets
	if (sidemorale > -200) then
	{
		{
			if (_x select 2 != side leader _grp) then
			{
				_targetArr deleteat _foreachindex;
			};
		} foreach _targetArr;	
	};
	
	//diag_log format ["dfk_aiPreCalArtyFireMission2 1 targetarr %1",_targetArr];
	private _target = selectrandom _targetArr;
	
	{
		private _val = _x select 3;
		private _acc = _x select 5;
		
		private _targetVal = _target select 3;
		private _targetAcc = _target select 5;
		
		if (_val > _targetVal) then
		{
			_target = _x;
		};
	} foreach _targetarr;
	
	diag_log format ["dfk_aiPreCalArtyFireMission2 target %1",_target];
	
	//get available arty
	{
		private _arty = _x getvariable "art_gunner";
		private _artGrp = _x;
		
		_alivecrew = {alive _x} count crew _arty;
		
		diag_log format ["dfk_aiPreCalArtyFireMission2 arty debg %1 %2 %3 %4 %5 %6",
			_artGrp,
			alive _arty,
			(count (getArtilleryAmmo [_arty]) >= 1),
			unitready leader _artGrp,
			(_target select 0) inRangeOfArtillery [[_arty], (getArtilleryAmmo [_arty]) select 0],
			(_arty getArtilleryETA [_target select 0, (getArtilleryAmmo [_arty]) select 0])];
		
		if (alive _arty &&
			(count (getArtilleryAmmo [_arty]) >= 1) &&
			unitready leader _arty &&
			(_target select 0) inRangeOfArtillery [[_arty], (getArtilleryAmmo [_arty]) select 0] &&
			(_arty getArtilleryETA [_target select 0, (getArtilleryAmmo [_arty]) select 0]) > 0) 
		then
		{
			_availablearty pushbackunique _artGrp;
			//diag_log format ["dfk_aiPreCalArtyFireMission2 added arty %1",_arty];
		};
	} foreach artilleryarray;
	
	diag_log format ["dfk_aiPreCalArtyFireMission2 avaiable arty %1",count _availablearty];
	if (count _availablearty <= 0) then
	{
		if (logistics < 1200) then
		{
			_answer = false;
			//diag_log "dfk_aiPreCalArtyFireMission2 no arty, low log, no support";
		}
		else
		{
			if (time > dfk_qrf_hkp_timer + 86400/timeMultiplier) then
			{
				_airstrike = true;
				_answer = true;
				diag_log "dfk_aiPreCalArtyFireMission2 airstrike inbound";
			};
			//airstrike
		}
	};
	
	//will we fire upon this target?
	//village/town check
	if (sidemorale > 200 && _answer) then
	{
		{
			if ((_target select 0) InArea _x) then
			{
				_answer = false;
				//diag_log "dfk_aiPreCalArtyFireMission2 abort target in blacklist";
			};
		} foreach artblacklist;
	};
	
	diag_log format ["dfk_aiPreCalArtyFireMission2 check2 %1 air? %2 mode %3",_answer,_airstrike,_mode];
	
	//PEW
	if (_answer && !_airstrike) then
	{
		if (_mode == 1) then
		{
			_artGrp = (selectrandom _availablearty);
			private _kind = _artGrp getvariable ["mortar",ObjNull];
			
			//diag_log format ["dfk_aiPreCalArtyFireMission2 mode %1 arty %2 kind %3",_mode,_artGrp,_kind];
			
			if (_kind iskindof "StaticMortar") then
			{
				[_target select 0,_artGrp,getpos (leader _artGrp),400,4] spawn dfk_artfunc_mortar;
			}
			else
			{
				[_target select 0,_artGrp,getpos art_spawn,400,4] spawn dfk_artfunc_art;
			};
		};	
	
		if (_mode == 2) then
		{
			_artGrp = (selectrandom _availablearty);
			private _kind = _artGrp getvariable ["mortar",ObjNull];
			
			//diag_log format ["dfk_aiPreCalArtyFireMission2 mode %1 arty %2 kind %3",_mode,_artGrp,_kind];
			
			if (_kind iskindof "StaticMortar") then
			{
				[_target select 0,_artGrp,getpos (leader _artGrp),400,8] spawn dfk_artfunc_mortar;
			}
			else
			{
				[_target select 0,_artGrp,getpos art_spawn,400,8] spawn dfk_artfunc_art;
			};
		};		
		
		if (_mode == 3) then
		{
			{
				_artGrp = (selectrandom _availablearty);
				private _kind = _artGrp getvariable ["mortar",ObjNull];
				
				//diag_log format ["dfk_aiPreCalArtyFireMission2 mode %1 arty %2 kind %3",_mode,_artGrp,_kind];
				
				if (_kind iskindof "StaticMortar") then
				{
					[_target select 0,_artGrp,getpos (leader _arty),400,8] spawn dfk_artfunc_mortar;
				}
				else
				{
					[_target select 0,_artGrp,getpos art_spawn,400,8] spawn dfk_artfunc_art;
				};				
			} foreach _availablearty;
		};				
	};
	
	if (_answer && _airstrike) then
	{
		[baselogic,150,selectrandom dfk_hkpstrike_typeclass,_target select 0,_kp_point] remoteExec ["dfk_qrf_hkpstrike",2];
	};
	
	_answer;
};

//last version... I swear!
dfk_aiMoraleHandler2000 =
{
	_moraleLowThreshold = 0;
	_moraleHighTreshold = 0;

	while {true} do 
	{
		{
			private _currentAliveDudes = {alive _x} count units _x;
			if (faction leader _x in BADFACTION && _currentAliveDudes > 0) then
			{
				private _grp = _x;
				private _unitsupportneed = _grp getvariable ["dfk_unitsupportneed","bajs"];
				
				if (typename _unitsupportneed == "STRING") then
				{
					_grp setvariable ["dfk_unitsupportneed",0,true];
					_unitsupportneed = 0;
					_dudes = {alive _x} count units _grp;
					_grp setvariable ["dfk_grpAliveT0",_dudes,true];
				};
				
				private _dfk_grpAliveT0 = _grp getvariable "dfk_grpAliveT0";
				
				private _currentAliveDudes = {alive _x} count units _grp;
				private _currentLosses = _dfk_grpAliveT0 - _currentAliveDudes;
				
				private _change = false;
				
				//jobbigt läge?
				private _targetArr = leader _grp neartargets viewdistance;
				private _targetArrObjects = [];
				{
					_targetArrObjects pushback (_x select 4);
				} foreach _targetArr;
				
				private _totalMorale = 0;
				if (_currentAliveDudes > 0) then
				{
					{
						if (alive _x) then
						{
							_totalMorale = _totalMorale + morale _x;
						}
						else
						{
							_totalMorale = _totalMorale - 1;
						};
					} foreach units _grp;
					
					_totalMorale = _totalMorale / _currentAliveDudes;
				}
				else
				{
					_totalMorale = 0.1;
				};
				
				if (_totalMorale < _moraleLowThreshold && ((_currentAliveDudes > 0) OR ((getSuppression (leader _grp)) > 0))) then
				{
					_change = true;
					_unitsupportneed = _unitsupportneed + 1;
					diag_log format ["dfk_aiMoraleHAndler2000 %1 %2 (+)",_grp,_unitsupportneed];
					//systemchat format ["dfk_aiMoraleHAndler2000 %1 %2 (+)",_grp,_unitsupportneed];
				};
				
				
				if ((_unitsupportneed > 0) && (_totalMorale >= _moraleHighTreshold) && (leader _grp countEnemy _targetArrObjects <= 0 && (getSuppression (leader _grp)) <= 0)) then
				{
					_unitsupportneed = _unitsupportneed - 1;
					diag_log format ["dfk_aiMoraleHAndler2000 %1 %2 (-)",_grp,_unitsupportneed];
					//systemchat format ["dfk_aiMoraleHAndler2000 %1 %2 (-)",_grp,_unitsupportneed];
				};
				
				if (_totalMorale <= 0) then
				{
					diag_log format ["dfk_aiMoraleHAndler2000 debug | %1 | %2 | %3 | %4 | %5",_grp,_totalMorale,_currentLosses,_unitsupportneed,(leader _grp countEnemy _targetArrObjects)];
					//systemchat format ["dfk_aiMoraleHAndler2000 debug | %1 | %2 | %3 | %4",_grp,_totalMorale,_currentLosses,_unitsupportneed];
				};
				
				if (_change) then
				{
					if (_unitsupportneed == 1) then
					{
						
						private _vehArr2000 = [];
						
						{
							_vehArr2000 pushbackunique (assignedVehicle _x);
						} foreach units _grp;
						
						private _go = true;
						{
							if (_x iskindof "AllVehicles") then
							{
								if (!alive _x) then {_go = false};
								if (!canmove _x) then {_go = false};
								if (fuel _x <= 0) then {_go = false};
								if (count crew _x > 0) then {_go = false};
							};
						} foreach _vehArr2000;
					
						if (_go) then
						{
							_buildingsarr = nearestObjects [leader _grp, ["House_F"], 200];
						
							if (count _buildingsarr > 0) then
							{
								[_grp] spawn dfk_AiGroupTakeBuilding;
								diag_log format ["dfk_aiMoraleHAndler2000 %1 take building %2",_grp,_unitsupportneed];
								//systemchat format ["dfk_aiMoraleHAndler2000 %1 take building %2",_grp,_unitsupportneed];
							}
							else
							{
								[_grp] spawn dfk_aiGroupTakeHill;
								diag_log format ["dfk_aiMoraleHAndler2000 %1 take hill %2",_grp,_unitsupportneed];
								//systemchat format ["dfk_aiMoraleHAndler2000 %1 take hill %2",_grp,_unitsupportneed];
							};
						};
					};
					
					if (random 5 < dfk_globalShortWaveRadioQuality && "ItemRadio" in (assignedItems (leader _grp)) && _currentLosses > 0) then
					{	
						if (_unitsupportneed == 2) then
						{
							//call art
							//if qrf, call qrf art
							//if no support, _unitsupportneed++
							//can also be handled in the separate support call functions.. !!! :D
							private _supportAnswer = true;
							
							if (!(_grp getvariable ["qrf",false])) then
							{
								_supportAnswer = [_grp,1] call dfk_aiPreCalArtyFireMission2;
							}
							else
							{
								_supportAnswer = [_grp,2] call dfk_aiPreCalArtyFireMission2;
							};
							
							if (!_supportAnswer) then {_unitsupportneed = _unitsupportneed + 1;};
							diag_log format ["dfk_aiMoraleHAndler2000 %1 request art %2 supportanswer %3",_grp,_unitsupportneed,_supportAnswer];
							//systemchat format ["dfk_aiMoraleHAndler2000 %1 request art %2 supportanswer %3",_grp,_unitsupportneed,_supportAnswer];
						};
						

						if (_unitsupportneed == 4) then
						{
							//call qrf
							//if qrf, call qrf panick art
							//if no support, _unitsupportneed++
							private _supportAnswer = true;
							
							if ((_grp getvariable ["qrf",false])) then
							{
								_supportAnswer = [_grp,3] call dfk_aiPreCalArtyFireMission2;
							}
							else
							{						
								private _t = selectrandom _targetarr;
								_supportAnswer = [((selectrandom _targetArr) select 0)] call DFK_request_support;	
								
								if (!_supportAnswer) then
								{
									_supportAnswer = [_grp,2] call dfk_aiPreCalArtyFireMission2;
								};
							};
						
							
							if (!_supportAnswer) then {_unitsupportneed = _unitsupportneed + 1;};
							diag_log format ["dfk_aiMoraleHAndler2000 %1 request qrf %2 supportanswer %3",_grp,_unitsupportneed,_supportAnswer];
							//systemchat format ["dfk_aiMoraleHAndler2000 %1 request qrf %2 supportanswer %3",_grp,_unitsupportneed,_supportAnswer];
						};
					}
					else
					{
						_unitsupportneed = _unitsupportneed + 1;
						diag_log format ["dfk_aiMoraleHAndler2000 %1 no radio/no connection %2 (+)",_grp,_unitsupportneed];
						//systemchat format ["dfk_aiMoraleHAndler2000 %1 no radio/no connection %2 (+)",_grp,_unitsupportneed];
					};
					
					if (_unitsupportneed >= 7) then
					{
						{
							private _arr1 = _x neartargets 200;
							private _arr2 = [];
							{
								_arr2 pushbackunique (_x select 4);
							} foreach _arr1;
							
							private _halpstr = "";
							
							
							
							private _halp = 0;
							
							if (typename getsuppression _x == "SCALAR") then
							{
								_halp = _halp + (getSuppression _x);
							};
							
							//diag_log format ["halpdebug000 %1 %2 %3 %4 %5 %6 %7 %8 %9 %10 %11",name _x,getsuppression _x,fleeing _x,handshit _x,canstand _x,someammo _x,isbleeding _x,damage _x,morale _x,_halp,_halpstr];
							
							_halpstr = _halpstr + format ["supp %1 ",getsuppression _x];
							
							if (fleeing _x) then {_halp = _halp + 1;_halpstr = _halpstr + "fleeing ";};
							if (handshit _x > 0) then {_halp = _halp + 2;_halpstr = _halpstr +"handshit ";};
							if (!canstand _x) then {_halp = _halp + 5;_halpstr = _halpstr + "cantstand ";};
							if (!someammo _x) then {_halp = _halp + 5;_halpstr = _halpstr + "noammo ";};
							if (isbleeding _x) then {_halp = _halp + 2;_halpstr = _halpstr + "bleeding ";};
							if (damage _x > 0.2 && damage _x < 0.5) then {_halp = _halp + 1;_halpstr = _halpstr + "0.2dmg ";};
							if (damage _x > 0.5 && damage _x < 0.75) then {_halp = _halp + 2;_halpstr = _halpstr + "0.5dmg ";};
							if (damage _x > 0.75 && damage _x < 1) then {_halp = _halp + 3;_halpstr = _halpstr + "0.75dmg ";};
							if (morale _x < -0.5) then {_halp = _halp + 1;_halpstr = _halpstr + "-0.5morale ";};
							if (morale _x < -1) then {_halp = _halp + 2;_halpstr = _halpstr + "-1morale";};
							if (morale _x < -1.5) then {_halp = _halp + 3;_halpstr = _halpstr + "-1.5morale ";};
							if (morale _x < -2) then {_halp = _halp + 4;_halpstr = _halpstr + "-2morale ";};
							
							if (_halp > 0) then
							{
								diag_log format ["%3 halpindex: %1 %2",_halp,_halpstr,name _x];
								//diag_log format ["halpdebug %1 %2 %3 %4 %5 %6 %7 %8 %9",name _x,getsuppression _x,fleeing _x,handshit _x,canstand _x,someammo _x,isbleeding _x,damage _x,morale _x];
								systemchat format ["halpindex: %1 %2",_halp,_halpstr];
							};
							
							if ((_x countEnemy _arr2 > 0) && !captive _x && _halp >= (7 + random 10)) then
							{
								[_x] call dfk_ai_surrender;
							};
						} foreach units _grp;
						diag_log format ["dfk_aiMoraleHAndler2000 %1 surrender %2 ",_grp,_unitsupportneed];
						//systemchat format ["dfk_aiMoraleHAndler2000 %1 surrender %2 ",_grp,_unitsupportneed];
					};
				};
				
				_grp setvariable ["dfk_unitsupportneed",_unitsupportneed,true];
			};
		} foreach allgroups;
		sleep 30;
	};
};

dfk_selectRadioSites = 
{
	{
		diag_log format ["dfk_selectRadioSites %1",_x];
		[selectrandom _x] spawn dfk_spawnRadioSite;
	} foreach dfk_radioDorkSites;
};

dfk_spawnRadioSite = 
{
	_radioSitePos = _this select 0;
	
	diag_log format ["dfk_spawnRadioSite %1",_radioSitePos];
	
	dfk_globalShortWaveRadioQuality = dfk_globalShortWaveRadioQuality + 1;

	_radioDorkSite = ["Land_TTowerSmall_2_F","Land_PortableGenerator_01_F","Land_PortableLongRangeRadio_F","Land_Laptop_device_F","Land_SatelliteAntenna_01_F","Land_SatellitePhone_F","Land_CanisterFuel_F","Land_CanisterFuel_F","CamoNet_ghex_open_F"];

	_lightKinds = ["Campfire_burning_F","FirePlace_burning_F","Land_Camping_Light_F","Land_PortableLight_double_F"];

	_importantradiostuffz = [];

	{
		private _veh = createVehicle [_x, _radioSitePos, [], 5, "NONE"];
		
		if (_x in ["Land_TTowerSmall_2_F","Land_PortableGenerator_01_F","Land_Laptop_device_F"]) then
		{
			_importantradiostuffz pushbackunique _veh;
		};
		
		if (_x == "Land_Laptop_device_F") then
		{
			//_veh addaction ;
			[_veh,["Hack enemy radio",{
											(_this select 0) setdamage 1;
											dfk_hackedEnemyRadar = dfk_hackedEnemyRadar + 1;
											publicvariable "dfk_hackedEnemyRadar";
											[format ["HACKACKHACKHACK\n###############\nEnemy radars %1/5 hacked!\n###############\n",dfk_hackedEnemyRadar]] remoteexec ["HintSilent",0,true];
											[(_this select 0),(_this select 2)] remoteexec ["removeaction",0,true]
									},[],1.5, true, true, "", "alive _target && _this getUnitTrait 'UAVHacker'", 2, false, "",""]] remoteexec ["Addaction",0,true];
			//[_dude,["Take prisoner",{_this execvm "dfk_take_prisoner.sqf";diag_log _this}]] remoteExec ["addAction",0,true];
		};
		
	} foreach _radioDorkSite;

	if (random 1 > 0.5) then {private _light = createVehicle [selectrandom _lightKinds, _radioSitePos, [], 10, "NONE"];};

	private _grp = creategroup east;
	private _id = ["radioDork"] call dfk_generate_radioid;
	_grp setGroupIdGlobal [_id];	
	for "_i" from 1 to (4 + random 4) do
	{
		private _unit = "";
		_unit = [_unit,_grp,_radioSitePos,20,"soldier"] call dfk_spawn_soldier;
		_unit enableDynamicSimulation true;
	};

	_wpx = _grp addwaypoint [_radioSitePos,20];
	_wpx setwaypointtype "SENTRY";
	_grp setbehaviour (selectrandom ["SAFE","AWARE","CARELESS"]);
		

	_grp enableDynamicSimulation true;
		
	_alive = true;

	while {_alive} do
	{
		_cnt = {alive _x} count units _grp;
		{
			if (!(alive _x) or _cnt <= 0) then {_alive = false};
		} foreach _importantradiostuffz;
		sleep 60;
	};

	dfk_globalShortWaveRadioQuality = dfk_globalShortWaveRadioQuality - 1;
	diag_log format ["dfk_globalShortWaveRadioQuality %1",dfk_globalShortWaveRadioQuality];
	[format ["###################\nEnemy shorwave radio quality: %1 / 5\n###################",dfk_globalShortWaveRadioQuality]] remoteexec ["HintSilent",0,true];
};

dfk_roadBlocksInit =
{
	{
		private _trg = createTrigger ["EmptyDetector", _x];
		_trg setTriggerArea [spawn_distance, spawn_distance, 0, false];
		_trg setTriggerActivation ["ANYPLAYER", "PRESENT", true];
		//_trg setTriggerStatements ["this", "[thisTrigger] spawn dfk_roadBlockSpawnFunction", "[thisTrigger] spawn dfk_roadBlockDeSpawnFunction"];
		_trg setTriggerStatements ["this", "", ""];
		//_trg setvariable ["spawn",false,true];
		_trg setvariable ["unitsArray",[],true];
		//[_trg,"colorblue","lolblock","hd_destroy",1] call dfk_generate_marker;
		
		_trg setvariable ["roadblockdudes",12,true];
		private _type = selectrandom ["cops","wrecks","military","empty"];
		_trg setvariable ["roadblocktype",_type,true];
		
		[_trg] spawn dfk_roadBlocksLoop;
	} foreach dfk_roadBlockPosArray;
};

dfk_roadBlocksLoop = 
{
	_trg = _this select 0;
	private _triggered = false;
	
	private _changetime = random 24;
	
	while {true} do
	{	
		if (time > 0 && (daytime % _changetime > 0) && (daytime % _changetime <= 0.1)) then
		{
			_changetime = random 24;
		
			private _type = selectrandom ["cops","wrecks","military","any","empty"];
			
			if (_type == "any") then
			{
				_type = selectrandom ["cops","wrecks","military","empty"];
			};
			
			_trg setvariable ["roadblocktype",_type];
			
			_trg setvariable ["roadblockdudes",12,true];
			diag_log format ["dfk_roadBlocksLoop reset %1 %2",_trg getvariable ["roadblocktype","lololololo"],_trg getvariable ["roadblockdudes","lololololo"]]
		};
		
		if (triggeractivated _trg && !_triggered) then
		{
			[_trg] spawn dfk_roadBlockSpawnFunction;
			_triggered = true;
		};
		
		if (!triggeractivated _trg && _triggered) then
		{
			[_trg] spawn dfk_roadBlockDeSpawnFunction;
			_triggered = false;
		};		
		sleep 1;
	};
};

dfk_coperize_beam =
{
	_car = _this select 0;
	
	if (_car iskindof "O_G_Offroad_01_F") then
	{
		//if (isServer) then {[_car,[0,"a3\soft_f_exp\offroad_01\data\offroad_01_ext_gen_co.paa"]] remoteExec ["setObjectTexture",0,true] };
		if (isServer) then {
			if (random 1 > 0.5) then
				{[_car,[0,"data\secfor_01_CO.paa"]] remoteExec ["setObjectTexture",0,true]}
				else
				{[_car,[0,"data\secfor_02_CO.paa"]] remoteExec ["setObjectTexture",0,true]};
			};
		//_car animateSource ["HidePolice", 0];
		_car forceFlagTexture dfk_rentacopflag;
	};
	
	if (_car iskindof "C_Offroad_02_unarmed_F") then
	{
		if (isServer) then 
		{
			[_car,[0,"\A3\Soft_F_Exp\Offroad_02\Data\offroad_02_ext_black_co.paa"]] remoteExec ["setObjectTexture",0,true];
			[_car,[1,"\A3\Soft_F_Exp\Offroad_02\Data\offroad_02_ext_black_co.paa"]] remoteExec ["setObjectTexture",0,true];
			[_car,[2,"\A3\Soft_F_Exp\Offroad_02\Data\offroad_02_int_black_co.paa"]] remoteExec ["setObjectTexture",0,true];
			[_car,[3,"\A3\Soft_F_Exp\Offroad_02\Data\offroad_02_int_black_co.paa"]] remoteExec ["setObjectTexture",0,true];
		};
							
		_car forceFlagTexture dfk_rentacopflag;
	};
	
	if (_car iskindof "O_LSV_02_unarmed_F") then
	{
		if (isServer) then 
			{
				[_car,[0,"\A3\Soft_F_Exp\LSV_02\Data\CSAT_LSV_01_black_CO.paa"]] remoteExec ["setObjectTexture",0,true];
				[_car,[1,"\A3\Soft_F_Exp\LSV_02\Data\CSAT_LSV_02_black_CO.paa"]] remoteExec ["setObjectTexture",0,true];
				[_car,[2,"\A3\Soft_F_Exp\LSV_02\Data\CSAT_LSV_03_black_CO.paa"]] remoteExec ["setObjectTexture",0,true];
			};
							
		_car forceFlagTexture dfk_rentacopflag;
	};	
};

dfk_roadBlockSpawnFunction =
{
	private _trg = _this select 0;
	
	//private _spawn = _trg getvariable ["spawn",false];
	private _unitsArray = _trg getvariable ["unitsArray",[]];
	
	private _dudes = _trg getvariable ["roadblockdudes",0];
	
	//systemchat format ["dfk_roadBlockSpawnFunction %1 %2 %3",_trg,_spawn,_unitsArray];
	//diag_log format ["dfk_roadBlockSpawnFunction %1 %2 %3",_trg,_unitsArray];
	
	if (count _unitsArray <= 0) then
	{
		//_trg setvariable ["spawn",true,true];
		//systemchat "spawn!";
		
		//spawn stuff
		//"military","renegade","mercs","idap","civilians","farmers",
		
		private _type = _trg getvariable ["roadblocktype","empty"];
		
		//any variable set elsewhere, just backup code to set _type to something usefull instead of having empty roadblocks
		if (_type == "any") then
		{
			_type = selectrandom ["cops","military","wrecks"];
		};
		
		if (_dudes <= 0 && (_type == "cops" || _type == "military")) then
		{
			_type = "wrecks";
		};
		
		diag_log format ["dfk_roadBlockSpawnFunction %1 %2 %3",_type,_dudes,_unitsArray];
		
		//renegades - drugdealers, criminals etc
		//mercs 
		//idap - not roadblocks, more a supplyconvoy of kinds
		//civilians - refugees, luggage, some cars, campfires, loot
		//farmers, one dude, a truck and LOTS of goats
		//wrecks - wrecks, fire, smoke, dead bodies, mines, loot
		
		switch (_type) do
		{
			case "empty":
			{
				//empty
			};
			case "wrecks":
			{
				//systemchat "default...";
				
				for "_i" from 1 to random 5 do
				{
					private _wreck = selectrandom superjunkarray;
					private _veh = createVehicle [_wreck, [0,0,1000], [], 0, "NONE"];
					private _pos = [_trg, 0, 30, 6, 0, 0.5, 0,10,"dfk_roadBlockSpawnFunction"] call dfk_find_fuken_safe_spawnpoint;
					if (count _pos <= 0) then {_pos = [(getpos _trg select 0) + (random 25) - (random 25),(getpos _trg select 1) + (random 25) - (random 25),0]};
					_veh setpos _pos;
					_veh setdir random 360;
					//systemchat format ["%1",_veh];
					
					_unitsArray pushbackunique _veh;
				};
				
				
				_class = selectrandom dfk_random_animals;
				private _civgrp = creategroup civilian;
				
				_civ = "";
				for "_i" from 1 to (random 10) do {_civ = [_civ,_civgrp,getpos _trg,20,"animal"] call dfk_spawn_soldier;_unitsArray pushback _civ;};
			};
			
			case "cops":
			{
				//systemchat "cops";
				
				for "_i1" from 0 to (1 + random 2) do
				{
					private _car = selectrandom roamingcar_cop;
				
					private _veh = createVehicle [_car, [0,0,1000], [], 0, "NONE"];
					private _pos = [_trg, 0, 30, 6, 0, 0.5, 0,10,"dfk_roadBlockSpawnFunction"] call dfk_find_fuken_safe_spawnpoint;
					if (count _pos <= 0) then {_pos = [(getpos _trg select 0) + (random 25) - (random 25),(getpos _trg select 1) + (random 25) - (random 25),0]};
					_veh setpos _pos;
					_veh setdir random 360;
					[_veh] call dfk_initvehicle;
					[_veh] call dfk_coperize_beam;
					
					_veh lock true;
					
					//_unitsArray pushbackunique group driver _veh;
					
					_unitsArray pushbackunique _veh;
				};

				private _grp = creategroup east;
				private _id = ["cops"] call dfk_generate_radioid;
				_grp setGroupIdGlobal [_id];	
				_unitsArray pushbackunique _grp;
				
				private _unit = "";
				for "_i2" from 0 to (4 + random 4) do
				{
					_unit = [_unit,_grp,getpos _trg,20,"cop"] call dfk_spawn_soldier;
					
				};
				
				waituntil {count units _grp >= 4};
				_unitsArray append units _grp;
				
				_wpx = _grp addwaypoint [getpos _trg,10];
				if (random 1 > 0.5) then {_wpx setwaypointtype "DISMISS";} else {_wpx setwaypointtype "SENTRY";};
				_grp setbehaviour (selectrandom ["DANGER","AWARE"]);
				_grp setcombatmode (selectrandom ["RED","YELLOW","GREEN"]);
				
				if (random 1 > 0.5) then
				{
					private _grp = creategroup east;
					private _id = ["cops"] call dfk_generate_radioid;
					_grp setGroupIdGlobal [_id];
					_unitsArray pushbackunique _grp;
					for "_i3" from 0 to (4 + random 4) do
					{
						_unit = [_unit,_grp,getpos _trg,20,"cop"] call dfk_spawn_soldier;
					};
					
					waituntil {count units _grp >= 4};
					_unitsArray append units _grp;
					
					_grp setbehaviour (selectrandom ["DANGER","AWARE"]);
					_grp setcombatmode (selectrandom ["RED","YELLOW","GREEN"]);				
					
					for "_i4" from 0 to 4 do
					{
						_wpx = _grp addwaypoint [getpos _trg,50];
						if (_i4 == 4) then {_wpx setwaypointtype "CYCLE";} else {_wpx setwaypointtype "MOVE";};
					};
				};
				
				private _lootarr = [getpos _trg,random 3,false] call dfk_spawn_town_loot;
				_unitsArray append _lootarr;
			};		

			case "military":
			{
				//systemchat "mil";
				
				private _blockadekind = roamingcar_opforkind;
				
				if (logistics > 3000) then {_blockadekind = selectrandom [dfk_sites_tanks,dfk_qrf_medium,roamingcar_opforkind]};
				if (logistics > 1500 && logistics <= 3000) then {_blockadekind = [dfk_qrf_medium,roamingcar_opforkind]};
				if (logistics <= 1500) then {_blockadekind = roamingcar_opforkind;};
				
				for "_i1" from 0 to (1 + random 2) do
				{
					private _car = selectrandom _blockadekind;
				
					private _veh = createVehicle [_car, [0,0,1000], [], 0, "NONE"];
					private _pos = [_trg, 0, 30, 6, 0, 0.5, 0,10,"dfk_roadBlockSpawnFunction"] call dfk_find_fuken_safe_spawnpoint;
					if (count _pos <= 0) then {_pos = [(getpos _trg select 0) + (random 25) - (random 25),(getpos _trg select 1) + (random 25) - (random 25),0]};
					_veh setpos _pos;
					_veh setdir random 360;
					
					private _grp0 = creategroup east;
					private _id = ["roadblock"] call dfk_generate_radioid;
					_grp0 setGroupIdGlobal [_id];	
					_unitsArray pushbackunique _grp0;
					
					[_veh] call dfk_initvehicle;
					createvehiclecrew _veh;

					{
						 [_x,"soldier"] call dfk_equip_soldier;
						 _x addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,"KILLEDEAST"] execVM "killer.sqf";}];
					} foreach crew _veh;
					
					_grp0 addvehicle _veh;
					
					//[_grp,_veh,"soldier"] remoteExec ["dfk_spawn_crew",2];
					
					_wpx = _grp0 addwaypoint [getpos _veh,0];
					_wpx setwaypointtype "SENTRY";

					{
						_unitsArray pushbackunique _x;
					} foreach units _grp0;

					_unitsArray pushbackunique _veh;
					
					_veh lock true;
				};

				_unit = "";
				
				private _grp1 = creategroup east;
				private _id = ["roadblock"] call dfk_generate_radioid;
				_grp1 setGroupIdGlobal [_id];	
				_unitsArray pushbackunique _grp1;
				for "_i2" from 0 to (4 + random 4) do
				{
					_unit = [_unit,_grp1,getpos _trg,20,"soldier"] call dfk_spawn_soldier;
				};
				
				_unitsArray append units _grp1;
				
				_wpx = _grp1 addwaypoint [getpos _trg,10];
				if (random 1 > 0.5) then {_wpx setwaypointtype "DISMISS";} else {_wpx setwaypointtype "SENTRY";};
				_grp1 setbehaviour (selectrandom ["SAFE","DANGER","AWARE","CARELESS"]);
				_grp1 setcombatmode (selectrandom ["RED","YELLOW","GREEN"]);
				
				if (random 1 > 0.5) then
				{
					private _grp2 = creategroup east;
					private _id = ["roadblock"] call dfk_generate_radioid;
					_grp2 setGroupIdGlobal [_id];	
					_unitsArray pushbackunique _grp2;
					
					for "_i3" from 0 to (4 + random 4) do
					{
						_unit = [_unit,_grp2,getpos _trg,20,"soldier"] call dfk_spawn_soldier;
					};
					
					_unitsArray append units _grp2;
					
					_grp2 setbehaviour (selectrandom ["SAFE","DANGER","AWARE","CARELESS"]);
					_grp2 setcombatmode (selectrandom ["RED","YELLOW","GREEN"]);				
					
					for "_i4" from 0 to 4 do
					{
						_wpx = _grp2 addwaypoint [getpos _trg,50];
						if (_i4 == 4) then {_wpx setwaypointtype "CYCLE";} else {_wpx setwaypointtype "MOVE";};
						_wpx setwaypointtimeout [random 60,random 180,random 300];
					};
				};
				
				_roads = getpos _trg nearroads 50;
				for "_blockshit" from 0 to random 10 do
				{
					private _block = createVehicle [selectrandom dfk_roadBlockBlockArray, getpos (selectrandom _roads), [], 10, "NONE"];
					_block setdir random 360;
					_unitsArray pushbackunique _block;
				};
				
				if (random 1 > 0.5) then
				{
					for "_griefturret" from 0 to random 4 do
					{
						private _terrorTurret = createVehicle [selectrandom dfk_roadBlockGunTurrets, getpos _trg, [], 100, "NONE"];
						createvehiclecrew _terrorTurret;
						_terrorTurret setdir ((_terrorTurret getdir _trg) - 180);
						_unitsArray pushbackunique _terrorTurret;
						_unitsArray append crew _terrorTurret;
					};
				};
				
				private _lootarr = [getpos _trg,random 3,false] call dfk_spawn_town_loot;
				_unitsArray append _lootarr;
			};		
			
		};
		
		_trg setvariable ["unitsArray",_unitsArray,true];
	};
};

dfk_roadBlockDeSpawnFunction =
{
	private _trg = _this select 0;
	
	//private _spawn = _trg getvariable ["spawn",false];
	private _unitsArray = _trg getvariable ["unitsArray",[]];
	
	//systemchat format ["dfk_roadBlockDeSpawnFunction %1 %2 %3",_trg,_spawn,_unitsArray];
	//diag_log format ["dfk_roadBlockDeSpawnFunction %1 %2 %3",_trg,_unitsArray];	
	private _counter = 0;
	
	if (count _unitsArray > 0) then
	{
		//systemchat "despawn!";
		//_trg setvariable ["spawn",false,true];
		
		private _grp = [];
		//diag_log format ["%1 %2 %3",count _unitsArray,count _grp,(count _unitsArray > 0 || count _grp > 0)];
		
		while {count _unitsArray > 0 || count _grp > 0} do
		{
			{
				private _del = false;
				if (typename _x == "GROUP") then
				{
					{
						_unitsArray pushbackunique _x;
					} foreach units _x;
					_unitsArray deleteat _forEachIndex;
					_del = true;
				};
				
				if (typename _x == "OBJECT") then
				{
					if (count crew _x > 0) then
					{
						{
							_unitsArray pushbackunique _x;
						} foreach crew _x;
					};
					
					if (alive _x && _x iskindof "Man") then
					{
						_counter = _counter + 1;
					};
					
					{
						deletevehicle _x;
					} foreach attachedObjects _x;
					
					deletevehicle _x;
					
					_unitsArray deleteat _forEachIndex;
					_del = true;
				};
				
				if (!_del) then {diag_log format ["dfk_roadBlockDeSpawnFunction wutt?? %1 %2 %3",_x,typename _x,typeof _x];deletevehicle _x;_unitsArray deleteat _forEachIndex;};
			} foreach _unitsArray;
			
			{
				deletegroup _x;
				_grp deleteat _forEachIndex;
			} foreach _grp;	
			
			{
				if (isnull _x) then
				{
					_unitsArray deleteat _forEachIndex;
				};
			} foreach _unitsArray;
			
			//diag_log format ["dfk_roadBlockDeSpawnFunction DERP %1 %2 %3 %4",_trg,_unitsArray,_grp];
		};

		
		_trg setvariable ["unitsArray",[],true];
		_trg setvariable ["roadblockdudes",_counter,true];
		
		diag_log format ["dfk_roadBlockDeSpawnFunction END %1 %2 %3 %4",_trg,_unitsArray,_counter,_trg getvariable ["roadblockdudes","lololol"]];
	};
};

dfk_aiPreCalArtyFireMission = 
{
	private _grp = _this select 0;
	private _targetarr = _this select 1;
	private _not_targets = _this select 2;

	private _leaderdude = leader _grp;
	private _targets = [];
	private _artsafe = true;
	private _arttarget = Objnull;
	
	private _support = false;
	
	//cant hit moving targets with arty sonny boy!
	{
		if (speed (_x select 4) < 10) then
		{
			// _targets is now just an array of map positions
			(_x select 0) pushback _targets;
		};
	} foreach _targetarr;
	
	//what does this stuff do again?
	if (count _targets > 0) then
	{
		{
			private _t = _x;
			
			{
				private _tQ = _x;
				//0 perceived pos,1 type,2 perceived side,3 cost,4 actual game object,5 pos accuracy
				_tQ_perceivedside = _tQ select 2;
				_tQ_perceivedtype = _tQ select 1;
				_tQ_perceivedpos = _tQ select 0;
				
				// ((_tQ select 4) distance _t) < (100 + random 100) &&
				// (_tQ select 2 == civilian OR _tQ select 2 == east)
				//check type - if say... bunker then fire artysafe
				//check side - if reistance or west, then artysafe
				//then if it's still civ or east check the distance, if far away enough it's artysafe
				//
				if (_tQ_perceivedside == (side (leader _grp)) or _tQ_perceivedside == civilian) then
				{
					if (_tQ_perceivedpos distance _t < (100 + random 100)) then
					{
						_artsafe = false;
					};
				};
			} foreach _not_targets;
		} foreach _targets;
		
		if (_artsafe) then 
		{
			_arttarget = selectrandom _targets;

			[_arttarget] remoteexec ["dfk_artyradar_addtarget",2];
			_grp setvariable ["support",true,true];		
			_support = true;
		};					
	};
	
	_support;
};

dfk_aiGroupTakeHill =
{
	private _grp = _this select 0;
		
	if (!(_grp getvariable ["dfk_aiBusy",false])) then
	{
		private _nearbyLocations = nearestLocations [position leader _grp, ["Mount"], 250];

		if (count _nearbyLocations > 0) then
		{
			{
				_x setwppos getpos leader _grp;
			} foreach waypoints _grp;
			
			{
				deletewaypoint _x;
			} foreach waypoints _grp;							
			
			_pos = locationposition selectrandom _nearbyLocations;
			
			{
				if ((getpos leader _grp distance locationposition _x) < (getpos leader _grp distance _pos)) then
				{
					_pos = locationposition _x;
				};
			} foreach _nearbyLocations;
			
			_wpx = _grp addwaypoint [_pos,0];
			{
				_x domove [(_pos select 0) + (random 100) - (random 100),(_pos select 1) + (random 100) - (random 100)];
			} foreach units _grp;
			
			//systemchat "run to the hills!";
			//[_pos,"coloryellow","flee hill","hd_flag",1] remoteExec ["dfk_generate_marker",2];
			diag_log format ["dfk_aiGroupTakeHill group %1 has decided to run to the hill(s) %2",_grp,_pos];
			
			_grp setvariable ["dfk_aiBusy",true,true];
			
			sleep 600;
			_grp setvariable ["dfk_aiBusy",false,true];			
			
		};	
	};
};

dfk_AiGroupTakeBuilding =
{
	private _grp = _this select 0;
	
	if (!(_grp getvariable ["dfk_aiBusy",false])) then
	{
	
		private _buildings = [];

		_buildings = nearestObjects [leader _grp, ["House_F"], 200];
		if (count _buildings > 0) then
		{
			private _das_house = _buildings select 0;
			{
				private _poscount = [];
				_poscount = _x buildingPos -1;
				if (count _poscount <= 0) then 
				{
					_buildings deleteat _foreachindex;
				}
				else
				{
					if ((count _poscount) > count (_das_house buildingPos -1)) then
					{
						_das_house = _x;
					};
				};
			} foreach _buildings;
			
			private _posArray = [];
			_posArray = _das_house buildingPos -1;
			if (count _posArray > 0) then
			{
				{
					_x setwppos getpos leader _grp;
				} foreach waypoints _grp;
				
				{
					deletewaypoint _x;
				} foreach waypoints _grp;							
				
				_wpx = _grp addwaypoint [getpos _das_house,0];
				
				{
					_x domove selectrandom _posArray;
				} foreach units _grp;
				_decided = true;
				//[getpos _das_house,"coloryellow","flee building","hd_flag",1] remoteExec ["dfk_generate_marker",2];
				//systemchat "flee building!";
				_grp setvariable ["dfk_aiBusy",true,true];
				diag_log format ["dfk_AiGroupTakeBuilding group %1 has decided to run to building %2",_grp,getpos _das_house];
				
				sleep 600;
				_grp setvariable ["dfk_aiBusy",false,true];
			};
		};
	};
};



//JINGLE JANGLE
//use spawn, not call
dfk_jingleJangleRespawnPoints =
{
	_marker = _x select 0;
	_laptop = _x select 1;
	
	//diag_log format ["dfk_jingleJangleRespawnPoints 0 %1 %2 %3",_x,_marker,_laptop];

	_pos = [getpos _laptop, 0, 25, 2, 0, 20, 0,10,"spawn_laptop"] call dfk_find_fuken_safe_spawnpoint;
	if (count _pos <= 0) then {_pos = [(getpos _laptop select 0) + (random 25) - (random 25),(getpos _laptop select 1) + (random 25) - (random 25),0]};
	_marker setmarkerpos _pos;
	_marker setmarkerdir random 360;
};

dfk_ai_surrender =
{
	_dude = _this select 0;
	
	if (alive _dude) then
	{	
		//surrender = true;throwdowngun = true; throwuparms = true
		dogetout _dude;
		unassignvehicle (vehicle _dude);
		
		_dude setcaptive true;
		removeallweapons _dude;
		_dude playaction "Surrender";

		//_dude addaction ["Take prisoner",{[_this select 0,_this select 1,_this select 2] call dfk_take_prisoner}];
		[_dude,format ["I, %1 has surrendered!",name _dude]] remoteexec ["globalchat"];
		//_dude globalchat format ["I, %1 has surrendered!",name _dude];
		diag_log format ["%1 has surrendered!",name _dude];
		
		//[_laptop, ["Review funds and intel", {publicvariable "funds";publicvariable "secrets";[format ["Funds: %1\nDirty secrets: %2",funds,secrets]] remoteExecCall ["hint"]}]] remoteExec ["addAction",0,true];
		[_dude,["Take prisoner",{_this execvm "dfk_take_prisoner.sqf";diag_log _this}]] remoteExec ["addAction",0,true];
		[_dude,"colorblue","","hd_flag",1,1,1,5,1] spawn dfk_shitty_marker_tracker;
			
		_dude disableai "ALL";
		
		//[_object, ["Greetings!", {hint "Hello!"}]] remoteExec ["addAction"];
		
		//[_boatspawn, [_str,"dfk_spawn_boat.sqf",[_x]]] remoteExec ["addAction",0,true];
		
		//*add take prisonner action*
	};
};

dfk_shitty_marker_tracker =
{
	private _pos = _this select 0;
	private _color = _this select 1;
	private _string = _this select 2;
	private _type = _this select 3;	
	private _alpha = _this select 4;
	private _size = _this select 6;
	private _delay = _this select 7;
	private _derpyness = _this select 8;

	//diag_log format ["dfk_shitty_marker_tracker 0 %1",getpos _pos];
	
	if (typename _pos == "OBJECT") then 
	{
		private _markername = [] call dfk_generate_randomname;
		_markerstr = createMarker [_markername,getpos _pos];
		_markerstr setMarkerShape "ICON";
		_markerstr setMarkerType _type;
		_markerstr setmarkercolor _color;
		_markerstr setmarkertext _string;
		_markerstr setmarkeralpha _alpha;

		while {alive _pos} do
		{
			
			_markerstr setmarkerpos [(getpos _pos select 0) + (random _derpyness) - (random _derpyness),(getpos _pos select 1) + (random _derpyness) - (random _derpyness)];
			//diag_log format ["dfk_shitty_marker_tracker 1 %1 %2",getpos _pos,getmarkerpos _markerstr];
			sleep _delay;	
		};

		deletemarker _markerstr;		
		
	};
};

//to select appropriate/ai usefull/terrorising weapons
dfk_arm_roaming_aircraft =
{
	private _aircraft = _this select 0;
	
	private _allPylons = "true" configClasses (configFile >> "CfgVehicles" >> (typeof _aircraft) >> "Components" >> "TransportPylonsComponent" >> "pylons");
	
	if ((_aircraft iskindof "O_UAV_02_dynamicLoadout_F") or (_aircraft iskindof "O_T_VTOL_02_infantry_dynamicLoadout_F")) then
	{
		_arr = 
		[
			"PylonRack_3Rnd_LG_scalpel",
			"PylonRack_19Rnd_Rocket_Skyfire",
			"PylonRack_20Rnd_Rocket_03_AP_F",
			"PylonRack_20Rnd_Rocket_03_HE_F"
		];

		for "_x" from 1 to count _allpylons do
		{
			_aircraft setpylonloadout [_x,selectrandom _arr];
		};
	};
};

dfk_take_prisoner =
{
	private _prisoner = _this select 0;
	private _caller = _this select 1;
	private _actionid = _this select 2;
	
	diag_log format ["take_prisoner %1 %2 %3",_prisoner,_caller,_actionid];
	
	_prisoner removeaction _actionid;
	
	if (alive _prisoner) then
	{	
	
		_cashmode = ["dfk_storekind"] call BIS_fnc_getParamValue;
		
		if (_cashmode == 4) then
		{
			_caller addPlayerScores [20,0,0,0,0];
		}
		else
		{
			_caller addscore 20;
		};
		
		hint "Prisoner taken!";
		
		[(20000 + (random 10000) + ((10000 + (random 10000)) * rankid _prisoner))] remoteExecCall ["dfk_add_funds",2];
		[resistance, ceil (random 5)] call BIS_fnc_respawnTickets;
		
		["TOOKPRISONER"] remoteexeccall ["DFK_fnc_remove_morale",2];
		["POW"] remoteexeccall ["DFK_fnc_remove_political",2];
	};
	
	deletevehicle _prisoner;
};

dfk_nerf_woodvehicle =
{
	private _veh = _this select 0;
	
	//_veh setdamage random 0.5;
	//_veh setfuel 0;
	[_veh,0] remoteexec ["setfuel"];
	
	{
		_veh removemagazine _x;
	} foreach magazines _veh;
	
	_all = (getAllHitPointsDamage _veh);
	
	if (count _all > 0) then
	{
		for "_x" from 0 to (count (_all select 1) - 1) do
		{
			_veh setHitIndex [_x,random 1];
		};
	};
	
};

dfk_spawn_woodvehicles =
{
	_badlocations = [];

	{
		//[locationposition _x,"colorblack",format ["%1 %2",_x,text _x],"select",0.2] call dfk_generate_marker;
		if (toLower(text _x) in ["airport","harbour","military base","power plant"]) then {_badlocations pushbackunique _x};
	} forEach nearestLocations [[5000,5000], ["NameCity","NameCityCapital","NameMarine","NameVillage","NameLocal","Airport"], 12000];

	_woodenlootpos = [];

	{
		_testpos = (_x select 0);
		
		_roads = _testpos nearroads 100;
		_locations = nearestLocations [_testpos,["NameCity","NameCityCapital","NameVillage"],500];
		_bad = 0;

		{
			_badplace = _x;
			if (_testpos distance _badplace < 1500) then {_c = "colorred";_bad = _bad + 1;};
			
		} foreach _badlocations;
		
		if (count _roads == 0 && count _locations == 0 && _bad == 0) then
		{
			//[_testpos,"colorgreen",format ["%1 %2 %3",count _roads,count _locations,_bad],"select",0.4] call dfk_generate_marker;
			_woodenlootpos pushbackunique _testpos;
		};
	} forEach selectBestPlaces [[5000,5000], 10000, "forest - (5*(houses + sea) + meadow)", 100, 100];

	//_woodvehicles = ["I_MBT_03_cannon_F","I_Truck_02_MRL_F","I_LT_01_AT_F","I_LT_01_scout_F","I_LT_01_cannon_F","I_MRAP_03_F","I_MRAP_03_gmg_F","I_MRAP_03_hmg_F","I_Truck_02_ammo_F","I_Truck_02_fuel_F","I_Truck_02_medical_F","I_Truck_02_box_F","I_Truck_02_transport_F","I_Truck_02_covered_F","I_HMG_01_F","I_GMG_01_F","I_Mortar_01_F","I_static_AA_F","I_static_AT_F","I_G_Offroad_01_AT_F","I_G_Offroad_01_armed_F","I_C_Offroad_02_AT_F","I_C_Offroad_02_LMG_F","I_G_Mortar_01_F"];

	_spawned = 0;
	_spawn_woodvehicles = ceil random 10;

	_trucks = [];

	while {_spawned < _spawn_woodvehicles} do
	{
		_tentativePos = selectrandom _woodenlootpos;
			
		_truck = createvehicle [selectrandom dfk_woodvehicles, _tentativePos, [], 10, "NONE"];
		_truck setdir random 360;
		_netfucker = createvehicle ["CamoNet_INDP_big_F", [0,0,0], [], 0, "NONE"];
		_netfucker attachto [_truck,[0,0,0]];
		_netfucker setdir (getdir _truck -180);
		_spawned = _spawned + 1;
		//[getpos _truck,"colorgreen","Woodtruck!!","hd_flag",1] call dfk_generate_marker;
		_woodenlootpos deleteat (_woodenlootpos find _tentativePos);	
		_trucks pushbackunique _truck;
		[_truck] call dfk_nerf_woodvehicle;
		[_truck] call dfk_initvehicle;
		_truck enableDynamicSimulation true;
	};

	{
		_truck = _x;
		if (!alive _truck) then 
		{
			{
				detach _x;
				deletevehicle _x;
			} forEach attachedObjects _truck;
			deletevehicle _truck;
		}
		else
		{
			{
				detach _x;
			} forEach attachedObjects _truck;		
			dfk_interestingAsFuckArray pushbackunique _truck;
		}
	} foreach _trucks;
	
	publicvariable "dfk_interestingAsFuckArray";
	
	true;
};

dfk_find_fuken_safe_spawnpoint =
{
	//origin position, has to be in pos format [x,y,z]
	_pos = _this select 0;
	//min distance
	_p1 = _this select 1;
	//max distance
	_p2 = _this select 2;
	//min distance to other object
	_p3 = _this select 3;
	//water mode, or shore mode
	_p4 = _this select 4;
	//max terrain inclination
	_p5 = _this select 5;
	//water or shore mode
	_p6 = _this select 6;
	
	_tries = _this select 7;
	_origin_str = _this select 8;
	private _t = 0;
	
	private _safepos = [];
	private _debugstr = format ["%1 Looking for safepos",_origin_str];
	
	//systemchat format ["dfk_find_fuken_safe_spawnpoint 0 %1 %2 %3 %4 %5 %6 %7 %8 %9 %10",_pos,_p1,_p2,_p3,_p4,_p5,_p6,_tries,_t,_safepos];
	
	private _damp = false;
	while {_t < _tries} do
	{
		_safepos = ObjNull;
		_safepos = [_pos, _p1, _p2, _p3, _p4, _p5, _p6] call BIS_fnc_findSafePos;
		waituntil {typename _safepos == "ARRAY"};
		//systemchat format ["Finding safespot... %1/%2 %3 %4 %5",_t,_tries,(_safepos distance _pos),(_t > _tries),((_safepos distance _pos) <= (_p2 + 1))];			
		_debugstr = _debugstr + ".";
		_t = _t + 1;
		if (!(_safepos isequalto []) && !(_safepos isequalto [6000,3200,100])) then {_t = 99999999};
	};
	
	if (_safepos isequalto [] || _safepos isequalto [6000,3200,100]) then
	{
		//systemchat format ["dfk_find_fuken_safe_spawnpoint 1 Safespot not found %1 %2 %3 %4 %5 %6 %7 %8 %9 %10",_pos,_p1,_p2,_p3,_p4,_p5,_p6,_tries,_t,_safepos];
		_debugstr = _debugstr + " not found!";
		_safepos = [];
	}
		else 
	{
		_debugstr = _debugstr + " found!";
		//systemchat format ["dfk_find_fuken_safe_spawnpoint 2 Safespot found! %1 %2 %3 %4 %5 %6 %7 %8 %9 %10",_pos,_p1,_p2,_p3,_p4,_p5,_p6,_tries,_t,_safepos];	
	};
	
	//systemchat _debugstr;
	diag_log _debugstr;
	
	_safepos;
};

//hint "lel";
dfk_spawn_medtents = 
{
	_dfk_medtent_destroyed_meditems = ["Land_PaperBox_01_small_destroyed_brown_F","Land_PaperBox_01_small_destroyed_brown_IDAP_F","Land_PaperBox_01_small_ransacked_brown_F","Land_PaperBox_01_small_ransacked_brown_IDAP_F","Land_PaperBox_01_small_destroyed_white_IDAP_F","Land_PaperBox_01_small_ransacked_white_IDAP_F","Land_EmergencyBlanket_02_discarded_F","Land_FoodSack_01_dmg_brown_F","Land_FoodSack_01_dmg_brown_idap_F","Land_FoodSack_01_empty_brown_F","Land_FoodSack_01_empty_brown_idap_F","Land_FoodSack_01_dmg_white_idap_F","Land_FoodSack_01_empty_white_idap_F","Land_WaterBottle_01_empty_F","Land_WaterBottle_01_compressed_F","BloodPool_01_Large_New_F","BloodPool_01_Large_Old_F","BloodPool_01_Medium_New_F","BloodPool_01_Medium_Old_F","BloodSplatter_01_Large_New_F","BloodSplatter_01_Large_Old_F","BloodSplatter_01_Medium_New_F","BloodSplatter_01_Medium_Old_F","BloodSplatter_01_Small_New_F","BloodSplatter_01_Small_Old_F","BloodSpray_01_New_F","BloodSpray_01_Old_F","BloodTrail_01_New_F","BloodTrail_01_Old_F","Land_Bodybag_01_black_F","Land_Bodybag_01_blue_F","Land_Bodybag_01_white_F","Land_IntravenBag_01_empty_F","MedicalGarbage_01_1x1_v1_F","MedicalGarbage_01_1x1_v2_F","MedicalGarbage_01_1x1_v3_F","MedicalGarbage_01_3x3_v1_F","MedicalGarbage_01_3x3_v2_F","MedicalGarbage_01_5x5_v1_F","MedicalGarbage_01_Bandage_F","MedicalGarbage_01_FirstAidKit_F","MedicalGarbage_01_Gloves_F","MedicalGarbage_01_Injector_F","MedicalGarbage_01_Packaging_F","Land_Garbage_square3_F","Land_Garbage_square5_F","Land_GarbageBags_F","Land_Garbage_line_F","Land_Magazine_rifle_F"];
	_dfk_medtent_normal_meditems = ["CargoNet_01_barrels_F","CargoNet_01_box_F","Land_PaperBox_01_open_boxes_F","Land_PaperBox_01_open_empty_F","Land_PaperBox_01_open_water_F","Land_PaperBox_01_small_closed_brown_F","Land_PaperBox_01_small_closed_brown_IDAP_F","Land_PaperBox_01_small_open_brown_F","Land_PaperBox_01_small_open_brown_IDAP_F","Land_PaperBox_01_small_closed_brown_food_F","Land_PaperBox_01_small_closed_white_med_F","Land_PaperBox_01_small_closed_white_IDAP_F","Land_PaperBox_01_small_open_white_IDAP_F","Land_PaperBox_01_small_stacked_F","Land_FoodSacks_01_cargo_brown_idap_F","Land_FoodSacks_01_cargo_white_idap_F","Land_EmergencyBlanket_01_F","Land_EmergencyBlanket_02_F","Land_EmergencyBlanket_01_stack_F","Land_EmergencyBlanket_02_stack_F","Land_FoodSack_01_full_brown_F","Land_FoodSack_01_full_brown_idap_F","Land_FoodSack_01_full_white_idap_F","Land_FoodSacks_01_large_brown_F","Land_FoodSacks_01_large_brown_idap_F","Land_FoodSacks_01_large_white_idap_F","Land_FoodSacks_01_small_brown_F","Land_FoodSacks_01_small_brown_idap_F","Land_FoodSacks_01_small_white_idap_F","Land_WaterBottle_01_full_F","Land_WaterBottle_01_pack_F","Land_WaterBottle_01_stack_F","Land_IntravenBag_01_full_F","Land_IntravenStand_01_2bags_F","Land_PainKillers_F","Land_Stethoscope_01_F","Land_Stretcher_01_F","Land_Stretcher_01_folded_F","Land_WaterPurificationTablets_F","Land_Antibiotic_F","Land_Bandage_F","Land_BloodBag_F","Land_Bodybag_01_empty_black_F","Land_Bodybag_01_empty_blue_F","Land_Defibrillator_F","Land_DisinfectantSpray_F","Land_FirstAidKit_01_closed_F","Land_FirstAidKit_01_open_F"];
	_tentkind = ["Land_MedicalTent_01_white_generic_inner_F","Land_MedicalTent_01_white_generic_open_F","Land_MedicalTent_01_white_generic_outer_F","Land_MedicalTent_01_white_IDAP_outer_F"];
	_tentfloorkind = ["Land_MedicalTent_01_floor_light_F","Land_MedicalTent_01_floor_dark_F"];
	
	{
		if (0.2 > random 1) then
		{
			//[locationPosition _x,"colorblack",text _x,"hd_flag",0.5] call dfk_generate_marker;
			
			private _safepos = [];
			
			_safepos = [(locationposition _x), 0, 175, 12, 0, 0.2, 0,10,"spawn_medtents"] call dfk_find_fuken_safe_spawnpoint;
			
			if (count _safepos >= 3) then
			{
				//private _safepos = [] call BIS_fnc_findSafePos;
				//waituntil {count _safepos > 0};
				
				//[_safepos,"colorred",text _x,"hd_flag",0.8] call dfk_generate_marker;
				
				_tent = createvehicle [selectrandom _tentkind, _safepos, [], 0, "NONE"];
				_tent setdir random 360;
				//[_tent,"colorred","IDAP aid centre","loc_hospital",0.8] call dfk_generate_marker;
				
				if (0.5 > random 1) then 
				{
					_tentfloor = createvehicle [selectrandom _tentfloorkind, getpos _tent, [], 0, "NONE"];
					_tentfloor setdir getdir _tent;
				};
				
				// if (0.5 > random 1) then
				// {
					// _basedir = getdir _tent;
					// for "_x" from 0 to (5 + random 15) do
					// {
						// _stuff = createvehicle [selectrandom _dfk_medtent_normal_meditems, getpos _tent, [], 10, "NONE"];
						// _stuff setdir _basedir;
						// _stuff enableSimulationGlobal false;
					// };
				// }
				// else
				// {
					// for "_x" from 0 to 20 do
					// {
						// _stuff = createvehicle [selectrandom _dfk_medtent_destroyed_meditems, getpos _tent, [], 20, "NONE"];
						// _stuff setdir random 360;
						// _stuff enableSimulationGlobal false;
					// };	
				// };
				
				for "_x" from 0 to 20 do
				{
					_stuff = createvehicle [selectrandom _dfk_medtent_destroyed_meditems, getpos _tent, [], 20, "NONE"];
					_stuff setdir random 360;
					_stuff enableSimulationGlobal true;
				};					
				
				if (0.2 > random 1) then {_tentfloor = createvehicle ["Item_Medikit", getpos _tent, [], 10, "NONE"];};
				if (0.2 > random 1) then {_tentfloor = createvehicle ["Item_ToolKit", getpos _tent, [], 10, "NONE"];};
				if (0.2 > random 1) then {_tentfloor = createvehicle ["Item_FirstAidKit", getpos _tent, [], 10, "NONE"];};
				if (0.2 > random 1) then {_tentfloor = createvehicle ["Item_FirstAidKit", getpos _tent, [], 10, "NONE"];};
				if (0.2 > random 1) then {_tentfloor = createvehicle ["Item_FirstAidKit", getpos _tent, [], 10, "NONE"];};
				if (0.2 > random 1) then {_tentfloor = createvehicle ["Item_FirstAidKit", getpos _tent, [], 10, "NONE"];};
				[getpos _tent,random 10,"medtentdrop"] remoteexec ["dfk_drop_loot",2];
			};
		};

	} forEach nearestLocations [[dfk_map_x,dfk_map_y], dfk_townspawn_townlocationtypes, dfk_map_raduis];
	
	_done = true;
	
	_done;
};

//testfunction for not spawning qrf in retarded ways
dfk_town_spawnpoints_convoy = 
{
	private _spawn_pos = _this select 0;

	private _spawn_roadarray = _spawn_pos nearroads 300;
	private _spawn_propagate = [];
	//[[_pos,_dir],[_pos,_dir]]
	private _returnarray = [];
	
	//diag_log _spawn_roadarray;
	if (count _spawn_roadarray > 0) then
	{
		{
			//[getpos _x,"colorred","","hd_dot",0.2] call dfk_generate_marker;
			private _asd = roadsConnectedTo _x;
			
			if (count _asd == 1) then
			{
				//private _apa = [getpos _x,"colorblack","","hd_arrow",0.7] call dfk_generate_marker;
				//private _derp = (roadsConnectedTo _x) select 0;
				//private _dir = _x getdir _derp;
				//_apa setmarkerdir _dir;
				_spawn_propagate pushbackunique _x;
			};			
		} foreach _spawn_roadarray;
		
		if (count _spawn_propagate > 0) then
		{
			{
				private _pos = getpos _x;
				private _point = _x;
				
				private _old = [_point];
				
				for "_veh" from 0 to 6 do
				{
					//private _apa = "";
					/* if (_veh == 5) then 
					{
						_apa = [_pos,"colorred",format ["LEADER %1",_veh],"hd_start",0.4] call dfk_generate_marker;
					} 
					else
					{
						_apa = [_pos,"colorred",format ["VEHICLE %1",_veh],"hd_start",0.4] call dfk_generate_marker;
					}; */
					_derp = (roadsConnectedTo _point) select 0;
					while {(_derp in _old) or count (roadsConnectedTo _point) < 1} do
					{
						_derp = selectrandom (roadsConnectedTo _point);
					};
					
					private _dir = _x getdir _derp;
					//_apa setmarkerdir _dir;	
					private _apa2000 = [_pos,_dir];
					_returnarray append [_apa2000];
					
					_old pushbackunique _point;
					_point = _derp;
					_pos = getpos _point;
				};
			} foreach _spawn_propagate;
		}
		else
		{
			//find edge
			//		distance from center detirmens edge case
			//get direction to target
			//	the direction with the least difference between the road direction and the direction towards the target
			
			//spawn accordingly
			
			private _spawn_edgearray = [];
			
			private _r = selectrandom _spawn_roadarray;
			
			private _pos = getpos _r;
			private _point = _r;
			
			private _old = [_point];
			
			for "_veh" from 0 to 5 do
			{
				private _apa = "";
			/* 	if (_veh == 5) then 
				{
					_apa = [_pos,"colorred",format ["R LEADER %1",_veh],"hd_start",0.4] call dfk_generate_marker;
				} 
				else
				{
					_apa = [_pos,"colorred",format ["R VEHICLE %1",_veh],"hd_start",0.4] call dfk_generate_marker;
				}; */
				_derp = (roadsConnectedTo _point) select 0;
				while {(_derp in _old) or count (roadsConnectedTo _point) < 1} do
				{
					_derp = selectrandom (roadsConnectedTo _point);
				};
				
				_dir = _r getdir _derp;
				_apa setmarkerdir _dir;	
				
				private _apa2000 = [_pos,_dir];
				_returnarray append [_apa2000];				
				
				_old pushbackunique _point;
				_point = _derp;
				_pos = getpos _point;
			};			
		};
	};
	
	reverse _returnarray;
	
	//diag_log format ["dfk_town_spawnpoints_convoy %1",_returnarray];
	
	_returnarray;
};


//precompile stuff for new istlands
//this one collects safehouse positions / suitable spawnpoints
//dumps positions in logfile Arma3_*.rpt
//then clean up all the positions and insert them into defines.h / #define precompiledhideoutpositions [...]
dfk_precompilesafehousepositions =
{
	private _ignoretriggers = _this select 0;
	private _towndistancelimiter = _this select 1;
	
	
	//private _trgarr = [island,base];
	private _hideout = [];

	{
		private _b = _x;
		private _add = true;
		
		_npos = 0;
		_npos = [_b,-1] call BIS_fnc_buildingPositions;
		
		if (count _npos <= 0) then
		{
			_add = false;
		};	
		
		if (_add) then
		{
			{
				if (_b distance locationposition _x < _towndistancelimiter) then
				{
					_add = false;
				};
			} foreach nearestLocations [[dfk_map_x,dfk_map_y], dfk_townspawn_townlocationtypes, dfk_map_raduis];		
		};
		
		if (_add) then
		{
			{
				if (_b inarea _x) then
				{
					_add = false;
				};
			} foreach _ignoretriggers;		
		};
		
		if (_add) then
		{
			_hideout pushbackunique _b;
			//diag_log (getpos _b);
			//hintsilent format ["b %1 %2",typeof _b,getpos _b];
			
		};
	} foreach (nearestObjects [[dfk_map_x,dfk_map_y],["House_F"] ,dfk_map_raduis]);
	
	diag_log "#### DFK precompiledhideoutpositions ####";
	{
		[getpos _x,"colorgreen","","hd_flag",1] call dfk_generate_marker;	
		diag_log getpos _x;
	} foreach _hideout;
	diag_log "#### DFK precompiledhideoutpositions END ####";
};

//function for cleaning up player controlled AI- ie retards :) 
dfk_deleteretard =
{
	_retard = _this select 0;
	
	sleep 2;
	deletevehicle _retard;
	[missionNamespace,-1] call BIS_fnc_respawnTickets;
};

dfk_radio_snooper =
{
	private _type = _this select 0;
	private _type_array = _this select 1;
	
	//diag_log format ["dfk_radio_snooper %1 %2 %3",dfk_radiosnooperability,_type,_type_array];
	
	switch (_type) do
	{
		case default
		{
			diag_log format ["dfk_radio_snooper derped hard debug %1 %2",_type,_type_array];
		};
		
		case "supplyrun":
		{
		
			if (dfk_hackedEnemyRadar == 1) then {[format ["ATTENTION: Supply convoy rolling out!"]] remoteexec ["systemchat"]};
			if (dfk_hackedEnemyRadar >= 3) then 
			{
				_namelist = "";
				{
					_namelist = _namelist + ", " + (_x select 1);
					[(_x select 0),"colorpink",(_x select 1),"hd_dot",0.5,1200,0] spawn dfk_mayfly_marker_icon;
					
				} foreach _type_array;
				//airtrackerguy globalchat format ["ATTENTION: Supply convoy rolling out! Visiting: %1",_namelist];
				[format ["ATTENTION: Supply convoy rolling out! Visiting: %1",_namelist]] remoteexec ["systemchat"];
			};			
		};		
		
		case "groundsupport":
		{
			if (dfk_hackedEnemyRadar == 1) then {[format ["ATTENTION: Some shit is going down!"]] remoteexec ["systemchat"];};
			if (dfk_hackedEnemyRadar >= 2) then 
			{
				//airtrackerguy globalchat format ["ATTENTION: QRF scrambled to %1 from %2 %3",mapGridPosition (_type_array select 1),mapGridPosition (_type_array select 0),(_type_array select 2)];
				[airtrackerguy, format ["ATTENTION: QRF scrambled to %1 from %2 %3",mapGridPosition (_type_array select 1),mapGridPosition (_type_array select 0),(_type_array select 2)]] remoteexec ["globalchat"];
				_heading = (_type_array select 1) getdir (_type_array select 0);
				
				[(_type_array select 1),"colorred","QRF!","hd_Warning",0.75,600,0] spawn dfk_mayfly_marker_icon;
				
				for "_dot" from 0 to ((_type_array select 0) distance (_type_array select 1)) step 100 do
				{
					[((_type_array select 1) getpos [_dot,_heading]),"colorred","","hd_dot",0.75,600,0] spawn dfk_mayfly_marker_icon;
				};				
			};			
		};
		
		case "airsupport":
		{
			if (dfk_hackedEnemyRadar == 1) then {[format ["ATTENTION: Some shit is going down!"]] remoteexec ["systemchat"];};
			if (dfk_hackedEnemyRadar >= 4) then 
			{
				//airtrackerguy globalchat format ["ATTENTION: QRF scrambled to %1 from %2 %3",mapGridPosition (_type_array select 0),mapGridPosition (_type_array select 1),(_type_array select 2)];
				[format ["ATTENTION: QRF scrambled to %1 from %2 %3",mapGridPosition (_type_array select 0),mapGridPosition (_type_array select 1),(_type_array select 2)]] remoteexec ["systemchat"];
				[(_type_array select 0),"colorred","QRF!","hd_Warning",0.75,600,0] spawn dfk_mayfly_marker_icon;
			};			
		};		
		
		case "hkpstrike":
		{
			if (dfk_hackedEnemyRadar == 1) then {[format ["ATTENTION: Some shit is going down!"]] remoteexec ["systemchat"];};
			if (dfk_hackedEnemyRadar >= 4) then 
			{
				//airtrackerguy globalchat format ["ATTENTION: QRF Helicopter scrambled! Target %1",mapGridPosition (_type_array select 0)];
				[format ["ATTENTION: QRF Helicopter scrambled! Target %1",mapGridPosition (_type_array select 0)]] remoteexec ["systemchat"];
				[(_type_array select 0),"colorred","","",0.6,600,100] spawn dfk_mayfly_marker_ellipse;
				[(_type_array select 0),"colorred","Incoming helicopter strike!","hd_Warning",0.6,600,0] spawn dfk_mayfly_marker_icon;
			};			
		};				
		
		case "mortar":
		{
			if (dfk_hackedEnemyRadar == 1) then {[format ["ATTENTION: Some shit is going down!"]] remoteexec ["systemchat"];};
			if (dfk_hackedEnemyRadar >= 2) then 
			{
				//airtrackerguy globalchat format ["ATTENTION: Incoming mortar fire at %1 ETA %2 Seconds! TAKE COVER!",mapGridPosition (_type_array select 0),round (_type_array select 1)];
				[format ["ATTENTION: Incoming mortar fire at %1 ETA %2 Seconds! TAKE COVER!",mapGridPosition (_type_array select 0),round (_type_array select 1)]] remoteexec ["systemchat"];
				
				[(_type_array select 0),"colorred","","",0.6,((4*15)+(_type_array select 1) + 60),100] spawn dfk_mayfly_marker_ellipse;
				[(_type_array select 0),"colorred","Incoming mortar fire!","hd_Warning",0.6,((4*15)+(_type_array select 1) + 60),0] spawn dfk_mayfly_marker_icon;
			};
		};
		
		case "artillery":
		{
			if (dfk_hackedEnemyRadar == 1) then {[format ["ATTENTION: Some shit is going down!"]] remoteexec ["systemchat"];};
			if (dfk_hackedEnemyRadar >= 3) then 
			{
				//airtrackerguy globalchat format ["ATTENTION: Incoming artillery fire at %1 ETA %2 Seconds! TAKE COVER",mapGridPosition (_type_array select 0),round (_type_array select 1)];
				[format ["ATTENTION: Incoming artillery fire at %1 ETA %2 Seconds! TAKE COVER",mapGridPosition (_type_array select 0),round (_type_array select 1)]] remoteexec ["systemchat"];
				
				[(_type_array select 0),"colorred","","",0.75,((4*15)+(_type_array select 1) + 60),200] spawn dfk_mayfly_marker_ellipse;
				[(_type_array select 0),"colorred","Incoming artillery fire!","hd_Warning",0.75,((4*15)+(_type_array select 1) + 60),0] spawn dfk_mayfly_marker_icon;
			};			
		};
	};
};

dfk_mayfly_marker_ellipse =
{
	private _pos = _this select 0;
	private _color = _this select 1;
	private _string = _this select 2;
	private _type = _this select 3;	
	private _alpha = _this select 4;
	private _life = _this select 5;
	private _size = _this select 6;

	private _position = [];
	if (typename _pos == "OBJECT") then {_position = getpos _pos};
	if(typename _pos == "ARRAY") then {_position = _pos};

	private _markername = [] call dfk_generate_randomname;
	_markerstr = createMarker [_markername,_position];
	_markerstr setMarkerShape "ELLIPSE";
	_markerstr setMarkerSize [_size,_size];
	_markerstr setMarkerType _type;
	_markerstr setmarkercolor _color;
	_markerstr setmarkertext _string;
	_markerstr setmarkeralpha _alpha;
	
	//diag_log format ["dfk_mayfly_marker %1 %2 %3 %4 %5 %6 %7 %8 %9",_this,_pos,_position,_color,_string,_type,_alpha,_life,_size];
		
	 sleep _life;
	 deletemarker _markerstr;
};

dfk_mayfly_marker_icon =
{
	private _pos = _this select 0;
	private _color = _this select 1;
	private _string = _this select 2;
	private _type = _this select 3;	
	private _alpha = _this select 4;
	private _life = _this select 5;
	private _size = _this select 6;

	private _position = [];
	if (typename _pos == "OBJECT") then {_position = getpos _pos};
	if(typename _pos == "ARRAY") then {_position = _pos};

	private _markername = [] call dfk_generate_randomname;
	_markerstr = createMarker [_markername,_position];
	_markerstr setMarkerShape "ICON";
	_markerstr setMarkerType _type;
	_markerstr setmarkercolor _color;
	_markerstr setmarkertext _string;
	_markerstr setmarkeralpha _alpha;
	
	//diag_log format ["dfk_mayfly_marker %1 %2 %3 %4 %5 %6 %7 %8 %9",_this,_pos,_position,_color,_string,_type,_alpha,_life,_size];
		
	 sleep _life;
	 deletemarker _markerstr;
};

dfk_generate_radioid =
{
	private _type = _this select 0;
	private _radioid = "";
	
	_letters = ["Alpaca","Bruce","Cake","Darion","Enuck","Fork","Garfield","Horse","Iris","Jake","Karoline","Lame","Max","Novo","Obfuscate","Pope","Qvisling","Rake","Saturate","Torment","Utilitarian","Volvo","Whisky","Xerxes","Young","ZZ-top"];
	
	_radioid = format ["%5 %1-%2 %3%4",selectrandom _letters,selectrandom _letters,floor random 9,floor random 9,toUpper _type];
	
	_radioid;
};	

dfk_artyradar_addtarget =
{
	private _artytrg = _this select 0;
	
	private _arr = [_artytrg,time + ((_artytrg distance [7000,1000])/340)];
	
	dfk_artyradar_targetarray pushback _arr;
	
	//diag_log format ["dfk_artyradar_addtarget %1 %2",_arr,dfk_artyradar_targetarray]; 
	//diag_log format ["dfk_artyradar_addtarget %1 %2",_arr,dfk_artyradar_targetarray];
	publicvariable "dfk_artyradar_targetarray";
};

dfk_artyradar =
{
	private _targetarr = [];
	
	//["dfk_artyradar0"] remoteExec ["systemchat"]; 
	
	if (count dfk_artyradar_targetarray > 0) then
	{
		_targetarr pushback ((dfk_artyradar_targetarray select 0) select 0);
		//diag_log _targetarr;
		//diag_log dfk_artyradar_targetarray;
		{
			private _t1 = _x select 0;
			{
				private _t2 = _x;
				//diag_log format ["t1 t2 %1 %2",_t1,_t2];
				if (_t1 distance _t2 > 100) then {_targetarr pushback _t1};
			} foreach _targetarr;
		} foreach dfk_artyradar_targetarray;
		
		{
			[_x] remoteexec ["dfk_request_arty",2];
		} foreach _targetarr;
		
		//[format ["dfk_artyradar %1 %2",_targetarr,dfk_artyradar_targetarray]] remoteExec ["systemchat"]; 
		//diag_log format ["dfk_artyradar %1 %2",_targetarr,dfk_artyradar_targetarray];		
		
		dfk_artyradar_targetarray = [];
		publicvariable "dfk_artyradar_targetarray";
	};
	//[getpos _bomberman] remoteexec ["dfk_request_arty",2];
};

dfk_debugdump =
{
	//diag_log format ["",];
	diag_log format ["time %1",time];
	diag_log format ["Allgroups %1", count allgroups];
	diag_log format ["allunits %1", count allunits];
	diag_log format ["vehicles %1", count vehicles];
	diag_log format ["mor %1 pol %2 log %3",sidemorale,politics,logistics];
	diag_log format ["funds %1 secrets %2 rabatt %3", funds,secrets,rabatt];
	diag_log format ["qrf pool %1",count dfk_qrf_pool_array];
	diag_log format ["drugrunner level %1 delivery %2 target %3",dfk_drugrunnerlevel,dfk_drugrunnerdelivery,dfk_drugrunnertarget];
	diag_log format ["artilleryarray %1",artilleryarray];
	
	diag_log "locationarray";
	{
		diag_log _x;
	} foreach locationarray;
};	

//main function, uses dfk_chute_this_dude and dfk_paradrop
dfk_dropofparadrop = 
{
	private _grp = _this select 0;
	private _target = _this select 1;
	
	private _initpos = [(floor random 1.5) * 12000,(floor random 1.5) * 12000,300];
	private _plane = createVehicle [selectrandom dfk_mercplanes,_initpos , [], 0, "NONE"];
	createvehiclecrew _plane;
	
	if (isServer && _plane iskindof "O_T_VTOL_02_infantry_dynamicLoadout_F") then
	{
		[_plane,[0,"\A3\Air_F_Exp\VTOL_02\Data\VTOL_02_EXT01_CO.paa"]] remoteExec ["setObjectTexture",0,true];
		[_plane,[1,"\A3\Air_F_Exp\VTOL_02\Data\VTOL_02_EXT02_CO.paa"]] remoteExec ["setObjectTexture",0,true];
		[_plane,[2,"\A3\Air_F_Exp\VTOL_02\Data\VTOL_02_EXT03_L_CO.paa"]] remoteExec ["setObjectTexture",0,true];
		[_plane,[3,"\A3\Air_F_Exp\VTOL_02\Data\VTOL_02_EXT03_R_CO.paa"]] remoteExec ["setObjectTexture",0,true];
	};	
	
	private _planegrp = group driver _plane;
	
	private _id = ["stalkersTP"] call dfk_generate_radioid;
	_planegrp setGroupIdGlobal [_id];
	
	_plane setdir (_target getdir _plane);
	_planegrp deletegroupwhenempty true;
	
	{
		_x attachto [_plane,[0,-3,-2]];
	} foreach units _grp;

	_plane flyinheight 300;
	
	_planegrp setvariable ["paragroup",_grp,true];

	private _parapos_1 = _target getpos [2000,(_target getdir _plane)];
	private _parapos0 = _target getpos [1000,(_target getdir _plane)];
	private _targetpos = _target;
	private _postparapos = _target getpos [10000,(_target getdir _plane) + 180];

	_parawp_1 = _planegrp addwaypoint [_parapos_1,0];
	_parawp_1 setwaypointspeed "FULL";

	_parawp0 = _planegrp addwaypoint [_parapos0,0];
	_parawp0 setwaypointspeed "LIMITED";
	_parawp0 setwaypointstatements ["true", "[group this] spawn dfk_paradrop"];

	_parawp_x = _planegrp addwaypoint [_targetpos,0];
	_parawp_x setwaypointstatements ["true", ""];
	_parawp_x setwaypointspeed "LIMITED";

	_parawp_x2 = _planegrp addwaypoint [_postparapos,0];
	_parawp_x2 setwaypointspeed "FULL";

	_parawp2 = _planegrp addwaypoint [_postparapos,0];
	_parawp2 setwaypointspeed "FULL";
	_parawp2 setwaypointstatements ["true", "{deletevehicle _x} foreach crew this; deletevehicle this;"];
};

//for use with dfk_dropofparadrop
dfk_chute_this_dude =
{
	private _dude = _this select 0;
	
	sleep (2 + random 2);
	private _chute = createVehicle ["Steerable_Parachute_F",getpos _dude , [], 0, "NONE"];
	_chute setpos getpos _dude;
	_chute setvelocity (velocity _dude);
	_dude moveindriver _chute;
	sleep 2;
	_dude allowdamage true;
};

//for use with dfk_dropofparadrop
dfk_paradrop = 
{
	private _grp = _this select 0;
	//diag_log _grp;
	
	private _plane = vehicle leader _grp; 
		
	private _paragrp = _grp getvariable ["paragroup",grpNull];

	if ((count units _paragrp) > 0) then
	{
		{
			private _dude = _x;
			_dude allowdamage false;
			detach _dude;

			//NonSteerable_Parachute_F
			//private _chute = createVehicle ["NonSteerable_Parachute_F",getpos _plane , [], 10, "FLY"];
			//_dude moveindriver _chute;
			//_dude setpos (_plane getpos [15,(getdir _plane) - 180]);
			_dude setvelocity (velocity _plane);
			_dude setdir getdir _plane;
			[_dude] spawn dfk_chute_this_dude;
			//diag_log format ["%1 | %2 %3 %4 | %5 %6 %7",_dude,_chute,getpos _chute,velocity _chute,_plane,getpos _plane,velocity _plane];
			
			sleep (0.5 + random 1);
		} foreach units _paragrp;
	};
};

//minimum init [_g1,"",[],[],[]] call dfk_merc_helo_insertion;
dfk_merc_helo_insertion =
{
	private _grp = _this select 0;
	private _hkptype = _this select 1;
	private _transitarr = _this select 2;
	private _target = _this select 3;
	private _posttargettransitarr = _this select 4;
	
	private _hkp = objNull;
	
	//diag_log format ["dfk_merc_helo_insertion | %1 %2 %3 %4 %5",_grp,_hkptype,_transitarr,_target,_posttargettransitarr];
	
	if (isnil "_hkptype" or _hkptype == "") then
	{
		_hkptype = selectrandom dfk_merchelos;
	};
	
	if (isnil "_transitarr" or count _transitarr <= 0) then
	{
		_transitarr = [];
	};
	
	if (isnil "_posttargettransitarr" or count _posttargettransitarr <= 0) then
	{
		_posttargettransitarr = [[(floor random 1.5) * 12000,(floor random 1.5) * 12000]];
	};	
	
	if (isnil "_target" or count _target <= 0) then
	{
		_target = [random 8000,random 8000,0];
	};	
	//diag_log _target;	

	private _spawnpos = [];
	private _spawncond = "FLY";
	if (count _transitarr <= 0) then
	{
		_spawnpos = [(floor random 1.5) * 12000,(floor random 1.5) * 12000,0];
	}
	else
	{
		_spawnpos = _transitarr select 0;
	};
	
	private _hkp = createVehicle [_hkptype, _spawnpos, [], 0, _spawncond];
	createvehiclecrew _hkp;
	private _id = ["stalkerTP"] call dfk_generate_radioid;
	(group driver _hkp) setGroupIdGlobal [_id];
	

	if (isnil "_grp") then
	{
		_grp = creategroup east;
		private _id = ["merc"] call dfk_generate_radioid;
		_grp setGroupIdGlobal [_id];				
	};

	//diag_log "dfk_merc_helo_insertion";
	//diag_log _grp;
	//diag_log units _grp;
	if (count units _grp <= 0) then
	{
		for "_i" from 0 to ((_hkp emptypositions "cargo") - 1) do
		{
			private _unit = _grp createUnit [dfk_merc_baseclass, _spawnpos, [],10, "NONE"];
			_unit assignascargo _hkp;
			_unit moveincargo _hkp;
			[_unit] call dfk_set_skill;
		};
	} 
	else
	{
		{
			_x assignascargo _hkp;
			_x moveincargo _hkp;			
		} foreach units _grp;	
	};
	
	//diag_log format ["dfk_merc_helo_insertion 2 | %1 %2 %3 %4 %5",_hkp,units _grp,_transitarr,_target,_posttargettransitarr];
	[_hkp,units _grp,_transitarr,_target,_posttargettransitarr] spawn dfk_retardocopter;
};

dfk_retardocopter =
{
	private _retardocopter = _this select 0;
	private _cargoarr = _this select 1;
	private _transitpos = _this select 2;
	private _targetpos = _this select 3;
	private _postargettransit = _this select 4;
	
	//diag_log format ["dfk_retardocopter | %1 %2 %3 %4 %5",_retardocopter,_cargoarr,_transitpos,_targetpos,_postargettransit];
	
	private _retardocoptergrp = group driver _retardocopter;
		
	{
		_x assignascargo _retardocopter;
		_x moveincargo _retardocopter;
	} foreach _cargoarr;
	_cargoarr ordergetin true;
	
	{if (!(_x in crew _retardocopter)) then {deletevehicle _x;}} foreach _cargoarr;
	
	{
		_retardocopter domove _x;
		waituntil {_retardocopter distance _x < 300};
	} foreach _transitpos;
	
	_retardocopter domove _targetpos;
	waituntil {_retardocopter distance _targetpos < 300};
	_retardocopter land "GET OUT";
	waituntil {landresult _retardocopter == "Found"};
	waituntil {getpos _retardocopter select 2 < 0.5};
	
	_cargoarr ordergetin false;

	{
		//_retardocopter land "GET OUT";	
		waituntil {getpos _retardocopter select 2 < 1};
		sleep 0.5;
		unassignvehicle _x;
		[_x] ordergetin false;
		[_x] allowgetin false;
		_x action ["GETOUT",vehicle _x];
		sleep 0.5;
	} foreach _cargoarr;
		
	_retardocopter land "NONE";
	
	{
		_retardocopter domove _x;
		waituntil {_retardocopter distance _x < 300};
	} foreach _postargettransit;
	
	{deletevehicle _x} foreach units _retardocoptergrp;
	deletegroup _retardocoptergrp;
	deletevehicle _retardocopter;
};

dfk_spawn_stalkers = 
{
	private _stalkersgrp = creategroup east;
	private _id = ["stalkers"] call dfk_generate_radioid;
	_stalkersgrp setGroupIdGlobal [_id];			
	//set origin/voices/faces
	
	// private _engvoices = 
	// [
		// ["MALE01ENGB","MALE02ENGB","MALE03ENGB","MALE04ENGB","MALE05ENGB"],
		// ["MALE01ENG","MALE02ENG","MALE03ENG","MALE04ENG","MALE05ENG","MALE06ENG","MALE07ENG","MALE08ENG","MALE09ENG","MALE10ENG","MALE11ENG","MALE12ENG"],
		// ["MALE01GRE","MALE02GRE","MALE03GRE","MALE04GRE","MALE05GRE","MALE06GRE"],
		// ["MALE01ENGFRE","MALE02ENGFRE"],
		// ["MALE01FRE","MALE02FRE","MALE03FRE"],
		// ["MALE01ENGB","MALE02ENGB","MALE03ENGB","MALE04ENGB","MALE05ENGB","MALE01ENG","MALE02ENG","MALE03ENG","MALE04ENG","MALE05ENG","MALE06ENG","MALE01GRE","MALE02GRE","MALE03GRE","MALE04GRE","MALE05GRE","MALE06GRE","MALE01ENGFRE","MALE02ENGFRE"]
	// ];
	private _merc = selectrandom [1,2,3,4,5,6,7];
	
	switch (_merc) do
	{
		case 1:
		{
			//A-team
			for "_stalki" from 1 to 10 do
			{
				private _stalker = _stalkersgrp createUnit [dfk_merc_baseclass, getpos baselogic, [], 50, "NONE"];
				[_stalker] call ([dfk_merc_gear_1,dfk_merc_gear_2,dfk_merc_gear_3,dfk_merc_gear_4,dfk_merc_gear_5,dfk_merc_gear_6,dfk_merc_gear_7,dfk_merc_gear_8,dfk_merc_gear_9,dfk_merc_gear_10,dfk_merc_gear_11] select _stalki);
				
				_v = selectrandom (selectrandom dfk_mercvoices);
				[_stalker, _v] remoteExecCall ["setSpeaker", 0];
			};	
		};
		
		//renegades
		case 2:
		{
			for "_stalki" from 1 to 10 do
			{
				private _stalker = _stalkersgrp createUnit [dfk_merc_baseclass, getpos baselogic, [], 50, "NONE"];
				[_stalker] call selectrandom [dfk_merc_renegade1,dfk_merc_renegade2,dfk_merc_renegade3,dfk_merc_renegade4];
				
				_v = selectrandom (selectrandom dfk_mercvoices);
				[_stalker, _v] remoteExecCall ["setSpeaker", 0];
			};			
		};
		
		//snipers
		case 3:
		{
			for "_stalki" from 1 to 10 do
			{
				private _stalker = _stalkersgrp createUnit [dfk_merc_sniperclass, getpos baselogic, [], 50, "NONE"];
				[_stalker] call selectrandom [dfk_merc_snapr1,dfk_merc_snapr2,dfk_merc_snapr3,dfk_merc_snapr4,dfk_merc_snapr5,dfk_merc_snapr6,dfk_merc_snapr7,dfk_merc_snapr8,dfk_merc_snapr9,dfk_merc_snapr10];
			};			
		};		
		
		//simple
		case 4:
		{
			for "_stalki" from 1 to 10 do
			{
				private _stalker = _stalkersgrp createUnit [dfk_merc_baseclass, getpos baselogic, [], 50, "NONE"];
				[_stalker] call dfk_merc_simple;
				
				_v = selectrandom (selectrandom dfk_mercvoices);
				[_stalker, _v] remoteExecCall ["setSpeaker", 0];				
			};			
		};			
		
		//viper1 O_V_Soldier_hex_F Male01PERVR
		case 5:
		{
			for "_stalki" from 1 to 10 do
			{
				private _stalker = _stalkersgrp createUnit [dfk_merc_operatorclass, getpos baselogic, [], 50, "NONE"];
				[_stalker, "Male01PERVR"] remoteExecCall ["setSpeaker", 0];				
			};			
		};
		
		case 6:
		{
			for "_stalki" from 1 to 10 do
			{
				private _stalker = _stalkersgrp createUnit [dfk_merc_baseclass, getpos baselogic, [], 50, "NONE"];
				[_stalker] call selectrandom [dfk_merc_stealthcop1,dfk_merc_stealthcop2,dfk_merc_stealthcop3,dfk_merc_stealthcop4,dfk_merc_stealthcop5];
				
				_v = selectrandom (selectrandom dfk_mercvoices);
				[_stalker, _v] remoteExecCall ["setSpeaker", 0];
			};			
		};		
		
		//urban?
		case 7:
		{
			for "_stalki" from 1 to 10 do
			{
				private _stalker = _stalkersgrp createUnit [dfk_merc_baseclass, getpos baselogic, [], 50, "NONE"];
				[_stalker] call selectrandom [dfk_merc_urban1,dfk_merc_urban2,dfk_merc_urban3,dfk_merc_urban4,dfk_merc_urban5,dfk_merc_urban6];
			};			
		};			
	};
	
	if (nerfgear) then 
	{
		//[_unit] remoteExec ["dfk_nerf_gear",2];
		{
			_x addMPEventHandler ["mpkilled", {Null = [_x] call dfk_nerf_gear;}];
			_x setskill random 1;
		} foreach units _stalkersgrp;
	};
	
	_stalkersgrp;
};

dfk_town_spawnparkedcars =
{
	_townpos = _this select 0;
	_numberofcars = _this select 1;
	_cartypesarray = _this select 2;
	
	private _veharray = [];
	
	//_numberofcars = _numberofcars * 3;

	//where we're going, we're gonna need roads
	private _roads = _townpos nearRoads 200;
	//[format ["Roads: %1",_roads]] remoteExec ["systemchat"]; 
	private _roadcars = [];
	private _wildcars = [];
	for [{_i = 1}, {_i < (1 + (random _numberofcars))},{_i = _i + 1}] do
	{
		private _car = "";
	
		if (count _roads > 0) then
		{
			private _pos = [];

			private _carclass = selectrandom _cartypesarray;
			
			private _car = createVehicle [_carclass, [0,0,1000], [], 100, "NONE"];
			_car enableSimulationGlobal true;		
			private _bbr = boundingBoxReal _car;
			private _p1 = _bbr select 0;
			private _p2 = _bbr select 1;
			private _maxWidth = abs ((_p2 select 0) - (_p1 select 0));
			
			_maxWidth = (_maxWidth / 2);	

			private _direction = 0;
			
			private _t = 0;
			
			while {(_pos isequalto []) && count _roads > 0 && _t < 10} do
			{
				_t = _t + 1;
				//systemchat format ["%1",_t];
			
				private _r_i = floor ((random (count _roads)));
				private _r = _roads select _r_i;
				_roads deleteat _r_i;

				private _r2 = roadsConnectedTo _r;
				
				_direction = _r getdir (_r2 select 0);
				
				private _park = 90;
				private _dist = 5;
				if (0.5 > random 1) then
				{
					_park = 90;
					_dist = 3;
				}
				else
				{
					_park = -90;
					_dist = -3;
					_direction = _direction - 180;
				};

				private _p1 = _r getpos [_dist,_park];
				_pos = _p1 isflatempty [_maxWidth,-1,0.5,_maxWidth,-1,false,ObjNull];
			};
			
			if (count _roads > 0 && count _pos >= 2) then
			{
				_car setpos [_pos select 0,_pos select 1,0];
				_car setdir _direction;
				if (!alive _car) then {deletevehicle _car};
				//[format ["spawncar on road: %1",getpos _car]] remoteExec ["systemchat"];
				if (debug) then {[_car,"colorwhite","road","hd_arrow",true,1] execvm "marker_track.sqf";};
				_veharray pushback _car;
				_roadcars pushback _car;
			}
			else
			{
				deletevehicle _car;
				//_i = _i - 1;
				//systemchat "Dumped a road car";
			};
		}
		else
		{
			private _carclass = selectrandom _cartypesarray;
			private _pos = [];
			//_pos = [_townpos, 0, 200, 12, 0, 0.5, 0] call BIS_fnc_findSafePos;
			
			private _car = createVehicle [_carclass, [0,0,1000], [], 100, "NONE"];
			
			private _bbr = boundingBoxReal _car;
			private _p1 = _bbr select 0;
			private _p2 = _bbr select 1;
			private _maxWidth = abs ((_p2 select 0) - (_p1 select 0));
			
			_maxWidth = (_maxWidth / 2) + 3;		
			
			_pos = [_townpos, 0, 200, _maxWidth, 0, 0.5, 0,10,"town_spawnparkedcars"] call dfk_find_fuken_safe_spawnpoint;
			
			if (!(_pos isequalto []) && !(_pos isequalto [6000,3200,100])) then
			{
				_car setpos _pos;
				//if (count _pos > 0) then {_car setpos _pos};
				//waituntil {count _pos > 0};
				_car setdir random 360;
				//[format ["spawncar wild: %1",getpos _car]] remoteExec ["systemchat"];
				if (debug) then {[_car,"colorwhite","wild","hd_arrow",true,1] execvm "marker_track.sqf";};
				_veharray pushback _car;
				_wildcars pushback _car;			
			}
			else
			{
				deletevehicle _car;
				
				//systemchat "Dumped a wild car";
				
				//_i = _i - 1;
			};
		};
	};			
	//diag_log _veharray;

	//debug stuff
	// private _alive = 0;
	// private _dead = 0;
	// private _faraway = 0;
	// private _farawayalive = 0;
	// private _farawaydead = 0;
	// private _pos = _townpos;
	// {
		// if (alive _x) then {_alive = _alive + 1} else {_dead = _dead + 1};
		// if (_x distance _pos > 300) then 
		// {
			// _faraway = _faraway + 1;
			// if (alive _x) then {_farawayalive = _farawayalive + 1} else {_farawaydead = _farawaydead + 1};
		// };
	// } foreach _veharray;

	// private _roadalive = 0;
	// private _roaddead = 0;
	// {
		// if (alive _x) then {_roadalive = _roadalive + 1} else {_roaddead = _roaddead + 1};
	// } foreach _roadcars;

	// private _wildalive = 0;
	// private _wilddead = 0;
	// {
		// _x allowdamage true;
		// if (alive _x) then {_wildalive = _wildalive + 1} else {_wilddead = _wilddead + 1};
	// } foreach _wildcars;

	//diag_log format ["%11 Alive: %1 Dead: %2 Faraway: %3 Alive: %4 Dead: %5 Count roads: %6 Road Alive: %7 D %8 Wild alive: %9 D: %10",_alive,_dead,_faraway,_farawayalive,_farawaydead,count _roads,_roadalive,_roaddead,_wildalive,_wilddead,_x select 1];				
	
	_veharray;
};

//function for populating/crewing bunkers
//needs to return array with groups, use with "call"
dfk_populate_bunkers =
{
	_populatebunkerstempinputarray = _this select 0;
	_populatebunkers_bunkersarray = _this select 1;
	_populatebunkers_troopsquota = _this select 2;
	
	//if (debug) then {[format ["populatebunkers 0 %1 %2 %3",_populatebunkerstempinputarray,_populatebunkers_bunkersarray,_populatebunkers_troopsquota]] remoteExec ["diag_log"];};		
	
	//private _bunkerdudesarr = ["O_Soldier_lite_F"];
	
	if (count _populatebunkers_bunkersarray > 0) then
	{
		for "_bunkerdude" from 1 to _populatebunkers_troopsquota do
		{
			private _bunker = selectrandom _populatebunkers_bunkersarray;
			private _bunkerposarr = _bunker buildingPos -1;
			
			private _grp = creategroup east;
			private _id = ["bunkerdude"] call dfk_generate_radioid;
			_grp setGroupIdGlobal [_id];					
			private _bunkerpos = selectrandom _bunkerposarr;
			private _unit = Objnull;
			
			_unit = [_unit,_grp,_bunkerpos,0,"bunker"] call dfk_spawn_soldier;
			
			_wp0 = _grp addwaypoint [_bunkerpos,0];
			_wp0 setwaypointtype "SENTRY";
			(vehicle _unit) setdir ((_unit getdir _bunker) + 180);		
			_bunkerposarr deleteat (_bunkerposarr find _bunkerpos);
			_populatebunkerstempinputarray pushback _grp;
			//if (debug) then {[format ["populatebunkers 2 %1 %2 %3 %4 %5 %6 %7",_unit,_grp,_dude,_bunkerpos,_wp0,_bunker]] remoteExec ["diag_log"];};				
		};
	};
/* 	{
		_bunkerposarr = _x buildingPos -1;
		_bunker = _x;
		
		if (debug) then {[format ["populatebunkers 1 %1 %2",_bunkerposarr,_bunker]] remoteExec ["diag_log"];};		
		
		if (count _bunkerposarr >= 3) then
		{
			select 3 random positions to add troops to
			for "_bunkerdudederp" from 1 to 3 do
			{
				_grp = creategroup east;
				_dude = selectrandom _bunkerdudesarr;
				_bunkerpos = selectrandom _bunkerposarr;
				_unit = Objnull;
				
				_unit = [_unit,_grp,_bunkerpos,0,"bunker"] call dfk_spawn_soldier;
				
				_wp0 = _grp addwaypoint [_bunkerpos,0];
				_wp0 setwaypointtype "SENTRY";
				(vehicle _unit) setdir ((_unit getdir _bunker) + 180);		
				_bunkerposarr deleteat (_bunkerposarr find _bunkerpos);
				_populatebunkerstempinputarray pushback _grp;
				if (debug) then {[format ["populatebunkers 2 %1 %2 %3 %4 %5 %6",_unit,_grp,_dude,_bunkerpos,_wp0]] remoteExec ["diag_log"];};		
			};
		};
		else
		{
			//add troops to each pos 
			{
				//select 3 random positions to add troops to
				_grp = creategroup east;
				_dude = selectrandom _bunkerdudesarr;
				_bunkerpos = _x;
				
				_populatebunkerstempinputarray pushback _grp;
				
				_unit = [_unit,_grp,_bunkerpos,0,"bunker"] call dfk_spawn_soldier;
								
				_wp0 = _grp addwaypoint [_bunkerpos,0];
				_wp0 setwaypointtype "SENTRY";
				(vehicle _unit) setdir ((_unit getdir _bunker) + 180);	
				
				if (debug) then {[format ["populatebunkers 3 %1 %2 %3 %4 %5 %6",_unit,_grp,_dude,_bunkerpos,_wp0]] remoteExec ["diag_log"];};		
			} foreach _bunkerposarr;
		};
	} foreach _populatebunkers_bunkersarray; */
	
	//if (debug) then {[format ["populatebunkers 99 %1",_populatebunkerstempinputarray]] remoteExec ["diag_log"];};		
	
	_populatebunkerstempinputarray;
};

//function for removing 90% of units gear upon death
//for balanceing reasons and/or just fucking with you :)
dfk_nerf_gear =
{
	private _dude = _this select 0;
	
	//remove all stuff
	//re-add random stuff based on equipment
	//also go fuck yourself :)
	
	removeAllHandgunItems _dude;
	//removeAllItems _dude;
	removeAllItemsWithMagazines _dude;
	removeAllPrimaryWeaponItems _dude;
	
	//get all magtypes
	if (count weapons _dude > 0) then
	{
		private _mags = [];
		{
			_mags append getarray(configfile >> "CfgWeapons" >> _x >> "magazines");
		} foreach weapons _dude;
		
		//sanitize array
		{
			if (!(_x in sanctionedmagsarray)) then
			{
				_mags deleteat _foreachindex;
			}
		} foreach _mags;

		//randomly readd mags
		//I am a generous God
		if (count _mags > 0) then
		{
			for "_thingie" from 1 to floor random 5 do
			{
				private _container = selectrandom [(uniformContainer _dude),(vestContainer _dude),(backpackContainer _dude)];
				private _mag = selectrandom _mags;
				
				if (_container canAdd _mag) then 
				{
					//_container addItemCargoGlobal [_mag,1];
					private _nr = getnumber(configfile >> "CfgMagazines" >> _mag >> "count");
					_container addMagazineAmmoCargo [_mag, 1, ceil random _nr];
				};
			};
		};
	};
};

dfk_nerf_gear_vehicle =
{
	private _vehicle = _this select 0;
	
	clearBackpackCargoGlobal _vehicle;
	clearItemCargoGlobal _vehicle;
	clearMagazineCargoGlobal _vehicle;
	clearWeaponCargoGlobal _vehicle;	
	
	_vehicle addItemCargoGlobal ["FirstAidKit",4];
	
	private _supplytruck = _vehicle getvariable ["supplytruckgrp",false];
	
	// #ifndef sanctionedmagsarray
		// #define sanctionedmagsarray sanctionedmagsarray_def
	// #endif	
	
	if (typename _supplytruck == "BOOL") then
	{
		//I am not a monster...
		for "_i" from 0 to (4 + ceil (random 10)) do
		{
			//"FlareWhite_F"
			//private _thingie = ["FirstAidKit","SmokeShell","SmokeShellGreen","SmokeShellPurple","SmokeShellOrange","Chemlight_green","Chemlight_yellow","Chemlight_red","Chemlight_blue","1Rnd_Smoke_Grenade_shell","UGL_FlareWhite_F","HandGrenade","MiniGrenade"];
			_vehicle addItemCargoGlobal [selectrandom dfk_nerfgear_vehicleshit1,random 3];
		};
			
		//20% * 9% = 0.018%
		if (0.5 > random 1) then 
		{
			_vehicle addItemCargoGlobal [selectrandom dfk_nerfgear_vehicleshit2,1];
		};
		
		//20% * 9% = 0.022%
		if (0.5 > random 1) then 
		{
			private _randomgun = selectrandom dfk_nerfgear_vehicleshit3;
			private _mags = getarray(configfile >> "CfgWeapons" >> _randomgun >> "magazines");
			
			{
				if (!(_x in sanctionedmagsarray)) then
				{
					_mags deleteat _foreachindex;
				}
			} foreach _mags;
			
			{
				_vehicle addItemCargoGlobal [_x,(4 + (ceil random 5))];
			} foreach _mags;
		};	
	};
	
	if (typename _supplytruck == "GROUP") then
	{
		//I am not a monster...
		for "_i" from 0 to (20) do
		{
			//"FlareWhite_F"
			//private _thingie = ["FirstAidKit","SmokeShell","SmokeShellGreen","SmokeShellPurple","SmokeShellOrange","Chemlight_green","Chemlight_yellow","Chemlight_red","Chemlight_blue","1Rnd_Smoke_Grenade_shell","UGL_FlareWhite_F","HandGrenade","MiniGrenade"];
			_vehicle addItemCargoGlobal [selectrandom dfk_nerfgear_vehicleshit1,random 5];
		};
			
		_vehicle addItemCargoGlobal [selectrandom dfk_nerfgear_vehicleshit2,random 3];
		_vehicle addItemCargoGlobal [selectrandom dfk_nerfgear_vehicleshit2,random 3];
		_vehicle addItemCargoGlobal [selectrandom dfk_nerfgear_vehicleshit2,random 3];
		_vehicle addItemCargoGlobal [selectrandom dfk_nerfgear_vehicleshit2,random 3];
		
		//20% * 9% = 0.022%
		for "_i" from 0 to (random 6) do
		{
			private _randomgun = selectrandom dfk_nerfgear_vehicleshit3;
			private _mags = getarray(configfile >> "CfgWeapons" >> _randomgun >> "magazines");
			
			{
				if (!(_x in sanctionedmagsarray)) then
				{
					_mags deleteat _foreachindex;
				}
			} foreach _mags;
			
			{
				_vehicle addItemCargoGlobal [_x,10];
			} foreach _mags;
		};	
		
		if (random 1 > 0.5) then
		{
			private _coolshit = selectrandom [
												[["O_GMG_01_A_weapon_F","O_HMG_01_A_weapon_F"],["O_HMG_01_support_F"]],
												[["O_HMG_01_high_weapon_F","O_HMG_01_weapon_F","O_GMG_01_high_weapon_F","O_GMG_01_weapon_F","O_AA_01_weapon_F","O_AT_01_weapon_F"],["O_HMG_01_support_high_F","O_HMG_01_support_F"]],
												[["O_Mortar_01_support_F"],["O_Mortar_01_weapon_F"]],
												[["O_Static_Designator_02_weapon_F"],["O_Static_Designator_02_weapon_F"]]
											];
			
			_vehicle addbackpackcargoglobal [selectrandom (_coolshit select 0),1];
			_vehicle addbackpackcargoglobal [selectrandom (_coolshit select 1),1];
		};
	};	
};

//preventing cars from blowing up do to retarded spawns(and lazy coding);
dfk_unset_godmode =
{
	private _car = _this select 0;
	
	sleep 10;
	_car enableSimulationGlobal true;
};

dfk_spawn_hkp =
{
	private _pos0 = _this select 0;
	private _pos = [_pos0 select 0,_pos0 select 1,0];
	private _referencearray = _this select 1;
	
	private _car = "";
	private _grp = "";
	private _arr = "";
	
	private _debugstr = "";
	
	//private ["_car","_grp","_arr","_pos"];
	
	private _type = (selectrandom (selectrandom roaming_air));
	//_type = "O_UAV_02_dynamicLoadout_F";
	//_type = selectrandom roaming_mercplanes;
	
	private _car = createVehicle [_type, [(_pos select 0) + (random 1000) - (random 1000),(_pos select 1) + (random 1000) - (random 1000),700], [], 1000, "FLY"];
	//_car setVectorUp [0, 0, 0];
	
	//"Van_02_medevac_base_F"
	[_car] remoteExec ["dfk_initvehicle",2];
	
	createvehiclecrew _car;
	
	// _grp = creategroup east;
	// private _id = ["drone"] call dfk_generate_radioid;
	// _grp setGroupIdGlobal [_id];	
	
	// private _pilot = _grp createUnit [dfk_opfor_baseclass, [0,0,0], [], 0, "NONE"];
	// private _gunner = _grp createUnit [dfk_opfor_baseclass, [0,0,0], [], 0, "NONE"];
	
	// _pilot moveinany _car;
	// _gunner moveinany _car;
	
	//diag_log format ["spawn_hkop log | %1 %2 %3 %4 %5 %6",_pos,_car,_type,_grp,_pilot,_gunner];
	
	_grp = group driver _car;
	_grp addvehicle _car;
	_grp setvariable ["patrol",true,true];
	
	if (faction _car in BADFACTION) then
	{
		{	
			_x addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,"KILLEDEAST"] execVM "killer.sqf";}];
		} foreach units _grp;
	};
	
	if (faction _car in CIVFACTION) then
	{
		{
			_x addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,""] execVM "killer.sqf";}];
		} foreach units _grp;
	};		
	
	_grp setvariable ["spawn_car_lastpos",getpos _car,true];
	
	//_car setdir (_car getdir _pos);
	
	_car flyinheight 300;
	driver _car flyinheight 300;
	
	_arr = [_car,_grp];

	_arr;
};

dfk_spawn_boat =
{
	private _pos = _this select 0;
	private _pos = [_pos select 0,_pos select 1,0];
	private _referencearray = _this select 1;
	
	private _car = "";
	private _grp = "";
	private _arr = "";
	
	private _debugstr = "";
	
	//private ["_car","_grp","_arr","_pos"];
	
	private _type = (selectrandom (selectrandom roamingboat_nestled));
	
	private _p = [_pos, viewdistance + 200, viewdistance + 200, 12, 2, 0, 0] call BIS_fnc_findSafePos;
	
	_car = createVehicle [_type, _p, [], 0, "NONE"];
	
	//"Van_02_medevac_base_F"
	[_car] remoteExec ["dfk_initvehicle",2];
	
	createvehiclecrew _car;
	
	_grp = group driver _car;
	_grp addvehicle _car;
	_grp setvariable ["patrol",true,true];
	
	if (faction _car in BADFACTION) then
	{
		{
			[_x,"driver"] remoteExec ["dfk_equip_soldier",2];		
			_x addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,"KILLEDEAST"] execVM "killer.sqf";}];
			//if (debug) then {[_x,"colorred",_debugstr,"hd_dot",true,1] execvm "marker_track.sqf";};
		} foreach units _grp;
		//[leader _grp] call dfk_equip_sqdld;
	};
	
	if (faction _car in CIVFACTION) then
	{
		{
			_x addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,""] execVM "killer.sqf";}];
			//if (debug) then {[_x,"colorblue",_debugstr,"hd_dot",true,1] execvm "marker_track.sqf";};
		} foreach units _grp;
	};		
	
	_grp setvariable ["spawn_car_lastpos",getpos _car,true];
	
	_car setdir ([_car, _pos] call BIS_fnc_dirTo);
	
	_arr = [_car,_grp];
	//if (debug) then {[format ["00 pos %1 count p_arr %2 _p %3 disp %4 type %5 car %6 crewtype %7 siden %8 sidestr %9 grp %10",_pos,count _p_array,_p,_dispersion,_type,_car,_crewtype,_sidenumber,_sidestr,_grp]] remoteExec ["systemchat"];};
	
	_arr;
};

dfk_mine_site_init =
{
	private _position = _this select 0;
	private _minetypes = _this select 1;
	private _inbuildings = _this select 2;
	private _numberofmines = _this select 3;
	private _radius = _this select 4;
	//position and array
	//["minesite init,.. derp"] remoteexec ["systemchat"];	
	
	// _grp = creategroup sidelogic;
	// _logic = _grp createUnit ["LOGIC", _position, [], 0, "NONE"];
	
	private _trigger = createTrigger ["EmptyDetector",_position,true];
	_trigger setTriggerArea [_radius + 100,_radius + 100, 100, false];
	_trigger setTriggerActivation ["ANYPLAYER", "PRESENT", true];
	_trigger setTriggerStatements ["this", "[thisTrigger] call dfk_spawn_minesite", "[thisTrigger] call dfk_despawn_minesite"];

	_trigger setvariable ["minesite_numberofmines",_numberofmines,true];
	_trigger setvariable ["minesite_typearray",_minetypes,true];
	_trigger setvariable ["minesite_inbuildings",_inbuildings,true];
	_trigger setvariable ["minesite_mymines",[],true];
};

dfk_spawn_minesite =
{
	private _trigger = _this select 0;
	//[format ["minesite spawned 0 %1 %2",_this,_trigger]] remoteexec ["systemchat"];	
	
	private _numberofmines = _trigger getvariable ["minesite_numberofmines",25];
	private _minetypes = _trigger getvariable ["minesite_typearray",["BombCluster_02_UXO1_F"]];
	private _inbuildings = _trigger getvariable ["minesite_inbuildings",false];
	
	private _initpos = getpos _trigger;
	private _radius = (triggerarea _trigger select 0) - 100;
	
	private _mine_arr = [];
	
	for "_i" from 1 to _numberofmines do
	{
		private _mine = createMine [selectrandom _minetypes, _initpos, [], _radius];
		_mine enableDynamicSimulation true;
		_mine_arr pushback _mine;
		if (_inbuildings && (random 1 > 0.5)) then
		{
			private _b = nearestbuilding getpos _mine;
			private _b_pos = [_b, -1] call BIS_fnc_buildingPositions;
			if (count _b_pos > 0) then
			{
				_mine setpos (selectrandom _b_pos);
			};
		};
	};
	_trigger setvariable ["minesite_mymines",_mine_arr,true];	
	//[format ["minesite spawned %1 | %2 | no %3 types %4 | %5 | %6 | %7 arr %8",_this,_trigger,_numberofmines,_minetypes,_inbuildings,_initpos,_radius,_mine_arr]] remoteexec ["systemchat"];	
};

dfk_despawn_minesite =
{
	private _trigger = _this select 0;
	//[format ["minesite despawned 0 %1 %2",_this,_trigger]] remoteexec ["systemchat"];		
	
	private _mine_arr = _trigger getvariable "minesite_mymines";
	
	{
		deletevehicle _x;
	} foreach _mine_arr;
	
	_trigger setvariable ["minesite_mymines",[],true];
	
	//[format ["minesite despawned %1 %2 %3",_trigger,_mine_arr,_trigger getvariable ["minesite_inbuildings"]]] remoteexec ["systemchat"];	
};

//drugrunning tasks and functions
dfk_drugdelivery =
{
	dfk_drugrunnerdelivery = dfk_drugrunnerdelivery + 1;
	//[format ["%1",dfk_drugrunnerdelivery]] remoteexec ["systemchat"];	
	["DRUGRUN1"] remoteexec ["DFK_fnc_add_political",2];
	
	private _cashmode = ["dfk_storekind"] call BIS_fnc_getParamValue;
	
	private _amp = 1;

	if (_cashmode == 4) then
	{	
		_amp = 2;
	};
	
	if (dfk_drugrunnerdelivery >= dfk_drugrunnertarget) then
	{
		private _derp = 10000 * dfk_drugrunnertarget;
		for "_i" from 1 to dfk_drugrunnertarget do
		{
			_derp = _derp + (round (random (10000 * dfk_drugrunnertarget)) * _amp);
		};
		[_derp] remoteexec ["dfk_add_funds",2];
		dfk_drugrunnerdelivery = 0;		
		dfk_drugrunnerlevel = dfk_drugrunnerlevel + 1;
		dfk_drugrunnertarget = dfk_drugrunnertarget + (ceil random 5);
		["DRUGRUN2"] remoteexec ["DFK_fnc_add_political",2];
		//[] remoteexec ["dfk_add_randomvipgear",2];
		if (count vipstorearray > 0 && count vipstorearray > 0) then {storearray append vipstorearray; vipstorearray = []; publicvariable "vipstorearray";publicvariable "storearray"};
		[] remoteExec ["dfk_generate_deliverypoint",2];
		//[format ["%1 %2 %3 %4",dfk_drugrunnerdelivery,dfk_drugrunnerlevel,dfk_drugrunnertarget]] remoteexec ["systemchat"];	
	};
};

dfk_generate_deliverypoint =
{
	private _try = 0;
	
	while {_try < 1000} do
	{
		private _pos = selectrandom precompiledhideoutpositions;
		private _houses = nearestObjects [_pos,["House_F"],30];
		private _housepositions = [];
		private _h = selectrandom _houses;
		private _far = true;
		
		if (count _houses == 0) then
		{
			_h = _pos;
		}
		else
		{
			if (_h iskindof "Land_Lighthouse_03_red_F" or _h iskindof "Land_Lighthouse_03_green_F" or !alive _h or _h iskindof "Ruins_F") then
			{
				_far = false;
			};		
		};
		
		if (_far) then
		{
			{
				_housepositions append ([_x, -1] call BIS_fnc_buildingPositions);
			} foreach _houses;
			_try = 999999999;
				
			if (count _houses == 0) then
			{
				drugrunnercrate2 setpos _h;
			}
			else
			{
				drugrunnercrate2 setpos selectrandom _housepositions;	
			};
			
			//[format ["Deliverycrate hidden somewhere at %1 (this is important, write it down)",mapGridPosition drugrunnercrate2]] remoteExec ["systemchat"];
			//[getpos _h,"coloryellow","L00thouse","hd_flag",1] remoteExec ["dfk_generate_marker",2];
		};
		_try = _try + 1;
	};
	drugrunnercrate1 setpos [(random 12000),(random 12000),0];
	
	for "_i" from 1 to dfk_drugrunnertarget do
	{
		private _cnd = createvehicle [selectrandom importantstuffarray, getpos drugrunnercrate1, [],10, "NONE"];
		_cnd setvariable ["cnd",true,true];
		_cnd setvariable ["important",true,true];
		_cnd allowdamage false;
		[_cnd, ["Take CnD-stuff", "dfk_cnd.sqf"]] remoteExec ["addAction",0,true];	
		[_cnd,"colorblue","CnD","hd_pickup",true,0.25] execvm "marker_track.sqf";					
	};		
	

	
	//["Deliverypoint failed?"] remoteExec ["systemchat"];
};

// dfk_add_randomvipgear =
// {
	//["TODO Magic adding of 1337 VIP GEAR"] remoteexec ["hint"];
	// if (count vipstorearray > 0) then
	// {
	
		// {
			 // _x;
		// } foreach ;
	// };
// };


//general function to retrieve value from locationarray
//input "id" as in locationarray index
//		"index" is the nested index for the field to retrieve
dfk_getlocationarrayvalue =
{
	private _id = _this select 0;
	private _index = _this select 1;
	
	((locationarray select _id) select _index);
};

//general function to write value to locationarray
//id - locationarray index
//index - nested spot in the array, field to write to
//value - data to write
//this function sets/overwrites the value, pbb needs some error/exceptionhandling here...
dfk_writelocationarrayvalue =
{
	private _id = _this select 0;
	private _index = _this select 1;
	private _value = _this select 2;
	
	(locationarray select _id) set [_index,_value];
};

//function for spawning a drone at special/important sites
//drones serve as extra surveilance
//drones are also derp as fuck and potato around the whole fuken 'hÃ¤rad' ... pivo feature
//pos - is position
//id - is locationarray index to write group belonging to, not sure if this works 100% so disabled for now
//group returned, use "call" function, does not work with remoteexec for nowÂ§
dfk_spawn_guard_drone =
{
	private _pos = _this select 0;
	private _id = _this select 1;

	private _dron = [];
	//_dronpos = [_pos, 0, 100, 5, 0, 0.5, 0] call BIS_fnc_findSafePos;
	//_dron = [_dronpos, random 360, "O_UAV_01_F", EAST] call bis_fnc_spawnvehicle;
	//_veh = createVehicle ["ah1w", position player, [], 0, "FLY"];
	_dron = createvehicle ["O_UAV_01_F",_pos, [],200, "FLY"];
	createvehiclecrew _dron;
	private _drongrp = driver _dron;
	
	[_dron] execvm "dfk_dron.sqf";
	_drongrp = group driver _dron;
	_dron flyinheight (50 + random 100);
	
	_wp0 = _drongrp addwaypoint [_pos,200];
	for "_i" from 0 to 10 do
	{
		_wpx = _drongrp addwaypoint [_pos,200];
		_wpx setwaypointtype (selectrandom ["MOVE","SAD"]);
	};
	_wpend = _drongrp addwaypoint [waypointposition _wp0,0];
	_wpend setwaypointtype "CYCLE";
	if (debug) then {[_dron,"colorred","","hd_warning",true,1] execvm "marker_track.sqf"};
	
	// _tmp = [];
	// _tmp = [_id,6] call dfk_getlocationarrayvalue;
	// [format ["dfk_spawn_guard_drone 0 %1",_tmp]] remoteExec ["systemchat"];	
	// _tmp append [_drongrp];
	// [format ["dfk_spawn_guard_drone 0 %1",_tmp]] remoteExec ["systemchat"];	
	
	// [_id,6,_tmp] call dfk_writelocationarrayvalue;
	// [format ["dfk_spawn_guard_drone 0 %1",	[_id,6] call dfk_getlocationarrayvalue]] remoteExec ["systemchat"];	
	//[format ["dfk_spawn_guard_drone 0 %1",	_drongrp]] remoteExec ["systemchat"];	
	
	[_drongrp];
};

//function for spawning loot in towns
//use call, not remoteexec for now
//having trouble writing data to locationarray reliably
//depending on param campsize, combinatory explosion possible... default set to 3, so shouldn't be to much of an issue
//performance over imerssion I guess
dfk_spawn_town_loot =
{
	private _pos = _this select 0;
	private _n = _this select 1;
	private _spawn_junk = _this select 2;
	
	//add silly hats later on
	private _loottypesarray = ["intel","money","money","money","money","food","food"];
	private _dfk_spawn_town_loot_veharray = [];
	//if (debug) then {[format ["loot 0 %1 %2 %3 %4",[_pos,_n,_spawn_junk,_veharray]]] remoteExec ["diag_log"];};	
	
	for "_l" from 0 to (_n) do
	{
		//select loot
		//intel is rare, cash kind of rare, food common
		private _loottype = (selectrandom _loottypesarray);

		private _loot = "";
		
		switch(_loottype) do
		{
			case "food":
			{
				_loot = createVehicle [(selectrandom consumeablesarray),[_pos select 0,_pos select 1,(_pos select 2) + 1], [], 200, "NONE"];
				[_loot, ["Consume food", {_this execvm "dfk_consume_food.sqf"}]] remoteExec ["addAction",0,true];			
			};
			case "intel":
			{
				_loot = createVehicle [(selectrandom intelthingsarray), [_pos select 0,_pos select 1,(_pos select 2) + 1], [], 200, "NONE"];
				[_loot, ["Take intel", {_this execvm "dfk_consume_intel.sqf"}]] remoteExec ["addAction",0,true];			
			};
			case "money":
			{
				_loot = createVehicle ["Land_Money_F", [_pos select 0,_pos select 1,(_pos select 2) + 1], [], 200, "NONE"];
				[_loot, ["Take cash", {_this execvm "dfk_consume_funds.sqf"}]] remoteExec ["addAction",0,true];
			
			};
		};
		
		if (dfk_RobinMode == 1) then
		{
			_source01 = "#particlesource" createVehicle position _loot;
			_source01 setParticleClass "AirFireSparks";
			_source01 attachTo [_loot,[0,0,0]];			
			_dfk_spawn_town_loot_veharray append [_source01];
		};
		
		_dfk_spawn_town_loot_veharray append [_loot];
		
		private _b = nearestBuilding _loot;
		private _bpos = [_b, -1] call BIS_fnc_buildingPositions;
		//diag_log format ["townloot 0 %1 %2",_b,_bpos];
		
		private _var ="lel";
		private _p = [];
		
		//_broken = false;
		//_one = false;
		//_two = false;
		//_noHitzone1 = isClass(configfile >> "CfgVehicles" >> typeof _b >> "HitPoints" >> "Hitzone_1_hitpoint");
		//_noHitzone2 = isClass(configfile >> "CfgVehicles" >> typeof _b >> "HitPoints" >> "Hitzone_2_hitpoint");			
		//() OR ((_b gethitpointdamage "Hitzone_2_hitpoint") == 1)
		//if (_noHitzone1) then {if ((_b gethitpointdamage "Hitzone_1_hitpoint") == 1) then {_one = true}};
		//if (_noHitzone2) then {if ((_b gethitpointdamage "Hitzone_2_hitpoint") == 1) then {_two = true}};
		
		if (isnull _b OR count _bpos == 0 OR (_b iskindof "Ruins_F") OR !(alive _b) OR count _Bpos <= 0 or _b getvariable ["taken",false]) then
		{
			_var = format ["wild %1", _loottype];
			//if (debug) then {[format ["spawnloot, dedbuilding? %1 %2 %3 %4 %5 %6 %7",_b,isnull _b,count _bpos,_b iskindof "Ruins_F",!alive _b,_b gethitpointdamage "Hitzone_1_hitpoint",_b gethitpointdamage "Hitzone_2_hitpoint"]] remoteExec ["systemchat"];};		
			//_p = (getpos _b) findEmptyPosition [5,15,typeof _loot];
			//_p = [] call BIS_fnc_findSafePos;
			
			_p = [getpos _b, 1, 100, 1, 0, 0.5, 0,10,"wild loot?"] call dfk_find_fuken_safe_spawnpoint;
			
			if (count _p >= 3) then
			{
				_loot setpos [_p select 0,_p select 1,(_p select 2) + 0.5];
				_b setvariable ["emptypositions",[],true];
				//diag_log format ["dfk_spawn_town_loot | wild loot | _p: %1 %2",_p,getpos _loot];
			}
			else
			{
				_loot setpos _pos;
			};
			
			//diag_log format ["townloot 0.1 %1 %2 %3",_b,_bpos,_b getvariable ["emptypositions","failz"]];	
			//diag_log "a";
			// if (count _p > 0) then 
			// {
				// if (_p distance _pos <= 200) then
				// {
					
				// };
			// };
		}
		else
		{
			_p = selectrandom (_bpos);
			_bpos deleteat (_bpos find _p);
			//diag_log format ["dfk_spawn_town_loot | bpos loot | _p: %1",_p];
			_loot setpos [(_p select 0) + (random 1) - (random 1),(_p select 1) + (random 1) - (random 1),(_p select 2) + 0.5];
			_var = format ["b %1", _loottype];
			_b setvariable ["emptypositions",_bpos,true];
			_b setvariable ["taken",true,true];
			_dfk_spawn_town_loot_veharray pushback _b;
			//diag_log format ["townloot 0.2 %1 %2 %3",_b,_bpos,_b getvariable ["emptypositions","failz"]];	
			//diag_log "b";
		};		

		private _fire = [];
		if (_spawn_junk) then {_fire = [_loot,_b] call dfk_spawn_burning_junk};
		_dfk_spawn_town_loot_veharray append _fire;
		//if (debug) then {[format ["loot 1 %1",_veharray]] remoteExec ["diag_log"];};		
		
		// _tmp = [];
		//_tmp = ((locationarray select _id) select 7);
		// _tmp = [_id,7] call dfk_getlocationarrayvalue;
		// _tmp append _veharray;
		
		//(locationarray select _id) set [7,_tmp];
		// [_id,7,_tmp] call dfk_writelocationarrayvalue;
		
		
		//if (debug) then {};
		//diag_log format ["%1 %2",_loot,_var];
		//[_loot,"colorgreen","townloot","hd_dot",true,1] execvm "marker_track.sqf";
	};
	//if (debug) then {[format ["loot 5 %1",_dfk_spawn_town_loot_veharray]] remoteExec ["diag_log"];};
	_dfk_spawn_town_loot_veharray;	
};

dfk_moraleStateMachine =
{
	private _stateMorale = "INIT";
	
	private _opforGroups = [];
	private _troubledOpforGroups = [];
	
	private _radioActionsGroups = [];
	private _ActionGroups = [];
	
	private _waitGroups = [];
	
	_sleepvar = 10;
	
	while {true} do
	{
		switch (_stateMorale) do
		{
			case default {diag_log "dfk_moraleStateMachine derped hard..."};
			
			case "INIT":
			{
				//diag_log "dfk_moraleStateMachine init";
				{
					if (faction leader _x in BADFACTION) then
					{
						_opforGroups pushbackunique _x;
					};
				} foreach allgroups;
				
				{
					private _countAlive = {alive _x} count units _x;
					
					if (_countAlive <= 0) then
					{
						_opforGroups deleteAt _foreachindex;
					};
					
					if ((_x getvariable ["support",false]) && morale leader _x > 0.8) then
					{
						_x setvariable ["support",false,true];
					};					
					
					if (_countAlive > 0) then
					{
						if (morale leader _x < 0.8) then
						{
							if (!(_x in _troubledOpforGroups)) then
							{
								if (!(_x getvariable ["support",false])) then
								{
									_troubledOpforGroups pushbackunique _x;
								};
							};
						};						
					};					
				} foreach _opforGroups;
				
				_stateMorale = "PLAN";
			};
			
			case "PLAN":
			{
				//diag_log "dfk_moraleStateMachine plan";
				{
					private _countAlive = {alive _x} count units _x;
					
					if (_countAlive <= 0 || _x getvariable ["support",false]) then
					{
						_troubledOpforGroups deleteAt _foreachindex;
					}
					else					
					{
						if (morale leader _x > 0.8) then
						{
							//diag_log format ["dfk_moraleStateMachine group %1 is not troubled",_x];
							_troubledOpforGroups deleteat _foreachindex;
						};
					};
				} foreach _troubledOpforGroups;	
			
				{						
					//debug lolz
					private _countAlive = {alive _x} count units _x;
					
					if (_countAlive > 0) then
					{
						private _minmorale = 9999;
						private _maxmorale = -9999;
						private _avgmorale = 0;
						private _leadermorale = 0;
						
						{
							_avgmorale = _avgmorale + morale _x;
							_minmorale = morale _x min _minmorale;
							_maxmorale = morale _x max _maxmorale;
						} foreach units _x;
						
						_avgmorale = _avgmorale / _countAlive;
						
						_leadermorale = morale leader _x;
						
						diag_log format ["dfk_moraleStateMachine debug moraleroll grp %1 leader %2 avg %3 min %4 max %5",_x,_leadermorale,_avgmorale,_minmorale,_maxmorale];														
					};

				
					private _targetarr0 = leader _x neartargets spawn_distance;
					private _targetarr = [];
					private _not_targets = [];
					private _leaderHasRadio = false;
					
					private _grp = _x;
					
					if ("ItemRadio" in (assignedItems (leader _grp))) then {_leaderHasRadio = true} else {diag_log format ["dfk_moraleStateMachine leader no can has radio! NO RADIO FOR YOO! %1",_grp]};
					
					{
						if (_x select 2 != side leader _grp && _x select 2 != civilian) then
						{
							_targetarr pushback _x;
						}
						else
						{
							_not_targets pushback _x;
						};
					} foreach _targetarr0;
					
					//only make one decision
					private _decided = false;
					diag_log format ["dfk_moraleStateMachine decide... %1 morale %2 radio? %3 targets %4",_grp,morale leader _grp,_leaderHasRadio,count _targetarr];
					if (count _targetarr >= 0 && (!_leaderHasRadio || _leaderHasRadio)) then
					{
						//actions that does not depend on having a radio
						if (!_decided && morale leader _grp < 0.15 && morale leader _grp > 0.4 && !(_grp getvariable ["dfk_aiBusy",false])) then
						{
							
							//retreat
							//... how to find a good retreatspot?
							//go hide in a building...
							
							private _buildings = [];
							
							_buildings = nearestObjects [leader _grp, ["House_F"], 200];
							if (count _buildings > 0) then
							{
								[_grp] spawn dfk_AiGroupTakeBuilding;
								_decided = true;
							}
							else
							{
								//find hill!
								_decided = true;
								
								[_grp] spawn dfk_aiGroupTakeHill;
							};
						};
						
						if (count _targetarr > 0 && !_decided && !captive leader _grp && (morale leader _grp < -1) && fleeing leader _grp) then
						{
							_decided = true;
							{
								if (!captive _x && (fleeing _x && morale _x < -1)) then
								{
									[_x] call dfk_ai_surrender;
								};
							} foreach units _grp;
						};						
					};
					
					if (count _targetarr > 0 && _leaderHasRadio && !_decided) then
					{
						diag_log format ["dfk_moraleStateMachine group %1 has radio, super actions...",_grp];
						//arty
						if (!_decided && morale leader _grp > -0.25 && morale leader _grp < 0.15) then
						{
							if (!(_grp getvariable ["qrf",false])) then
							{
								systemchat "arty";
								
								_decided = true;
								diag_log "dfk_moraleStateMachine arty call";
								[_grp,_targetarr,_not_targets] call dfk_aiPreCalArtyFireMission;
							}
							else
							{
								systemchat "qrf arty";
								
								_decided = true;
								diag_log "dfk_moraleStateMachine qrf arty call";
								{
									[_grp,[_x],_not_targets] call dfk_aiPreCalArtyFireMission;
								} foreach _targetarr;
							};
						};
						
						//qrf
						if (!_decided && morale leader _grp > -1 && morale leader _grp < -0.25 && !(_grp getvariable ["qrf",false])) then
						{
							systemchat "qrf";
							_decided = true;
							diag_log "dfk_moraleStateMachine qrf call";
							
							private _t = selectrandom _targetarr;
							[_t select 0] call DFK_request_support;
						};						
					};
				} foreach _troubledOpforGroups;
				
				_stateMorale = "INIT";
			};

			case "ACTION":
			{
				diag_log "dfk_moraleStateMachine action";
			};			

			case "WAIT":
			{
				diag_log "dfk_moraleStateMachine waIT";
			};
		};
		
		if (dfk_globalShortWaveRadioQuality >= 5) then {_waitvar = 30};
		if (dfk_globalShortWaveRadioQuality == 4) then {_waitvar = 60};
		if (dfk_globalShortWaveRadioQuality == 3) then {_waitvar = 120};
		if (dfk_globalShortWaveRadioQuality == 2) then {_waitvar = 180};
		if (dfk_globalShortWaveRadioQuality == 1) then {_waitvar = 240};
		if (dfk_globalShortWaveRadioQuality <= 0) then {_waitvar = 300};
		
		sleep 10;
	};
};

//function for looping over (enemy?) groups, get morale, if shit morale, call support
//needs doing: why is the morale crap? often it's because of unit losses, handles actual enemies reasonably well
//mines/arty... not so much, needs fixing eventually
dfk_morale_and_support_rollcall =
{
	{
		private _grp = _x;
		if ((_grp getvariable "support") && morale leader _grp > 0.8) then
		{
			_grp setvariable ["support",false,true];
		};
		
		private _targetarr0 = leader _grp neartargets viewdistance;
		private _targetarr = [];
		private _not_targets = [];
		{
			if (_x select 2 != side leader _grp && _x select 2 != civilian) then
			{
				_targetarr pushback _x;
			}
			else
			{
				_not_targets pushback _x;
			};
		} foreach _targetarr0;
		
		//0 perceived pos,1 type,2 perceived side,3 cost,4 actual game object,5 pos accuracy
	
		//&& place not full of mines...
		//leader _grp globalchat format ["halp0/1! %1 %2 %3 %4 %5",faction leader _grp in BADFACTION,alive leader _grp,!(_grp getvariable ["support",false]),count (leader _grp targets [false, 800, [resistance,sideEnemy]]),"ItemRadio" in (assignedItems (leader _grp))]; 
		if (faction leader _grp in BADFACTION && alive leader _grp && !(_grp getvariable ["support",false]) && (count _targetarr > 0) && "ItemRadio" in (assignedItems (leader _grp)) && (morale leader _grp < 0.45 && morale leader _grp > 0)) then
		{
			private _leaderdude = leader _grp;
			private _targets = [];
			private _artsafe = true;
			private _arttarget = Objnull;
			
			//cant hit moving targets with arty sonny boy!
			{
				if (speed (_x select 4) < 10) then
				{
					// _targets is now just an array of map positions
					(_x select 0) pushback _targets;
				};
			} foreach _targetarr;
			
			private _validtargets = [];
			
			//what does this stuff do again?
			if (count _targets > 0) then
			{
				{
					private _t = _x;
					
					{
						private _tQ = _x;
						//0 perceived pos,1 type,2 perceived side,3 cost,4 actual game object,5 pos accuracy
						_tQ_perceivedside = _tQ select 2;
						_tQ_perceivedtype = _tQ select 1;
						_tQ_perceivedpos = _tQ select 0;
						
						// ((_tQ select 4) distance _t) < (100 + random 100) &&
						// (_tQ select 2 == civilian OR _tQ select 2 == east)
						//check type - if say... bunker then fire artysafe
						//check side - if reistance or west, then artysafe
						//then if it's still civ or east check the distance, if far away enough it's artysafe
						//
						if (_tQ_perceivedside == (side (leader _grp)) or _tQ_perceivedside == civilian) then
						{
							if (_tQ_perceivedpos distance _t < (100 + random 100)) then
							{
								_artsafe = false;
							};
						};
					} foreach _not_targets;
				} foreach _targets;
				
				if (_artsafe) then 
				{
					_arttarget = selectrandom _targets;

					[_arttarget] remoteexec ["dfk_artyradar_addtarget",2];
					_grp setvariable ["support",true,true];							
				};					
			};
		};	
	
		if (faction leader _grp in BADFACTION && alive leader _grp && !(_grp getvariable ["support",false]) && "ItemRadio" in (assignedItems (leader _grp)) && morale leader _grp < 0) then
		{			
			//0 perceived pos,1 type,2 perceived side,3 cost,4 actual game object,5 pos accuracy
			if (count _targetarr > 0) then
			{
				private _t = selectrandom _targetarr;
				[_t select 0] call DFK_request_support;
				_grp setvariable ["support",true,true];
				
				{
					private _dude = _x;
					private _target = selectrandom _targetarr;
					_dude doSuppressiveFire (_target select 0);
					_dude suppressFor 100;
				} foreach units _grp;
				
				private _radiotrigger = createTrigger ["EmptyDetector", getpos leader _grp];
				_radiotrigger setTriggerArea [2000, 2000, 0, false];
				_radiotrigger setTriggerActivation ["EAST", "PRESENT", true];		

				{
					private _othergrp = _x;	
					{
						_arr = _x;
						private _target = _x select 5;
						if (_othergrp knowsabout _target < 1 && ("ItemRadio" in (assignedItems (leader _othergrp)))) then
						{							
								{
									_dude = _x;
									_dude doSuppressiveFire _arr select 0;
									_dude suppressFor 100;
									
								} foreach units _othergrp;
						};									
					} foreach _targetarr;

					//_debuggrplist = _debuggrplist + [_othergrp];
					//[format ["%1 reveal to grp %2",_x,_grp]] remoteExec ["systemchat"];
				} foreach list _radiotrigger;		
				deletevehicle _radiotrigger;
			}
			else
			{
				diag_log format ["teh fuk are you so scared of %1 %2 %3 %4 %5 %6 ?",leader _grp,_grp,typeof (vehicle (leader _grp)),units _grp,leader _grp targets []];
			};
		};
		
		if (faction leader _grp in BADFACTION && fleeing leader _grp) then
		{
			{
				_dude = _x;
				if (fleeing _dude) then
				{
					private _enemytargets = (_dude targets [false, 200, [resistance,sideEnemy]]);
					//diag_log format ["rollcall surrender? %1 %2 %3 %4",_dude,fleeing _dude,morale _dude,count _enemytargets];
					//_dude globalchat format ["%1 is afraid | %2",name _dude,_grp];
					
					if (fleeing _dude && (morale _dude <= -0.5) && count _enemytargets > 1 && !captive _dude) then 
					{
						[_dude] call dfk_ai_surrender;
					};
				};
			} foreach units _grp;	
		};
	} foreach allgroups; 
};

//function for spawning ambulating cars, adding immersion and randomness etc.
dfk_spawn_car =
{
	private _pos = _this select 0;
	private _pos = [_pos select 0,_pos select 1,0];
	private _referencearray = _this select 1;
	
	private _car = "";
	private _grp = "";
	private _arr = "";
	
	private _debugstr = "";
	
	//private ["_car","_grp","_arr","_pos"];
	
	private _dispersion = 0;
	
	private _faction = random 100;
	private _factioni = 0;
	
	//25% civ, 50% opfor, 25% cops
	if (_faction > 0 && _faction < 33) then {_factioni = 0};
	if (_faction > 33 && _faction < 66) then {_factioni = 1};
	if (_faction > 66 && _faction < 100) then {_factioni = 2};
	
	
	
	//_type = (selectrandom (selectrandom roamingcar_nestled));
	private _type = selectrandom (roamingcar_nestled select _factioni);
	private _direction = random 360;
	
	private _p_array = _pos nearroads 100;
	private _p = [];
	if (count _p_array > 0) then
	{
		_p = selectrandom _p_array;
		private _r2 = roadsConnectedTo _p;
		_direction = [_p, _r2 select 0] call BIS_fnc_DirTo;		
		_debugstr = "onroad";
	}
	else
	{
		//_p = [_pos, 0, 100, 12, 0, 0.5, 0] call BIS_fnc_findSafePos;
		//_safepos = [(locationposition _x), 0, 175, 12, 0, 0.2, 0,10] call dfk_find_fuken_safe_spawnpoint;
		_p = [_pos, 0, 100, 12, 0, 0.5, 0,10,"spawn_car"] call dfk_find_fuken_safe_spawnpoint;
		if (count _p <= 0) then {_p = _pos;_dispersion = 100} else {_dispersion = 0;};
		
		_direction = random 360;
		_debugstr = "WILD";
	};
	
	if (isnil "_direction") then
	{
		_direction = random 360;
	};
	
	private _car = createVehicle [_type, _P, [], _dispersion, "NONE"];
	//_car setpos _p;
	if (_car distance _pos > 300) then {diag_log format ["did this car spawn off? init %1 _p %2 getpos %3",_pos,_p,getpos _car]};
	
	private _crewtype = getText(configfile >> "CfgVehicles" >> _type >> "crew");
	private _sidenumber = getnumber(configfile >> "CfgVehicles" >> _crewtype >> "side");
	private _sidestr = "";
	
	if (_sidenumber == 2) then {_sidestr = resistance};
	if (_sidenumber == 3) then {_sidestr = civilian};
	if (_sidenumber == 0) then {_sidestr = east};
	if (_sidenumber == 1) then {_sidestr = west};

	//"Van_02_medevac_base_F"
	[_car] remoteExec ["dfk_initvehicle",2];
	
	//diag_log format ["dfk_spawn_car %1 %2 %3 %4 %5",_faction,_factioni,_type,_car,_pos];
	//systemchat format ["dfk_spawn_car %1 %2 %3 %4 %5",_faction,_factioni,_type,_car,_pos];
	
	//special case for cops since they are technically on the west side
	//roamingcar_cop = ["C_Offroad_01_F","O_LSV_02_unarmed_F","C_Offroad_02_unarmed_F"];
	if (_type in roamingcar_cop) then
	{
		[_car] call dfk_coperize_beam;
		if (_type == "O_G_Offroad_01_F") then
		{		
			private _grp = creategroup east;
			private _id = ["cop"] call dfk_generate_radioid;
			_grp setGroupIdGlobal [_id];
			
			private _d1 = "";
			private _d2 = "";
			private _d3 = "";
			private _d4 = "";
			
			_d1 = [_d1,_grp,position _car,100,"cop"] call dfk_spawn_soldier;
			_d2 = [_d2,_grp,position _car,100,"cop"] call dfk_spawn_soldier;
			_d3 = [_d3,_grp,position _car,100,"cop"] call dfk_spawn_soldier;
			_d4 = [_d4,_grp,position _car,100,"cop"] call dfk_spawn_soldier;
			if (logistics >= 1200) then
			{
				private _d5 = "";
				private _d6 = "";
				_d5 = [_d5,_grp,position _car,100,"cop"] call dfk_spawn_soldier;
				_d6 = [_d6,_grp,position _car,100,"cop"] call dfk_spawn_soldier;	
				_d5 moveincargo _car;
				_d6 moveincargo _car;
			};
			
			_d1 moveindriver _car;
			_d2 moveincargo _car;
			_d3 moveincargo _car;
			_d4 moveincargo _car;
			
			//AI speed limit?! what a time to be alive!
			_car limitspeed 70;
		};
		
		if (_type == "C_Offroad_02_unarmed_F") then
		{		
			private _grp = creategroup east;
			private _id = ["cop"] call dfk_generate_radioid;
			_grp setGroupIdGlobal [_id];
			
			private _d1 = "";
			private _d2 = "";
			private _d3 = "";
			private _d4 = "";
			
			_d1 = [_d1,_grp,position _car,100,"cop"] call dfk_spawn_soldier;
			_d2 = [_d2,_grp,position _car,100,"cop"] call dfk_spawn_soldier;
			_d3 = [_d3,_grp,position _car,100,"cop"] call dfk_spawn_soldier;
			_d4 = [_d4,_grp,position _car,100,"cop"] call dfk_spawn_soldier;

			_d1 moveindriver _car;
			_d2 moveincargo _car;
			_d3 moveincargo _car;
			_d4 moveincargo _car;
			
			//AI speed limit?! what a time to be alive!
			//_car limitspeed 80;
		};		
		
		if (_type == "O_LSV_02_unarmed_F") then
		{	
			private _grp = creategroup east;
			private _id = ["cop"] call dfk_generate_radioid;
			_grp setGroupIdGlobal [_id];
			private _d1 = "";
			private _d2 = "";
			private _d3 = "";
			private _d4 = "";
			
			_d1 = [_d1,_grp,position _car,100,"cop"] call dfk_spawn_soldier;
			_d2 = [_d2,_grp,position _car,100,"cop"] call dfk_spawn_soldier;
			_d3 = [_d3,_grp,position _car,100,"cop"] call dfk_spawn_soldier;
			_d4 = [_d4,_grp,position _car,100,"cop"] call dfk_spawn_soldier;
			
			if (logistics >= 1200) then
			{
				private _d5 = "";
				private _d6 = "";
				private _d7 = "";
				_d5 = [_d5,_grp,position _car,100,"cop"] call dfk_spawn_soldier;
				_d6 = [_d6,_grp,position _car,100,"cop"] call dfk_spawn_soldier;	
				_d7 = [_d7,_grp,position _car,100,"cop"] call dfk_spawn_soldier;	
				_d5 moveincargo _car;
				_d6 moveincargo _car;
				_d7 moveincargo _car;
			};	

			_d1 moveindriver _car;
			_d2 moveincargo _car;
			_d3 moveincargo _car;
			_d4 moveincargo _car;
			
			//AI speed limit?! what a time to be alive!
			_car limitspeed 80;		
		};				
	};
	
	if (faction _car in BADFACTION && !(typeof _car in roamingcar_cop)) then
	{
		if (_type == "O_UGV_01_rcws_F") then
		{
			createvehiclecrew _car;
			_grp = driver _car;
			private _id = ["patrol"] call dfk_generate_radioid;
			_grp setGroupIdGlobal [_id];			
		}
		else
		{
			private _grp = creategroup east;
			private _id = ["patrol"] call dfk_generate_radioid;
			_grp setGroupIdGlobal [_id];
			
			if (_car emptypositions "Driver" > 0) then
			{
				private _driver = ObjNull;
				_driver = [_driver,_grp,position _car,100,"driver"] call dfk_spawn_soldier;
				_driver moveindriver _car;					
				_driver assignasdriver _car;			
			};
			
			if (_car emptypositions "Gunner" > 0) then
			{
				private _gunner = ObjNull;
				_gunner = [_gunner,_grp,position _car,100,"crew"] call dfk_spawn_soldier;
				_gunner moveingunner _car;					
				_gunner assignasgunner _car;				
			};

			if (_car emptypositions "Commander" > 0) then
			{
				private _commander = ObjNull;
				_commander = [_commander,_grp,position _car,100,"crew"] call dfk_spawn_soldier;
				_commander moveincommander _car;					
				_commander assignascommander _car;			
			};				
			
			for "_x" from 0 to random (_car emptypositions "cargo") do
			{
				private _passenger = "";
				if (1 > random 20) then
				{
					_passenger = [_passenger,_grp,position _car,100,"boss"] call dfk_spawn_soldier;
					_passenger moveincargo _car;			
				}
				else
				{
					private _type = selectrandom ["bunker","soldier","driver"];
					_passenger = [_passenger,_grp,position _car,100,_type] call dfk_spawn_soldier;
					_passenger moveincargo _car;
				};
				//[_x,_type] remoteExec ["dfk_equip_soldier",2];
			};	
		
			{
				_x addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,"KILLEDEAST"] execVM "killer.sqf";}];
				//if (debug) then {[_x,"colorred",_debugstr,"hd_dot",true,1] execvm "marker_track.sqf";};
			} foreach units _grp;
			[leader _grp] call dfk_equip_sqdld;
			[driver _car,"driver"] remoteExec ["dfk_equip_soldier",2];
		};
	};
	
	if (faction _car in CIVFACTION && !(typeof _car in roamingcar_cop)) then
	{
		createvehiclecrew _car;
		private _grp = group driver _car;
		
		{
			private _id = ["civ"] call dfk_generate_radioid;
			_grp setGroupIdGlobal [_id];
			_x addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,""] execVM "killer.sqf";}];
			//if (debug) then {[_x,"colorblue",_debugstr,"hd_dot",true,1] execvm "marker_track.sqf";};
		} foreach units _grp;
		
		if (faction _car == "CIV_IDAP_F") then
		{
			private _id = ["idap"] call dfk_generate_radioid;
			_grp setGroupIdGlobal [_id];
			_car forceflagtexture "\A3\Data_F_Orange\Flags\flag_IDAP_CO.paa";
		};
	};		
	
	_car setdir _direction;
	
	private _grp = group driver _car;
	_grp addvehicle _car;
	_grp setvariable ["patrol",true,true];
	_car setUnloadInCombat [true, false];		
	
	_grp setvariable ["spawn_car_lastpos",getpos _car,true];
	
	_arr = [_car,_grp];
	//if (debug) then {[format ["00 pos %1 count p_arr %2 _p %3 disp %4 type %5 car %6 crewtype %7 siden %8 sidestr %9 grp %10",_pos,count _p_array,_p,_dispersion,_type,_car,_crewtype,_sidenumber,_sidestr,_grp]] remoteExec ["systemchat"];};
	
	_arr;
};

//supportfunction to 'dfk_spawn_car' 
//counts alive/ambulating/somewhat functioning cars
//returns alive cars
//use call
dfk_count_alive_cars =
{
	private _arr = _this select 0;
	private _alive = 0;
	
	//diag_log format ["dfk_count_alive_cars0 %1 %2",_arr,_alive];

	
	{
		private _car = _x select 0;
		private _grp = _x select 1;
		
		if (_car iskindof "Car") then
		{
			_far = 99999;
			{
				if (_car distance _x < _far) then {_far = _car distance _x};
			} foreach allplayers;
			
			if (_far > 4000) then {_car setvariable ["move_strike",true,true];};
		};
		
		//if the car is stuck or for some ARMA/pivocode/pivo-AI reason doesn't do their GOD DAMN JOB (ie move)
		private _oldpos = _grp getvariable ["spawn_car_lastpos",getpos _car];
		
		
		if (_car getvariable ["move_strike",false]) then
		{
		
			private _seen = false;
			{
				if (_car distance _x < viewdistance && !_seen) then
				{
					if (lineIntersects [ eyePos _x, aimPos _car, _x, _car]) then
					{
						_seen = true;
					};
				};
			} foreach allplayers;		
		
			if (!_seen) then
			{
				{
					{
						deletevehicle _x;
					} foreach attachedobjects _x;
					deletevehicle _x;
				} foreach crew _car;
				
				//_car setpos [0,0,1000];
				[_car] call dfk_deletevehicle;
				
				_arr deleteat _foreachindex;
			};
		}
		else
		{	
			private _targetArr = leader driver _car neartargets 500;
			private _targetArrObjects = [];
			{
				_targetArrObjects pushback (_x select 4);
			} foreach _targetArr;		
		
			if (_oldpos distance getpos _car < 100 && speed _car <= 0 && leader driver _car countenemy _targetArrObjects <= 0) then
			{
				(driver _car) domove (getwppos [_grp,currentwaypoint _grp]);
				_car setvariable ["move_strike",true,true];
				//diag_log format ["dfk_count_alive_cars0 | %1",_grp];
			}
			else
			{
				_grp setvariable ["spawn_car_lastpos",getpos _car,true];
			};
		
			if (!alive _car OR !canmove _car OR (fuel _car <= 0) OR !(alive driver _car) or (count crew _car <= 0)) then
			{
				//diag_log format ["dfk_count_alive_cars1 delete | %1 %2",_grp,_car];
				[_grp,getpos _car] spawn dfk_removegroup;
				[_car] call dfk_deletevehicle;
				_arr deleteat _foreachindex;
			}
			else
			{
				_alive = _alive + 1;
			};
		};
	} foreach _arr;
	
	_alive;
};

//loot beacons
//can also be used to spawn random camping gear crap later on...
dfk_spawn_burning_junk = 
{
	private _thingie = _this select 0;
	private _das_building = _this select 1;
	//[format ["burningjunk 0 %1 %2",_thingie,_das_building]] remoteExec ["systemchat"]; 
	
	private _burningthing = selectrandom dfk_campfires;
	private _p = [];
	
	private _burningjunk = "";
	
	private _arr = [];
	private _arr2 = [_burningthing];
	
	if (campsize > 0) then
	{
		for "_x" from 1 to campsize do
		{
			_arr2 pushback (selectrandom campsitejunkarray);
		};
	};
	
	private _building_pos_array = _das_building getvariable ["emptypositions",[]];
	//[format ["burningjunk 1 %1 %2",_building_pos_array,([_das_building, -1] call BIS_fnc_buildingPositions)]] remoteExec ["systemchat"]; 
	//isnull _b OR count _bpos == 0 OR (_b iskindof "Ruins_F") OR !(alive _b)
	//if (isnull _das_building or !alive _das_building or _das_building iskindof "Ruins_F") then
	if (count _building_pos_array <= 0) then
	{
		_burningjunk = createVehicle [_burningthing, getpos _thingie, [], 10, "NONE"];
		_arr pushback _burningjunk;
		if (debug) then {[_burningjunk,"colorred","burningjunk wild","hd_dot",true,1] execvm "marker_track.sqf";};
		
		// for "_i" from 1 to campsize do
		// {
		{
			//[getpos _thingie select 0,getpos _thingie select 1,0]
			_p = [getpos _thingie, 1, 12, 1, 0, 0.5, 0] call BIS_fnc_findSafePos;
			_campjunk = createVehicle [selectrandom campsitejunkarray, [0,0,0], [], 10, "NONE"];
			_campjunk setpos _p;
			_campjunk setdir random 360;
			_campjunk enableSimulationGlobal true;
			_arr pushback _campjunk;
			if (debug) then {[_campjunk,"colorred","junk wild","hd_dot",true,1] execvm "marker_track.sqf";};			
		} foreach _arr2;
		//};
	}
	else
	{
		//_hpos = [_das_building, -1] call BIS_fnc_buildingPositions;
		{
			private _das_thingie = _x;
			private _burningjunk = "";
			//diag_log _das_thingie;
			
			private _p = [];
			//if (count _hpos > 0) then
			if (count _building_pos_array > 0) then
			{
				//_i = floor (random ((count _hpos) - 1));
				private _i = floor (random ((count _building_pos_array) - 1));
				_p = (_building_pos_array select _i);// findEmptyPosition [0,1,_x];
				_building_pos_array deleteat _i;
			};
			
			if (count _p > 0) then
			{	
				_burningjunk = createVehicle [_das_thingie, [0,0,0], [], 100, "NONE"];		
				_burningjunk setpos _p;
				if (debug) then {[_burningjunk,"colorblue","junk b","hd_dot",true,1] execvm "marker_track.sqf";};	
			}
			else
			{
				_p = [getpos _thingie, 1, 12, 1, 0, 0.5, 0] call BIS_fnc_findSafePos;
				_burningjunk = createVehicle [_das_thingie, _p , [], 0, "NONE"];
				if (debug) then {[_burningjunk,"coloryellow","junk bw","hd_dot",true,1] execvm "marker_track.sqf";};	
			};
			
			_burningjunk setdir random 360;
			_burningjunk enableSimulationGlobal true;
			_arr pushback _burningjunk;
		} foreach _arr2;
	};		
	
	_das_building setvariable ["emptypositions",_building_pos_array];	

	//[format ["burningjunk 2 %1 %2 %3 %4 %5",_thingie,_das_building,_arr,_das_building getvariable ["emptypositions","fail"],_burningjunk]] remoteExec ["systemchat"]; 
	
	//stop-gap measure/spaggeticode/fulhack2000 for removing shit that spawns off, as in the ocean off, not a problem per se, but doesn't get removed and then pbb impacts performance after a couple of hours
	// {
		// if (_x distance _thingie > 100) then 
		// {
			// diag_log format ["derp derp dfk_spawn_burning_junk spawning off? %1 %2 deleteting, isbackpack? %3",_x,_x distance _thingie,_x iskindof "Bag_Base"];
			// if (_x iskindof "Bag_Base") then {removeBackpackGlobal _x;deletevehicle _x} else {deletevehicle _x;};
			// diag_log _x;
		// };
	// } foreach _arr;
	_arr;
};

//supportfunction to 'dfk_supplyconvoy_delivery'
//determines appropriate guard/escport vehicle depending on logistical availability
//returns vehicle, use call
dfk_apropriate_escort = 
{
	private _escortkind = "";

	if (logistics > 1000) then {_escortkind = selectrandom ["O_MRAP_02_gmg_F","O_APC_Wheeled_02_rcws_F"]};
	if (logistics > 600 && logistics <= 1000) then {_escortkind = selectrandom ["O_MRAP_02_hmg_F"]};
	if (logistics > 300 && logistics <= 600) then {_escortkind = selectrandom ["O_MRAP_02_F","I_C_Offroad_02_LMG_F"]};
	if (logistics < 300) then {_escortkind = selectrandom ["I_C_Offroad_02_LMG_F"]};
	
	_escortkind;
};

//function for "delivering" supplies to a town/site
//executed from waypoint statements
//array of targets stored in group variables, when activated it resets site troopcount
dfk_supplyconvoy_delivery =
{
	private _grp = _this select 0;
	
	private _sca = [];
	_sca = _grp getvariable "supplyconvoyarray";
	diag_log format ["_sca %1",_sca];
	
	private _locationarrayindex = _sca select 0;
	
	if (sentsupply) then
	{
		private _a = locationarray select _locationarrayindex;
		private _init_troops = _a select 12;
		(locationarray select _locationarrayindex) set [5,_init_troops];
	};
	// _checkpointvaule = _checkpoint getvariable "damp";
	// _checkpointvaule = _checkpointvaule + 1;
	// _checkpoint setvariable ["damp",_checkpointvaule,true];
	
	//replenish troops
	
	_sca deleteat 0;
	_grp setvariable ["supplyconvoyarray",_sca,true];
};

//inits ammoboxes at safehouses
//clears cargo, inits arsenal script
//adds them to globalarray of ammoboxes
dfk_initammobox = 
{
	private _ammobox = _this select 0;
	
	clearBackpackCargoGlobal _ammobox;
	clearItemCargoGlobal _ammobox;
	clearMagazineCargoGlobal _ammobox;
	clearWeaponCargoGlobal _ammobox;
	0 = ["AmmoboxInit",[_ammobox,false,true,"Select equipment",false]] spawn xla_fnc_arsenal;
	
	ammoboxarray pushback _ammobox;
};

//updates all ammoboxes arsenalscript with new content
dfk_blip_ammoboxes = 
{
	private _riflearray = _this select 0;
	private _itemarray = _this select 1;
	private _magarray = _this select 2;
	private _backpackarray = _this select 3;
	
	//diag_log ammoboxarray;
	{	
		[_x,_riflearray,true] call xla_fnc_addVirtualWeaponCargo;
		[_x,_itemarray,true] call xla_fnc_addVirtualItemCargo;
		[_x,_magarray,true] call xla_fnc_addVirtualMagazineCargo;
		[_x,_backpackarray,true] call xla_fnc_addVirtualBackpackCargo;	
	} foreach ammoboxarray;
};


dfk_spawn_mortar_truck =
{
	private _pos = _this select 0;
	
	//mort2 attachto [dampfan2,[0,-3,-0.1]];crejt2 attachto [dampfan2,[0,+0.3,0]]; group this setvariable ["mortar",mort2,true];group this setvariable ["truck",this,true]
	//mort attachto [dampfan,[0,-4.2,0.3]];crejt attachto [dampfan,[0,-1.2,0.3]]; group this setvariable ["mortar",mort,true];group this setvariable ["truck",this,true]
	
	private _r = selectrandom dfk_mortartruck;
	
	private _truck = createVehicle [(_r select 0), _pos, [], 0, "NONE"];
	private _mortar = createVehicle [dfk_mortar, _pos, [], 0, "NONE"];
	private _crate = createVehicle [dfk_mortarcrate, _pos, [], 0, "NONE"];
	
	_mortar attachto [_truck,(_r select 1)];
	_crate attachto [_truck,(_r select 2)];
	
	private _grp = creategroup east;
	_grp setvariable ["truck",_truck,true];
	_grp setvariable ["mortar",_mortar,true];
	_grp setvariable ["art_gunner",_mortar,true];
	
	private _id = ["mortar"] call dfk_generate_radioid;
	_grp setGroupIdGlobal [_id];			
	
	private _leader = _grp createUnit [dfk_opfor_baseclass, _pos, [], 0, "NONE"];
	private _gunner = _grp createUnit [dfk_opfor_baseclass, _pos, [], 0, "NONE"];
	private _truckdriver = _grp createUnit [dfk_opfor_baseclass, _pos, [], 0, "NONE"];
	private _mortargunner = _grp createUnit [dfk_opfor_baseclass, _pos, [], 0, "NONE"];
	
	_grp selectleader _leader;
	_mortargunner moveingunner _mortar;
	_truckdriver moveindriver _truck;
	
	{[_x] call dfk_set_skill;} foreach units _grp;
	
	private _id1 = 0;
	private _id2 = 0;
	
	if (typeof _truck == "O_Truck_02_transport_F") then {_id1 = 15; _id2 = 14};
	if (typeof _truck == "O_Truck_03_transport_F") then {_id1 = 5; _id2 = 11};
	
	_leader moveincargo [_truck,_id1];
	_leader assignascargoindex [_truck,_id1];
	_gunner moveincargo [_truck,_id2];
	_gunner assignascargoindex [_truck,_id2];
	
	[_truck] call dfk_initvehicle;
	
	_truck setUnloadInCombat [true, false];
	artilleryarray pushbackunique _grp;
};

dfk_artfunc_mortar =
{
	private _target = _this select 0;
	private _grp = _this select 1;
	private _ref = _this select 2;
	private _ref_r = _this select 3;
	private _rounds = _this select 4;
	
	diag_log format ["dfk_artfunc_mortar %1 %2 %3 %4",_target,_grp,_ref,_ref_r];
	
	if (unitready leader _grp) then
	{

		sleep 1;
		
		private _mortar = _grp getvariable ["mortar",objnull];
		private _truck = _grp getvariable ["truck",objnull];
		_truck setUnloadInCombat [false, false];

		if ((alive _mortar && alive _truck) or (_mortar iskindof "StaticWeapon" && _truck iskindof "AllVehicles")) then
		{
			if (dfk_hackedEnemyRadar >= 1) then 
			{
				[
					"mortar",
					[
						_target,
						(_mortar getArtilleryETA [_target,(getArtilleryAmmo [_mortar]) select 0])
					]
				] spawn dfk_radio_snooper
			};
			
			private _time = (_mortar getArtilleryETA [_target, (getArtilleryAmmo [_mortar]) select 0]);
			
			_mortar doArtilleryFire [_target, ((getArtilleryAmmo [_mortar]) select 0), _rounds];
			
			diag_log format ["dfk_artfunc_mortar1 %1 %2 %3 %4 %5",_target,_grp,_mortar,_truck,_time];

			waituntil {unitready _mortar};
			
			//private _pos = [_ref, 500, _ref_r, 10, 0, 0.5, 0] call BIS_fnc_findSafePos;
			_grp addwaypoint [_ref,_ref_r];
			_truck domove _ref;
			
			//diag_log format ["dfk_artfunc_mortar2 %1 %2 %3 %4",_pos];

			waituntil {unitready _truck};
			
			diag_log "artytruck done";
		
			_truck setUnloadInCombat [true, false];	
		};
	};
};

dfk_artfunc_art =
{
	private _target = _this select 0;
	private _grp = _this select 1;
	private _ref = _this select 2;
	private _ref_r = _this select 3;
	private _rounds = _this select 4;
//	_alive = {alive _x} count units _grp;
	private _alive = 0;
	
	{
		if (alive _x) then {_alive = _alive + 1};
	} foreach units _grp;

	if (unitready leader _grp && _alive >= 3) then
	{
		if (dfk_hackedEnemyRadar >= 1) then 
		{
			[
				"artillery",
				[
					_target,
					((vehicle leader _grp) getArtilleryETA [_target,(getArtilleryAmmo [vehicle leader _grp]) select 0])
				]
			] spawn dfk_radio_snooper
		};	
	
		diag_log format ["dfk_artfunc_art %1 %2 %3 %4 %5 %6 %7",_target,_grp,_rounds,vehicle leader _grp,(getArtilleryAmmo [vehicle leader _grp]) select 0,((vehicle leader _grp) getArtilleryETA [_target,(getArtilleryAmmo [vehicle leader _grp]) select 0])];
		
		sleep 1;

		(vehicle (leader _grp)) doArtilleryFire [_target, (getArtilleryAmmo [vehicle leader _grp]) select 0, _rounds];

		waituntil {unitready (gunner (vehicle (leader _grp)))};
		
		//private _pos = [_ref, 500, _ref_r, 10, 0, 0.5, 0] call BIS_fnc_findSafePos;
		_grp addwaypoint [_ref,_ref_r];

		waituntil {unitready (driver (vehicle (leader _grp)))};
	
		true;
	};	
};

dfk_request_arty =
{
	private _targets = _this select 0;
	if (debug) then {[_targets,"coloryellow","trg","hd_destroy",1] remoteExec ["dfk_generate_marker",2]};	
	scopename "supportloop";
	private _supporttype = "init";
	private _blargh = "init";
	private _support = true;
	if (debug) then {["Support called"] remoteExec ["diag_log"];};
	private _base = base;
	private _kp_point = base_kp;
	private _base_h = base_h;
	private _debugstr = ["support debug "];
	
	private _qrf_spawnpoint = [];
	private _qrf_kp_point = [];
	private _base_no = -1;
	private _troops = 0;	
	
	{
		private _alivecrew = {alive _x} count units _x;
		private _veh = vehicle leader _x;
		
		if (!alive _veh OR _alivecrew < 3) then
		{
			//diag_log format ["dfk_request_arty unit dead, removing %1 %2",_veh,_x];
			artilleryarray deleteat _foreachindex;
		};
	} foreach artilleryarray;	

	while {_support && count artilleryarray > 0} do
	{
		switch (_supporttype) do
		{
			//[format ["sw: %1",_supporttype]] remoteExec ["systemchat"]; 
			_debugstr append [_supporttype];
			case "init":
			{
				//["Support init!"] remoteExec ["systemchat"]; 
				_supporttype = "arty";
				//check for blacklist
				if (sidemorale > 0 or sidemorale > (random 200)) then
				{
					{
						if (_targets InArea _x) then
						{
							//[format ["pos %1 is blacklisted",_targets]] remoteExec ["systemchat"]; 
							_debugstr append [format ["pos %1 is blacklisted",_targets]];
							_supporttype = "exit";
						};
					} foreach artblacklist;
				};				
				//[format ["init -1: %1",_supporttype]] remoteExec ["systemchat"]; 
				_debugstr append [ format ["init -1: %1",_supporttype]];
			};
			
			case "arty":
			{
				private _artfail = true;
				//get random alive, ready artyunit
				{
					//!!!!!!!!!!
					//is now a group, not unit!!
					//!!!!!!!!!!
					private _unit = _x;
					//!!!!!!!!!!
					private _artilleryunit = _unit getvariable "art_gunner";
					//diag_log format ["r 1979 _artilleryunit %1",_artilleryunit];
					if (
							unitready leader _unit &&
							(count (getArtilleryAmmo [_artilleryunit]) >= 1) &&
							_targets inRangeOfArtillery [[_artilleryunit], (getArtilleryAmmo [_artilleryunit]) select 0] &&
							(_artilleryunit getArtilleryETA [_targets, (getArtilleryAmmo [_artilleryunit]) select 0]) > 0 &&
							_artfail
						) then
					{
						//[format ["ART FIRE MISSION ACC %1 %2 %3 %4 %5 %6 %7 %8 %9 10",	_unit,_lasttime,_targets,alive _unit,unitready _unit,(count (getArtilleryAmmo [_unit]) >= 1),_targets inRangeOfArtillery [[_unit],(getArtilleryAmmo [_unit]) select 0],time < (_lasttime + _artcooldown),_artfail]] remoteExec ["systemchat"]; 
						//[format ["%1 %2 %3",_unit,_targets,(getArtilleryAmmo _x) select 0]] remoteExec ["systemchat"];
						_debugstr append [format ["ART FIRE MISSION ACC %1 %2 %3 %4 %5 %6 %7 %8 %9 10",	_unit,_targets,unitready leader _unit,(count (getArtilleryAmmo [_artilleryunit]) >= 1),_targets inRangeOfArtillery [[_artilleryunit],(getArtilleryAmmo [_artilleryunit]) select 0],_artfail]];
						//_debugstr append [format ["%1 %2 %3",_unit,_targets,(getArtilleryAmmo _x) select 0]];
						//_unit doArtilleryFire [_targets, (getArtilleryAmmo _x) select 0, 4];
						
						private _derp = _unit getvariable ["mortar",ObjNull];
						diag_log format ["dfk_request_arty %1 %2 %3 %4",_derp,_x,_unit,typeof _derp];
						diag_log format ["debug dfk_request_arty time %1",(_artilleryunit getArtilleryETA [_targets, (getArtilleryAmmo [_artilleryunit]) select 0])];
						
						if (_derp iskindof "StaticMortar") then
						{
							[_targets,_unit,getpos (leader _unit),400,4] spawn dfk_artfunc_mortar;
						}
						else
						{
							[_targets,_unit,getpos art_spawn,400,4] spawn dfk_artfunc_art;
						};
						//BIS_fnc_fireSupport use instead?
						//(artilleryarray select _foreachindex) set [1,time];
						//[format ["%1",artilleryarray select _foreachindex]] remoteExec ["systemchat"];
						_debugstr append [format ["%1",artilleryarray select _foreachindex]];
						_artfail = false;
						_supporttype = "exit";
					}
					else
					{
						//[format ["ART FAIL %1 %2 %3 %4 %5 %6 %7 %8 %9",_unit,_lasttime,alive _unit,unitready _unit, (count (getArtilleryAmmo [_unit]) >= 1),_targets inRangeOfArtillery [[_unit], (getArtilleryAmmo [_unit]) select 0],time < (_lasttime + _artcooldown),_artfail]] remoteExec ["systemchat"]; 
						if (_artfail) then {_debugstr append [format ["ART FAIL %1 %2 %3 %4 %5 %6 %7 %8 %9",_unit,unitready leader _unit, (count (getArtilleryAmmo [_artilleryunit]) >= 1),_targets inRangeOfArtillery [[_artilleryunit], (getArtilleryAmmo [_artilleryunit]) select 0],_artfail]]};
					};
				} foreach artilleryarray;
				
				if (_artfail) then
				{
					//["Sad trombone... Waaa waa waaaaaaa... no juice!"] remoteExec ["systemchat"]; 
					_debugstr append ["Sad trombone... Waaa waa waaaaaaa... no juice!"];
					_supporttype = "tryhkp";
				};
			};
			
			case "hkpstrike":
			{
				[_base_h,150,dfk_hkpstrike_typeclass,_targets,_kp_point] remoteExec ["dfk_qrf_hkpstrike",2];	
				_supporttype = "exit";
			};
			
			case "tryhkp":
			{
				if (logistics >= 1200 && time > dfk_qrf_hkp_timer + 86400/timeMultiplier) then {_supporttype = "hkpstrike"} else {_supporttype = "exit"};
			};
			
			case "exit":
			{
				//["Support exit"] remoteExec ["systemchat"]; 
				_support = false;
				if (debug) then
				{
					{
						if(debug) then {[format ["%1",_x]] remoteExec ["diag_log"];};
					} foreach _debugstr;
				};
			};			
		};
	};
};

dfk_qrf_hkpstrike = 
{
	//[base_h,150,[],getpos player,base_kp] remoteExec ["dfk_qrf_hkpstrike",2];
	dfk_qrf_hkp_timer = time;

	private _initpos = _this select 0;
	private _initradius = _this select 1;
	private _vehtypearray = _this select 2;
	private _targetpos = _this select 3;
	private _kp_point = _this select 4;

	//private _pos = [_initpos, 0, 100, 25, 0, 0.1, 0] call BIS_fnc_findSafePos;
	private _car = createVehicle [dfk_hkpstrike_typeclass,_initpos, [], 50, "FLY"];
	_car setpos _pos;
	createvehiclecrew _car;
	private _grp = group driver _car;
	_grp setvariable ["support",false,true];
	[_car] remoteExec ["dfk_initvehicle",2];	

	_grp addVehicle _car;
	
	_car setpylonloadout [1,dfk_hkpstrike_weaponkind];
	_car setpylonloadout [2,dfk_hkpstrike_weaponkind];
	_car setpylonloadout [3,dfk_hkpstrike_weaponkind];
	_car setpylonloadout [4,dfk_hkpstrike_weaponkind];	
	
	//fix later
	_car setdir 270;
	//[_grp,_pilotgrp,_car] remoteExec ["dfk_spawn_hkp_crew",2];			
	//[crew _car] joinsilent _grp;		
	
	_hkpwp1 = _grp addwaypoint [_targetpos,0];
	_hkpwp1 setwaypointtype "MOVE";
	_hkpwp1 setwaypointbehaviour "AWARE";
	_hkpwp1 setWaypointCompletionRadius 1000;
	
	_hkpwp2 = _grp addwaypoint [_targetpos,0];
	_hkpwp2 setwaypointtype "SAD";
	_hkpwp2 setwaypointcombatmode "RED";
	_hkpwp2 setwaypointbehaviour "COMBAT";
	_hkpwp2 setwaypointspeed "FULL";
					
	_hkpwp8 = _grp addwaypoint [_initpos,0];
	_hkpwp8 setwaypointtype "MOVE";		
	_hkpwp8 setwaypointbehaviour "AWARE";
	_hkpwp8 setwaypointstatements ["true","[group this,getpos this] remoteExec ['dfk_removegroup',2]"];
	
	["hkpstrike",[_targetpos]] spawn dfk_radio_snooper;
	
	// _support = false;
	// _supporttype = "exit";
};

//AI request support function art/reinforcements
//TODO: clean up debug trace-promts/debugtext
DFK_request_support =
{
	private _targets = _this select 0;
	if (debug) then {[_targets,"coloryellow","trg","hd_destroy",1] remoteExec ["dfk_generate_marker",2]};	
	scopename "supportloop";
	private _supporttype = "init";
	private _blargh = "init";
	private _support = true;
	if (debug) then {["Support called"] remoteExec ["systemchat"];};
	private _base = base;
	private _kp_point = base_kp;
	private _base_h = base_h;
	_debugstr = ["support debug "];
	
	private _qrf_spawnpoint = [];
	private _qrf_kp_point = [];
	private _base_no = -1;
	private _troops = 0;
	
	private _theygetsupport = false;
	
	private _islandblacklist = false;
	{
		if (_targets InArea _x) then
		{
			//[format ["pos %1 is an island...to expensive",_targets]] remoteExec ["systemchat"]; 
			_islandblacklist = true;
		};
	} foreach islandblacklist;		

	_support = false;
	{
		if (_x distance _targets <= 300) then
		{
			_support = true;
		};
	} foreach playableunits;
	
	_debugstr append [_supporttype];
	
	while {_support} do
	{
		switch (_supporttype) do
		{
			case "init":
			{
				//["Support init!"] remoteExec ["systemchat"]; 
				_supporttype = "noarty";
				//check for blacklist
				
				//[format ["init -1: %1",_supporttype]] remoteExec ["systemchat"]; 
				_debugstr append [ format ["init -1: %1",_supporttype]];
			};
			
			case "noarty":
			{
				switch (_blargh) do
				{
					case "init":
					{
						if (lastqrf < (time + qrfcooldown) && !_islandblacklist && ((count dfk_qrf_pool_array) < dfk_qrf_pool_limit)) then
						{			
						
							//&& place/targets not riddeled with mines...then call EOD!
						
							private _hkpvar = ["hkp1"];
							//this is qrf... so lets qrf!
							//if (logistics > 3000) then {_blargh = "4tank"};				
							if (logistics > 3000) then {
															if (random 1 > 0.5) then
															{
																_blargh = selectrandom _hkpvar;
															}
															else
															{
																_blargh = "ifv";
															};
														};				
														
							if (logistics > 1300 && logistics <= 3000) then {_blargh = "ifv"};
							if (logistics > 600 && logistics <= 1300) then {_blargh = "mrap"};
							if (logistics > 300 && logistics <= 600) then {_blargh = "truck"};
							if (logistics <= 300) then {_blargh = "softskins"};
							//[format ["%1",_blargh]] remoteExec ["systemchat"]; 
							_debugstr append [_blargh];
							lastqrf = time;
							publicvariable "lastqrf";
							
							//[format ["derp0 %1 %2 %3 %4",lastqrf,qrfcooldown,time,lastqrf <= time + qrfcooldown]] remoteExec ["systemchat"]; 
							private _damp = [];
							{
								private _pos = _x select 0;
								private _troops = _x select 5;
								private _spawned = _x select 3;							
								//[format ["? %1 %2 %3 %4 %5",(_x select 1),_pos,_spawned,_troops,_pos distance _targets]] remoteExec ["systemchat"]; 	
								private _island = false;
								{
									if (_pos inArea _x) then {_island = true};
								} foreach islandblacklist;
								
								if (((_pos distance _targets) < 3000 AND (_pos distance _targets) > 800) AND not(_spawned) AND (_troops >= 6) AND ((_x select 9) < random 1) && !_island) then
								{
									//[format ["valid qrf point found in %1 distance %2, troops %3 spawned? %4 ",(_x select 1), (_x select 0) distance _targets,(_x select 5),(_x select 3)]] remoteExec ["systemchat"]; 
									_damp append [[(_x select 0),_pos distance _targets,_forEachIndex]];
								};
							} foreach locationarray;
							
							if (count _damp >= 1) then
							{
								private _dist = 6000;
								private _s = [];
								{
									//[format ["damp %1 ",_x]] remoteExec ["systemchat"]; 
									if (_x select 1 < _dist) then
									{
										_dist = _x select 1;
										_s = _x select 0;
										private _base_no = (_x select 2);
										(locationarray select (_x select 2)) set [5,0];
									};
								} foreach _damp;
								//_loc = selectrandom _damp;
								_qrf_spawnpoint = _s;
								_qrf_kp_point = _s;
								//[format ["qrf spawnpoint %1",_qrf_spawnpoint]] remoteExec ["systemchat"]; 
							}
							else
							{
								//[format ["qrf base spawn"]] remoteExec ["systemchat"]; 
								_qrf_spawnpoint = base;
								_qrf_kp_point = base_kp;
							};
						}
						else
						{
							if (count dfk_qrf_pool_array >= dfk_qrf_pool_limit) then
							{
								_blargh = "recalc";
								_debugstr append ["recalc"];
							}
							else
							{
								_blargh = "exit";
								_debugstr append [format ["derp1 %1 %2 %3 %4",lastqrf,qrfcooldown,time,lastqrf <= time + qrfcooldown]];
								_debugstr append ["support request denied, still cooldown... or island"];								
							}
						};
						_qrf_spawnpoint set [2,0];
					};
					
					case "hkp1":
					{
							//[format ["%1",_blargh]] remoteExec ["systemchat"]; 
							[_base_h,150,dfk_qrf_helokind,_targets,_kp_point] remoteExec ["dfk_hkpbasedsupport",2];
							_theygetsupport = true;
						_blargh = "exit";
					};							
					
					//not used
					case "tank":
					{
						//[format ["%1",_blargh]] remoteExec ["systemchat"]; 
						[_qrf_spawnpoint,150,dfk_qrf_heavy,_targets,_qrf_kp_point,_base_no] remoteExec ["dfk_wheelbasedsupport",2];
						_blargh = "exit";
						_theygetsupport = true;
					};
					
					case "ifv":
					{
						//[format ["%1",_blargh]] remoteExec ["systemchat"]; 
						[_qrf_spawnpoint,150,dfk_qrf_medium,_targets,_qrf_kp_point,_base_no] remoteExec ["dfk_wheelbasedsupport",2];
						_blargh = "exit";
						_theygetsupport = true;
					};					

					case "mrap":
					{
						//[format ["%1",_blargh]] remoteExec ["systemchat"]; 
						[_qrf_spawnpoint,150,dfk_qrf_mrap,_targets,_qrf_kp_point,_base_no] remoteExec ["dfk_wheelbasedsupport",2];
						_blargh = "exit";
						_theygetsupport = true;
					};
					
					case "truck":
					{
						//[format ["%1",_blargh]] remoteExec ["systemchat"]; 
						[_qrf_spawnpoint,150,dfk_qrf_light,_targets,_qrf_kp_point,_base_no] remoteExec ["dfk_wheelbasedsupport",2];
						_blargh = "exit";
						_theygetsupport = true;
					};					

					case "softskins":
					{
						//[format ["%1",_blargh]] remoteExec ["systemchat"]; 

						[_qrf_spawnpoint,150,qrf_softskins,_targets,_qrf_kp_point,_base_no] remoteExec ["dfk_wheelbasedsupport",2];
						_blargh = "exit";
						_theygetsupport = true;
					};						
					
					case "recalc":
					{
						{
							if (!alive _x) then {[_x] call dfk_deletevehicle; dfk_qrf_pool_array deleteat _foreachindex};
						} foreach dfk_qrf_pool_array;
						
						_debugstr append [format ["recalc %1 %2 %3 %4",count dfk_qrf_pool_array,dfk_qrf_pool_array]];
						
						if (count dfk_qrf_pool_array >= dfk_qrf_pool_limit) then
						{
							_blargh = "exit";
							_debugstr append ["recalc fail, arr still full of lively troops"];
						}
						else
						{
							_blargh = "init";
							_debugstr append ["arr emptied, reinitializing..."];
						};
					};
					
					case "exit":
					{
						_wheelsupport = false;
						_supporttype = "exit";
					};
				};
				
				
			};
			
			case "exit":
			{
				//["Support exit"] remoteExec ["systemchat"]; 
				_support = false;
				//diag_log [format ["%1",_x]] remoteExec ["diag_log"];
				if (debug) then
				{
					{
						if(debug) then {[format ["%1",_x]] remoteExec ["diag_log"];};
					} foreach _debugstr;
				};
			};

		};
	};
	_theygetsupport;
};

//function for helicopterbased reinforcementsÂ§
dfk_hkpbasedsupport =
{
	private _initpos = _this select 0;
	private _initradius = _this select 1;
	private _vehtypearray = _this select 2;
	private _targetpos = _this select 3;
	private _kp_point = _this select 4;
	
	//_roads = _initpos nearRoads 150;
	
	private _veharray = [];
	
	private _qrfDudesArray = [];
	_derp = 0;
	while {count _qrfDudesArray < dfk_qrf_pool_limit} do
	{
		private _grp = creategroup east;
		_grp setvariable ["support",false,true];
		_grp setvariable ["qrf",true,true];
		private _pilotgrp = creategroup east;
		_pilotgrp setvariable ["support",false,true];
		_pilotgrp setvariable ["qrf",true,true];
		
		private _id = ["qrf"] call dfk_generate_radioid;
		_grp setGroupIdGlobal [_id];
		private _id = ["qrf"] call dfk_generate_radioid;
		_pilotgrp setGroupIdGlobal [_id];	
		
		private _pos = [_initpos, 0, 100, 25, 0, 0.1, 0] call BIS_fnc_findSafePos;
		private _car = createVehicle [selectrandom _vehtypearray,_initpos, [], 50, "FLY"];
		_car setpos _pos;
		[_car] remoteExec ["dfk_initvehicle",2];	

		_pilotgrp addVehicle _car;
		
		//fix later
		_car setdir 270;
		[_grp,_pilotgrp,_car] remoteExec ["dfk_spawn_hkp_crew",2];			
		_veharray = _veharray + [_car];
		//[crew _car] joinsilent _grp;
		
		{waituntil count crew _car > 2};
		sleep 1;
		_qrfDudesArray append crew _car;
		
		//dfk_qrf_pool_array append (units _grp);
		//dfk_qrf_pool_array append (units _pilotgrp);

		[units _grp] remoteexec ["dfk_add_qrfpool",2];
		[units _pilotgrp] remoteexec ["dfk_add_qrfpool",2];
		
		["airsupport",[_initpos,_targetpos,_vehtypearray]] spawn dfk_radio_snooper;
		
		[_grp,_pilotgrp,_veharray,_targetpos,_initpos,_kp_point] remoteExec ["dfk_hkp_support_wps",2];
		_derp = _derp + 1;
		sleep 1;
	};
};

//groundbased qrf
//max 6 vehicles
dfk_wheelbasedsupport =
{
	private _wbs_spawn = _this select 0;
	private _initradius = _this select 1;
	private _vehtypearray = _this select 2;
	private _targetpos = _this select 3;
	private _kp_point = _this select 4;
	private _base_no = _this select 5;

	private _roads = _wbs_spawn nearRoads 300;
	private _spawnpos = [];
	private _r = "";
	private _r2 = "";
	private _sprd = 0;
	private _direction = 0;	
	private _car = "";
	private _class = "";
	
	private _p1 = "";
	private _p2 = "";
	private _p3 = "";
	private _p4 = "";
	private _p5 = "";
	private _p6 = "";
	private _spawnposarray = [];
	
	diag_log format ["dfk_wheelbasedsupport0 | %1 %2 %3 %4 %5 %6",_wbs_spawn,_initradius,_vehtypearray,_targetpos,_kp_point,_base_no];
	
	["groundsupport",[_wbs_spawn,_targetpos,_vehtypearray]] spawn dfk_radio_snooper;
	
	
	private _roads2000 = [];
	_roads2000 = [_wbs_spawn] call dfk_town_spawnpoints_convoy;
	
	if (count _roads > 10 && count _roads2000 <= 0) then
	{
		// _r = selectrandom _roads;
		// _roads deleteat (_roads find _r);
		// _r2 = roadsConnectedTo _r;
		// _direction = _r getdir _r2 select 0;
		// _spawnpos = getpos _r;		
		// _sprd = 0;
		//[format ["roads %1 %2 %3 %4 %5 %6",_r,_r2,_direction,_spawnpos,_sprd,count _roads]] remoteExec ["systemchat"]
		
		_p1 = selectrandom _roads;
		_p2 = (roadsConnectedTo _p1) select 0;
		_p3 = (roadsConnectedTo _p2) select 0;
		_p4 = (roadsConnectedTo _p3) select 0;
		_p5 = (roadsConnectedTo _p4) select 0;			
		_p6 = (roadsConnectedTo _p5) select 0;
		_spawnposarray = [_p1,_p2,_p3,_p4,_p5,_p6];
	};	

	private _veharray = [];

	private _qrfDudesArray = [];
	_spawnpos = [];
	_derp = 0;
	
	while {count _qrfDudesArray < dfk_qrf_pool_limit} do
	{
		private _grp = creategroup east;
		_grp setvariable ["support",false,true];	
		_grp setvariable ["qrf",true,true];	
		
		private _id = ["qrf"] call dfk_generate_radioid;
		_grp setGroupIdGlobal [_id];
	
		if (count _roads2000 > 0) then
		{
			_spawnpos = (_roads2000 select _derp) select 0;
			_direction = (_roads2000 select _derp) select 1;
			_sprd = 0;			
		}
		else
		{
			if (count _spawnposarray > 0 && count _roads2000 <= 0) then
			{
				_spawnpos = _spawnposarray select _derp;
				_direction = _spawnpos getdir (_spawnposarray select (_derp + 1));
				_sprd = 0;
			}
			else
			{
				//_spawnpos = [_wbs_spawn, 0, 100, 12, 0, 0.2, 0] call BIS_fnc_findSafePos;
				//_safepos = [(locationposition _x), 0, 175, 12, 0, 0.2, 0,10] call dfk_find_fuken_safe_spawnpoint;
				_spawnpos = [_wbs_spawn, 0, 100, 12, 0, 0.2, 0,10,"wheelbasedsupport"] call dfk_find_fuken_safe_spawnpoint;
				//_spawnpos = _wbs_spawn;
				
				if (count _spawnpos <= 0) then {_spawnpos = _wbs_spawn;_sprd = 200; _direction = random 360};
				
				//_sprd = 50;
				//_direction = random 360;
				//[format ["no roads %1 %2 %3 %4",_spawnpos,_sprd,_direction,count _roads]] remoteExec ["systemchat"]
			};
		};
		
		//_pos = [_spawnpos, 0, 25, 12, 0, 20, 0] call BIS_fnc_findSafePos;
		private _car = createVehicle [selectrandom _vehtypearray,_spawnpos, [], _sprd, "NONE"];
		if (typeof _car in qrf_softskins) then {_car forceflagtexture "\A3\Data_F\Flags\Flag_CSAT_CO.paa"};
		
		if (debug) then {[_car,"colorred","qrf veh","hd_dot",true,1] execvm "marker_track.sqf";};
		
		[_car] remoteExec ["dfk_initvehicle",2];	

		_grp addVehicle _car;
		
		//fix later
		_car setdir _direction;
		
		[_grp,_car,"qrf"] remoteExec ["dfk_spawn_crew",2];
		_veharray pushbackunique _car;
		[crew _car] joinsilent _grp;		
		//[format ["dfk_wheelbasedsupport 2| %1 %2 %3 %4 %5 %6 %7 %8",_x,_class,_car,getpos _car,_spawnpos,count _roads,_wbs_spawn,_grp]] remoteExec ["systemchat"]; 
			//sleep 0.1;
		_grp setvariable ["returnid",_base_no,true];
		//[_grp] remoteExec ["dfk_set_qrf_leader",2];
		
		//_car forceFollowRoad true;
		//_car setConvoySeparation 25;
		[[_grp,_veharray,_targetpos,_wbs_spawn,_kp_point],dfk_qrf_support_wps] remoteExec ["spawn",2];	
		
		waituntil {count units _grp >= 2};
		sleep 1;
		
		_qrfDudesArray append units _grp;
		
		systemchat format ["%1 %2 %3",count _qrfDudesArray,dfk_qrf_pool_limit,_derp];
		_derp = _derp + 1;
	};
};

dfk_set_qrf_leader =
{
	private _grp = _this select 0;
	sleep 5;

	_grp selectleader (selectrandom (units _grp));
	[leader _grp] call dfk_equip_pltch;
	if (debug) then {[leader _grp,"coloryellow","qrf leader","hd_dot",true,1] execvm "marker_track.sqf";};
};

dfk_add_qrfpool =
{
	private _arr = _this select 0;
	

	//[format ["dfk_add_qrfpool %1 %2",_arr,dfk_qrf_pool_array]] remoteExec ["systemschat"];
	
	dfk_qrf_pool_array append _arr;
	
	//[format ["dfk_add_qrfpool %1",dfk_qrf_pool_array]] remoteExec ["systemschat"];
	//hint format ["derp %1\n\n%2",_arr,dfk_qrf_pool_array];
};

//groundbased qrf waypoint handling
dfk_qrf_support_wps =
{
	private _grp = _this select 0;
	private _cararray = _this select 1;
	private _targets = _this select 2;
	private _initpos = _this select 3;
	private _kp_point = _this select 4;
	private _base_no = _this select 5;

	diag_log format ["dfk_qrf_support_wps1| %1 %2 %3 %4",_grp,_cararray,_targets,_initpos];
	{
		_x setwppos getpos leader _grp;
	} foreach waypoints _grp;

	_wp00 = _grp addwaypoint [getpos leader _grp,0];
	_wp00 setWaypointCompletionRadius 100;
	_wp00 setwaypointtype "MOVE";
	_wp00 setwaypointbehaviour "AWARE";
	_wp00 setwaypointspeed "FULL";
	_wp00 setwaypointformation "COLUMN";
	
	_wp0 =_grp addWaypoint [_targets, 0];
	//_wp0 setwaypointbehaviour "SAFE";
	_wp0 setwaypointspeed "FULL";
	_wp0 setwaypointformation "COLUMN";
	_wp0 setWaypointCompletionRadius 200;
	
	_wp1 =_grp addWaypoint [_targets, 0];
	_wp1 setwaypointtype "UNLOAD";
	_wp1 setwaypointbehaviour "AWARE";
	_wp1 setWaypointCompletionRadius 150;
	
	_wp2 =_grp addWaypoint [_targets, 25];
	// _wp2 setwaypointtype "SAD";
	_wp2 setwaypointtype "MOVE";
	_wp2 setwaypointbehaviour "COMBAT";
	_wp2 setwaypointcombatmode "RED";
	_wp2 setwaypointformation "LINE";
	
	_wp3 =_grp addWaypoint [_targets, 100];
	_wp3 setwaypointtype "SAD";
	_wp3 setwaypointbehaviour "COMBAT";
	_wp3 setwaypointcombatmode "RED";
	_wp2 setwaypointformation "LINE";			
	
	_wp4 =_grp addWaypoint [_targets, 300];
	_wp4 setwaypointtype "SAD";
	_wp4 setwaypointbehaviour "COMBAT";
	_wp4 setwaypointcombatmode "RED";
	_wp4 setwaypointformation "LINE";
	
	_wp7 =_grp addWaypoint [([_targets, 0, 200, 12, 0, 0.3, 0] call BIS_fnc_findSafePos), 0];
	_wp7 setwaypointtype "MOVE";
	_wp7 setwaypointstatements ["true","[group this] remoteExec ['dfk_setlocation_troops',2];[group this,getpos this] remoteExec ['dfk_removegroup',2]"];
	//[format ["dfk_qrf_support_wps2| %1",waypoints _grp]] remoteExec ["systemchat"]; 
	
	sleep 5;
	
	private _alive = {alive _x} count units _grp;
	private _alive0 = _alive + 0;
	private _strike = 0;
	while {_alive > 0} do
	{
		_alive = {alive _x} count units _grp;
		
		{
			if (count crew _x > 0 && speed _x <= 5 && (getpos _x) distance (getwppos [_grp,currentwaypoint _grp]) < 100) then
			{
				diag_log format ["dfk_qrf_support_wps driver %1 %2 get a move on! %3",driver _x,_x,((getpos _x) distance (getwppos [_grp,currentwaypoint _grp]) < 100)];
				{
					_x domove (getwppos [_grp,currentwaypoint _grp]);
				} foreach units _grp;
				
				_strike = _strike + 1;
				//(driver _x) domove (getwppos [_grp,currentwaypoint _grp]);
			};
		} foreach _cararray;
		
		if (_strike >= 5) then
		{
			[_grp,currentwaypoint _grp] call dfk_removegroup;
		};
		
		sleep 30;
	};
	diag_log "dfk_qrf_support_wps END";
};

//this function is used to add/remove troops to sites/towns when they go on/return from qrf missions
//returnid is the index in the locationarray saved in the group as a variable
dfk_setlocation_troops =
{
	private _grpid = _this select 0;
	
	private _locationid = -1;
	_locationid = _grpid getvariable "returnid";
	//[format ["dfk_setlocation_troops0| %1 %2",_grpid,_locationid]] remoteExec ["diag_log"];
	private _t = 0;
	private _t2 = 0;
	
	_t = (locationarray select _locationid) select 5;
	
	private _alive = 0;
	_alive = ({alive _X} count units _grpid);
	_t2 = _t + _alive;
	
	(locationarray select _locationid) set [5,0];
	(locationarray select _locationid) set [5,_t2];
	//[format ["dfk_setlocation_troops1| %1 %2 %3 %4 %5 %6",_grpid,_locationid,_t,_t2,_alive,(locationarray select _locationid) select 5]] remoteExec ["systemchat"];
};

//locationarray select _base_no set [5,{alive _x} count units group this]

//function for handling helicopter qrf groups waypoints
//handles helicoptergroups waypoints and the grunts waypoints
dfk_hkp_support_wps =
{
	private _grp = _this select 0;
	private _hkp_grp = _this select 1;
	private _cararray = _this select 2;
	private _targets = _this select 3;
	private _initpos = _this select 4;
	private _kp_point = _this select 5;
	
	//[format ["dfk_qrf_support_wps1| %1 %2 %3 %4",_grp,_cararray,_targets,_initpos]] remoteExec ["systemchat"]; 

	_wp0 =_grp addWaypoint [_targets, 0];
	_wp0 setwaypointbehaviour "SAFE";
	_wp0 setwaypointspeed "FULL";
	_wp0 setwaypointformation "COLUMN";
	_wp0 setwaypointcombatmode "RED";
	_wp0 setWaypointCompletionRadius 300;
	
	_wp1 =_grp addWaypoint [_targets, 0];
	_wp1 setwaypointtype "TR UNLOAD";
	_wp1 setwaypointbehaviour "AWARE";
	_wp1 setWaypointCompletionRadius 300;
	
	_wp2 =_grp addWaypoint [_targets, 0];
	// _wp2 setwaypointtype "SAD";
	_wp2 setwaypointtype "MOVE";
	_wp2 setwaypointbehaviour "COMBAT";
	_wp2 setwaypointcombatmode "RED";
	_wp2 setwaypointformation "LINE";
	
	_wp3 =_grp addWaypoint [_targets, 100];
	_wp3 setwaypointtype "SAD";
	_wp2 setwaypointbehaviour "COMBAT";
	_wp2 setwaypointcombatmode "RED";
	_wp2 setwaypointformation "LINE";			
	
	_wp4 =_grp addWaypoint [_targets, 300];
	_wp4 setwaypointtype "SAD";
	_wp2 setwaypointbehaviour "COMBAT";
	_wp2 setwaypointcombatmode "RED";
	_wp2 setwaypointformation "LINE";	
	
	_wp7 =_grp addWaypoint [_targets, 0];
	_wp7 setwaypointtype "MOVE";
	_wp7 setwaypointbehaviour "AWARE";
	//_wp7 setwaypointcombatmode "RED";
	_wp7 setwaypointformation "COLUMN";		
	_wp7 setwaypointstatements ["true","[group this,getpos this] remoteExec ['dfk_removegroup',2]"];
	//[format ["dfk_qrf_support_wps2| %1",waypoints _grp]] remoteExec ["systemchat"]; 
	
	_hkpwp0 = _hkp_grp addwaypoint [_kp_point,0];
	_hkpwp0 setwaypointtype "MOVE";
	_hkpwp0 setwaypointbehaviour "SAFE";
	_hkpwp0 setwaypointspeed "FULL";
	_hkpwp0 setwaypointformation "COLUMN";
	_hkpwp0 setwaypointcombatmode "RED";
	
	_hkpwp1 = _hkp_grp addwaypoint [_targets,200];
	_hkpwp1 setwaypointtype "MOVE";
	_hkpwp1 setwaypointbehaviour "AWARE";
	
	_hkpwp2 = _hkp_grp addwaypoint [_targets,200];
	_hkpwp2 setwaypointtype "TR UNLOAD";
	_hkpwp2 setwaypointbehaviour "COMBAT";
		
	_hkpwp8 = _hkp_grp addwaypoint [_initpos,200];
	_hkpwp8 setwaypointtype "MOVE";		
	_hkpwp8 setwaypointbehaviour "AWARE";
	_hkpwp8 setwaypointstatements ["true","[group this,getpos this] remoteExec ['dfk_removegroup',2]"];	
	
};

//deletes vehicle
//determines if the player is nearby
//then if the vehicle is empty, dead or dry -> delete it
//used on qrf and ambulating 'dfk_spawn_car' (I think)
//cleanup function mostly
dfk_deletevehicle =
{
	private _car = _this select 0;
	private _far = true;
	
	for "_derp" from 0 to ((count allplayers) - 1) do
	{
		if ((allplayers select _derp) distance _car < 600) then
		{
			_far = false;
			_derp = 9999;
		};
	};
	
	if (_far && (count crew _car <= 0 OR !(canmove _car) or fuel _car <= 0)) then
	{
	
		if (damage _car > 0 && alive _car && (faction _car in BADFACTION or (typeof _car in roamingcar_cop))) then
		{
			//get cost
			private _logcost = 0;
			if (_car iskindof "Vehicle") then {_logcost = dfk_log_genericvehiclecost;};
			if (_car iskindof "Car") then {_logcost = dfk_log_carcost;};
			if (_car iskindof "Truck") then {_logcost = dfk_log_truckcost;};
			if (_car iskindof "APC") then {_logcost = dfk_log_apccost;};
			if (_car iskindof "Tank") then {_logcost = dfk_log_tankcost;};
			if (_car iskindof "Air") then {_logcost = dfk_log_aircost;};			
			
			//get damage 
			private _arr = (getAllHitPointsDamage  _car) select 2;
			private _d = 0;
			
			{
				if (_x >= 0.5) then {_d = _d + 1};
			} foreach _arr;
			
			_logcost = _logcost * (_d/count _arr);
			
			diag_log format ["dfk_deletevehicle | %1 %2 %3 %4",_logcost,_arr,_d,count _arr];
			
			//subtract logistics
			if (_logcost > 0) then {[_logcost] call DFK_fnc_remove_logistics};
			
			deletevehicle _car;
		}
		else
		{
			deletevehicle _car;
		};
	};
};

//when group has finished it's task, remove it to save performance
//but only if the players aren't nearby (breaks immersion)
dfk_removegroup = 
{
	private _grp = _this select 0;
	private _wppos = _this select 1;
	
	//diag_log format ["dfk_removegroup %1 %2",_grp,_wppos];
	private _del = true;
	private _veh = [];
	
	for "_derp" from 0 to ((count allplayers) - 1) do
	{
		_player = allplayers select _derp;
		//[format ["dfk_removegroup 0 %1",_player]] remoteExec ["systemchat"];
		for "_herp" from 0 to ((count units _grp) - 1) do
		{
			_dude = units _grp select _herp;
			//[format ["dfk_removegroup 1 %1",_dude]] remoteExec ["systemchat"];
			
			if ((_player distance _dude) < 600) then
			{
				//[format ["dfk_removegroup 2 %1",_player distance _dude]] remoteExec ["systemchat"];
				//if (debug) then {["playerunits close to de-spawn, moving"] remoteExec ["systemchat"];};
				_del = false;
				_herp = 999;
				_derp = 999;
			};			
		};
	};
	
	if (_del) then
	{
		
		while {count units _grp > 0} do
		{
			//["dfk_removegroup 3 delete"] remoteExec ["systemchat"];
			{
				if (_x distance _wppos < 100) then
				{
					if (vehicle _x != _x) then
					{
						_veh = _veh + [vehicle _x];
					};
					
					{
						deletevehicle _x;
					} foreach attachedObjects _x;
					
					deletevehicle _x;
				};
			} foreach units _grp;
			
			{
				if (_x distance _wppos < 100) then
				{
					{
						{
							deletevehicle _x;
						} foreach attachedObjects _x;
					
						deletevehicle _x;
					} foreach crew _x;
					
					{
						deletevehicle _x;
					} foreach attachedObjects _x;
					
					deletevehicle _x;
					[_x] call dfk_deletevehicle;
				};
			} foreach _veh;
			sleep 5;
		};
	}
	else
	{
		//["new despawn wp"] remoteExec ["systemchat"]; 				
		_arr = nearestLocations [leader _grp, ["NameCityCapital","NameVillage","NameLocal"], 6000];
		{
			if ((locationposition _x) distance leader _grp < 1000) then
			{
				_arr deleteat _foreachindex;
			};
		} foreach _arr;
		
		_endpoint = selectrandom _arr;
		
		_wp = _grp addwaypoint [locationposition _endpoint,0];
		_wp setwaypointstatements ["true","[group this,getpos this] remoteExec ['dfk_removegroup',2]"];
		_grp setspeedmode "FULL";
		leader _grp setbehaviour "AWARE";
	};
};

//stub
//used to init eventhandlers etc on spawned vehicles
dfk_initvehicle = 
{
	private _veh = _this select 0;

	// if (logistics <= 1000) then 
	// {
		// if (logistics > 0) then 
		// {
			// _veh setfuel (0.2 + (logistics/100));
			// if (getfuelcargo _veh > 0) then
			// {
				// _veh setfuelcargo (0.05 + (logistics/100));
			// };
			
		// };
		
		// if (logistics <= 0) then 
		// {
			// _veh setfuel (0.05);
			// if (getfuelcargo _veh > 0) then
			// {
				// _veh setfuelcargo (0.05);
			// };			
		// };
	// };
	
	if (faction _veh in BADFACTION or typeof _veh in roamingcar_cop) then
	{
		//systemchat "sabotageaction added to this vehicle";
		
		private _str = format ["Set fire to %1",gettext (configfile >> "CfgVehicles" >> (typeof _veh) >> "displayname")];
		
		//[_this select 0,_this select 1] spawn dfk_setFireToTheVehicle;
		//[[_this select 0,_this select 1],dfk_setFireToTheVehicle] remoteexec ["spawn",2]
		
		//"(vehicle _this) == _this && !(captive _target) && _this distance _target < 5 && alive _target &&	speed _target <= 1"
		
		[
			_veh,											// Object the action is attached to
			_str,										// Title of the action
			//"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa",	// Idle icon shown on screen
			"data\setFire_ca.paa",
			"data\setFire_ca.paa",	// Progress icon shown on screen
			"(vehicle _this) == _this && !(captive _this) && !(captive _target) && _this distance _target < 5 && alive _target &&	speed _target <= 1",						// Condition for the action to be shown
			"_caller distance _target < 5",						// Condition for the action to progress
			{},													// Code executed when action starts
			{},													// Code executed on every progress tick
			{_this execvm "dfk_setFireToTheVehicle.sqf"},				// Code executed on completion
			{},													// Code executed on interrupted
			[],													// Arguments passed to the scripts as _this select 3
			10,													// Action duration [s]
			0,													// Priority
			true,												// Remove on completion
			false												// Show in unconscious state 
		] remoteExec ["BIS_fnc_holdActionAdd", 0, _veh];	// MP compatible implementation		
		
		// [_veh,["Sabotage enemy vehicle",
		// {
			//(_this select 0) setdamage 1;
			// [_this select 0,_this select 1] spawn dfk_setFireToTheVehicle;
			// [(_this select 0),(_this select 2)] remoteexec ["removeaction",0,true];
		// },[],1.5, true, true, "", "alive _target && count crew _target <= 0 && speed _target <= 0 && fuel _target > 0", 5, false, "",""]] remoteexec ["Addaction",0,true];
		//alive _target && ((count (crew _target)) <= 0) && ((speed _target) <= 0) && ((fuel _target) > 0)
	};
	
	if (nerfgear) then {[_veh] remoteexec ["dfk_nerf_gear_vehicle",2]};
	if (dfk_locked_vehicles) then {_veh setVehicleLock "LOCKEDPLAYER"};
	
	_veh setVehicleRadar 1;
	{
		//diag_log format ["vehsensor %1",_x];
		_sensorarray = _x;
		{_veh enableVehicleSensor [_x, true];} foreach _sensorarray;
	} foreach listVehicleSensors _veh;

	_veh addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,""] execVM "killer.sqf";}];
};

//[_temp,_grp,_trewps,(_x select 0),_type,_dispersion] call 
//spawn troops at sites/towns
//use call
//returns array with groups
dfk_spawn_bunch_troops =
{
	private _tempgrparray = _this select 0;
	private _side = _this select 1;
	private _troops_no = _this select 2;
	private _spawnpos = _this select 3;
	private _distance = _this select 4;
	private _type = _this select 5;
	private _dispersion = _this select 6;
	private _sitetype = _this select 7;

	//diag_log format ["dfk_spawn_bunch_troops 0 | %1 %2 %3 %4 %5 %6",_tempgrparray,_side,_troops_no,_spawnpos,_distance,_type,_dispersion,_sitetype]; 					
	
	//divide into bite-sized groups
	private _g = floor (_troops_no / 8);
	for "_i" from 1 to _g do
	{
		private _grp = creategroup _side;
		private _id = ["towngrp"] call dfk_generate_radioid;
		_grp setGroupIdGlobal [_id];				
		_grp setvariable ["support",false,true];
		
		//_spot = [(_spawnpos select 0) + (random _distance) - (random _distance),(_spawnpos select 1) + (random _distance) - (random _distance)];		
		private _spot = [_spawnpos, 0, _distance, 1, 0, 0.5, 0] call BIS_fnc_findSafePos;	
		
		for "_ii" from 0 to 8 do
		{
			private _dude = "";
			_dude = [_dude,_grp,_spot,_dispersion,_type] call dfk_spawn_soldier;
			//BIS_fnc_spawnGroup use instead?
			//equipment
		};
		if (_sitetype != "") then
		{
			//[_grp, _spawnpos] call bis_fnc_taskDefend;
			if (0.5 > random 1) then 
			{
				[_grp, _spawnpos] call bis_fnc_taskDefend;
			}
			else
			{
				[_grp, _spawnpos, _distance,blacklist_water] call bis_fnc_taskPatrol;
			};
			_grp setcombatmode "RED";
			_grp setbehaviour "AWARE";
			_grp setspeedmode "NORMAL";			
		}
		else
		{
			if (0.5 > random 1) then 
			{
				[_grp, _spawnpos, _distance,blacklist_water] call bis_fnc_taskPatrol
			}
			else
			{
				_wp = _grp addwaypoint [_spawnpos,100];
				_wp setwaypointtype "DISMISS";
				
				_wp2 = _grp addwaypoint [_spawnpos,100];
				_wp2 setwaypointtype "DISMISS";

				_wp3 = _grp addwaypoint [_spawnpos,100];
				_wp3 setwaypointtype "DISMISS";				
			};
			_grp setcombatmode "RED";
			_grp setbehaviour "SAFE";
			_grp setspeedmode "LIMITED";
		};

		_grp deleteGroupWhenEmpty true;
		_tempgrparray append [_grp];
		if (_type != "cop") then {[leader _grp] call dfk_equip_sqdld;};
	};
	
	private _slatt = _troops_no mod 8;
	
	//if there are troops remaining, but not enough to form av big group, add them here
	if (_slatt > 0) then
	{
		_grp = creategroup _side;
		_grp setvariable ["support",false,true];
		
		private _id = ["towngrp"] call dfk_generate_radioid;
		_grp setGroupIdGlobal [_id];				
		
		_spot = [(_spawnpos select 0) + (random _distance) - (random _distance),(_spawnpos select 1) + (random _distance) - (random _distance)];		
		
		for "_ii" from 0 to _slatt do
		{
			_dude = "";
			_dude = [_dude,_grp,_spot,_dispersion,_type] call dfk_spawn_soldier;
			//equipment
		};
		//[_grp, _spawnpos, _distance,blacklist_water] call bis_fnc_taskPatrol;
		
		_wp = _grp addwaypoint [_spawnpos,100];
		_wp setwaypointtype "DISMISS";
		
		_grp setcombatmode "RED";
		_grp setbehaviour "SAFE";
		_grp setspeedmode "NORMAL";		
		_grp deleteGroupWhenEmpty true;
		_tempgrparray append [_grp];	
	};
	//spawnloop
	
	//[format ["dfk_spawn_bunch_troops 1 | %1",_tempgrparray]] remoteExec ["systemchat"]; 					
	
	//return group array;
	_tempgrparray;
};

//important dudes drop loot when they die
//TODO: add repetition, so we don't have to use multiple eventhandlers to handle multiple loot drops
dfk_drop_loot = 
{
	private _dude = _this select 0;
	private _n = _this select 1;
	private _debugvar = _this select 2;
	
	private _position = [];
	if (typename _dude == "OBJECT") then {_position = getpos _dude};
	if (typename _dude == "ARRAY") then {_position = _dude};	

	private _shit = selectrandom ["intel","food","food","food","money","money","money","money","money"];
	private _loot = ObjNull;
	
	if (isServer) then
	{
	
		for "_i" from 0 to _n do
		{
			switch (_shit) do
			{
				case "food":
				{
					_loot = createVehicle [(selectrandom consumeablesarray), _position, [], 10, "NONE"];
					[_loot, ["Consume food", {_this execvm "dfk_consume_food.sqf"}]] remoteExec ["addAction",0,true];
					//if (debug) then {[_food,"colorgreen",_debugvar,"hd_dot",true,1] execvm "marker_track.sqf";};
				};
				
				case "intel":
				{
					_loot = createVehicle [(selectrandom intelthingsarray),_position, [], 10, "NONE"];
					[_loot, ["Take intel", {_this execvm "dfk_consume_intel.sqf"}]] remoteExec ["addAction",0,true];
					//if (debug) then {[_intel,"colorgreen",_debugvar,"hd_dot",true,1] execvm "marker_track.sqf";};
				};
				
				case "money":
				{
					_loot = createVehicle ["Land_Money_F", _position, [], 10, "NONE"];
					[_loot, ["Take cash", {_this execvm "dfk_consume_funds.sqf"}]] remoteExec ["addAction",0,true];
					//if (debug) then {[_money,"colorgreen",_debugvar,"hd_dot",true,1] execvm "marker_track.sqf";};
				};
			};
			
			if (dfk_RobinMode == 1) then
			{
				_source01 = "#particlesource" createVehicle position _loot;
				_source01 setParticleClass "AirFireSparks";
				_source01 attachTo [_loot,[0,0,0]];			
			};
		};
	};

	_loot;
};

//function for equiping and setting up squad leaders
dfk_equip_sqdld =
{
	private _dude = _this select 0;
	[_dude] call dfk_equipment_sqdld;
	_dude setunitrank "SERGEANT";
	_dude removeAllMPEventHandlers "mpkilled"; 
	_dude addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,"KILLEDEAST_level1"] execVM "killer.sqf";}];
	_dude addMPEventHandler ["mpkilled", {[_this select 0,1,"sqdlddrop"] remoteexec ["dfk_drop_loot",2];}];
};

dfk_equip_pltch =
{
	private _dude = _this select 0;
	[_dude] call dfk_equipment_sqdld;
	_dude setunitrank "LIEUTENANT";
	_dude removeAllMPEventHandlers "mpkilled"; 
	_dude addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,"KILLEDEAST_level2"] execVM "killer.sqf";}];
	_dude addMPEventHandler ["mpkilled", {[_this select 0,2,"pltchdrop"] remoteexec ["dfk_drop_loot",2];}];
};

dfk_equip_compch =
{
	private _dude = _this select 0;
	[_dude] call dfk_equipment_compch;
	_dude setunitrank "CAPTAIN";
	_dude removeAllMPEventHandlers "mpkilled"; 
	_dude addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,"KILLEDEAST_level3"] execVM "killer.sqf";}];
	_dude addMPEventHandler ["mpkilled", {[_this select 0,3,"compchdrop"] remoteexec ["dfk_drop_loot",2];}];
};

dfk_equip_boss =
{
	private _dude = _this select 0;
	[_dude] call dfk_equipment_boss;
	_dude setunitrank "COLONEL";
	_dude removeAllMPEventHandlers "mpkilled"; 
	_dude addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,"KILLEDEAST_level4"] execVM "killer.sqf";}];
	_dude addMPEventHandler ["mpkilled", {[_this select 0,5,"bossdrop"] remoteexec ["dfk_drop_loot",2];}];
};

//equip grunts with custom loadout depending on type and logistical availability
dfk_equip_soldier =
{
	private _dude = _this select 0;
	private _type = _this select 1;	
	
	if (_type == "cop" && logistics < 1200) then
	{
		private _arr = [dfk_equipment_cop1,dfk_equipment_cop2,dfk_equipment_cop2_1,dfk_equipment_cop2_2,dfk_equipment_cop1_2];
	
		[_dude] call selectrandom _arr;
	
		// if (random 1 > 0.5) then
		// {
			// [_dude] call dfk_equipment_cop1;
		// }
			// else
		// {
			// [_dude] call dfk_equipment_cop2;
		// };
	};
	
	if (_type == "cop" && logistics >= 1200) then
	{
		private _arr = [dfk_equipment_cop3,dfk_equipment_cop4,dfk_equipment_cop5];
	
		[_dude] call selectrandom _arr;
	};
	
	if (_type == "crew") then
	{
		[_dude] call dfk_equipment_crew;
	};
	
	if (_type == "driver") then
	{
		[_dude] call dfk_equipment_driver;
	};

	if (_type == "soldier") then
	{
		if (logistics >= 3000) then
		{
			[_dude] call dfk_equipment_normal3;
		};		
	
		if (logistics >= 2000 && logistics < 3000) then
		{
			[_dude] call dfk_equipment_normal2;
		};	
	
		if (logistics >= 1000 && logistics < 2000) then
		{
			[_dude] call dfk_equipment_normal1;
		};
	
		if (logistics >= 500 && logistics < 1000) then
		{
			[_dude] call dfk_equipment_low2;
			[_dude] call dfk_equipment_simulate_hat;
		};	
		if (logistics < 500) then
		{
			private _arr = [dfk_equipment_low1,dfk_equipment_low2];
			[_dude] call (selectrandom _arr);
			[_dude] call dfk_equipment_simulate_hat;			
		};
	};
	
	if (_type == "bunker") then
	{
		if (logistics >= 3000) then
		{
			private _arr = [dfk_equipment_mg_normal1,dfk_equipment_at1,dfk_equipment_mg_normal2,dfk_equipment_mg_hightech];
			[_dude] call (selectrandom _arr);
		};		
	
		if (logistics >= 2000 && logistics < 3000) then
		{
			private _arr = [dfk_equipment_mg_normal1,dfk_equipment_at1,dfk_equipment_mg_normal2];
			[_dude] call (selectrandom _arr);
		};	
	
		if (logistics >= 1000 && logistics < 2000) then
		{
			private _arr = [dfk_equipment_normal1,dfk_equipment_mg_low,dfk_equipment_mg_normal1,dfk_equipment_at1];
			[_dude] call (selectrandom _arr);
		};
	
		if (logistics >= 500 && logistics < 1000) then
		{
			private _arr = [dfk_equipment_normal1,dfk_equipment_low2,dfk_equipment_mg_low];
			[_dude] call (selectrandom _arr);
		};	
		if (logistics < 500) then
		{
			[_dude] call dfk_equipment_low2;
			[_dude] call dfk_equipment_simulate_hat;			
		};
	};
	
	if (_type == "boss") then
	{
		[_dude] call dfk_equip_boss;
	};
		
	_dude setVehicleRadar 1;
	{
		diag_log format ["dudesensor %1",_x];
		_dude enableVehicleSensor [_x, true];
	} foreach listVehicleSensors _dude;			
	
};

//spawn soldier, cops, civs etc
//also inits scripts and functions on said soldier
dfk_spawn_soldier =
{
	private _name = _this select 0;
	private _grp = _this select 1;
	private _pos = _this select 2;
	private _spread = _this select 3;
	private _type = _this select 4;
	
	//fetch specific classname
	private _class = "";
	
	switch (_type) do
	{
		default {_class = dfk_opfor_baseclass;};
		case "cop": {_class = dfk_opfor_baseclass;};
		case "soldier": {_class = dfk_opfor_baseclass;};
		case "crew": {_class = dfk_opfor_crew;};
		case "driver": {_class = dfk_opfor_crew;};
		case "civ": {_class = dfk_civclass;};
		case "worker": {_class = dfk_worker;};
		case "hkppilot": {_class = dfk_opfor_helipolotclass;};
		case "bunker": {_class = dfk_opfor_baseclass;};
		case "boss": {_class = dfk_opfor_bossclass;};
		case "bodyguard_opfor": {_class = dfk_opfor_bodyguard;};
		case "animal": {_class = selectrandom dfk_random_animals;};
		case "qrf": {_class = selectrandom dfk_qrfkindsarray;};
	};
		
	private _safepos = [_pos, 0, 50, 1, 0, 0.5, 0] call BIS_fnc_findSafePos;
	private _name = _grp createUnit [_class,_pos, [], _spread, "NONE"];
	_name setpos _safepos;
	
	/* if (_type == "soldier" or _type == "crew" or _type == "cop" or _type == "bunker" or _ty) then
	{
		
	}; */
	
	switch (_type) do
	{
		default {[_name,_type] remoteExec ["dfk_equip_soldier",2];};
		case "civ": {[_name,_type] remoteExec ["dfk_equipment_random_civ",2];};
	};


/* 	if (_type == "boss") then
	{
		[_name,_type] remoteExec ["dfk_equip_soldier",2];
		[_name] call dfk_set_skill;		
	}; */
	
	//scripts n shit
	//faction _x in BADFACTION && morale _x < 0
	if (faction _name in BADFACTION && !(_type == "boss")) then
	{
		_name addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,"KILLEDEAST"] execVM "killer.sqf";}];
		[_name] call dfk_set_skill;
		//[_name,"colorred","","hd_dot",true,1] execvm "marker_track.sqf";
	};
	
	if (faction _name in CIVFACTION) then
	{
		_name addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,""] execVM "killer.sqf";}];
		//[_name,"colorblue","","hd_dot",true,1] execvm "marker_track.sqf";
	};	
	
	if (random 1 > 0.5) then {_name enableGunLights "forceOn"};
	
	//if important (leader, officer etc) exec diff killscript
	
	//return _dude;
	_name;
};

dfk_set_skill =
{
	private _dude = _this select 0;

	_dude setskill ["aimingAccuracy",dfk_ai_aimingAccuracy];
	_dude setskill ["aimingShake",dfk_ai_aimingShake];
	_dude setskill ["aimingSpeed",dfk_ai_aimingSpeed];
	_dude setskill ["spotDistance",dfk_ai_spotDistance];
	_dude setskill ["spotTime",dfk_ai_spotTime];
	_dude setskill ["courage",dfk_ai_courage];
	_dude setskill ["reloadSpeed",dfk_ai_reloadSpeed];
	_dude setskill ["commanding",dfk_ai_commanding];
	_dude setskill ["general",dfk_ai_general];
};

//crew up a vehice with appropriate dudes
dfk_spawn_crew =
{
	private _grp = _this select 0;
	private _car = _this select 1;
	private _code = _this select 2;
		
	private _leader = false;
	
	private _driver = _car emptyPositions "driver";
	private _gunner = _car emptyPositions "gunner";
	private _commander = _car emptyPositions "commander";
	private _cargo = _car emptyPositions "cargo";
	private _arr = [[_commander,"commander"],[_gunner,"gunner"],[_driver,"driver"],[_cargo,"cargo"]];
	
	diag_log format ["spawn_crew | %1 %2 %3 %4 %5 %6 %7 %8",_grp,_car,_code,_driver,_gunner,_commander,_cargo,_arr];
	
	{
		for "_i" from 0 to ((_x select 0) - 1) do
		{
			private _dude = "";
			
			//addSwitchableUnit _dude;
			
			//_dude = _grp createUnit ["B_Soldier_lite_F", position _car, [], 100, "NONE"];
			//[format ["dfk_spawn_crew1| %1 %2",_dude,_grp]] remoteExec ["systemchat"]; 	
			

			if ((_x select 1) == "cargo") then
			{
				_dude = [_dude,_grp,position _car,100,_code] call dfk_spawn_soldier;
				_dude assignascargo _car;
				_dude moveincargo _car;
			};				
			
			if ((_x select 1) == "gunner") then
			{
				_dude = [_dude,_grp,position _car,100,"crew"] call dfk_spawn_soldier;
				_dude assignasgunner _car;
				_dude moveingunner _car;
			};
			if ((_x select 1) == "driver") then
			{
				_dude = [_dude,_grp,position _car,100,"driver"] call dfk_spawn_soldier;
				_dude assignasdriver _car;
				_dude moveindriver _car;
			};			
			if ((_x select 1) == "commander") then
			{
				_dude = [_dude,_grp,position _car,100,"crew"] call dfk_spawn_soldier;
				_dude assignascommander _car;
				_dude moveincommander _car;
				// _grp selectleader _dude;
				// _leader = true;			
				//[format ["dfk_spawn_crew2| leader is %1 %2",_dude,_leader]] remoteExec ["systemchat"]; 					
			};			
		};
	} foreach _arr;
	
	if (_code == "qrf") then
	{
		[units _grp] remoteexec ["dfk_add_qrfpool",2];
	};
	//_grp addVehicle _car;
	
	if (debug) then
	{
		{
			[_x,"colorred","","hd_dot",true,1] execvm "marker_track.sqf";
		} foreach units _grp;
	};	
};

dfk_spawn_hkp_crew =
{
	private _grp = _this select 0;
	private _pilot_grp = _this select 1;
	private _car = _this select 2;
	
	private _leader = false;
	
	private _driver = _car emptyPositions "driver";
	private _gunner = _car emptyPositions "gunner";
	private _commander = _car emptyPositions "commander";
	private _cargo = _car emptyPositions "cargo";
	private _arr2 = [[_commander,"commander"],[_gunner,"gunner"],[_driver,"driver"]];
	
	private _arr = [[_cargo,"cargo"]];
	
	{
		for "_i" from 0 to ((_x select 0) - 1) do
		{
			_dude = "";
			_dude = [_dude,_grp,position _car,10,"qrf"] call dfk_spawn_soldier;

			if ((_x select 1) == "cargo") then
			{
				_dude assignascargo _car;
				_dude moveincargo _car;
			};						
		};
	} foreach _arr;
	
	{
		for "_i" from 0 to ((_x select 0) - 1) do
		{		
			if ((_x select 1) == "commander") then
			{
				_dude = "";		
				_dude = [_dude,_pilot_grp,position _car,10,"hkppilot"] call dfk_spawn_soldier;
				_dude assignascommander _car;
				_dude moveincommander _car;
				// _grp selectleader _dude;
				// _leader = true;			
				//[format ["dfk_spawn_crew2| leader is %1 %2",_dude,_leader]] remoteExec ["systemchat"]; 					
			};			
			if ((_x select 1) == "driver") then
			{
				_dude = "";
				_dude = [_dude,_pilot_grp,position _car,10,"hkppilot"] call dfk_spawn_soldier;
				_dude assignasdriver _car;
				_dude moveindriver _car;
			};			
			if ((_x select 1) == "gunner") then
			{
				_dude = "";
				_dude = [_dude,_pilot_grp,position _car,10,"hkppilot"] call dfk_spawn_soldier;
				_dude assignasgunner _car;
				_dude moveingunner _car;
			};

		};
	} foreach _arr2;

	{
		if (!(_x in crew _car)) then {deletevehicle _x};
	} foreach units _grp;
	
	{
		if (!(_x in crew _car)) then {deletevehicle _x};
	} foreach units _pilot_grp;	
	
	//_grp addVehicle _car;
};

//remove morale based on crimetype
DFK_fnc_remove_morale = 
{
	private _crimetype = _this select 0;
	
	//civilian death
	private _crimecost = 1;
	
	if (_crimetype == "KILLEDEAST") then {_crimecost = 2};
	if (_crimetype == "KILLEDEAST_level1") then {_crimecost = 10};
	if (_crimetype == "KILLEDEAST_level2") then {_crimecost = 50};
	if (_crimetype == "KILLEDEAST_level3") then {_crimecost = 100};
	if (_crimetype == "KILLEDEAST_level4") then {_crimecost = 200};
	if (_crimetype == "LOGISTICAL") then {_crimecost = 5};
	if (_crimetype == "FF") then {_crimecost = 15};
	if (_crimetype == "TOOKPRISONER") then {_crimecost = 20};
	
	
	//
	if (_crimetype == "ACC") then
	{
		private _moralneg = 0;
		{
			private _incl = false;
			{
				if (faction _x in BADFACTION && morale _x < -0 && alive _x) then
				{
					_moralneg = _moralneg + (morale _x);
					//[format ["-m %1",_moralneg]] remoteExec ["systemchat"];
				};
			} foreach units _x;
			
		} foreach allgroups;	
		//[format ["-mx %1",_moralneg]] remoteExec ["systemchat"];
		
		_crimecost = _moralneg * -1;
	};
	
	sidemorale = sidemorale - _crimecost;
	diag_log [format ["-morale %1 %2",_crimetype,sidemorale]];
};

//increase enemy morale
DFK_fnc_add_morale = 
{
	private _crimetype = _this select 0;
	
	private _crimecost = 0;
	
	if (_crimetype == "KILLEDCIV") then {_crimecost = 30};
	if (_crimetype == "KILLEDPRISONER") then {_crimecost = 50};
	if (_crimetype == "ENEMYDESTROYEDVEHICLE") then {_crimecost = 15};
	if (_crimetype == "INIT") then {_crimecost = ["init_morale"] call BIS_fnc_getParamValue;};	
	
	sidemorale = sidemorale + _crimecost;
	diag_log [format ["+morale %1 %2",_crimetype,sidemorale]];
};

//logistics
//remove logistics
DFK_fnc_remove_logistics = 
{
	//cfg class
	private _crimetype = _this select 0;
	//[format ["DFK_fnc_remove_logistics| Vehicle! %1 %2 Side %3 Faction %4 Crew %5",_crimetype,typeof _crimetype,side _crimetype,faction _crimetype,(configFile >> "CfgVehicles" >> typeof _crimetype >> "crew") call BIS_fnc_GetCfgData]] remoteExec ["systemchat"]; 
	
	//civilian death
	private _crimecost = 0;

	if (typename _crimetype == "OBJECT") then
	{
		if (not(_crimetype iskindof "Man")) then
		{
			//logistical vehicles, transports
			//_logisticsstuff = ["Box_NATO_AmmoVeh_F","Box_IND_Ammo_F","Box_T_East_Ammo_F","Box_East_Ammo_F","Box_NATO_Ammo_F","Box_Syndicate_Ammo_F","Box_IND_Wps_F","Box_T_East_Wps_F","Box_East_Wps_F","Box_T_NATO_Wps_F","Box_NATO_Wps_F","Box_Syndicate_Wps_F","Box_AAF_Equip_F","Box_CSAT_Equip_F","Box_IDAP_Equip_F","Box_NATO_Equip_F","Box_IED_Exp_F","Box_IND_AmmoOrd_F","Box_East_AmmoOrd_F","Box_IDAP_AmmoOrd_F","Box_NATO_AmmoOrd_F","Box_FIA_Ammo_F","Box_FIA_Support_F","Box_FIA_Wps_F","Box_IND_Grenades_F","Box_East_Grenades_F","Box_NATO_Grenades_F","Box_IND_WpsLaunch_F","Box_East_WpsLaunch_F","Box_NATO_WpsLaunch_F","Box_Syndicate_WpsLaunch_F","Box_IND_WpsSpecial_F","Box_T_East_WpsSpecial_F","Box_East_WpsSpecial_F","Box_T_NATO_WpsSpecial_F","Box_NATO_WpsSpecial_F","I_supplyCrate_F","O_supplyCrate_F","C_T_supplyCrate_F","C_supplyCrate_F","IG_supplyCrate_F","Box_GEN_Equip_F","C_IDAP_supplyCrate_F","B_supplyCrate_F","Box_IND_Support_F","Box_East_Support_F","Box_NATO_Support_F","Box_AAF_Uniforms_F","Box_CSAT_Uniforms_F","Box_IDAP_Uniforms_F","Box_NATO_Uniforms_F","Box_IND_AmmoVeh_F","Box_East_AmmoVeh_F","Box_NATO_AmmoVeh_F","CargoNet_01_barrels_F","CargoNet_01_box_F","I_CargoNet_01_ammo_F","O_CargoNet_01_ammo_F","C_IDAP_CargoNet_01_supplies_F","B_CargoNet_01_ammo_F","B_Slingload_01_Ammo_F","B_Slingload_01_Cargo_F","B_Slingload_01_Fuel_F","B_Slingload_01_Repair_F","Land_Pod_Heli_Transport_04_ammo_F","Land_Pod_Heli_Transport_04_box_F","Land_Pod_Heli_Transport_04_fuel_F","Land_Pod_Heli_Transport_04_repair_F"];

			private _test = -1;
			if (_crimetype iskindof "Vehicle") then {_crimecost = dfk_log_genericvehiclecost; _test = 1};
			if (_crimetype iskindof "Car") then {_crimecost = dfk_log_carcost; _test = 2};
			if (_crimetype iskindof "Truck") then {_crimecost = dfk_log_truckcost; _test = 3};
			if (_crimetype iskindof "APC") then {_crimecost = dfk_log_apccost; _test = 4};
			if (_crimetype iskindof "Tank") then {_crimecost = dfk_log_tankcost; _test = 5};
			if (_crimetype iskindof "Air") then {_crimecost = dfk_log_aircost; _test = 6};
			if ((typeof _crimetype) in logisticaltargetsarray) then {_crimecost = 400; _test = 7};
			if ((typeof _crimetype) in supplytruckkindarray) then {_crimecost = 200; _test = 8};
			if ((typeof _crimetype) in dfk_biglogisticstuff) then {_crimecost = 300; _test = 9};
			if ((typeof _crimetype) in dfk_expensivetoys) then {_crimecost = 500; _test = 9};

			logistics = logistics - _crimecost;
			diag_log format ["-log %1 %2 %3 %4",typeof _crimetype,_crimecost,_test,logistics];
			
			dfk_opfor_vehcost = dfk_opfor_vehcost + (getNumber (configFile >> "CfgVehicles" >> typeof _crimetype >> "cost"));

			//["LOGISTICAL"] remoteExec ["DFK_fnc_remove_morale"];
		};
	};
	
	if (typename _crimetype == "SCALAR") then
	{
		logistics = logistics - _crimetype;
		diag_log format ["-log %1 %2 %3 %4",_crimetype,logistics];
	};
};

//increase enemy log
DFK_fnc_add_logistics = 
{
	private _crimetype = _this select 0;
	
	private _crimecost = 1;
	
	if (_crimetype == "AIRDROP") then {_crimecost = dfk_airdrop_logisticsvalue1};
	if (_crimetype == "AIRDELIVERYSMALL") then {_crimecost = dfk_airdrop_logisticsvalue2};
	if (_crimetype == "AIRDELIVERYBIG") then {_crimecost = dfk_airdrop_logisticsvalue3};
	if (_crimetype == "INIT") then {_crimecost = ["init_logistics"] call BIS_fnc_getParamValue;};

	logistics = logistics + _crimecost;
	diag_log [format ["+log %1 %2",_crimetype,logistics]];
};

DFK_fnc_remove_political = 
{
	private _crimetype = _this select 0;
	
	//civilian death
	//wth?
	private _crimecost = 1;
	
	//resupplying logistics is expensive, hence political cost
	if (_crimetype == "LOGISTICS") then {_crimecost = 50};	
	//exposing warcrime, unsure if used...
	if (_crimetype == "WARCRIME") then {_crimecost = 200};
	//blackmail/using intel on the enemy
	if (_crimetype == "BLACKMAIL_GENERIC") then {_crimecost = 200 + round random 200};
	
	//political cost of lots of dead guys/"the politics of black plastic bags"
	if (_crimetype == "ACC") then {_crimecost = round ((originalnumberofdudes - aliveenemies)/100)};
	//_f = (((originalnumberofdudes mod aliveenemies)/100) + 1);
	
	if (_crimetype == "MERC")  then {_crimecost = 15 + random 25};
	if (_crimetype == "POW")  then {_crimecost = 20 + random 80};
	
	// _killedeastarray = ["KILLEDEAST","KILLEDEAST_level1","KILLEDEAST_level2","KILLEDEAST_level3","KILLEDEAST_level4"];
	// if (_crimetype in _killedeastarray) then {_crimecost = (1.0 * _f) - _crimecost};
	
	politics = politics - _crimecost;
	diag_log [format ["-pol %1 %2 %3",_crimetype,politics]];
};

//add enemy political value
DFK_fnc_add_political = 
{
	private _crimetype = _this select 0;
	
	private _crimecost = 1;
	
	if (_crimetype == "DRUGRUN1") then {_crimecost = 20};
	if (_crimetype == "DRUGRUN2") then {_crimecost = 50};
	if (_crimetype == "DRUGBUST1") then {_crimecost = 20};
	if (_crimetype == "DRUGBUST2") then {_crimecost = 50};
	if (_crimetype == "DRUGBUST3") then {_crimecost = 100};
	if (_crimetype == "WARCRIME") then {_crimecost = 50};
	if (_crimetype == "INIT") then {_crimecost = ["init_political"] call BIS_fnc_getParamValue;};
	
	politics = politics + _crimecost;
	diag_log [format ["+pol %1 %2",_crimetype,politics]];
};

//you killed someone
//track total number of alive dudes
DFK_deaddude =
{
	private _dead = _this select 0;
	
	aliveenemies = aliveenemies - _dead;
	//["DFK_deaddude 1"] remoteExec ["systemchat"]; 
};

//you found a secret
dfk_add_secret =
{
	secrets = secrets + 1;
	secrets_all = secrets_all + 1;
	publicvariable "secrets";
	publicvariable "secrets_all";
	
	// if (count dfk_interestingAsFuckArray > 0) then
	// {
		// _poi = selectrandom dfk_interestingAsFuckArray;
		// dfk_interestingAsFuckArray deleteAt (dfk_interestingAsFuckArray find _poi);
		
		// [getpos _poi,"colorgreen","Point of Interest","hd_unknown",1] remoteExec ["dfk_generate_marker",2];	
		
		// publicvariable "dfk_interestingAsFuckArray";
	// };
};

//you used a secret to lower political
dfk_remove_secret = 
{
	secrets = secrets - 1;
	secrets_used = secrets_used + 1;
	publicvariable "secrets";
	publicvariable "secrets_used";
};

//you found some cash
dfk_add_funds =
{
	private _value = _this select 0;
	//[format ["funds %1",_value]] remoteExec ["systemchat"]; 	
	funds = funds + _value;
	funds_all = funds_all + _value;
	publicvariable "funds";
	publicvariable "funds_all";
	
	//[format ["funds %1",_value]] remoteExec ["systemchat"]; 	
};

//you bought something
dfk_remove_funds = 
{
	private _value = _this select 0;
	funds = funds - _value;
	funds_spent = funds_spent + _value;
	publicvariable "funds";
	publicvariable "funds_spent";
};

//function for generating a markername
//derpy as fuck
dfk_generate_randomname = 
{
	private _randomname = format ["%1_%2_%3_%4",selectrandom randomlettersarray,selectrandom randomlettersarray, random 1000, random 666,random 42];
	_randomname;
};

//generate map marker_track
//mostly for debugging
dfk_generate_marker =
{
	private _pos = _this select 0;
	private _color = _this select 1;
	private _string = _this select 2;
	private _type = _this select 3;	
	private _alpha = _this select 4;

	private _position = [];
	if (typename _pos == "OBJECT") then {_position = getpos _pos};
	if(typename _pos == "ARRAY") then {_position = _pos};

	private _markername = [] call dfk_generate_randomname;
	_markerstr = createMarker [_markername,_position];
	_markerstr setMarkerShape "ICON";
	_markerstr setMarkerType _type;
	_markerstr setmarkercolor _color;
	_markerstr setmarkertext _string;
	_markerstr setmarkeralpha _alpha;
		
	_markerstr;
};

//adding actions
//not sure if this works or if its actually usedÂ§
dfk_addactionMP = 
{
	private ["_object", "_screenMsg", "_scriptToCall", "_arguments", "_priority", "_showWindow", "_hideOnUse", "_shortcut", "_condition"];
	_object = _this select 0;
	_screenMsg = _this select 1;
	_scriptToCall = _this select 2;
	_arguments = _this select 3;
	_priority = _this select 4;
	_showWindow = _this select 5;
	_hideOnUse = _this select 6;
	_shortcut = _this select 7;
	_condition = _this select 8;

	if(isNull _object) exitWith {};
	_object addaction [_screenMsg,_scriptToCall,_arguments,_priority,_showWindow,_hideOnUse,_shortcut,_condition];
};;