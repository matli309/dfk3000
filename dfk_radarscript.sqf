#include "defines.h";

_dir0 = 0;
_alive = 0;

{_x setdir _dir0;_x enableVehicleSensor  ["ActiveRadarSensorComponent",true];_x enableVehicleSensor ["DataLinkSensorComponent",true];if (alive _x) then {_alive = _alive + 1};} foreach dfk_airfield_aaunits;

private _dronearray = [];
private _old_length = 0;
_old_length = count allUnitsUAV;
while {_alive > 0} do
{
	{	
		if (count (_x targets [true, 16000, [west,resistance,sideEnemy]]) <= 0) then
		{
			_x dowatch (_x getpos [100,_dir0]);
		};
		
		_alive = 0;
		if (alive _x) then {_alive = _alive + 1};
	} foreach dfk_airfield_aaunits;
	sleep 2;	
	
	{	
		if (count (_x targets [true, 16000, [west,resistance,sideEnemy]]) <= 0) then
		{
			_x dowatch (_x getpos [100,_dir0 + (120)]);
		};
	} foreach dfk_airfield_aaunits;
	sleep 2;		
	
	{	
		if (count (_x targets [true, 16000, [west,resistance,sideEnemy]]) <= 0) then
		{
			_x dowatch (_x getpos [100,_dir0 + (240)]);
		};
	} foreach dfk_airfield_aaunits;
	sleep 2;
	
	//hint "blip";
};