//DRÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖÖN DRÖN DRÖN DÖRÖÖRÖRÖRÖRÖRÖRRÖÖÖÖÖNN!!
//drone scout script
//reports all west/res spotted to all east units within 2km each 30s

_trg = [WEST,RESISTANCE,INDEPENDENT,sideunknown,sideenemy];
_dron = _this select 0;

(group driver _dron) deleteGroupWhenEmpty true;

_radiotrigger = createTrigger ["EmptyDetector", getPos _dron];
_radiotrigger setTriggerArea [2000, 2000, 0, false];
_radiotrigger setTriggerActivation ["EAST", "PRESENT", true];

while {alive _dron} do
{
	_finaltrglist = [vehicle _dron];
	// _trglist1 = leader _dron targetsQuery ["O_Soldier_Base_f",sideunknown,"",[],0];
	_trglist1 = leader _dron nearTargets 600;
	
	{
		if ((_x select 2) in _trg && !(_x select 4 in _finaltrglist)) then
		{
			_finaltrglist = _finaltrglist + [(_x select 4)];	
		};
	} foreach _trglist1;

	_debuggrplist = [];

	_radiotrigger setpos getpos _dron;	
	if (count _finaltrglist > 0) then
	{
		{	
			if (side _x == east && (getpos (leader _x)) inarea _radiotrigger && "ItemRadio" in (assignedItems  (leader _x))) then
			{
				_grp = _x;	
				{
					if (_grp knowsabout _x < 1 && side _x != east) then
					{
						_grp reveal [_x,1];
						_debuggrplist = _debuggrplist + [_grp];
						//[format ["%1 reveal to grp %2",_x,_grp]] remoteExec ["systemchat"];
					};
				} foreach _finaltrglist;
			};
		} foreach allgroups;
	};

	sleep 30;
};

{
	deletevehicle _x;

} foreach crew _dron;
deletevehicle _dron;
deletevehicle _radiotrigger;