disableserialization;
// _display = _this select 0;
// _listbox = _this select 1;
// player globalchat format ["%1 %2",_display,_listbox];
_display = findDisplay 2000;
_listbox = _display displayCtrl 2005;

//[format ["derp %1 %2 %3",_display,_listbox]] remoteExec ["systemchat"]; 
publicvariable "storearray";
publicvariable "rabatt";

{
	_gunclass = _x select 0;
	_gunprice = _x select 1;
	_type = "";
	_buystr = "Error, lel";
	
	publicvariable "rabatt";
	_gunprice = _gunprice * rabatt;
	
	//Smokes-let'sgo!
	if (_gunclass == "Smokes") then
	{
		_type = "smokes";
		_buystr = format ["%1 for $%2","Smokes",_gunprice];
	};	
	
	if (_gunclass == "Grenades") then
	{
		_type = "Grenades";
		_buystr = format ["%1 for $%2","Grenades",_gunprice];
	};		
	
	if (_gunclass == "Chemlights") then
	{
		_type = "Chemlights";
		_buystr = format ["%1 for $%2","Chemlights",_gunprice];
	};		
	
	if (_gunclass == "Bunch of glasses") then
	{
		_type = "glasses";
		_buystr = format ["%Bunch of glasses $%2","Chemlights",_gunprice];
	};

	if (_gunclass == "Diving googles") then
	{
		_type = "diving";
		_buystr = format ["%Diving googles $%2","Chemlights",_gunprice];		
	};

	if (_gunclass == "Balacalavas") then
	{
		_type = "Balacalavas";
		_buystr = format ["Balacalavas for $%2","Chemlights",_gunprice];		
	};

	if (_gunclass == "Bandanas") then
	{
		_type = "Bandanas";
		_buystr = format ["Bandanas for %2","Chemlights",_gunprice];		
	};
	if (_gunclass == "IED") then
	{
		_type = "IED";
		_buystr = format ["IED (small) for $%2","Chemlights",_gunprice];		
	};	
	
	_bombsarr = ["ATMine_Range_Mag","APERSMine_Range_Mag","APERSBoundingMine_Range_Mag","SLAMDirectionalMine_Wire_Mag","APERSTripMine_Wire_Mag","ClaymoreDirectionalMine_Remote_Mag","SatchelCharge_Remote_Mag","DemoCharge_Remote_Mag","IEDUrbanBig_Remote_Mag","IEDLandBig_Remote_Mag"];
	if (_gunclass in _bombsarr) then
	{
		_type = "bomb";
		_buystr = format ["%1 for $%2",getText(configfile >> "CfgMagazines" >> _gunclass >> "displayname"),_gunprice];
	};	
	
	if (_gunclass iskindof ["Rifle", configFile >> "CfgWeapons"] or _gunclass iskindof ["Pistol", configFile >> "CfgWeapons"] or _gunclass iskindof ["Launcher", configFile >> "CfgWeapons"] or _gunclass iskindof ["Mgun", configFile >> "CfgWeapons"]) then
	{
		_type = "rifle";
		_buystr = format ["%1 for $%2",getText(configfile >> "CfgWeapons" >> _gunclass >> "displayname"),_gunprice];
	};
	_derparr = ["MineDetector","Binocular","NVGoggles_INDEP","Rangefinder"];
	if (_gunclass iskindof ["ItemCore", configFile >> "CfgWeapons"] or _gunclass in _derparr) then
	{
		_type = "item";
		_buystr = format ["%1 for $%2",getText(configfile >> "CfgWeapons" >> _gunclass >> "displayname"),_gunprice];
	};
	
	if (_gunclass iskindof "Bag_Base") then
	{
		_type = "backpack";
		_buystr = format ["%1 for $%2",getText(configfile >> "CfgVehicles" >> _gunclass >> "displayname"),_gunprice];
	};	

	_index = _listbox lbAdd _buystr;
	_listbox lbsetcolor [_index, [1,1,1,1]];

	//player globalchat format ["%1",_x];
} foreach storearray;

while {true} do
{
	_fundstr = format ["Funds: %1",funds];
	_fundstext = _display displayctrl 2006;
	_fundstext ctrlSetText _fundstr;
	
	sleep 1;
};