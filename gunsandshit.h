_ammoboxriflearray = 
[
	"hgun_Pistol_Signal_F",
	"bnae_saa_virtual",
	"bnae_spr220_virtual",
	"bnae_spr220_so_virtual"
];

_ammoboxitemarray = 
[
	"U_C_Poor_1",
	"U_C_Man_casual_5_F",
	"U_C_Man_casual_4_F",
	"U_C_Man_casual_1_F",
	"U_C_Man_casual_3_F",
	"U_C_Man_casual_2_F",
	"U_OrestesBody",
	"U_C_Journalist",
	"U_Marshal",
	"U_C_Mechanic_01_F",
	"U_Rangemaster",
	"U_I_G_Story_Protagonist_F",
	"U_BG_Guerilla1_2_F",
	"U_BG_Guerilla2_1",
	"U_BG_Guerilla2_2",
	"U_BG_Guerilla2_3",
	"U_BG_Guerilla3_1",
	"U_BG_Guerilla3_2",
	"U_OG_Guerilla2_3",
	"U_OG_Guerilla1_1",
	"U_OG_leader",
	"U_C_HunterBody_grn",
	"U_BG_Guerrilla_6_1",
	"U_BG_Leader",
	"U_BG_Guerilla1_2_F",
	"U_I_G_resistanceLeader_F",
	"H_Construction_headset_black_F",

	"H_Cap_blk_raven",
	"H_Cap_oli",
	"H_MilCap_gry",
	"H_BandMask_blk",
	"H_Bandanna_surfer_grn",
	"H_Bandanna_khk",
	"H_Shemag_olive",
	"H_Beret_blk",
	"H_Watchcap_camo",
	"H_StrawHat",
	"H_Hat_blue",
	"H_Hat_brown",
	"H_Hat_grey",
	"H_Hat_tan",
	"H_milcap_dgtl",
	"H_Booniehat_oli",
	"H_Bandanna_dgtl",
	"H_Hat_Safari_sand_F",
	"U_I_C_Soldier_Bandit_1_F",
	"U_I_C_Soldier_Bandit_2_F",
	"U_I_C_Soldier_Bandit_3_F",
	"U_I_C_Soldier_Bandit_4_F",
	"U_I_C_Soldier_Bandit_5_F",
	"V_Pocketed_black_F",
	"ItemWatch",
	"ItemCompass",
	"ItemRadio",
	"ItemMap",
	"FirstAidKit",
	"acc_flashlight",
	"acc_flashlight_pistol",
	"acc_flashlight_smg_01",
	"SAN_Headlamp_v2",
	"Medikit",
	"ToolKit",
	"G_Spectacles","G_Spectacles_Tinted","G_Combat","G_Lowprofile","G_Shades_Black","G_Shades_Green","G_Shades_Red","G_Squares","G_Squares_Tinted","G_Sport_BlackWhite","G_Sport_Blackyellow","G_Sport_Greenblack","G_Sport_Checkered","G_Sport_Red","G_Tactical_Black","G_Aviator","G_Lady_Mirror","G_Lady_Dark","G_Lady_Red","G_Lady_Blue","G_Diving","G_I_Diving","G_Balaclava_blk","G_Balaclava_oli","G_Balaclava_combat","G_Balaclava_lowprofile","G_Bandanna_blk","G_Bandanna_oli","G_Bandanna_khk","G_Bandanna_tan","G_Bandanna_beast","G_Bandanna_shades","G_Bandanna_sport","G_Bandanna_aviator","G_Shades_Blue","G_Sport_Blackred","G_Tactical_Clear","G_Combat_Goggles_tna_F","G_Respirator_base_F","G_Respirator_white_F","G_Respirator_yellow_F","G_Respirator_blue_F","G_EyeProtectors_base_F","G_EyeProtectors_F","G_EyeProtectors_Earpiece_F","G_WirelessEarpiece_base_F","G_WirelessEarpiece_F"
];

_ammoboxmagazinearray = 
[
	"Chemlight_red",
	"Chemlight_yellow",
	"Chemlight_blue",
	"Chemlight_green",
	"6Rnd_GreenSignal_F",
	"6Rnd_RedSignal_F",
	"6Rnd_357M_Magazine",
	"6Rnd_00_Buckshot_Magazine",
	"6Rnd_Slug_Magazine",
	"2Rnd_00_Buckshot_Magazine",
	"2Rnd_Slug_Magazine"
];

_ammoboxbackpackarray = 
[
	"B_Messenger_Coyote_F"
];

//[ammobox,_ammoboxriflearray,true] call xla_fnc_addVirtualWeaponCargo;
//[ammobox,_ammoboxitemarray,true] call xla_fnc_addVirtuaItemCargo;
//[ammobox,_ammoboxmagazinearray,true] call xla_fnc_addVirtualMagazineCargo;
//[ammobox,_ammoboxbackpackarray,true] call xla_fnc_addVirtuaBackpackCargo;