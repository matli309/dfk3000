private _prisoner = _this select 0;
private _caller = _this select 1;
private _actionid = _this select 2;

diag_log format ["take_prisoner %1 %2 %3",_prisoner,_caller,_actionid];

_prisoner removeaction _actionid;

if (alive _prisoner) then
{	

	_cashmode = ["dfk_storekind"] call BIS_fnc_getParamValue;
	
	//_caller addscore 20;
	diag_log format ["caller score 0 %1",getPlayerScores _caller];
	[_caller,3] remoteExec ["addscore",0,true];
	diag_log format ["caller score 1 %1",getPlayerScores _caller];
	
	hint "Prisoner taken!";
	
	[(10000 + (random 10000) + ((10000 + (random 10000)) * rankid _prisoner))] remoteExecCall ["dfk_add_funds",2];
	private _damp = [resistance,0] call BIS_fnc_respawnTickets;
	diag_log format ["take_prisoner 0 %1",_damp];
	private _val = _damp + ceil (random 5);
	diag_log format ["take_prisoner 1 %1",_val];
	_derp = [resistance, _val] call BIS_fnc_respawnTickets;
	diag_log format ["lives after taken prisoner %1",_derp];
	
	["TOOKPRISONER"] remoteexeccall ["DFK_fnc_remove_morale",2];
	["POW"] remoteexeccall ["DFK_fnc_remove_political",2];
};

deletevehicle _prisoner;