
_tracked = [];
_markers = [];

private _dfk_airradar = ["dfk_airradar"] call BIS_fnc_getParamValue;
//_dir0 = 0;
dfk_hackedEnemyRadar = 0;

while {_dfk_airradar > 0} do
{
	if (dfk_hackedEnemyRadar >= 3) then
	{
		{
			//diag_log _x;
			if (!(_x iskindof "Steerable_Parachute_F" or _x iskindof "O_Parachute_02_F") && (_x iskindof "Helicopter" or _x iskindof "Plane") && !(_x in _tracked) && alive _x) then
			{
				private _pos = [(getpos _x select 0) + (random 500) - (random 500),(getpos _x select 1) + (random 500) - (random 500)];
				private _m = "";
				if (side _x == east) then {_m = [_pos,"colorOPFOR","","hd_unknown",0.5] call dfk_generate_marker};
				if (side _x == civilian) then {_m = [_pos,"colorCIV","","c_air",0.5] call dfk_generate_marker};
				_tracked pushback _x;
				_markers pushback [_x,_m];
				private _type = "";
				if (_x iskindof "Helicopter") then {_type = "Helicopter"};
				if (_x iskindof "UAV_01_base_F" or _x iskindof "UAV_06_base_F") then {_type = "Drone"};
				if (_x iskindof "Plane") then {_type = "Fast-mover"};
				//airtrackerguy globalchat format ["New airborne contact! %1 coming from direction %2",_type,airtrackerguy getdir _x];
				[format ["New airborne contact! %1 coming from direction %2",_type,selectrandom playableunits getdir _x]] remoteexec ["systemchat"];
			};
		} foreach vehicles;
		
		{
			private _thingie = _x select 0;
			private _marker = _x select 1;
			
			if (!alive _thingie) 
			then 
			{
				_markers deleteat _foreachindex;
				deletemarker _marker;
			}
			else
			{
				private _pos = [];
				if (_dfk_airradar == 0) then {_pos = position _thingie};
				if (_dfk_airradar == 1) then {_pos = [(getpos _thingie select 0) + (random (speed _thingie * 4)) - (random (speed _thingie * 4)),(getpos _thingie select 1) + (random (speed _thingie * 4)) - (random (speed _thingie * 4))];};
				if (_dfk_airradar == 2) then {_pos = position _thingie};
				
				_marker setmarkerpos _pos;
			};
			
		} foreach _markers;
	};
	sleep 5;
};

//if (!alive airtrackerguy) then {hint "Airtrackerguy dead, no radar-air tracking for you!"};