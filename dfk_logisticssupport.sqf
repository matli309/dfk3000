_hkptypes = [];
//,"O_T_VTOL_02_vehicle_dynamicLoadout_F"

_target = _this select 0;

//_cargotypes = ["Land_Pod_Heli_Transport_04_ammo_F","Land_Pod_Heli_Transport_04_box_F","Land_Pod_Heli_Transport_04_fuel_F","Land_Pod_Heli_Transport_04_repair_F"];
//"CargoNet_01_barrels_F","CargoNet_01_box_F","O_CargoNet_01_ammo_F","C_IDAP_CargoNet_01_supplies_F",

createcenter east;

//group createUnit [type, position, markers, placement, special]

_xk = 0;
_yk = 0;

_arr = ["left","right","top","bottom"];

_side = selectrandom _arr;

#include "defines.h";

switch (_side) do
{
	case "left":
	{
		_xk = 0;
		_yk = random 3000 +random 3000 +random 3000 +random 3000;
	};
	case "right":
	{
		_xk = 12000;
		_yk = random 3000 +random 3000 +random 3000 +random 3000;
	};	
	case "top":
	{
		_xk = 12000;
		_yk = random 3000 +random 3000 +random 3000 +random 3000;
	};
	case "bottom":
	{
		_xk = random 3000 +random 3000 +random 3000 +random 3000;
		_yk = 0;
	};		
};

_pos = [_xk,_yk,300];

_hkp = createVehicle [selectrandom dfk_support_hkp, _pos, [], 100 + random 500, "FLY"];
//_hkp setpos [(getpos _hkp select 0),(getpos _hkp select 1),(getpos _hkp select 2) + 300];
_hkp setdir (_hkp getdir _target);
_hkp flyinheight 100 + random 200;
_crate = createVehicle [selectrandom dfk_support_cargotypes, _pos, [], 100, "NONE"]; 

if (debug) then {[_hkp,"colorred","hkp","hd_destroy",true,1] execvm "marker_track.sqf";[_crate,"colorpink","crate","hd_flag",true,1] execvm "marker_track.sqf";};

_hkp setvariable ["target",_target,true];

airlogisticsarray pushbackunique _hkp;
publicvariable "airlogisticsarray";

createvehiclecrew _hkp;
_grp = group driver _hkp;

private _id = ["airlift"] call dfk_generate_radioid;
_grp setGroupIdGlobal [_id];		

{_x addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,"KILLEDEAST"] execVM "killer.sqf";}]} foreach units _grp;
//[_hkp] remoteExec ["dfk_initvehicle",2];

{
	_poskind = _x;
	_n = _hkp emptyPositions _poskind;
	if (_n > 0) then
	{
		for "_derp" from 1 to _n do
		{
			//_dude = _grp createunit ["O_helipilot_F",,[],100,"NONE"];
			
			_dude = "";
			_dude = [_dude,_grp,[0,0,100],0,"hkppilot"] call dfk_spawn_soldier;
			
			if (_poskind == "commander") then {_dude moveincommander _hkp};
			if (_poskind == "driver") then {_dude moveindriver _hkp};
			if (_poskind == "gunner") then {_dude moveingunner _hkp};
			if (_poskind == "cargo") then {_dude moveincargo _hkp};
		};
	};
	//player globalchat format ["%1 %2 %3",_poskind,_n, count units _grp];
} foreach ["commander","gunner","driver","cargo"];

_hkp setslingload _crate;
_wp = _grp addwaypoint [getpos _target,0];
_wp setwaypointstatements ["true", "0 = [(getslingload (vehicle leader group this)),'unload',this getvariable 'target'] execvm 'dfk_cargochute.sqf';(vehicle leader group this) setslingload objnull"];

_wphome = _grp addwaypoint [[0,8000,0],0];
_wphome setwaypointstatements ["true", "[group this,getpos this] remoteExec ['dfk_removegroup',2]"];

_grp setspeedmode "FULL";
_grp setbehaviour "AWARE";
_grp setcombatmode "BLUE";

if (debug) then {_grp setspeedmode "FULL";_grp setbehaviour "CARELESS";_grp setcombatmode "BLUE";};

_drop = false;

while {!_drop} do
{
	_crewdmg = [];
	_crewmorale = [];
	
	_crewded = false;
	{
		_crewdmg pushback (damage _x);
		_crewmorale pushback (morale _x);
		if (!alive _x) then {_crewded = true};
	} foreach units _grp;

	_derp = getAllHitPointsDamage _hkp;
	_herp = [];
	_hitlist= [];
	for "_i" from 0 to ((count (_derp select 0)) - 1) do
	{
		if (_derp select 2 select _i > 0.5 && !((_derp select 0 select _i) in _hitlist)) then
		{
			_herp = _herp + [[_derp select 0 select _i,_derp select 2 select _i]];
			_hitlist = _hitlist + [_derp select 0 select _i];
		};
	};
	
	_crithit = false;
	_criticalparts = ["hitfuel","hitengine","HitEngine2","HitEngine3","HitHRotor","HitVRotor","HitWinch_Source"];
	{
		if (_x in _criticalparts) then {_crithit = true};
	} foreach _hitlist;
	
	//morale driver _hkp < (0.8 + _badass)
	if (_crithit OR (fuel _hkp < 0.2) OR _crewded or !alive _hkp) then
	{
		_drop = true;
		_hkp setslingload objnull;
		0 = [_crate,"panic",_hkp getvariable "target"] execvm "dfk_cargochute.sqf";
		
		{
			_x setwaypointstatements ["true", ""];
			_x setwppos getpos leader _grp;
			_x setWaypointCompletionRadius 2000;
		} foreach waypoints _grp;

		_wpflee = _grp addwaypoint [_pos,0];
		_wpflee setwaypointstatements ["true","[group this,getpos this] remoteExec ['dfk_removegroup',2]"];
		_grp setcombatmode "BLUE";
		_grp setspeedmode "FULL";		
	};
	
	//hint format ["Drop %9\nDist %10\nDmg %1\nFuel %2\nCrewdmg %3\nCrewmorale %4\nSlingload: %5\nCanfly %6\n%7\n%8",damage _hkp,fuel _hkp,_crewdmg,_crewmorale,getslingload _hkp,canmove _hkp,_herp,_hitlist,_drop,player distance _hkp];
	//[format ["logisticssupport | %1 %2 %3",morale driver _hkp,_crewmorale,_badass]] remoteExec ["systemchat"]; 	
	sleep 1;
};