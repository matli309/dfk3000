
private _allguns = (ammoboxarray select 0) call xla_fnc_getVirtualWeaponCargo;
private _backpacks = (ammoboxarray select 0) call xla_fnc_getVirtualBackpackCargo;
private _allitems = (ammoboxarray select 0) call xla_fnc_getVirtualItemCargo;
private _vests = [];
private _pistols = [];
private _uniforms = [];
private _scopes = [];
private _launchers = [];
private _guns = [];

private _retardvests = [];
{
	if (_x iskindof ["Vest_Camo_Base", configFile >> "CfgWeapons"] && getContainerMaxLoad _x > 40) then {_vests pushback _x};
	if (_x iskindof ["Vest_Camo_Base", configFile >> "CfgWeapons"] && getContainerMaxLoad _x < 40 && getContainerMaxLoad _x > 0) then {_retardvests pushback _x};
	if (_x iskindof ["Uniform_Base", configFile >> "CfgWeapons"]) then {_uniforms pushback _x};
	//if ("optic" in _x) then {_scopes pushback _x};
} foreach _allitems;

//configfile >> "CfgWeapons" >> "V_RebreatherIA" >> "ItemInfo" >> "containerClass"

private _actualbackpacks = [];
//remove 'useless shit'
{
	if (!(_x in ["I_UAV_01_backpack_F","I_GMG_01_A_weapon_F","I_HMG_01_A_weapon_F","I_HMG_01_weapon_F","I_GMG_01_weapon_F","I_Mortar_01_support_F","I_Mortar_01_weapon_F","I_HMG_01_support_F","I_HMG_01_support_high_F","I_AA_01_weapon_F","I_AT_01_weapon_F","B_Parachute"])) then
	{
		_actualbackpacks pushback _x;
	};
} foreach _backpacks;

{
	_added = false;
	if (_x iskindof ["Pistol_Base_F", configFile >> "CfgWeapons"]) then {_pistols pushback _x;_added = true};
	if (_x iskindof ["Launcher_Base_F", configFile >> "CfgWeapons"]) then {_launchers pushback _x; _added = true};
	if (_x iskindof ["Weapon_arifle_SDAR_F", configFile >> "CfgWeapons"]) then {_guns deleteat _foreachindex; _added = true};
	if (!_added) then {_guns pushback _x};
} foreach _allguns;

// diag_log "Guns backpacks scopes vests uniforms pistols launchers";
// diag_log _guns;
// diag_log _actualbackpacks;
// diag_log _scopes;
// diag_log _vests;
// diag_log _uniforms;
// diag_log _pistols;
// diag_log _launchers;

while {count (units (group (_this select 1))) < 12} do
{
	_p = [position (_this select 1), 1, 25, 3, 0, 0.5, 0] call BIS_fnc_findSafePos;
	_retard = group (_this select 1) createUnit ["I_G_Soldier_lite_F", [0,0,1000], [], 0, "FORM"];
	_retard setpos _p;

	_retard addMPEventHandler ["mpkilled", {[missionNamespace,-1] call BIS_fnc_respawnTickets; Null = [_this select 0] spawn dfk_deleteretard;}];
	_retard setvariable ["retard",true,true];
	
	#include "defines.h";

	// #ifndef sanctionedmagsarray
		// #define sanctionedmagsarray sanctionedmagsarray_def
	// #endif

	removeAllWeapons _retard;
	removeAllItems _retard;
	removeAllAssignedItems _retard;
	removeUniform _retard;
	removeVest _retard;
	removeBackpack _retard;

	_retard forceAddUniform selectrandom _uniforms;

	_retard addItemToUniform "FirstAidKit";
	_retard addItemToUniform "FirstAidKit";
	_retard linkItem "ItemRadio";
	_retard linkItem "ItemMap";
	_retard linkItem "ItemCompass";

	_gun = "BAAAAJS";
	if (count _guns <= 0 && count _pistols > 0) then
	{
		_gun = selectrandom _pistols;
	};
	if (count _guns > 0 && count _pistols > 0) then
	{
		_gun = selectrandom _guns;
	};	
	if (count _guns > 0 && count _pistols <= 0) then
	{
		_gun = selectrandom _guns;
	};		

	if (count _scopes > 0) then {_retard addprimaryweaponitem selectrandom _scopes};
	if ("acc_flashlight" in _allitems && !("acc_pointer_IR" in _allitems)) then {_retard addPrimaryWeaponItem "acc_flashlight"};
	if (!("acc_flashlight" in _allitems) && "acc_pointer_IR" in _allitems) then {_retard addPrimaryWeaponItem "acc_pointer_IR"};
	if ("NVGoggles_INDEP" in _allitems) then {_retard linkItem "NVGoggles_INDEP"} else {_retard linkItem "SAN_Headlamp_v2"};

	// _dude addPrimaryWeaponItem "acc_flashlight";
	// _dude addPrimaryWeaponItem "optic_Yorris";
	// _dude addHandgunItem "acc_flashlight_pistol";
	
	_tempmags = getarray(configfile >> "CfgWeapons" >> _gun >> "magazines");
	_sanctionedmags = [];
	private _glmags = [];
	
	{
		if (_x in sanctionedmagsarray) then {_sanctionedmags pushback _x};
	} foreach _tempmags;

	private _mz = getarray(configfile >> "CfgWeapons" >> _gun >> "muzzles");
	
	if (count _mz >= 2) then
	{
		if ("EGLM" in _mz or "GL_3GL_F" in _mz) then
		{
			private _derp = [
						"1Rnd_HE_Grenade_shell",
						"1Rnd_Smoke_Grenade_shell",
						"1Rnd_SmokeRed_Grenade_shell",
						"1Rnd_SmokeGreen_Grenade_shell",
						"1Rnd_SmokeYellow_Grenade_shell",
						"1Rnd_SmokePurple_Grenade_shell",
						"1Rnd_SmokeBlue_Grenade_shell",
						"1Rnd_SmokeOrange_Grenade_shell",
						"UGL_FlareWhite_F",
						"UGL_FlareGreen_F",
						"UGL_FlareRed_F",
						"UGL_FlareYellow_F"
					];
					
			_glmags append _derp;
		};
				
		if ("Secondary" in _mz) then
		{
			_derp = [
							"10Rnd_50BW_Mag_F"
					];
					
			_sanctionedmags append _derp;
		};			
	};			
	
	_mag = "";
	_mag = (selectrandom _sanctionedmags);

	_try = 0;
	
	if (count _vests > 0) then 
	{
		_retard addvest (selectrandom _vests);
		while {_retard canAddItemToVest _mag && loadAbs _retard < 1000 && _try < 500} do
		{
			_retard additemtovest _mag;
			if (count _glmags > 0 && random 1 < 0.33) then
			{
				_mag = (selectrandom _glmags);
			}
			else
			{
				_mag = (selectrandom _sanctionedmags);
			};
			_try = _try + 1;
		};
	};		
	
	if (count _retardvests > 0 && count _vests < 0) then 
	{
		_retard addvest (selectrandom _retardvests);
		while {_retard canAddItemToVest _mag && loadAbs _retard < 1000 && _try < 500} do
		{
			_retard additemtovest _mag;
			if (count _glmags > 0 && random 1 < 0.33) then
			{
				_mag = (selectrandom _glmags);
			}
			else
			{
				_mag = (selectrandom _sanctionedmags);
			};
			_try = _try + 1;
		};
	};		

	if (count _actualbackpacks > 0 && count _launchers > 0 && ((1000 - loadabs _retard) >= 600)) then
	{
		_retard addbackpack (selectrandom _actualbackpacks);
		_l = selectrandom _launchers;

		_launchermags = getarray(configfile >> "CfgWeapons" >> _l >> "magazines");
		_launchermag = selectrandom _launchermags;
		while {_retard canAddItemToBackpack _launchermag && loadabs _retard < 1000 && _try < 500} do
		{
			_retard additemtoBackPack _launchermag;
			_launchermag = selectrandom _launchermags;
			_try = _try + 1;
		};		
		_retard addweapon _l;		
		_retard additemtoBackPack _launchermag;
	}
	else
	{
		if (count _actualbackpacks > 0) then 
		{
			_retard addbackpack (selectrandom _actualbackpacks);
			while {_retard canAddItemToBackpack _mag && loadabs _retard < 1000 && _try < 500} do
			{
				_retard additemtoBackPack _mag;
				_mag = (selectrandom _sanctionedmags);
				_try = _try + 1;
			};		
		};
	};
	
	_retard additemtouniform (selectrandom _sanctionedmags);
	_retard addWeapon _gun;	
	_mag = (selectrandom _sanctionedmags);
	while {_retard canAddItemTouniform _mag && loadabs _retard < 1000 && _try < 500} do
	{
		_retard additemtouniform (selectrandom _sanctionedmags);
		_try = _try + 1;
	};
	while {_retard canAddItemTovest _mag && loadabs _retard < 1000 && _try < 500} do
	{
		_retard additemtovest (selectrandom _sanctionedmags);
		_try = _try + 1;
	};	
	while {_retard canAddItemTobackpack _mag && loadabs _retard < 1000 && _try < 500} do
	{
		_retard canAddItemTobackpack (selectrandom _sanctionedmags);
		_try = _try + 1;
	};	
};

// while {count (units (group (_this select 1))) > 0} do
// {	
	// {
		// if (lifeState _x == "INCAPACITATED") then
		// {
			// _x setunconscious false;
			// _x setcaptive false;
			// _x setdamage 0;
		// } ;
	// } foreach units group (_this select 1);
	// sleep 5;
// };

// waituntil {!alive _retard};
// sleep 3;
// deletevehicle _retard;
// there is no such thing as a free lunch!
// [missionNamespace,-1] call BIS_fnc_respawnTickets;