_str = "";

//count?
//COUNT!
//count whom?
//EVERYONE!!!

_alive_veh = 0;
_dmg_veh = 0;
_junk_veh = 0;
_dead_veh = 0;
_unseen_veh = 0;

_stuck_veh = 0;

_veh_car = 0;
_veh_boat = 0;
_veh_thing = 0;
_veh_static = 0;
_veh_air = 0;
_veh_else = 0;
_veh_else_arr = [];

_deleteque = [];
_deletegrpque = [];
_stuckgrp = [];
//groups
_str = _str + str format ["AllGroups: %1\n",(count allgroups)];


_deadgrps = 0;
{
	_grp = _x;
	private _cnt = 0;
	{
		if (alive _x) then {_cnt = _cnt + 1};
	} foreach units _grp;
	//_cnt = {alive _x} count units _grp;
	if (_cnt <= 0) then {deletegroup _grp;_deadgrps = _deadgrps + 1};
	
	if ((_x getvariable ["qrf",false]) or (_x getvariable ["patrol",false])) then 
	{
		private _lastpos = _x getvariable ["lastpos",[]];
		if (_lastpos isEqualto []) then
		{
			_x setvariable ["lastpos",getpos leader _x,true];
		}
		else
		{
			if (_lastpos distance getpos leader _x < 10) then 
			{
				//diag_log format ["this group %1 is obvs stuck and/or retarded delete! moved %2 since last update",_x,getpos leader _x distance _lastpos];_stuckgrp pushBackUnique _x;
			}
			else
			{
				_b = _x getvariable ["qrf","derp"];
				_c = _x getvariable ["patrol","derp"];
				_d = _lastpos distance getpos leader _x;
				//diag_log format ["debugstatus test01 %1 %2 %3 %4 %5 distance %6",_x,getpos leader _x,_lastpos,_b,_c,_d];
			};
		};
		
		if (vehicle leader _x == leader _x) then 
		{
			//diag_log format ["debugstatus test02 %1 group has lost it's vehicle, queing for remove",_x,vehicle leader _x];
			_stuckgrp pushBackUnique _x;
		};
		//diag_log format ["debugstatus trackgroup %1 lastpos: %2 vs %3 %4 %5",_x,_x getvariable "lastpos",getpos leader _x,(_x getvariable ["qrf",false]),(_x getvariable ["patrol",false])];
	};
} foreach allgroups;
_str = _str + str format ["Dead groups: %1\n",_deadgrps];

_tot_veh = count vehicles;
{
	_typed = false;
	if (!alive _x && !(_x getvariable ["important",false])) then
	{
		_dead_veh = _dead_veh + 1;
		_deleteque pushBackUnique _x;
	}
	else
	{
		_alive_veh = _alive_veh + 1;
		if (damage _x > 0) then {_dmg_veh = _dmg_veh + 1};
		if (_x iskindof "AllVehicles" && (!canmove _x or (fuel _x <= 0)) && !(_x getvariable ["donotdelete",false])) then {_junk_veh = _junk_veh + 1;_deleteque pushBackUnique _x;};
		
		if (_x iskindof "LandVehicle" && !_typed) then {_veh_car = _veh_car + 1;_typed = true};
		if (_x iskindof "Ship" && !_typed) then {_veh_boat = _veh_boat + 1;_typed = true};
		if (_x iskindof "Air" && !_typed) then {_veh_air = _veh_air + 1;_typed = true};
		if (_x iskindof "Thing" && !_typed) then {_veh_thing = _veh_thing + 1;_typed = true};
		if (_x iskindof "Static" && !_typed) then {_veh_static = _veh_static + 1;_typed = true};
		if (!_typed) then {_veh_else = _veh_else + 1;_veh_else_arr pushBackUnique (typeof _x)};
		_v = _x;
		{
			if (_v distance _x > viewdistance) then {_unseen_veh = _unseen_veh + 1};
			if (_v iskindof "groundWeaponHolder" && _v distance _x > 200) then {_deleteque pushbackunique _x};
		} forEach playableUnits;
		
		
		
		if (count (_x getvariable ["lastpos",[]]) == 0 && (_x iskindof "Vehicle") && !(_x getvariable ["important",false]) && !(_x getvariable ["donotdelete",false])) then 
		{
			_x setvariable ["lastpos",getpos _x,true];
		}
		else
		{
			if (_x getvariable "lastpos" distance getpos _x < 10 && !(_x getvariable ["important",false]) && !(_x getvariable ["donotdelete",false])) then {_stuck_veh = _stuck_veh + 1;_deleteque pushbackunique _x};
		};
	};
} foreach vehicles;

_str = _str + str format 
	[
		"
			All vehicles: %1\n
			Alive vehicles: %2\n
			Damaged  vehicles: %3\n
			Derelict vehicles: %4\n
			Dead vehicles: %5\n
			Vehicles outside viewdistance: %6\n
			Vehicles that haven't moved much since last check: %7\n
			LandVehicles: %8\n
			Boat: %9\n
			Air: %10\n
			Thing: %11\n
			Static: %12\n
			Other: %13\n
			Other arr: %14\n
		",
		_tot_veh,
		_alive_veh,
		_dmg_veh,
		_junk_veh,
		_dead_veh,
		_unseen_veh,
		_stuck_veh,
		_veh_car,
		_veh_boat,
		_veh_air,
		_veh_thing,
		_veh_static,
		_veh_else,
		_veh_else_arr
	];

_str = _str + str format 
	[
		"
			All alive units: %1\n
			All dead men: %2\n
		",
		count allunits,
		count alldeadmen
	];

{_deleteque pushBackUnique _x} foreach alldeadmen;
	
_str = _str + str format 
	[
		"
			All UAVs/Drones/Autoturrets: %1\n
		",
		count allUnitsUAV
	];	

_str = _str + str format 
[
	"
		All Mines: %1\n
	",
	count allMines
];	

//[format ["%1",_str]] remoteexec ["hint",0,true];
//sleep 10;
//[format ["%1",_deleteque]] remoteexec ["hint",0,true];
//diag_log _str;
//diag_log _deleteque;
//sleep 10;

/* diag_log format ["debugdelete empty groups %1 %2",count _deletegrpque,_deletegrpque];
{
	_grp = _x;
	_cnt = {alive _x} count units _grp;
	if (_cnt <= 0) then
	{
		deletegroup _grp;
	}
		else
	{
		diag_log format ["grp not empty %1 %2",_grp,_cnt];
	};
} foreach _deletegrpque;
diag_log count _deletegrpque; */

{
	_deletegrp = _x;
	_delete = true;

	{
		_player = _x;
		if (leader _deletegrp distance _player < viewdistance) then 
		{
			_delete = false;
		};
	} foreach playableUnits;	
	
	if (_delete) then
	{
		//diag_log format ["delete stuck group %1", _x];
		{
			deletevehicle vehicle _x;
			deletevehicle _x;
		} foreach units _deletegrp;
		deletegroup _deletegrp;
	};
	
} foreach _stuckgrp;

{
	//[_x,"colorblack","","hd_destroy",1] remoteExec ["dfk_generate_marker",2];
	_deleteme = _x;
	_kill = true;
	{
		_player = _x;
		//<1200 - delete
		//>300 dont delete
		// 300-1200 check visiblity -> delete
		private _dist = _deleteme distance _player;
		if (_dist < viewdistance) then 
		{
			if (_dist <= 300) then
			{
				_kill = false;
			}
			else
			{
				private _derp = getpos _deleteme;
				_derp set [2,(_derp select 2) + 1];
				if ([objNull, "VIEW"] checkVisibility [eyePos _player, _derp] > 0) then
				{
					_kill = false;
				};
			};
		};
	} foreach playableUnits;
	
	if (_kill) then {
						//diag_log format ["delete %1", _x];
						{
							deletevehicle _x;
						} foreach crew _deleteme;
						deletevehicle _deleteme;
					};
} foreach _deleteque;

//["Done."] remoteexec ["hint",0,true];