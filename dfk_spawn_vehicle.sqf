_gen = _this select 0;
_caller = _this select 1;
_id = _this select 2;
_arr1 = _this select 3;

//if (isServer) then
//{
_cartype = _arr1 select 0;
_lvl = _arr1 select 1;
_dudelvl = _caller getvariable ["gunlvl",0];
//[format ["%1 %2 %3 %4 %5 %6",_this,_gen,_caller,_id,_arr1,_cartype]] remoteexec ["systemchat"];

if (_dudelvl >= _lvl) then
{
	_veh = createVehicle [_cartype, [0,0,1000], [], 25, "NONE"];
	//_veh forceflagtexture flagkind;
	[_veh,flagkind] remoteexec ["forceflagtexture"];

	_pos = [];
	_pos = [position _gen, 1, 40, 7, 0, 0.5, 0] call BIS_fnc_findSafePos;
	if (count _pos > 0) then {_veh setpos _pos;};

	[_veh] remoteExec ["dfk_initvehicle",2];
	[_veh,"colorGUER","","hd_dot",true,1] execvm "marker_track.sqf";
	_veh setvariable ["important",true,true];

	dfk_spawned_vehicles = dfk_spawned_vehicles + 1;
	publicvariable "dfk_spawned_vehicles";
}
else
{
	[format ["%1 requires level %2, current level %3",gettext (configfile >> "CfgVehicles" >> _cartype >> "displayname"),_lvl,_dudelvl]] remoteexec ["systemchat"];
};

//};