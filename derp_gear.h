_rifles = 
[
		"arifle_Mk20C_F",
		"arifle_TRG20_F",
		"SMG_01_F",
		"SMG_02_F",
		"SMG_05_F",
		"hgun_Pistol_01_F",
		"arifle_TRG21_F",
		"arifle_AKM_F",
		"arifle_AKS_F",
		"hgun_P07_F",
		"hgun_Pistol_heavy_01_F",
		"arifle_Mk20_GL_ACO_F",
		"arifle_TRG21_GL_F"		
];
_items = 
[
	"MineDetector",
	"Binocular",
	"NVGoggles_INDEP",
	"Rangefinder",
	"Laserdesignator_03",
	"Medikit",
	"ToolKit",
	"I_UavTerminal",
	"acc_pointer_IR"
];
_mags = 
[
	"Laserbatteries"
];

_ammoboxriflearray append _rifles;
_ammoboxitemarray append _items;
_ammoboxmagazinearray append _mags;

