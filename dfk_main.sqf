//init
//variables and shit
diag_log "DFK_MAIN INIT";

//adding stuff to the built-in BIS respawn functions/modules
//this is the selectable loadout upon respawn


//respawnTemplates[] = {"MenuInventory"};

//debug stuff, to track whatever is inited in MP
hint "IR DFK MAIN lel";
systemchat "IR SERVER";
"hello this should run everywhere" remoteExec ["hint"]; 
myJipID = "hello, this should run on JIP" remoteExec ["systemchat", -2, true]; 
myJipID2 = "hello this will only display on server/host" remoteExec ["systemchat", 2, true]; 

airlogisticsarray = [];
vehiclearray = [];
groupsarray = [];
eastunitsarray = [];

//set game values after mission params
sidemorale =  ["init_morale"] call BIS_fnc_getParamValue;
logistics = ["init_logistics"] call BIS_fnc_getParamValue;
politics = ["init_politics"] call BIS_fnc_getParamValue;

dfk_storekind_type = ["dfk_storekind"] call BIS_fnc_getParamValue;

//init and track number of alive dudes on the enemy side
aliveenemies =  ["battalionsize"] call BIS_fnc_getParamValue;
originalnumberofdudes = aliveenemies;

//flagtype
private _flag =  ["flagtype"] call BIS_fnc_getParamValue;
//{"Jolly Roger","Altis","Altis Colonial","AAF","FIA","CTRG","Syndicat"};
//"\A3\Data_F_Exp\Flags\flag_GEN_CO.paa"
_root = parsingNamespace getVariable "MISSION_ROOT";
flagkind = [_root + "jr.jpg",_root + "fredag.jpg","\A3\Data_F\Flags\flag_Altis_co.paa","\A3\Data_F\Flags\flag_altiscolonial_co.paa","\A3\Data_F\Flags\flag_AAF_co.paa","\A3\Data_F\Flags\flag_FIA_co.paa"] select _flag;

publicvariable "flagkind";

//init globalvariables for funds and secrets
secrets = 0;
secrets_all = secrets;
secrets_used = secrets;
funds = ["init_funds"] call BIS_fnc_getParamValue;
rabatt = ["init_discount"] call BIS_fnc_getParamValue;
funds_all = funds;
funds_spent = funds;

//reference arrays for the good, bad and ugly side
BADFACTION = ["OPF_F","OPF_G_F","OPF_T_F","OPF_V_F"];
GOODFACTION = ["IND_F","IND_G_F","BLU_F","BLU_G_F","BLU_CTRG_F","BLU_GEN_F","BLU_T_F","IND_C_F"];
CIVFACTION = ["CIV_F","CIV_IDAP_F"];

//referencearray for artillery
artilleryarray = [];
artblacklist = [];
islandblacklist = [];
artcooldown = 120;
qrfcooldown = 600;
lastqrf = time;

//enemy mainbase spawnpoints
base = getpos baselogic;
base_h = getpos baselogic_h;
base_kp = getpos baselogic_kp;

//initparam campsize, potentially lowers performance with a whole bunch of crap spaning around town loot
//hence default 0
campsize = 0;
campsize = ["campsitesize"] call BIS_fnc_getParamValue;

max_trummer = 0;
max_trummer = ["max_trummer"] call BIS_fnc_getParamValue;

spawn_distance = 800;
spawn_distance = ["spawn_distance"] call BIS_fnc_getParamValue;

//init params for victory and timemultiplier
private _victoryconditions = ["victoryconditions"] call BIS_fnc_getParamValue;
settimemultiplier (["timemultiplier"] call BIS_fnc_getParamValue);
nerfgear = false;
private _asd = ["nerfgear"] call BIS_fnc_getParamValue;
if (_asd > 0) then {nerfgear  = true};

globalminevar = 0;
globalminevar = ["mines"] call BIS_fnc_getParamValue;

dfk_radiosnooperability = 0;
dfk_radiosnooperability = ["dfk_snooper"] call BIS_fnc_getParamValue;
publicvariable "dfk_radiosnooperability";
//qrf, resupply (air and ground), artillery, mercs

res_civkills_array = [];
res_civkills_vehcost = 0;
opfor_civkills = 0;
opfor_civkills_vehcost = 0;

//include defines
#include "defines.h";
#include "dfk_functions.sqf";
#include "dfk_functions_logisticstuffarray.h";
#include "dfk_store.sqf";

//rentacop (totally not cops! hired thugs with guns posing as some sort of security force)
//merc 'security' force
//set flag
//TODO: draw funny milcorp/rentacop flags and other stuff like caps, vests etc

dfk_rentacopflag = selectrandom dfk_rentacopflagtypes;
publicvariable "dfk_rentacopflag";

//determine when logistics need to be refilled, and when it's to expensive to do so
private _targetlogisticslevel = (["targetlogisticslevel"] call BIS_fnc_getParamValue);
private _politicssupportthreshhold = (["politicssupportthreshhold"] call BIS_fnc_getParamValue);
//diag_log format ["_targetlogisticslevel %1 %2 _politicssupportthreshhold %3 %4",_targetlogisticslevel,(["targetlogisticslevel"] call BIS_fnc_getParamValue),_politicssupportthreshhold,(["politicssupportthreshhold"] call BIS_fnc_getParamValue)];

//limit reinforcements
dfk_qrf_pool_limit = (["qrfpool"] call BIS_fnc_getParamValue);
dfk_qrf_pool_array = [];
dfk_qrf_hkp_timer = -86400;

//enemy vehicles locked?
private _derplock = (["locked_vehicles"] call BIS_fnc_getParamValue);
dfk_locked_vehicles = false;
if (_derplock == 1) then
{
	dfk_locked_vehicles = true;
};

private _dfk_town_spawnchance = (["spawn_chance"] call BIS_fnc_getParamValue) * 0.1;

//array for safehouse ammoboxes
ammoboxarray = [];

//define triggersize for spawning troops in towns and bases
//used to be 800, but trying 600 for performance's sake
_defaulttowntriggersize = 800;
_largetowntriggersize = 800;
_smalltowntriggersize = 600;

systemchat "INIT1";

//spawn ze trigger
//dunno if this is used anymore
private _trg = createTrigger ["EmptyDetector", [5000,6000,0]];
_trg setTriggerArea [15000, 15000, 15000, false];
_trg setTriggerActivation ["ANY", "PRESENT", true];
_trg setTriggerStatements ["this", "", ""];

//init editor-placed vehicles
{
	_x addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,"Vehicle"] execVM "killer.sqf";}];
} foreach vehicles;

dfk_drugrunnerlevel = 0;
dfk_drugrunnerdelivery = 0;
dfk_drugrunnertarget = 5;

drugrunnercrate1 allowdamage false;
drugrunnercrate2 allowdamage false;

drugrunnercrate1 enableSimulationGlobal true;
drugrunnercrate2 enableSimulationGlobal true;

//,[],1.5,true,false,"!isnull(backpackcontainer _target)"
[drugrunnercrate2, ["Deposit deliverybackpack", {[_this] execvm "dfk_deliveryscript.sqf"}]] remoteExec ["addAction",0,true];

[drugrunnercrate1,"colorUNKNOWN","Pick-up point Alpha","hd_pickup",true,1] execvm "marker_track.sqf";
[drugrunnercrate2,"colorUNKNOWN","Delivery point Omega","hd_end",true,1] execvm "marker_track.sqf";

[] remoteExec ["dfk_generate_deliverypoint",2];
systemchat "Drugrunner done";
{
	// [_x, ["Spawn Motorboat","dfk_spawn_boat.sqf",["C_Boat_Civil_01_F"]]] remoteExec ["addAction",0,true];
	// [_x, ["Spawn RHIB","dfk_spawn_boat.sqf",["C_Boat_Transport_02_F"]]] remoteExec ["addAction",0,true];
	// [_x, ["Spawn Water scooter","dfk_spawn_boat.sqf",["C_Scooter_Transport_01_F"]]] remoteExec ["addAction",0,true];
	// [_x, ["Spawn Rubber boat","dfk_spawn_boat.sqf",["C_Rubberboat"]]] remoteExec ["addAction",0,true];
	// [_x, ["Spawn SDV","dfk_spawn_boat.sqf",["I_SDV_01_F"]]] remoteExec ["addAction",0,true];
	
	_boatspawn = _x;
	_boatspawn enableSimulationGlobal true;
	{	
		private _str = str format ["Spawn %1 @ level %2",gettext (configfile >> "CfgVehicles" >> _x select 0 >> "displayname"),_x select 1];
		[_boatspawn, [_str,"dfk_spawn_boat.sqf",[_x select 0,_x select 1]]] remoteExec ["addAction",0,true];
	} foreach dfk_boatspawn_types;	
	
	[getpos _x,"colorblue","Boat spawn","loc_civildefense",0.3] remoteExec ["dfk_generate_marker",2];
} foreach dfk_boatspawnlist;
systemchat "Boatpspawns done";
//["marker_track.sqf",drugrunnercrate2,"colorUNKNOWN","Delivery point Omega","hd_end",true,1] remoteexec ["execvm",2];
//["marker_track.sqf",drugrunnercrate1,"colorUNKNOWN","Pick-up point Alpha","hd_pickup",true,1] remoteexec ["execvm",2];

{
	_airspawn = _x;
	_airspawn enableSimulationGlobal true;
	{	
		private _str = str format ["Spawn %1",gettext (configfile >> "CfgVehicles" >> _x >> "displayname")];
		[_airspawn, [_str,"dfk_spawn_vehicle.sqf",[_x]]] remoteExec ["addAction",0,true];
	} foreach dfk_airspawn_types;	
	
} foreach dfk_airspawnlist;

Null = [] execvm "dfk_track_air.sqf";

{
	[getpos _x,dfk_minefield_minetypes,true,400,200] call dfk_mine_site_init;
} foreach dfk_minefields;

systemchat "Minefields done";
//set respawntickets
private _lives = ["lives"] call BIS_fnc_getParamValue;
[resistance,(_lives)] call BIS_fnc_respawnTickets;
private _init_lives = _lives;

dfk_spawned_vehicles = 0;
dfk_opfor_vehcost = 0;

dfk_ai_aimingAccuracy = (["AIaccuracy"] call BIS_fnc_getParamValue)/10;
dfk_ai_aimingShake = (["AIshake"] call BIS_fnc_getParamValue)/10;
dfk_ai_aimingSpeed = (["AIaimspeed"] call BIS_fnc_getParamValue)/10;
dfk_ai_spotDistance = (["AIspotdistance"] call BIS_fnc_getParamValue)/10;
dfk_ai_spotTime = (["AIspotTime"] call BIS_fnc_getParamValue)/10;
dfk_ai_courage = (["AIcourage"] call BIS_fnc_getParamValue)/10;
dfk_ai_reloadSpeed = (["AIreload"] call BIS_fnc_getParamValue)/10;
dfk_ai_commanding = (["AIcommanding"] call BIS_fnc_getParamValue)/10;
dfk_ai_general = (["AIgeneral"] call BIS_fnc_getParamValue)/10;

systemchat "AI spec inited";

//set init arsenal loadout, store content, gunloot options and/or gunloot crates content, gunloot crates spawn code is located below safehouses
//ammoboxes bliped at safehouse stuff
private _arse = ["arsenal_level"] call BIS_fnc_getParamValue;

private _gunloot = ["gunloot"] call BIS_fnc_getParamValue;

//gunloot random, store and vipstore intact
if (_gunloot == 2) then 
{
	gunlootarray append storearray;
	gunlootarray append vipstorearray;
	publicvariable "gunlootarray";
};

//negate store and vipstore, set arsenal to very basic
//add store/vipstore content to gunloot,spawn crates randomly
if (_gunloot == 3 && _arse >= 2) then 
{
	_arse == 2;
	
	gunlootarray append storearray;
	gunlootarray append vipstorearray;
	publicvariable "gunlootarray";
	storearray = [];
	vipstorearray = [];
	publicvariable "storearray";
	publicvariable "vipstorearray";
};

//negate _arse / arsenal_level to not include vipstore
//add vipstore content to gunloot, spawn crates randomly
if (_gunloot == 4 && _arse >= 4) then
{
	_arse == 4;
	
	gunlootarray append vipstorearray;
	publicvariable "gunlootarray";
	vipstorearray = [];
	publicvariable "vipstorearray";	
};

private _ammoboxriflearray = [];
private _ammoboxitemarray = [];
private _ammoboxmagazinearray = [];
private _ammoboxbackpackarray = [];

//best variable name ever
switch (_arse) do
{
	//empty
	case 1:
	{
		#include "gunsandshit.h";
	};
	
	//basic stuff (default)
	case 2:
	{
		#include "gunsandshit.h";
	};
	
	case 3:
	{
		#include "gunsandshit.h";	
		#include "derp_gear.h";
	};
	
	case 4:
	{
		#include "gunsandshit.h";	
		funds = 1000;
		private _old_rabatt = rabatt;
		rabatt = 0;
		
		_ammoboxmagazinearray append ["Chemlight_green","Chemlight_red","Chemlight_yellow","Chemlight_blue"];	
		_ammoboxmagazinearray append ["HandGrenade","MiniGrenade","I_IR_Grenade"];	
		_ammoboxmagazinearray append ["SmokeShell","SmokeShellRed","SmokeShellGreen","SmokeShellYellow","SmokeShellPurple","SmokeShellBlue","SmokeShellOrange"];		
		
		{
			private _type = "";
	
			private _gunclass = _x select 0;
		
			if (_gunclass iskindof ["Rifle", configFile >> "CfgWeapons"] or _gunclass iskindof ["Pistol", configFile >> "CfgWeapons"] or _gunclass iskindof ["Launcher", configFile >> "CfgWeapons"] or _gunclass iskindof ["Mgun", configFile >> "CfgWeapons"]) then
			{
				_ammoboxriflearray append [_gunclass];
				private _tempmags = getarray(configfile >> "CfgWeapons" >> _gunclass >> "magazines");
				private _sanctionedmags = [];
				
				private _mz = getarray(configfile >> "CfgWeapons" >> _gunclass >> "muzzles");
				if (count _mz >= 2) then
				{
					if ("EGLM" in _mz or "GL_3GL_F" in _mz) then
					{
						private _derp = [
									"1Rnd_HE_Grenade_shell",
									"3Rnd_HE_Grenade_shell",
									"1Rnd_Smoke_Grenade_shell",
									"3Rnd_Smoke_Grenade_shell",
									"1Rnd_SmokeRed_Grenade_shell",
									"3Rnd_SmokeRed_Grenade_shell",
									"1Rnd_SmokeGreen_Grenade_shell",
									"3Rnd_SmokeGreen_Grenade_shell",
									"1Rnd_SmokeYellow_Grenade_shell",
									"3Rnd_SmokeYellow_Grenade_shell",
									"1Rnd_SmokePurple_Grenade_shell",
									"3Rnd_SmokePurple_Grenade_shell",
									"1Rnd_SmokeBlue_Grenade_shell",
									"3Rnd_SmokeBlue_Grenade_shell",
									"1Rnd_SmokeOrange_Grenade_shell",
									"3Rnd_SmokeOrange_Grenade_shell",
									"UGL_FlareWhite_F",
									"3Rnd_UGL_FlareWhite_F",
									"UGL_FlareGreen_F",
									"3Rnd_UGL_FlareGreen_F",
									"UGL_FlareRed_F",
									"3Rnd_UGL_FlareRed_F",
									"UGL_FlareYellow_F",
									"3Rnd_UGL_FlareYellow_F",
									"UGL_FlareCIR_F",
									"3Rnd_UGL_FlareCIR_F",
									"FlareWhite_F",
									"FlareGreen_F",
									"FlareRed_F",
									"FlareYellow_F"							
								];
								
						_sanctionedmags append _derp;
					};
					
					if ("Secondary" in _mz) then
					{
						_derp = [
										"10Rnd_50BW_Mag_F"
								];
								
						_sanctionedmags append _derp;
					};			
				};

				{
					if (_x in sanctionedmagsarray) then {_sanctionedmags append [_x]};
				} foreach _tempmags;
				
				_ammoboxmagazinearray append _sanctionedmags;
			};
			_derparr = ["MineDetector","Binocular","NVGoggles_INDEP","Rangefinder"];
			if (_gunclass iskindof ["ItemCore", configFile >> "CfgWeapons"] or _gunclass in _derparr) then
			{
				_ammoboxitemarray append [_gunclass];
			};

			if (_gunclass iskindof "Bag_Base") then
			{
				_ammoboxbackpackarray append [_gunclass];
			};	
			
			_bombsarr = ["ATMine_Range_Mag","APERSMine_Range_Mag","APERSBoundingMine_Range_Mag","SLAMDirectionalMine_Wire_Mag","APERSTripMine_Wire_Mag","ClaymoreDirectionalMine_Remote_Mag","SatchelCharge_Remote_Mag","DemoCharge_Remote_Mag","IEDUrbanBig_Remote_Mag","IEDLandBig_Remote_Mag"];
			if (_gunclass in _bombsarr or (_x select 0) in _bombsarr) then
			{
				_ammoboxmagazinearray append [_gunclass];	
				//[format ["bs %",_gunclass]] remoteExec ["systemchat"]; 
			};	

			//[_ammoboxriflearray,_ammoboxitemarray,_ammoboxmagazinearray,_ammoboxbackpackarray] remoteExec ["dfk_blip_ammoboxes"];			
		} foreach storearray;

		storearray = [];
		publicvariable "storearray";
		funds = 0;
		rabatt = _old_rabatt;
	};	
	
	case 5:
	{
		#include "gunsandshit.h";	
		funds = 1000;
		private _old_rabatt = rabatt;
		rabatt = 0;
		storearray append vipstorearray;
		vipstorearray = [];
		publicvariable "vipstorearray";
		publicvariable "storearray";
		
		_ammoboxmagazinearray append ["Chemlight_green","Chemlight_red","Chemlight_yellow","Chemlight_blue"];	
		_ammoboxmagazinearray append ["HandGrenade","MiniGrenade","I_IR_Grenade"];	
		_ammoboxmagazinearray append ["SmokeShell","SmokeShellRed","SmokeShellGreen","SmokeShellYellow","SmokeShellPurple","SmokeShellBlue","SmokeShellOrange"];			
		
		{
			private _type = "";
	
			private _gunclass = _x select 0;
		
			if (_gunclass iskindof ["Rifle", configFile >> "CfgWeapons"] or _gunclass iskindof ["Pistol", configFile >> "CfgWeapons"] or _gunclass iskindof ["Launcher", configFile >> "CfgWeapons"] or _gunclass iskindof ["Mgun", configFile >> "CfgWeapons"]) then
			{
				_ammoboxriflearray append [_gunclass];
				private _tempmags = getarray(configfile >> "CfgWeapons" >> _gunclass >> "magazines");
				_sanctionedmags = [];
				
				private _mz = getarray(configfile >> "CfgWeapons" >> _gunclass >> "muzzles");
				if (count _mz >= 2) then
				{
					if ("EGLM" in _mz or "GL_3GL_F" in _mz) then
					{
						private _derp = [
									"1Rnd_HE_Grenade_shell",
									"3Rnd_HE_Grenade_shell",
									"1Rnd_Smoke_Grenade_shell",
									"3Rnd_Smoke_Grenade_shell",
									"1Rnd_SmokeRed_Grenade_shell",
									"3Rnd_SmokeRed_Grenade_shell",
									"1Rnd_SmokeGreen_Grenade_shell",
									"3Rnd_SmokeGreen_Grenade_shell",
									"1Rnd_SmokeYellow_Grenade_shell",
									"3Rnd_SmokeYellow_Grenade_shell",
									"1Rnd_SmokePurple_Grenade_shell",
									"3Rnd_SmokePurple_Grenade_shell",
									"1Rnd_SmokeBlue_Grenade_shell",
									"3Rnd_SmokeBlue_Grenade_shell",
									"1Rnd_SmokeOrange_Grenade_shell",
									"3Rnd_SmokeOrange_Grenade_shell",
									"UGL_FlareWhite_F",
									"3Rnd_UGL_FlareWhite_F",
									"UGL_FlareGreen_F",
									"3Rnd_UGL_FlareGreen_F",
									"UGL_FlareRed_F",
									"3Rnd_UGL_FlareRed_F",
									"UGL_FlareYellow_F",
									"3Rnd_UGL_FlareYellow_F",
									"UGL_FlareCIR_F",
									"3Rnd_UGL_FlareCIR_F",
									"FlareWhite_F",
									"FlareGreen_F",
									"FlareRed_F",
									"FlareYellow_F"							
								];
								
						_sanctionedmags append _derp;
					};
					
					if ("Secondary" in _mz) then
					{
						_derp = [
										"10Rnd_50BW_Mag_F"
								];
								
						_sanctionedmags append _derp;
					};			
				};

				{
					if (_x in sanctionedmagsarray) then {_sanctionedmags append [_x]};
				} foreach _tempmags;
				
				_ammoboxmagazinearray append _sanctionedmags;
			};
			_derparr = ["MineDetector","Binocular","NVGoggles_INDEP","Rangefinder"];
			if (_gunclass iskindof ["ItemCore", configFile >> "CfgWeapons"] or _gunclass in _derparr) then
			{
				_ammoboxitemarray append [_gunclass];
			};

			if (_gunclass iskindof "Bag_Base") then
			{
				_ammoboxbackpackarray append [_gunclass];
			};	
			
			_bombsarr = ["ATMine_Range_Mag","APERSMine_Range_Mag","APERSBoundingMine_Range_Mag","SLAMDirectionalMine_Wire_Mag","APERSTripMine_Wire_Mag","ClaymoreDirectionalMine_Remote_Mag","SatchelCharge_Remote_Mag","DemoCharge_Remote_Mag","IEDUrbanBig_Remote_Mag","IEDLandBig_Remote_Mag"];
			if (_gunclass in _bombsarr or (_x select 0) in _bombsarr) then
			{
				_ammoboxmagazinearray append [_gunclass];	
				//[format ["bs %",_gunclass]] remoteExec ["systemchat"]; 
			};	

			//[_ammoboxriflearray,_ammoboxitemarray,_ammoboxmagazinearray,_ammoboxbackpackarray] remoteExec ["dfk_blip_ammoboxes"];			
		} foreach storearray;
		
		storearray = [];
		publicvariable "storearray";	
		funds = 0;		
		rabatt = _old_rabatt;
	};
};

systemchat "Starting gear set";
//makeshift debug inits and gunstore
//TODO add better gunstore (fucking effort)
//[format ["Initialized\n\nAi skill: %1\nStart time: %2\nViewdistance: %3\nTime multiplier: %4\nLog: %5 %6\nMorale: %7 %8\nPol: %9 %10\nVictorycond: %11\nCampsize: %12\nDebug: %13\nRespawntickets: %14\nArsenal start level: %15\n",["Recruit","Regular","Veteran"] select (["AISkill"] call BIS_fnc_getParamValue),["Daytime"] call BIS_fnc_getParamValue,["ViewDistance"] call BIS_fnc_getParamValue,["timemultiplier"] call BIS_fnc_getParamValue,["init_logistics"] call BIS_fnc_getParamValue,logistics,["init_morale"] call BIS_fnc_getParamValue,sidemorale,["init_politics"] call BIS_fnc_getParamValue,politics,["Any","All","Kill 'em all", "Logistical","Morale","Political","Log+Mor","Log+Pol","Mor+Pol"] select (["victoryconditions"] call BIS_fnc_getParamValue),campsize,["debug"] call BIS_fnc_getParamValue,["Infinite","None","10","20","50","100"] select ["lives"] call BIS_fnc_getParamValue,["Empty","Very basic","Some stuff","Basic","vip"] select (["arsenal_level"] call BIS_fnc_getParamValue)]] remoteExec ["hint"]; 

//new fancy gunstore, now with gui!
[laptop2, ["Open gunstore", {publicvariable "storearray";0 = CreateDialog "DFK_GUNSTORE"}]] remoteExec ["addAction",0,true];
laptop2 setvariable ["important",true,true];

//artillery radar/tracking stuff
//not used
//???
dfk_artyradar_targetarray = [];

private _airboundlogisticsresupplytick = 12;
private _airboundlogisticsresupplytick = ["resupplytick"] call BIS_fnc_getParamValue;

//TODO this could pbb be made more efficient
//define and generate locationarray on startup
allLocationTypes = [];
"allLocationTypes pushback configName _x" configClasses (configFile >> "CfgLocationTypes");

locationarray = [];
private _derparray = [];

//["lumber mill","le roi","la trinité","dourdan","houdan","bosquet","saint louis","golfe vert","guran","larche","arudy","airport","radio station","lavalle","isaro","dents du midi","military base","mont chauve","saint jean","harbor","pic de feas","feas","harbor","dorres","eperon","military base","monte","sainte marie","power plant","baie du port","le port","chapoi","les sables","lolisse","blanches","goisse","baie du nain","corton","vigny","la pessagne","harbor","harbor","cancon","harbor","saint martin","la rivière","baie du cancon","harbor","golfe d'azur","arette","moray","pegasus air co.","faro"]

systemchat "Some stuff done";

//load destruction param from init
private _globaldestruction = ["town_destruction"] call BIS_fnc_getParamValue;
private _dfk_troopfactor = 1;
private _dfk_troopfactor_temp = ["dfk_troopfactor"] call BIS_fnc_getParamValue;

_dfk_troopfactor = _dfk_troopfactor + (_dfk_troopfactor_temp * 0.1);

_dfk_record_destruction1 = 0;
_dfk_record_destruction2 = 0;
_dfk_record_house = 0;
dfk_record_house2 = 0;

private _mortarsites = [];

publicvariable "ignore_artblacklist";
//malden blacklist
private _timederp1 = diag_Ticktime;
{
	if (text _x != "") then
	{
		//[format ["%1",tolower(text _x)]] remoteExec ["systemchat"]; 
		private _use = true;
		private _special = false;

		private _derparray = _derparray + [tolower(text _x)];

		if (tolower(text _x) in dfk_townspawn_blacklist) then 
		{_use = false;}
		else
		{	
			//special cases
			//private _speciallist = ;
			
			private _specialtype = "";
			
			private _largertriggerareaspawns = [];
			
			if (tolower(text _x) in dfk_townspawn_specialsites or (tolower(text _x) in dfk_important_towns)) then
			{
				_special = true;
				[locationPosition _x,"colorred",text _x,"hd_flag",0.6] remoteExec ["dfk_generate_marker",2];
				_specialtype = (tolower(text _x));
				if (!(tolower(text _x) == "airport")) then {_mortarsites pushBackUnique (locationposition _x)};
			}
			else
			{
				[locationPosition _x,"colorblack",text _x,"hd_flag",0.3] remoteExec ["dfk_generate_marker",2];
			};

			if (_use) then 
			{		
				private _trg = createTrigger ["EmptyDetector", locationposition _x];
				private _trgsize = spawn_distance;
				
				if (tolower(text _x) in dfk_townspawn_smoltown) then
				{
					_trgsize = spawn_distance;
				}
				else
				{
					_trgsize = spawn_distance;
				};
				_trg setTriggerArea [_trgsize, _trgsize, 0, false];
				_trg setTriggerActivation ["ANYPLAYER", "PRESENT", true];
				_trg setTriggerStatements ["this", "", ""];		

				if (!(tolower(text _x) in dfk_townspawn_ignoreartblacklist)) then
				{
					_artblklist = createTrigger ["EmptyDetector", locationposition _x];
					_artblklist setTriggerArea [200, 200, 0, false];
					
					artblacklist pushback _artblklist;
				};
				
				private _spawned = false;
				private _taken = false;
				private _vehicles = [];
				private _patrol = 0;
				
				
				private _list = locationposition _x nearObjects ["House_F", 350];
				private _list2 = locationposition _x nearObjects ["Building", 500];
				
				_dfk_record_house = _dfk_record_house + (count _list);
				
				private _townsize = count _list;
				private _townpositions = [];

				private _troops = 0;
				
				private _bunkerarray = [];
				
				if ((_townsize / 20) > 6) then
				{
					_troops = ceil(_townsize / 20);
				}
				else
				{
					_troops = 6;
				};		

				if (_special) then
				{
					_patrol = 0;
					
					if ((tolower(text _x) == "military base") OR (tolower(text _x) == "airport") or (tolower(text _x) == "military") or (tolower(text _x) in dfk_important_towns)) then
					{
						_troops = _troops * 3;
					}
					else
					{
						_troops = ceil(_troops * (2 + random 1));
					};
				}
				else
				{		
					_patrol = _dfk_town_spawnchance;
				};	
				
				_troops = ceil (_dfk_troopfactor * _troops);
				
				//some towns overlap and doesn't really need troops, but mostly for playability reasons... 3 sites equal alot of stuff going on, ergo shit performance
				//so don't spawn troops in them
				private _notroopsarray = [];
				if (tolower(text _x) in _notroopsarray) then
				{
					_patrol = 2;
					_troops = 0;
				};
				
				private _junk = random 1;
				private _destruction = 0;
				if (_globaldestruction == -1) then {_destruction = random 1} else {_destruction = _globaldestruction/10};
				private _junkarray = [];
				

				
				if (_destruction > 0.8 && 0.5 > random 1) then 
				{
					_town = locationposition _x;
					{
						if (_town distance _x <= 5) then {deletevehicle _x};
					} foreach artblacklist;			
				};				

				{
				//get bunkers
					if ((typeof _x) in dfk_townspawn_bunkerkinds) then
					{
						 //BUNKOOOOOOOOOR
						 //add to troops to accomodate dudes in bunkers
						 _troops = _troops + 3;
						 _bunkerarray pushback _x;
						 
					};				
				} foreach _list2;
				
				
				
				{
					
					if (_destruction >= random 1 && !(typeof _x in logisticaltargetsarray)) then
					{
						// if (typeof _x in dfk_townspawn_polekind) then
						if (_x iskindof "Lamps_base_F" or _x iskindof "PowerLines_base_F" or _x iskindof "PowerLines_Small_base_F") then
						{		
							if (random 1 > random 1) then
							{
								_x setHit ["light_1_hitpoint", 0.97];				
								_x setHit ["light_2_hitpoint", 0.97];
								_x setHit ["light_3_hitpoint", 0.97];
								_x setHit ["light_4_hitpoint", 0.97];
								
								_dfk_record_destruction1 = _dfk_record_destruction1 + 1;
							};
							// if (typeof _x in streetlights) then
							// {
								
							// };
						}
						else
						{
							private _noHitzone1 = isClass(configfile >> "CfgVehicles" >> typeof _x >> "HitPoints" >> "Hitzone_1_hitpoint");
							private _noHitzone2 = isClass(configfile >> "CfgVehicles" >> typeof _x >> "HitPoints" >> "Hitzone_2_hitpoint");		
							private _one = false;
							private _two = false;

							if (_noHitzone1) then
							{
								if (0.5 > random 1) then {_one = true};
							};
							if (_noHitzone2) then
							{
								if (0.5 > random 1) then {_two = true};
							};
							
							if (_one) then 
							{
								//_x setHitpointDamage ["Hitzone_1_hitpoint",1,false];
								[_x, ["Hitzone_1_hitpoint",1,false]] remoteExec ["setHitpointDamage",0,true];
							};

							if (_two) then 
							{
								//_x setHitpointDamage ["Hitzone_1_hitpoint",1,false];
								[_x, ["Hitzone_2_hitpoint",1,false]] remoteExec ["setHitpointDamage",0,true];
							};			

							if (!_one && !_two) then
							{
								[_x, [1,false]] remoteExec ["setdamage",0,true];
							};
							
							_dfk_record_destruction1 = _dfk_record_destruction1 + 1;
						};
					};
				} foreach _list;
				
				//if (debug) then {[format ["bunkercount: %1 %2",text _x,_bunkerarray]] remoteExec ["diag_log"];};		
				
				private _a = [locationposition _x,text _x,_trg,_spawned,_taken,_troops,[],_vehicles,_townsize,_patrol,_destruction,_specialtype,_troops,_bunkerarray];
				locationarray append [_a];
				//[format ["%1",_a]] remoteExec ["systemchat"]; 
			};
		};
	};	
} forEach nearestLocations [[dfk_map_x,dfk_map_y], dfk_townspawn_townlocationtypes, dfk_map_raduis];
systemchat "Towns and sites done";

//makeshift solution for adding 'extra' military bases (to cover the whole of "military island" on malden and the like
{
	private _special = true;
	[getpos _x,"colorred","military base","hd_flag",0.6] remoteExec ["dfk_generate_marker",2];
	private _specialtype = "military base";

	private _trg = createTrigger ["EmptyDetector", getpos _x];
	_trg setTriggerArea [spawn_distance, spawn_distance, 0, false];
	_trg setTriggerActivation ["ANYPLAYER", "PRESENT", true];
	_trg setTriggerStatements ["this", "", ""];		

	private _artblklist = createTrigger ["EmptyDetector", getpos _x];
	_artblklist setTriggerArea [200, 200, 0, false];
	
	artblacklist pushback _artblklist;
	
	private _spawned = false;
	private _taken = false;
	private _vehicles = [];
	private _patrol = 0;
	
	private _list = getpos _x nearObjects ["House_F", 350];
	private _list2 = getpos _x nearObjects ["Building", 350];
	
	private _townsize = count _list;
	private _townpositions = [];

	private _troops = 0;
	
	private _bunkerarray = [];
	
	if ((_townsize / 20) > 6) then
	{
		_troops = ceil(_townsize / 20);
	}
	else
	{
		_troops = 6;
	};				
	
	_troops = ceil( _dfk_troopfactor * _troops);
	
	//_troops = _troops * 3;
	private _patrol = 0;
	
	private _junk = random 1;
	private _destruction = 0;
	if (_globaldestruction == -1) then {_destruction = random 1} else {_destruction = _globaldestruction/10};
	private _junkarray = [];

	{
	//get bunkers
		if ((typeof _x) in dfk_townspawn_bunkerkinds) then
		{
			 //BUNKOOOOOOOOOR
			 //add to troops to accomodate dudes in bunkers
			 //_troops = _troops + 3;
			 _bunkerarray pushback _x;
			 
		};				
	} foreach _list2;
	
	{
		if (_destruction >= random 1 && !(typeof _x in logisticaltargetsarray)) then
		{
			if (typeof _x in streetlights) then
			{
				deletevehicle _x;
			}
			else
			{
				private _noHitzone1 = isClass(configfile >> "CfgVehicles" >> typeof _x >> "HitPoints" >> "Hitzone_1_hitpoint");
				private _noHitzone2 = isClass(configfile >> "CfgVehicles" >> typeof _x >> "HitPoints" >> "Hitzone_2_hitpoint");		
				private _one = false;
				private _two = false;

				if (_noHitzone1) then
				{
					if (0.5 > random 1) then {_one = true};
				};
				if (_noHitzone2) then
				{
					if (0.5 > random 1) then {_two = true};
				};
				
				if (_one) then 
				{
					//_x setHitpointDamage ["Hitzone_1_hitpoint",1,false];
					[_x, ["Hitzone_1_hitpoint",1,false]] remoteExec ["setHitpointDamage",0,true];
				};

				if (_two) then 
				{
					//_x setHitpointDamage ["Hitzone_1_hitpoint",1,false];
					[_x, ["Hitzone_2_hitpoint",1,false]] remoteExec ["setHitpointDamage",0,true];
				};			

				if (!_one && !_two) then
				{
					[_x, [1,false]] remoteExec ["setdamage",0,true];
				};
			};
		};
	} foreach _list;
	
	//if (debug) then {[format ["bunkercount: %1 %2",text _x,_bunkerarray]] remoteExec ["diag_log"];};		
	
	_a = [getpos _x,"military base",_trg,_spawned,_taken,_troops,[],_vehicles,_townsize,_patrol,_destruction,_specialtype,_troops,_bunkerarray];
	locationarray append [_a];
	//[format ["%1",_a]] remoteExec ["systemchat"]; 
} foreach dfk_tempmilbases;
systemchat "Extra milsites set";

private _timederp2 = diag_Ticktime;
//diag_log format ["timederp1 %1",_timederp2 - _timederp1];

{
	private _phoneboth = _x;
	_phoneboth allowdamage false;
	//diag_log _x;
	[getpos _phoneboth,"colorblue","Info board","loc_civildefense",0.6] remoteExec ["dfk_generate_marker",2];
	
	private _scarecrowname =selectrandom scarecrowcodewords;
	scarecrowcodewords deleteat (scarecrowcodewords find _scarecrowname);
	
	[_phoneboth, ["Order Scarecrow to watch this location","dfk_scarecrow.sqf",[getpos _phoneboth,_scarecrowname,10]]] remoteExec ["addAction",0,true];	
	_phoneboth setvariable ["important",true];
} foreach dfk_phonebothlist;

systemchat "Phoneboths done";

//get fancy list of logistical targets
//waiting for fancy static object killing functions in next update
//right now buggy and derpy as fuck
private _logisticstargetsalivetrackingarray = [];
{
	if (alive _x) then
	{
		[getpos _x,"colorblack","","hd_destroy",0.5] remoteExec ["dfk_generate_marker",2];
		_logisticstargetsalivetrackingarray pushBackUnique _x; 
		//_x addMPEventHandler ["mpkilled", {Null = [_this select 0,_this select 1,_this select 2,"LOGISTICALBUILDING"] execVM "killer.sqf";}];
	};
} foreach ( nearestObjects [[dfk_map_x,dfk_map_y],logisticaltargetsarray ,dfk_map_raduis]);
private _log_logisticstargetsalivetrackingarray = count _logisticstargetsalivetrackingarray;
systemchat "Logistical objects trackerarray done";

//generate safehouses
private _safehouses = [];
private _try = 0;

_dfk_islandmode = 0;
_dfk_islandmode = ["dfk_islandmode"] call BIS_fnc_getParamValue;
private _dfk_temp_islandmode_array = [];

private _dfk_numberofsafehouses = 5;
_safehousederp = ["dfk_safehouses"] call BIS_fnc_getParamValue;

if (_safehousederp > 0 && _safehousederp <= 10) then 
	{_dfk_numberofsafehouses = _safehousederp}
	else 
	{_dfk_numberofsafehouses = floor random 10};

if (_dfk_islandmode == 1) then
{
	_dfk_numberofsafehouses = 0;
	{
		_islandpos = _x;
		_dfk_numberofsafehouses = _dfk_numberofsafehouses + 1;
		_building_array = ( nearestObjects [getpos _islandpos,["House_F"] ,500]);
		{
			_b = _x;
			private _b_arr = _b buildingpos -1;
			if (count _b_arr > 0 and !(_b iskindof "Land_Lighthouse_03_red_F" or _b iskindof "Land_Lighthouse_03_green_F")) then {_dfk_temp_islandmode_array pushBackUnique _b};
		} foreach _building_array;
	} foreach [islandmode_island1,islandmode_island2,islandmode_island3,islandmode_island4];
};

dfk_respawnmarkers = [];
private _safehousestrylimit = 2000;

_recalc_safehouses = false;

dfk_respawnMarkersArray = [];

while {count _safehouses < _dfk_numberofsafehouses && _try < _safehousestrylimit} do
{
	private _pos = [];
	
	if (_dfk_islandmode == 0) then
	{
		_pos = selectrandom precompiledhideoutpositions;
	}
	else
	{
		_pos = selectrandom _dfk_temp_islandmode_array;
	};		
	
	private _houses = nearestObjects [_pos,["House_F"],30];
	private _housepositions = [];
	private _h = selectrandom _houses;
	private _far = true;

	if (count _houses == 0) then
	{
		_h = createVehicle ["HeliH",_pos , [], 0, "NONE"];
	};

	if (typeof _h in dfk_forbiddenSpawnHouse or !alive _h or _h iskindof "Ruins_F") then
	{
		_far = false;
	};	
		
	if (!(typeof _h in dfk_forbiddenSpawnHouse) or !alive _h or _h iskindof "Ruins_F") then
	{
		{
			if (_pos distance _x < 2000) then
			{
				_far = false;
			};
		} foreach _safehouses;
	};			

	if (_far && !(_h in _safehouses)) then
	{
		{
			if (!(typeof _x in dfk_forbiddenSpawnHouse)) then
			{
				_housepositions append ([_x, -1] call BIS_fnc_buildingPositions);
			};
		} foreach _houses;
		
		private _laptop = createVehicle ["Land_Laptop_02_unfolded_F",[0,0,1000] , [], 25, "NONE"];
		_laptop setpos getpos _h;
		_laptop setvariable ["important",true,true];
		if (count _housepositions > 0) then
		{
			private _hp = selectrandom _housepositions;
			_housepositions deleteat (_housepositions find _hp);
			_laptop setpos [_hp select 0,_hp select 1,(_hp select 2) + 1];
		};
		
		{
			private _damp = createVehicle [ _x,[0,0,1000], [], 0, "NONE"];
			_damp setpos getpos _h;
			//_damp enableSimulationGlobal false;
			
			if (_damp iskindof "ReammoBox_F") then
			{
				_damp allowdamage false;
				//_damp enableSimulationGlobal true;
				[_damp] remoteexec ["dfk_initammobox",2];
				
				if (count _housepositions > 0) then
				{
					_hp = selectrandom _housepositions;
					_housepositions deleteat (_housepositions find _hp);
					_damp setpos _hp;
					_ammoboxmarker = [getpos _damp,"colorgreen","Ammobox","mil_box",0.5] call dfk_generate_marker;
				};					
			};
			if (count _housepositions > 0 && !(_damp iskindof "ReammoBox_F")) then
			{
				_hp = selectrandom _housepositions;
				_housepositions deleteat (_housepositions find _hp);
				_damp setpos _hp;
			};			

			_damp setdir random 360;
			_damp setvariable ["important",true,true];
			
		} foreach dfk_safehousecrap;
		
		{
			private _damp = createVehicle [ _x,[0,0,0] , [], 0, "NONE"];
			
			private _pos = [getpos _h, 5, 30, 7, 0, 20, 0] call BIS_fnc_findSafePos;
			
			_damp setpos _pos;
			//_damp enableSimulationGlobal false;
			_damp setvariable ["important",true,true];

		} foreach dfk_safehousecrap2;
		
		[_laptop, ["Review funds and intel", {publicvariable "funds";publicvariable "secrets";[format ["Funds: %1\nDirty secrets: %2",funds,secrets]] remoteExecCall ["hint"]}]] remoteExec ["addAction",0,true];
		[_laptop, ["Use secrets to expose enemy politicians", {["dfk_expose_politician.sqf"] remoteExec ["execvm",2]}]] remoteExec ["addAction",0,true];
		if (dfk_storekind_type == 2) then {[_laptop, ["Remotely access gunstore", {publicvariable "storearray";0 = CreateDialog "DFK_GUNSTORE"}]] remoteExec ["addAction",0,true]};
		// [_laptop, ["Spawn Quad Bike","dfk_spawn_vehicle.sqf",["C_Quadbike_01_F"]]] remoteExec ["addAction",0,true];
		// [_laptop, ["Spawn Offroad","dfk_spawn_vehicle.sqf",["C_Offroad_01_F"]]] remoteExec ["addAction",0,true];
		// [_laptop, ["Spawn Truck","dfk_spawn_vehicle.sqf",["C_Van_01_transport_F"]]] remoteExec ["addAction",0,true];
		// [_laptop, ["Spawn 4WD","dfk_spawn_vehicle.sqf",["C_Offroad_02_unarmed_F"]]] remoteExec ["addAction",0,true];
		// [_laptop, ["Spawn Car","dfk_spawn_vehicle.sqf",["C_Hatchback_01_F"]]] remoteExec ["addAction",0,true];
		{	
			private _str = str format ["Spawn %1 @ level %2",gettext (configfile >> "CfgVehicles" >> (_x select 0) >> "displayname"),_x select 1];
			[_laptop, [_str,"dfk_spawn_vehicle.sqf",[(_x select 0),(_x select 1)]]] remoteExec ["addAction",0,true];			
		} foreach dfk_safehouse_cars;
		
		[_laptop, ["Spawn 12x Retards","retard.sqf",[],1.5,false,true]] remoteExec ["addAction",0,true];
		
		_safehouses append [_h];
		[getpos _laptop,"colorgreen","Safehouse","hd_flag",0.8] remoteExec ["dfk_generate_marker",2];
		
		_respawnstr = format ["respawn_%1",_try];
		_respawnmarker = [getpos _laptop,"colorgreen",_respawnstr,"hd_flag",0] call dfk_generate_marker;
		//_pos = [getpos _laptop, 0, 25, 2, 0, 20, 0] call BIS_fnc_findSafePos;
		_pos = [getpos _laptop, 0, 25, 2, 0, 20, 0,10,"spawn_laptop"] call dfk_find_fuken_safe_spawnpoint;
		if (count _pos <= 0) then {_pos = [(getpos _laptop select 0) + (random 25) - (random 25),(getpos _laptop select 1) + (random 25) - (random 25),0]};
		_respawnmarker setmarkerpos _pos;
		[resistance,_respawnmarker] call BIS_fnc_addRespawnPosition;		
		dfk_respawnmarkers pushback _respawnmarker;
		dfk_respawnMarkersArray pushbackunique [_respawnmarker,_laptop];
		systemchat format ["Safehouse %1/%2 Done. Try %3/%4",count _safehouses,_dfk_numberofsafehouses,_try,_safehousestrylimit];
	};
	_try = _try + 1;
	if (_try mod 100 == 1) then {systemchat format ["Safehouse trying... %1/%2",_try,_safehousestrylimit];};
};
systemchat "Safehouses done";

[_ammoboxriflearray,_ammoboxitemarray,_ammoboxmagazinearray,_ammoboxbackpackarray] remoteExec ["dfk_blip_ammoboxes",2];

dfk_interestingAsFuckArray = [];

//spawn gunloot crates
if (_gunloot > 1) then 
{
	private _gunloot_no = 4 + ceil random 10;
	private _gunloot_try = 0;
	private _gunloot_try_limit = 2000;

	while {_gunloot_no > 0 && _gunloot_try < _gunloot_try_limit} do
	{
		private _gunloot_position_prospect = selectrandom precompiledhideoutpositions;
		private _sofarsogood = true;
		private _house = "";
		private _housepositions = [];
		
		{
			if (_gunloot_position_prospect distance _x < 100 && _sofarsogood) then
			{
				_sofarsogood = false;
			}
				else
			{
				_house = nearestObjects [_gunloot_position_prospect,["House_F"],15];
				_housepositions append ([_house, -1] call BIS_fnc_buildingPositions);
				
				if (count _housepositions < 0) then {_sofarsogood = false;};
				if (typename _house == "OBJECT") then
				{
					if (!((typeof _house) in dfk_forbiddenSpawnHouse)) then {_sofarsogood = false;};
				};
			};
		} foreach _safehouses;
		
		if (_sofarsogood) then
		{
			_house = nearestObjects [_gunloot_position_prospect,["House_F"],15];
			_housepositions = [];
			
			_housepositions append ([_house, -1] call BIS_fnc_buildingPositions);
			
			private _pos = [];
			if (count _housepositions > 0) then
			{
				_pos = selectrandom _housepositions;
			}
			else
			{
				_pos = _gunloot_position_prospect;
			};
			
			private _gunloot_boxkind = ["Box_FIA_Ammo_F","Box_FIA_Support_F","Box_FIA_Wps_F","Box_Syndicate_WpsLaunch_F","Box_Syndicate_Ammo_F","Box_Syndicate_Wps_F","Box_IED_Exp_F"];
			
			private _gunloot_actualbox = createVehicle [selectrandom _gunloot_boxkind,[_pos select 0,_pos select 1,(_pos select 2) + 0.5], [], 0, "NONE"];
			_gunloot_actualbox allowdamage false;
			
			clearBackpackCargoGlobal _gunloot_actualbox;
			clearItemCargoGlobal _gunloot_actualbox;
			clearMagazineCargoGlobal _gunloot_actualbox;
			clearWeaponCargoGlobal _gunloot_actualbox;
			
			dfk_interestingAsFuckArray pushbackunique _gunloot_actualbox;
			
			if (0.5 > random 1) then
			{			
				[getpos _gunloot_actualbox,["APERSTripMine","APERSBoundingMine","APERSMine","BombCluster_02_UXO1_F","BombCluster_02_UXO4_F","BombCluster_02_UXO2_F","BombCluster_02_UXO2_F","BombCluster_02_UXO3_F"],true,random 10,20] call dfk_mine_site_init;
			};
			
			[getpos _gunloot_actualbox,["placed_chemlight_yellow","placed_I_IR_grenade"],true,4,20] call dfk_mine_site_init;
			
			
			// _asd = [getpos _gunloot_actualbox,"colorpink","gunloot","hd_flag",1] call dfk_generate_marker;
			// [_gunloot_actualbox,"colorpink","gunloot","hd_destroy",true,1] execvm "marker_track.sqf";
			
			_diff = 10 + ceil random 15;
			for "_x" from 0 to _diff do
			{
				_gunloot_thingie = (selectrandom gunlootarray) select 0;
				
				[_gunloot_actualbox,_gunloot_thingie,random 10] call dfk_addRandomEquipmentToBox;
			};
			_gunloot_no = _gunloot_no - 1;
			//systemchat format ["Gunloot %1 Done. Try %2/%3",_gunloot_no,_gunloot_try,_gunloot_try_limit];
		};
		
		_gunloot_try = _gunloot_try + 1;
		//if (_try mod 100 == 1) then {systemchat format ["Gunloot trying... %1/%2",_gunloot_try,_gunloot_try_limit];};
	};

//precompiledhideoutpositions
//_safehouses
};

_blankedkinds = ["I_LT_01_AT_F","I_LT_01_scout_F","I_LT_01_cannon_F","I_MRAP_03_F","I_MRAP_03_gmg_F","I_MRAP_03_hmg_F","I_Truck_02_ammo_F","I_Truck_02_fuel_F","I_Truck_02_medical_F","I_Truck_02_box_F","I_Truck_02_transport_F","I_Truck_02_covered_F","I_HMG_01_F","I_GMG_01_F","I_Mortar_01_F","I_static_AA_F","I_static_AT_F","I_G_Offroad_01_AT_F","I_G_Offroad_01_armed_F","I_C_Offroad_02_AT_F","I_C_Offroad_02_LMG_F","I_G_Mortar_01_F"];

{
	if (0.2 > random 1) then
	{
		_building = _x;
		private _housepositions = [];
		_housepositions append (_building buildingPos -1);
		
		//[getpos _building,"colorblack","shed","hd_destroy",1] call dfk_generate_marker;
		
		// {
			// _stuff = createvehicle [_derp select _foreachindex, _x, [], 0, "NONE"];
		// } foreach _housepositions;
		
		_blanket = selectrandom _blankedkinds;
		_stuff = createvehicle [_blanket, [0,0,1000], [], 0, "NONE"];
		_stuff setdir (getdir _building) - 90;
		_stuff setpos (_housepositions select 5);
		
		dfk_interestingAsFuckArray pushbackunique _stuff;
		
		//[getpos _stuff,"colorpink","blankie","hd_destroy",1] call dfk_generate_marker;
	};
	
} foreach (nearestObjects [[dfk_map_x,dfk_map_y],["Land_i_Shed_Ind_F"] ,dfk_map_raduis]);

//systemchat "Gunloot done";



//move respawnmarkers to safehouses
/* private _arr = ["respawn1","respawn2","respawn3","respawn4","respawn5"];
{
	private _m = _arr select _forEachIndex;
	private _pos = [];
	_pos = [getpos _x, 0, 50, 10, 0, 20, 0] call BIS_fnc_findSafePos;
	_m setmarkerpos _pos;
} foreach _safehouses;
systemchat "Respawn set"; */


//spawn mortar trucks
for "_i" from 0 to (2 + random 2) do
{
	private _pos = [selectrandom _mortarsites, 10, 300, 10, 0, 0.5, 0] call BIS_fnc_findSafePos;
	[_pos] call dfk_spawn_mortar_truck;
};
systemchat "Mortars done";

//spawn artillery
for "_i" from 1 to 3 do
{
	private _pos = [getpos art_spawn, 50, 300, 10, 0, 0.5, 0] call BIS_fnc_findSafePos;
	
	private _veh = createVehicle [selectrandom dfk_enemy_artillerykindarray,[0,0,0], [], 0, "NONE"];
	_veh setpos _pos;
	[_veh] remoteExec ["dfk_initvehicle",2];
	_veh setvariable ["important",true,true];
	
	private _grp = creategroup east;
	private _id = ["arty"] call dfk_generate_radioid;
	_grp setGroupIdGlobal [_id];				

	
	[_grp,_veh,""] call dfk_spawn_crew;
	
	_grp setvariable ["art_gunner",_veh,true];
	artilleryarray pushbackunique _grp;
};
systemchat "Artillery done";



//best variablenames are best!
//(lol?)
private _blargh = true;

//morningstatus/supplyconvoy stuff
private _cuckadoodledo = false;
sentsupply = false;
private _supplytruckarray = [];
private _supplyescort = false;

//vars for ambulating cars and stuff
private _alivecarstarget = 6;
private _alivecarstarget = ["roaming_vehicles"] call BIS_fnc_getParamValue;
private _alivecars = 0;
private _alivecarsarray = [];


//vars for ambulating cars and stuff
private _aliveAirTarget = 0;
private _aliveAirTarget = ["roaming_vehicles_air"] call BIS_fnc_getParamValue;
private _aliveAir = 0;
private _aliveAirArray = [];

//spawn medtents
_done = false;
_done = [] call dfk_spawn_medtents;
waituntil {_done};
systemchat "Medtents done";

_done = false;
_done = [] call dfk_spawn_woodvehicles;
waituntil {_done};
systemchat "woodvehicles done";
publicvariable "dfk_interestingAsFuckArray";

dfk_hackedEnemyRadar = 0;
dfk_globalShortWaveRadioQuality = 0;
publicvariable "dfk_globalShortWaveRadioQuality";
publicvariable "dfk_hackedEnemyRadar";

dfk_printstatus = false;
publicvariable "dfk_printstatus";

[] call dfk_roadBlocksInit;
[] call dfk_selectRadioSites;
systemchat "roadblocks and radiosites init done";

//broadcast new variable content
//dunno if this is a good or bad way... 
//should transmit to all connected, non jip connected clients before mission start

#include "dfk_publicvars.h"

if (isServer) then
{
	0 = [] execvm "dfk_stalker.sqf";
};

systemchat "publicvariables updated";

//radar script
//makes the radars "look around" to detect enemy aircraft
//(otherwise they just look in one direction)
//doesn't always look pritty, but it works :)
[] execvm "dfk_radarscript.sqf";
systemchat "Enemy radar-script online";

//main loop
private _tick = 0;
private _maingame = true;

[format ["Init done? %1 You may now proceed with the game, kind sir.", _maingame]] remoteExec ["systemchat"];

// {
	// diag_log format ["%1 %2",_x,([_x] call BIS_fnc_getParamValue)];
// } foreach ["AIaccuracy","AIshake","AIaimspeed","AIcommanding","AIgeneral","AIreload","AIcourage","AIspotTime","AIspotdistance","Daytime","ViewDistance","timemultiplier","init_logistics", "init_morale","init_politics","init_funds","init_discount","victoryconditions","campsitesize","debug","lives","arsenal_level","town_destruction","targetlogisticslevel","politicssupportthreshhold","qrfpool","nerfgear","mines","locked_vehicles","roaming_vehicles"];

if (debug) then {[] execvm "track_all_units.sqf"};
if (dfk_storekind_type == 3 or dfk_storekind_type == 4) then {[] execvm "dfk_gun_lvl_loop.sqf"};

endgame = false;

//[] spawn dfk_moraleStatemachine;
[] spawn dfk_aiMoraleHAndler2000;

{
	if (random 1 < 0.33) then
	{
		[getpos _x,"colorgreen","","hd_unknown",0.5] remoteExec ["dfk_generate_marker",2];
		
		dfk_interestingAsFuckArray deleteat _foreachindex;
	};

} foreach dfk_interestingAsFuckArray;

while {_maingame} do
{
	//locationarray loop
	//this is where the magic happens
	{
		//private _a = [locationposition _x,text _x,_trg,_spawned,_taken,_troops,[],_vehicles,_townsize,_patrol,_destruction,_specialtype,_troops,_bunkerarray];
		// locationposition _x,
		private _townPosition = _x select 0;
		// text _x,
		private _townName = _x select 1;
		// _trg,
		private _townTrigger = _x select 2;
		// _spawned,
		private _townSpawned = _x select 3;
		// _taken,
		private _townTaken = _x select 4;
		// _troops,
		private _townTroops = _x select 5;
		// [],
		private _townArray = _x select 6;
		// _vehicles,
		private _townVehicles = _x select 7;
		// _townsize,
		private _townSize = _x select 8;
		// _patrol,
		private _townPatrol = _x select 9;
		// _destruction,
		private _townDesctruction = _x select 10;
		// _specialtype,
		private _townSpecialtype = _x select 11;
		// _troops,
		private _townTroops = _x select 12;
		// _bunkerarray
		private _townBunkerarray = _x select 13;

		if ((triggeractivated _townTrigger) && !_townSpawned) then
		{
			//diag_log format ["%1",_x];
			//spawn shit
			//BADSIDE
			// private _a = [locationposition _x,text _x,_trg,_spawned,_taken,_troops,[],_vehicles,_townsize,_patrol,_destruction,_specialtype,_troops,_bunkerarray];
			
			private _veharray = [];
			private _grparray = [];
			
			private _trewps = _townTroops;
			//_grp = creategroup east;
			
			private _spawnedtroops = false;
			private _temp = [];
			
			//needs redoing... not working (done)
			
			//if special -> spawn soldiers
			//else check with spawnchanse
				//then spawn either troops or "cops"
				
			if (!(_townSpecialtype == "")) then
			{
				//spawn milshit
				//diag_log "towndebug spawn milshit 0";
				
				_temp = [_temp,east,_townTroops,_townPosition,200,"soldier",50,_townSpecialtype] call dfk_spawn_bunch_troops;
				_grparray append _temp;				
				
				//spawn bunkertroops
				if (count _townBunkerarray > 0) then
				{
					private _temp2 = [];
					_temp2 = [_temp2,_townBunkerarray,(count _townBunkerarray) * 3] call dfk_populate_bunkers;
					_grparray append _temp2;				
				};
			}
			else
			{
				//diag_log "towndebug not special, normal spawning rules 1";
				//roll against spawnchanse
				if (random 1 < _townPatrol) then
				{
					//diag_log "towndebug spawn some kind of troops in town 2";
					private _troopskind = random 1;
					if (_troopskind > 0 && _troopskind <= 0.33) then
					{
						//diag_log "towndebug spawn soldiers 3";
						//spawn soldiers + bunkers (if any)
						
						_temp = [_temp,east,_townTroops,_townPosition,200,"soldier",50,_townSpecialtype] call dfk_spawn_bunch_troops;
						_grparray append _temp;		
						
						//spawn bunkers
						if (count _townBunkerarray > 0) then
						{
							private _temp2 = [];
							_temp2 = [_temp2,_townBunkerarray,(count _townBunkerarray) * 3] call dfk_populate_bunkers;
							_grparray append _temp2;				
						};
						
						//spawn parked mil cars
						if (random 1 < 0.5) then
						{
							//private _crates = ["Box_T_East_Ammo_F","Box_East_Ammo_F","Box_T_East_Wps_F","Box_East_Wps_F","Box_CSAT_Equip_F","Box_East_AmmoOrd_F","Box_East_Grenades_F","Box_East_WpsLaunch_F","Box_T_East_WpsSpecial_F","Box_East_WpsSpecial_F","O_supplyCrate_F","Box_East_Support_F","Box_CSAT_Uniforms_F","Box_East_AmmoVeh_F"];
							//private _cars = ["O_MRAP_02_F","O_LSV_02_unarmed_F","O_Truck_03_transport_F","O_Truck_03_covered_F","O_Truck_02_transport_F","O_Truck_02_covered_F"];
							
							private _n = _trewps/6;
							if (_n > 2 + random 3) then {_n = 2 + random 3};
							
							private _milcars = [];
							_milcars = [_townPosition,_n,dfk_sites_parkedcars] call dfk_town_spawnparkedcars;
							
							{
								[_x] call dfk_initvehicle;
							} foreach _milcars;
							
							_veharray append _milcars;
						};		

						if (random 1 > 0.5) then
						{
							_asd = [_townPosition] call dfk_litdeparad;
							_veharray append _asd;
						};
					}
					else
					{
						//diag_log "towndebug spawn cops 4";
						//spawn "cops"
						
						_temp = [_temp,east,_trewps,_townPosition,200,"cop",50,_townSpecialtype] call dfk_spawn_bunch_troops;
						_grparray append _temp;
						
						if (random 1 < 0.5) then
						{
							//private _crates = ["Box_T_East_Ammo_F","Box_East_Ammo_F","Box_T_East_Wps_F","Box_East_Wps_F","Box_CSAT_Equip_F","Box_East_AmmoOrd_F","Box_East_Grenades_F","Box_East_WpsLaunch_F","Box_T_East_WpsSpecial_F","Box_East_WpsSpecial_F","O_supplyCrate_F","Box_East_Support_F","Box_CSAT_Uniforms_F","Box_East_AmmoVeh_F"];
							//private _cars = ["O_MRAP_02_F","O_LSV_02_unarmed_F","O_Truck_03_transport_F","O_Truck_03_covered_F","O_Truck_02_transport_F","O_Truck_02_covered_F"];
							
							private _n = _trewps/6;
							if (_n > 2 + random 3) then {_n = 2 + random 3};
							
							private _milcars = [];
							_copcars = [_townPosition,_n,roamingcar_cop] call dfk_town_spawnparkedcars;
							
							{
								[_x] call dfk_coperize_beam;								
								[_x] call dfk_initvehicle;
							} foreach _milcars;
							
							_veharray append _copcars;
						};
						
						if (random 1 > 0.5) then
						{
							_asd = [_townPosition] call dfk_litdeparad;
							_veharray append _asd;
						};						
					};
					
					_spawnedtroops = true;	
				}
				else
				{
					diag_log "towndebug not spawning troops 5";
				};
			};
		
			//spawn other things
			if (_townSpecialtype == "") then
			{
				//_veharray = [];
				//_veharray = _x select 7;
				//diag_log format ["%1 veharray check2000 %2 %3",_x select 1,_veharray,_x select 7];
			
				for [{_i = 0},{_i < random (_townSize / 7)},{_i = _i + 1}] do
				{
					private _civgrp = creategroup civilian;
					private _id = ["civ"] call dfk_generate_radioid;
					_civgrp setGroupIdGlobal [_id];								
					//_civ = _civgrp createunit ["C_man_p_beggar_F",locationposition (_x select 0), [], 120, "FORM"];
					//[_civgrp,locationposition (_x select 0),120,"civ"] remoteExec ["dfk_spawn_soldier"];
					private _civ = "";
					
					//if destruction is to great, on random, spawn animals instead
					if (_townDesctruction < 0.8) then
					{
						_civ = [_civ,_civgrp,_townPosition,120,"civ"] call dfk_spawn_soldier;
					}
						else
					{
						_civ = [_civ,_civgrp,_townPosition,120,"animal"] call dfk_spawn_soldier;
					};
					
					_grparray append [_civgrp];
					_wp =_civgrp addwaypoint [_townPosition,150];
					_wp setwaypointtype "DISMISS";
					_civgrp setbehaviour (selectrandom ["CARELESS","SAFE","AWARE"]);
					_civgrp setspeedmode (selectrandom ["LIMITED","NORMAL"]);
					_civ allowfleeing 1;
					
					//put civie indoors
					//fix later
					//[_civgrp, (_x select 0), 200,blacklist_water] call bis_fnc_taskPatrol;
				};
				
				private _civcars = [];
				_civcars = [_townPosition,_townSize/30,civiliancarsarray] call dfk_town_spawnparkedcars;
				
				_veharray append _civcars;

				private _trummer = (_townDesctruction * _townSize);
				private _roads = _townPosition nearRoads 200;
				//[format ["trümmerlevel %1",_trummer]] remoteExec ["systemchat"];
				//diag_log "max trummerlevel reached"
				if (_trummer > max_trummer) then {_trummer = max_trummer};
				for "junk" from 0 to _trummer/6 do
				{				
					private _junk = "";
					private _r = "";
					private _p = [];
					
					private _j = (selectrandom superjunkarray);
					
					if (count _roads > 0) then
					{
						//_r = selectrandom _roads;	
						_r_i = floor ((random (count _roads)));
						_r = _roads select _r_i;
						_roads deleteat _r_i;						
						_p = getpos _r findEmptyPosition [0,25,_j];					
					};
					
					private _dispersion = 0;

					if (count _p == 0) then {_p = _townPosition;_dispersion = 200};
					
					_junk = createVehicle [_j, _p, [], _dispersion, "NONE"];
					_junk setdir random 360;
					_junk enableSimulationGlobal false;
					
					_veharray pushbackunique _junk;		
				};
				//diag_log _veharray;
				
				//if high destruction, add UXO and IEDs (unexploded arty shells)
				
				private _minearr = [];
				//private _minetypesarr = ["IEDLandSmall_F","IEDUrbanSmall_F","BombCluster_02_UXO1_F","BombCluster_02_UXO4_F","BombCluster_02_UXO2_F","BombCluster_02_UXO2_F","BombCluster_02_UXO3_F","IEDLandBig_F","IEDUrbanBig_F","BombCluster_02_UXO1_F","BombCluster_02_UXO4_F","BombCluster_02_UXO2_F","BombCluster_02_UXO2_F","BombCluster_02_UXO3_F"];

				//["spawn mines!"] remoteExec ["systemchat"];
				if (globalminevar > 0) then
				{
					for "_i" from 1 to floor ((globalminevar/6) * _townDesctruction * 30) do
					{
						//_foo = 0;
						private _mine = ObjNull;
						private _minetype = selectrandom dfk_townspawn_mines;
						if (random 1 > 0.5) then
						{
							//spawn in house if possible
							_mine = createMine [_minetype, [_townPosition select 0,_townPosition select 1,0.1], [], 200];
							_mine enableSimulationGlobal true;
							private _b = nearestbuilding _mine;
							private _b_arr = _b buildingpos -1;
							//_foo = 1;
							
							if (count _b_arr > 0 && ((_b_arr select 0) select 2) > 0) then
							{
								//_foo = 2;
								private _temppos = selectrandom _b_arr;
								_mine setpos _temppos;
								//diag_log format ["%1 %2 %3 %4",_foo,getpos _mine,_temppos,_b_arr];
							};
							
							_veharray pushbackunique _mine;
						}
							else
						{
							//_foo = 3;
							//spawn on ground
							private _mine = createMine [_minetype, [_townPosition select 0,_townPosition select 1,0.1], [], 200];
							_mine enableSimulationGlobal true;
							_veharray pushbackunique _mine;
						};
					};
				};	
				//_veharray append _minearr;
				
				private _lootarray = [];
				_lootarray = [_townPosition,(5 + random 5),true] call dfk_spawn_town_loot;
				//if (debug) then {[format ["town 0 %1",_lootarray]] remoteExec ["diag_log"];};					

				_veharray append _lootarray;		
				//diag_log _veharray;
			}
			else
			//spawning of sites/important military stuff (radiomast, harbour etc,base)
			{
				//["harbor","military base","airport","radio station","power plant"];
				
				if ((_townSpecialtype == "airport") OR (_townSpecialtype == "military base")) then
				{					
					private _tankgrp = creategroup east;
					private _id = ["arty"] call dfk_generate_radioid;
					_tankgrp setGroupIdGlobal [_id];						
					//_tankskind = ["O_MBT_02_cannon_F","O_APC_Tracked_02_cannon_F","O_APC_Wheeled_02_rcws_F"];
					
					for "_asd" from 1 to random 6 do
					{
						private _pos = [_townPosition, 0, 500, 15, 0, 0.5, 0] call BIS_fnc_findSafePos;
						private _veh = createVehicle [selectrandom dfk_sites_tanks, [0,0,10000], [], 500, "NONE"];
						_veh setpos _pos;
						createvehiclecrew _veh;
						crew _veh joinsilent _tankgrp;
						[_veh] remoteExec ["dfk_initvehicle",2];
					};		

					_wp0 = _tankgrp addwaypoint [_townPosition,500];
					_wp0 setwaypointcombatmode "RED";
					_wp0 setwaypointbehaviour "COMBAT";
					_wp0 setwaypointtype "GUARD";
					
					
					//roadblocks/guardposts
					
					//big boss
					if ((_townSpecialtype == "airport")) then
					{
					
						private _bossgrp = creategroup east;
						private _id = ["boss"] call dfk_generate_radioid;
						_bossgrp setGroupIdGlobal [_id];							
						private _boss = "";
						_boss = [_boss,_bossgrp,_townPosition,25,"boss"] call dfk_spawn_soldier;
						
						for "_asd" from 1 to random 6 do
						{
							_bg = "";
							_bg = [_bg,_bossgrp,_townPosition,25,"bodyguard_opfor"] call dfk_spawn_soldier;
						};
						
						_wp0 = _bossgrp addwaypoint [_townPosition,50];
						_wp0 setwaypointcombatmode "RED";
						_wp0 setwaypointbehaviour "AWARE";
						_wp0 setwaypointtype "DISMISS";
						
						_grparray pushback _bossgrp;
					};

					//_civ allowfleeing 1;					

					//alarm
					
					//when alarm at night "i woke up like this"

					_grparray append [_tankgrp];
				};
				
				if (_townTroops > 0) then
				{
					private _drongrparray = [];
					_drongrparray = [_townPosition,_foreachindex] call dfk_spawn_guard_drone;
					_grparray append _drongrparray;
				};
		
				for [{_i = 0},{_i < random (_townSize / 30)},{_i = _i + 1}] do
				{
					private _civgrp = creategroup civilian;
					private _id = ["worker"] call dfk_generate_radioid;
					_civgrp setGroupIdGlobal [_id];							
					//_civ = _civgrp createunit ["C_man_p_beggar_F",locationposition (_x select 0), [], 120, "FORM"];
					//[_civgrp,locationposition (_x select 0),120,"civ"] remoteExec ["dfk_spawn_soldier"];
					private _civ = "";
					_civ = [_civ,_civgrp,_townPosition,75,"worker"] call dfk_spawn_soldier;
					_grparray = _grparray + [_civgrp];
					//_civ allowfleeing 1;
					[_civgrp, _townPosition, 200,blacklist_water] call bis_fnc_taskPatrol;
				};
				
				//private _crates = ["Box_T_East_Ammo_F","Box_East_Ammo_F","Box_T_East_Wps_F","Box_East_Wps_F","Box_CSAT_Equip_F","Box_East_AmmoOrd_F","Box_East_Grenades_F","Box_East_WpsLaunch_F","Box_T_East_WpsSpecial_F","Box_East_WpsSpecial_F","O_supplyCrate_F","Box_East_Support_F","Box_CSAT_Uniforms_F","Box_East_AmmoVeh_F"];
				//private _cars = ["O_MRAP_02_F","O_LSV_02_unarmed_F","O_Truck_03_transport_F","O_Truck_03_covered_F","O_Truck_02_transport_F","O_Truck_02_covered_F"];

				if (_townTroops > 0) then
				{
					private _milsiteparkedcars = [_townPosition,_townTroops/3,dfk_specialsites_paredcars] call dfk_town_spawnparkedcars;
					
					{
						[_x] call dfk_initvehicle;
						_veharray pushBackUnique _x;
					} foreach _milsiteparkedcars;
				};
				
				//_veharray append _milsiteparkedcars;						
				
				private _lootarray = [];
				_lootarray = [_townPosition,10,false] call dfk_spawn_town_loot;
				{
					_veharray pushBackUnique _x;
				} foreach _lootarray;
				
				//_veharray append _lootarray;				
			};
			
			//set is practially bad, and very expensive, replace with something more efficient...
			//TODO
			(locationarray select _forEachIndex) set [4,_spawnedtroops];
			(locationarray select _forEachIndex) set [7,_veharray];
			(locationarray select _forEachIndex) set [6,_grparray];
			(locationarray select _forEachIndex) set [3,true];
			
			//if (debug) then {diag_log format ["Site spawned2 %1 spawned %2 taken %3 troops %4 grparr %5 veh %6 townsize %7 patrol %8 destr %9 spec %10",_x select 1,_x select 3,_x select 4,_x select 5,_x select 6,count (_x select 7),_x select 8,_x select 9,_x select 10,_x select 11]};
		};	
		
		if (!(triggeractivated _townTrigger) && _townSpawned) then
		{
			//despawn shit
			private _alive = 0;
			{
				{
					if (alive _x && faction _x in BADFACTION) then {_alive = _alive + 1};
					deletevehicle vehicle _x;
					unassignvehicle _x;
					{
						deletevehicle _x;
					} foreach attachedObjects _x;
					deletevehicle _x;
				} foreach units _x;
				deletegroup _x;
			} foreach _townArray;
			//diag_log "despawn townshit derpbug, vehiclearray";
			//diag_log "array0:";
			//diag_log (_x select 7);
			{
				
				//diag_log _x;
				if (_x iskindof "ThingX") then {deletevehicle _x};
				if (_x iskindof "House_F") then
				{
					private _pbos = count (_x getvariable ["emptypositions",[]]);
					//diag_log _pbos;
					if (_pbos > 0) then
					{
						_x setvariable ["emptypositions",[],true];
					};

					private _taken = _x getvariable ["taken",false];
					//diag_log _taken;
					if (_taken) then
					{
						_x setvariable ["taken",false,true];
					};					
					
					deletevehicle _x;
				}
				else
				{
					if (count crew _x == 0) then {deletevehicle _x;};
				};
			} foreach _townVehicles;
			// diag_log "array1:";
			// diag_log (_x select 7);

						
			if (_townTaken) then {(locationarray select _forEachIndex) set [5,_alive];};			
			(locationarray select _forEachIndex) set [6,[]];
			(locationarray select _forEachIndex) set [7,[]];
			(locationarray select _forEachIndex) set [3,false];
			//diag_log "array2:";
			//diag_log (_x select 7);			
			
			//-locationposition _x,text _x,-_trg,_spawned,_taken,_troops,[],_vehicles,_townsize,_patrol,_destruction,_specialtype
			//if (debug) then {diag_log format ["Site DEspawned %1 spawned %2 taken %3 troops %4 grparr %5 veh %6 townsize %7 patrol %8 destr %9 spec %10",_x select 1,_x select 3,_x select 4,_x select 5,_x select 6,count (_x select 7),_x select 8,_x select 9,_x select 10,_x select 11]};
		};		
	} foreach locationarray;
	
	//if ((_tick mod 5 == 0) && time > 0) then {[] call dfk_morale_and_support_rollcall};
	//if ((_tick mod 5 == 0) && time > 0) then {[] call dfk_moraleStatemachine};
	
	//high pol boosts morale
	//low logistics lowers morale
	//high logistics have a small but steady moralic boost up until a certain value
	
	//values affecting each other
	//ticks each hour in-game
	//or equivalent thereof (make initparam perhaps?)
	if (_tick mod ((3600)/timemultiplier) == 0 && time > 0) then
	{
		//remove sidemorale for demoralized units
		["ACC"] remoteExecCall ["DFK_fnc_remove_morale", 2];
		
		//remove political based on dead dudes

		if (logistics < 0) then {sidemorale = sidemorale - 5};
		if (politics < 0) then {sidemorale = sidemorale - 5};		
		
		if (politics > 1000 && sidemorale < 1000) then {sidemorale = sidemorale + 5};
		if (logistics > 1000 && sidemorale < 1200 && sidemorale > 1000) then {sidemorale = sidemorale + 1};
	};
	
	//_victoryconditions
	//1"Any",2"All",3"Kill 'em all", 4"Logistical",5Morale",6"Political",7"Log+Mor",8"Log+Pol",9"Mor+Pol"
	if (_victoryconditions == 1 && ((aliveenemies <= 0) OR (logistics <= 0) OR (politics <= 0) OR (sidemorale <= 0))) then
	{
		_maingame = false;
		"EveryoneWon" call BIS_fnc_endMissionServer;
	};
	
	if (_victoryconditions == 2 && ((aliveenemies <= 0) && (logistics <= 0) && (politics <= 0) && (sidemorale <= 0))) then
	{
		_maingame = false;
		"EveryoneWon" call BIS_fnc_endMissionServer;
	};	
	
	if (aliveenemies <= 0 && _victoryconditions == 3) then
	{
		_maingame = false;
		"EveryoneWon" call BIS_fnc_endMissionServer;
	};
	
	if (logistics <= 0 && _victoryconditions == 4) then
	{
		_maingame = false;
		"EveryoneWon" call BIS_fnc_endMissionServer;
	};

	if (sidemorale <= 0 && _victoryconditions == 5) then
	{
		_maingame = false;
		"EveryoneWon" call BIS_fnc_endMissionServer;
	};
	
	if (politics <= 0 && _victoryconditions == 6) then
	{
		_maingame = false;
		"EveryoneWon" call BIS_fnc_endMissionServer;
	};	
	
	if (logistics <= 0 && sidemorale <= 0 && _victoryconditions == 7) then
	{
		_maingame = false;
		"EveryoneWon" call BIS_fnc_endMissionServer;
	};	
	
	if (logistics <= 0 && politics <= 0 && _victoryconditions == 8) then
	{
		_maingame = false;
		"EveryoneWon" call BIS_fnc_endMissionServer;
	};	
	
	if (politics <= 0 && sidemorale <= 0 && _victoryconditions == 9) then
	{
		_maingame = false;
		"EveryoneWon" call BIS_fnc_endMissionServer;
	};
	
	if (politics <= 0 && sidemorale <= 0 && _victoryconditions == 9) then
	{
		_maingame = false;
		"EveryoneWon" call BIS_fnc_endMissionServer;
	};	
	
	_currentlives = [resistance,0] call BIS_fnc_respawnTickets;
	if (_currentlives <= 0) then
	{
		_maingame = false;
		"EveryoneLost" call BIS_fnc_endMissionServer;
	};		
	
	if (endgame) then
	{
		_maingame = false;
		"EveryoneLost" call BIS_fnc_endMissionServer;
	};

	
	//[format ["%1 %2",daytime % _airboundlogisticsresupplytick,_cuckadoodledo]] remoteExec ["systemchat"];	
	
	//resupply low logistics
	private _groundresupply = [];
	private _airresupply = [];	
	
	
	//supply convoy
	if ((daytime % _airboundlogisticsresupplytick > 0 && daytime % _airboundlogisticsresupplytick <= 0.2) && !_cuckadoodledo) then
	{
		_cuckadoodledo = true;
		//[format ["logisticsairresupply! %1 %2 %3 %4",(daytime % _airboundlogisticsresupplytick),daytime,_cuckadoodledo,logistics]] remoteExec ["systemchat"];
		//diag_log format ["logisticsairresupply! %1 %2 %3 %4",(daytime % _airboundlogisticsresupplytick),daytime,_cuckadoodledo,logistics];
		
		//diag_log format ["_targetlogisticslevel %1 _politicssupportthreshhold %2 vs log %3 pol %4",_targetlogisticslevel,_politicssupportthreshhold,logistics,politics];	
		{
			if (!alive _x) then {airlogisticsarray deleteat _foreachindex}
		} foreach airlogisticsarray;
		
		if (logistics < _targetlogisticslevel && politics >= _politicssupportthreshhold) then
		{
			//support!
			//support is expensive, hence, very finite :P
			if (logistics > 0 && politics >= _politicssupportthreshhold && count airlogisticsarray < 6) then
			{
				[baselogic] execvm "dfk_logisticssupport.sqf";
				["LOGISTICS"] remoteExec ["DFK_fnc_remove_political", 2];
				_airboundlogisticsresupplytick = ["resupplytick"] call BIS_fnc_getParamValue;
			};
			
			if (logistics < 0 && politics >= _politicssupportthreshhold && count airlogisticsarray < 6) then
			{
				[baselogic] execvm "dfk_logisticssupport.sqf";
				["LOGISTICS"] remoteExec ["DFK_fnc_remove_political", 2];
				["LOGISTICS"] remoteExec ["DFK_fnc_remove_political", 2];
				_airboundlogisticsresupplytick = 1;
			};			

			if (logistics < 0 && politics < _politicssupportthreshhold) then
			{
				diag_log "No airsupport for you!";
			};
			//else
			//{
				//resupplying logistics is hardcoded atm
				//LOGISTICS = 50;
				//AIRDELIVERYSMALL = 400
				//AIRDELIVERYBIG = 1000
				
				//panicky "oh shitshitshit RESUPPLY NOWWEWEEHHEHEEHHE!
				//perhaps increase this cost?
				//not cheap to scramble a bunch of supply from elsewhere...
				//just to get logistics above 0
				
				//scramble to reestablish logistics to 1000...
				/* private _n = (ceil ((1000 - logistics)/dfk_airdrop_logisticsvalue1)) + 1;
				
				//_n = (ceil (logistics/400)) + 1;
				if (_n < 0 && _n != 0) then {_n = _n * -1};
				private _cost = _n * 100;
				diag_log format ["scramble logistics resupply, log %1 _n %2 _cost %3",logistics,_n,_cost];
				
				if (_cost < politics) then
				{
					for "_i" from 1 to _n do
					{
						[baselogic] execvm "dfk_logisticssupport.sqf";
						["LOGISTICS"] remoteExec ["DFK_fnc_remove_political", 2];
						["LOGISTICS"] remoteExec ["DFK_fnc_remove_political", 2];
					};
				}
				else
				{
					_n2 = ceil(politics/50);
					diag_log format ["scramble2 logistics resupply, _n2 %1",_n2];
					
					for "_i" from 1 to _n2 do
					{
						[baselogic] execvm "dfk_logisticssupport.sqf";
						["LOGISTICS"] remoteExec ["DFK_fnc_remove_political", 2];
						["LOGISTICS"] remoteExec ["DFK_fnc_remove_political", 2];
					};
				}; */
				
			//};
		};
		
		//compile "logistics" as in troopscount, current troops vs whatever it should be
		//set as supplyconvoytarget if needed
		
		{
			//-locationposition _x,text _x,-_trg,_spawned,_taken,_troops,[],_vehicles,_townsize,_patrol,_destruction,_specialtype
			//[format ["Site spawned1 %1 spawned %2 taken %3 troops %4 grparr %5 veh %6 townsize %7 patrol %8 destr %9 spec %10",_x select 1,_x select 3,_x select 4,_x select 5,_x select 6,count (_x select 7),_x select 8,_x select 9,_x select 10,_x select 11]] remoteExec ["systemchat"];			
			
			
			
			private _basepos = _x select 0;
			private _spawned = _x select 3;
			private _troops = _x select 5;
			private _townsize = _x select 8;
			private _special = _x select 11;
			private _init_troops = _x select 12;
					
			private _ground = true;
			private _air = false;
						
			if (_init_troops > _troops && !_spawned) then
			{
				{if (_basepos inarea _x) then {_ground = false;_air = true;}} foreach islandblacklist;
				
				if (_ground) then
				{
					_groundresupply append [[_forEachIndex,_x select 1,_basepos,_troops,_init_troops]];
				};
				
				if (_air) then
				{
					_airresupply append [[_forEachIndex,_x select 1,_basepos,_troops,_init_troops]];
				};
			};
		} foreach locationarray;
				
		private _a = {alive _x} count _supplytruckarray;
		
		if (count _groundresupply > 0 && !sentsupply && _a <= 0) then
		{	
			sentsupply = true;
			_supplytruckarray = [];
			if (debug) then {[format ["resupply\n%1",_groundresupply]] remoteExec ["systemchat"];};
			
			_supportgrp = creategroup east;
			_supportgrp_dudes = creategroup east;
			
			private _id = ["supplytruck"] call dfk_generate_radioid;
			_supportgrp setGroupIdGlobal [_id];			
		
			private _id2 = ["supplytruck_dudes"] call dfk_generate_radioid;
			_supportgrp_dudes setGroupIdGlobal [_id2];
			
			private _spawnpos_array = [];
			//_spawnpos_array = [getpos baselogic] call dfk_town_spawnpoints_convoy;
			//
			//diag_log _spawnpos_array;	
			
			{
				deletevehicle _x;
			} foreach list baselogic_support;			
			
			for "_asd" from 0 to 9 do
			{
				_spawnpos_array append [[((getpos baselogic_support) getpos [(((triggerarea baselogic_support) select 1) / 2) - (_asd * 10),getdir baselogic_support]),getdir baselogic_support]];
			};
			
			//{[_x select 0,"colorred","","hd_dot",0.2] call dfk_generate_marker;} foreach _spawnpos_array;
			
			//player setpos ((_spawnpos_array select ((count _spawnpos_array) - 1)) select 0);
			
			private _temp_vehicle_array = [];
			for "_i" from 0 to 3 do
			{
				private _convoykind = "truck";
				if (_i == 0 or _i == 4 or _i == 8) then {_convoykind = "escort"} else {_convoykind = "truck"};
				
				switch(_convoykind) do
				{
					case "truck":
					{
						//_pos = [getpos baselogic, 0, 100, 15, 0, 20, 0] call BIS_fnc_findSafePos;
						private _pos = _spawnpos_array select _i;
						//diag_log format ["main supplyconvoy posshit %1",_pos];
						_supplytruck = createVehicle [selectrandom supplytruckkindarray,_pos select 0, [], 0, "NONE"];
						_supplytruck setdir (_pos select 1);
						
						_supportgrp addVehicle _supplytruck;
						[_supportgrp,_supplytruck,"support"] remoteExec ["dfk_spawn_crew",2];
						[crew _supplytruck] joinsilent _supportgrp;	
						if (debug) then {[_supplytruck,"colorpink","Supply","hd_destroy",true,1] execvm "marker_track.sqf";};
						//[_supplytruck,"colorpink","Supply","hd_destroy",true,1] execvm "marker_track.sqf";
						[_supplytruck,"colorpink","Supply","hd_destroy",1,1,1,60,200] spawn dfk_shitty_marker_tracker;
						_supplytruckarray pushback _supplytruck;
						_supplytruck setvariable ["supplytruckgrp",_supportgrp,true];
						_supplytruck addMPEventHandler ["mpkilled", {[_this select 0,5,"supplytruckdrop"] remoteexec ["dfk_drop_loot",2];}];	
						_temp_vehicle_array	pushBackUnique _supplytruck;
						[_supplytruck] remoteExec ["dfk_initvehicle",2];
						diag_log format ["supplyconvoy %1 %2",_supplytruck,crew _supplytruck];
					};
					
					case "escort":
					{
						 private _pos = _spawnpos_array select ((count _spawnpos_array) - 1);
						 
						 _escortkind = [] call dfk_apropriate_escort;
						 
						_escort = createVehicle [_escortkind,_pos select 0, [], 0, "NONE"];
						//[_escort,"colorpink","escort","hd_destroy",true,1] execvm "marker_track.sqf";
						_escort setdir (_pos select 1);
						[_escort] remoteExec ["dfk_initvehicle",2];				
						_supportgrp addVehicle _escort;
						[_supportgrp,_escort,"supportescort"] remoteExec ["dfk_spawn_crew",2];
						[crew _escort] joinsilent _supportgrp;				
						_temp_vehicle_array	pushBackUnique _escort;						
					};
				};
			};
			
			{
				{
					[(_x select 0)] joinsilent _supportgrp_dudes;
				} foreach (fullcrew [_x,"cargo",false]);
			} foreach _temp_vehicle_array;
			
			[leader _supportgrp] call dfk_equip_pltch;
			
			_supportgrp setbehaviour "SAFE";
			_supportgrp setspeedmode "FULL";
			//_supportgrp setcombatmode "GREEN";
			_supportgrp setformation "COLUMN";
			
			private _maxspeed = 999999;
			{
				private _speed =getnumber  (configfile >> "CfgVehicles" >> typeof _x >> "maxspeed");
				_maxspeed = _maxspeed min _speed;
				
				//_x forceFollowRoad true;
				//_x setConvoySeparation 15;
				_x setUnloadInCombat [false,false];
			} foreach units _supportgrp;
			
			(leader _supportgrp) limitspeed (_maxspeed -10);
			for "_i" from 1 to ((count units _supportgrp) - 1) do
			{
				vehicle ((units _supportgrp) select _i) limitspeed _maxspeed;
			};
			
			// _sca = [];
			// _supportgrp setvariable ["supplyconvoyarray",_groundresupply,true];
			{_x setwppos getpos leader _supportgrp} foreach waypoints _supportgrp;

			_wp0 = _supportgrp addwaypoint [getpos baselogic_kp,0];
			
			_sca = [];
			private _snooperlist = [];
			//diag_log format ["sca 0 %1", _groundresupply];
			//diag_log format ["sca 0 %1", _groundresupply];
			
			{
				//diag_log format ["sca 1 %1", _x];
				_index = _x select 0;
				_name = _x select 1;
				_pos = _x select 2;
				_troops = _x select 3;
				_init_troops = _x select 4;
				
				_snooperlist pushback [_pos,_name];
					
				_sca pushback _index;
				_roadpos = getpos (selectrandom (_pos nearroads 50));

				_wpx = _supportgrp addwaypoint [_pos,0];
				_wpx setwaypointtimeout [60,150,300];
				_wpx setWaypointStatements ["true", "[group this] remoteExec ['dfk_supplyconvoy_delivery',2]"];
			} foreach _groundresupply;
			
			_supportgrp setvariable ["supplyconvoyarray",_sca,true];
			
			["supplyrun",_snooperlist] call dfk_radio_snooper;
			
			//diag_log format ["sca 2 %1", _sca];
			//diag_log format ["sca 3 %1",_supportgrp getvariable "supplyconvoyarray"];
			
			_wp_end = _supportgrp addwaypoint [getpos baselogic_kp,0];
			_wp_end setwaypointstatements ["true","[group this,getpos this] remoteExec ['dfk_removegroup',2];sentsupply = false;"];
		};
	};
	
	private _abortsupply = true;
	{
		if (alive _x and canmove _x and (fuel _x > 0) and alive (driver _x)) then 
		{
			_abortsupply = false;
		};
		// else
		// {
			//diag_log format ["supplytruck %1 %2 %3 %4 %5 %6 %7 %8",_x,alive _x,canmove _x,(fuel _x > 0),alive (driver _x),(alive _x and canmove _x and (fuel _x > 0) and alive (driver _x))];
			// private _grp = _x getvariable ["supplytruckgrp",grpnull];
			// {
				// deletewaypoint _x;
			// } foreach waypoints _grp;
			
			// diag_log "supplyconvoy fubbed, RTB";
			// _wp_end = _grp addwaypoint [getpos baselogic_kp,0];
			// _wp_end setwaypointstatements ["true","[group this,getpos this] remoteExec ['dfk_removegroup',2];sentsupply = false;"];
			// _grp setspeedmode "FULL";
		// };
	} foreach _supplytruckarray;
	
	if (_abortsupply) then 
	{
		sentsupply = false;
		//_supplyescort = true;
		
		{
			private _grp = _x getvariable ["supplytruckgrp",grpnull];
			{
				deletewaypoint _x;
			} foreach waypoints _grp;
			
			diag_log "supplyconvoy fubbed, RTB";
			_wp_end = _grp addwaypoint [getpos baselogic_kp,0];
			_wp_end setwaypointstatements ["true","[group this,getpos this] remoteExec ['dfk_removegroup',2];sentsupply = false;"];
			
			{
				_x domove getpos baselogic_kp;
			} foreach units _grp;
			
			_grp setspeedmode "FULL";
			_grp setbehaviour "CARELESS";
		};
	}
	else
	{
		private _temparr = [];
		
		{
			_temparr append [[_x,group _x]];
		} foreach _supplytruckarray;
		
		[_temparr] call dfk_count_alive_cars;
	};
	
	
	
	//reset supply var

	if ((daytime % _airboundlogisticsresupplytick > 0.2) && _cuckadoodledo) then 
	{
		// [format ["_cuckadoodledo reset! %1 %2 %3 %4",(daytime % _airboundlogisticsresupplytick),daytime,_cuckadoodledo,logistics]] remoteExec ["systemchat"];
		// diag_log format ["_cuckadoodledo reset! %1 %2 %3 %4",(daytime % _airboundlogisticsresupplytick),daytime,_cuckadoodledo,logistics];	
		_cuckadoodledo = false;
	};
	
	//ambulating cars and stuff
	//roaming
	//patrol
	if (_tick mod 10 == 0 && time > 5) then
	{
		//diag_log format ["1 _alivecars %1 _alivecarsarray %2 _aliveair %3 _aliveairarray %4",_alivecars,_alivecarsarray,_aliveair,_aliveairarray];
	
		private _alivecars = [_alivecarsarray] call dfk_count_alive_cars;
		private _aliveAir = [_aliveAirArray] call dfk_count_alive_cars;
		
		diag_log format ["1 _alivecars %1 _alivecarsarray %2 _aliveair %3 _aliveairarray %4",_alivecars,_alivecarsarray,_aliveair,_aliveairarray];
	
		if (_alivecars < _alivecarstarget) then
		{
			private _temparr = [];
			
			private _targetdude = selectrandom allplayers;

			{
				private _loc = _x;
				if (!(_loc in _temparr) && ((locationposition _loc) distance _targetdude > 600) && !(locationposition _loc inArea island1) && !(locationposition _loc inArea island2)) then
				{
					_temparr pushback _loc;
				};
				
			} forEach nearestLocations [getpos _targetdude, ["NameCityCapital","NameVillage","NameLocal"], 3000];
			
			//[format ["%1",_temparr]] remoteExec ["systemchat"];	
						
			if (count _temparr > 0) then
			{
				if (_alivecarstarget > _alivecars) then
				{
					private _pos = locationposition (selectrandom _temparr);
					
					//[format ["1 %1",_temparr]] remoteExec ["systemchat"];	
					//[format ["2 %1",_pos]] remoteExec ["systemchat"];
					//_veharr = [_pos,_alivecarsarray,""] call dfk_spawn_car;
					private _veharr = [];
					//if (0.1 > random 1) then {_veharr = [_pos,_alivecarsarray] call dfk_spawn_hkp;} else {_veharr = [_pos,_alivecarsarray] call dfk_spawn_car;};
					
					_veharr = [_pos,_alivecarsarray] call dfk_spawn_car;
					
					private _car = _veharr select 0;
					private _grp = _veharr select 1;
					_alivecarsarray append [_veharr];
					//[format ["3 %1 %2 %3",_veharr,_grp,_alivecarsarray]] remoteExec ["systemchat"];

					private _str = str format ["%1",gettext (configfile >> "CfgVehicles" >> typeof _car >> "displayname")];
					if (debug) then {[_car,"coloryellow",_str,"hd_arrow",true,1] execvm "marker_track.sqf";};

					private _location0 = locationposition (selectrandom _temparr);
					_wp0 = _grp addwaypoint [_location0,0];
					
					(driver _car) domove _location0;
					
					_wp10 = _grp addwaypoint [locationposition (selectrandom _temparr),0];
					_wp10 setwaypointstatements ["true","[group this,getpos this] spawn dfk_removegroup"];

					_grp setspeedmode (selectrandom ["NORMAL","FULL"]);
					_grp setbehaviour (selectrandom ["SAFE","AWARE","CARELESS","STEALTH"]);
					_grp setcombatmode (selectrandom ["RED","GREEN","WHITE","YELLOW"]);

					//[format ["4 car %1 %7 from %2 to %3 at %4, %5, %6",_car,_pos,getwppos _wp10,speedmode _grp, behaviour leader _grp,combatmode _grp,_pos]] remoteExec ["systemchat"];
				};	
			}
			else
			{
				//well, spawn bloody boats then!
				//make sure players are at sea
				private _waterarr = [];
				{
					if (surfaceIsWater position _x) then
					{
						_waterarr append [_x];
					};
				} foreach allplayers;
				
				if (count _waterarr > 0) then
				{
					if (_alivecarstarget > _alivecars) then
					{
						private _waterboi = selectrandom _waterarr;
					
						private _veharr = [];
						if (0.1 > random 1) then {_veharr = [getpos _waterboi,_alivecarsarray] call dfk_spawn_hkp;} else {_veharr = [getpos _waterboi,_alivecarsarray] call dfk_spawn_boat;};
						_alivecarsarray append [_veharr];	
						private _boat = _veharr select 0;
						private _grp = _veharr select 1;
						
						private _str = str format ["%1",gettext (configfile >> "CfgVehicles" >> typeof _boat >> "displayname")];
						
						if (debug) then {[_boat,"colorblue",_str,"hd_arrow",true,1] execvm "marker_track.sqf";};
						
						//sätt mitt framför player, dispersion 500 m för spridning etc
						//använd relpos i setpos/getpos
						
						//s = v * t;
						//t = v * s;
						private _p1 = _waterboi getpos [(speed _waterboi) * (1000 + random 2000),getdir _waterboi];
						private _direction1 = [_boat,_p1] call BIS_fnc_DirTo;
						private _p2 = _p1 getpos [1000,_direction1];
						
						_boat setdir _direction1;
						
						if (debug) then {[_p1,"colorblue","p1","hd_destroy",0.6] remoteExec ["dfk_generate_marker",2];};
						if (debug) then {[_p2,"colorblue","p1","hd_destroy",0.6] remoteExec ["dfk_generate_marker",2];};
						
						_wp0 = _grp addwaypoint [_p1,200];
						
						_wp10 = _grp addwaypoint [_p2,500];
						_wp10 setwaypointstatements ["true","[group this,getpos this] spawn dfk_removegroup"];
						
						_grp setspeedmode (selectrandom ["LIMITED","NORMAL","FULL"]);
						_grp setbehaviour (selectrandom ["SAFE","AWARE","CARELESS","STEALTH"]);
						_grp setcombatmode (selectrandom ["RED","GREEN","BLUE"]);	
						//diag_log format ["veharr %1 alivecarsarray %2 boat %3 getpos boat %4 grp %5 str %6 p1 %7 p2 %8 dir1 %9 wp0 %10 wp10 %11",_veharr,_alivecarsarray,_boat,getpos _boat,_grp,_str,_p1,_p2,_direction1,_wp0,_wp10];
					};
				};
			};
		};
		
		if (_aliveAir < _aliveAirTarget && (diag_fps > 20)) then
		{
			private _pos = [selectrandom [(dfk_map_x - dfk_map_raduis),(dfk_map_x + dfk_map_raduis)],selectrandom [(dfk_map_y - dfk_map_raduis),(dfk_map_y + dfk_map_raduis)]];
			//[format ["1 %1",_temparr]] remoteExec ["systemchat"];	
			//[format ["2 %1",_pos]] remoteExec ["systemchat"];
			//_veharr = [_pos,_alivecarsarray,""] call dfk_spawn_car;
			private _veharr = [];
			//if (0.1 > random 1) then {_veharr = [_pos,_alivecarsarray] call dfk_spawn_hkp;} else {_veharr = [_pos,_alivecarsarray] call dfk_spawn_car;};
			
			_veharr = [_pos,_aliveairarray] call dfk_spawn_hkp;
			
			private _car = _veharr select 0;
			private _grp = _veharr select 1;
			_aliveairarray append [_veharr];
			
			if (_car iskindof "UAV") then {[_car] execvm "dfk_dronesearch.sqf"};
			[_car] call dfk_arm_roaming_aircraft;
			
			if (typeof _car in roaming_mercplanes) then
			{
				_car flyinheight (200 + (random 300));
				
				if (isServer && _car iskindof "O_T_VTOL_02_infantry_dynamicLoadout_F") then
				{
					[_car,[0,"\A3\Air_F_Exp\VTOL_02\Data\VTOL_02_EXT01_bhex_CO.paa"]] remoteExec ["setObjectTexture",0,true];
					[_car,[1,"\A3\Air_F_Exp\VTOL_02\Data\VTOL_02_EXT02_bhex_CO.paa"]] remoteExec ["setObjectTexture",0,true];
					[_car,[2,"\A3\Air_F_Exp\VTOL_02\Data\VTOL_02_EXT03_L_bhex_CO.paa"]] remoteExec ["setObjectTexture",0,true];
					[_car,[3,"\A3\Air_F_Exp\VTOL_02\Data\VTOL_02_EXT03_R_bhex_CO.paa"]] remoteExec ["setObjectTexture",0,true];
				};

				{
					_x addMPEventHandler ["mpkilled", {[_this select 0,3,"roamingplanecrew"] remoteexec ["dfk_drop_loot",2];}];
				} foreach crew _car;
				
				_car addMPEventHandler ["mpkilled", {[_this select 0,5,"roamingplane"] remoteexec ["dfk_drop_loot",2];}];
				
				//_car setvariable ["donotdelete",true,true];
			
				if (random 1 > 0.5) then
				{
					_wp0 = _grp addwaypoint [[14760.9,16320.8,0],0];
					_wp0 setwaypointtype "SCRIPTED";
					_wp0 setWaypointScript "A3\functions_f\waypoints\fn_wpLand.sqf";
					
					_wp1 = _grp addwaypoint [[14684.1,16656.9,0],0];
					_wp1 setwaypointtype "GETOUT";
					
					_wp2 = _grp addwaypoint [[14551.9,16852.9,0],0];
				}
				else
				{
					for "_wp_x" from 0 to random 5 do
					{
						if (random 1 > 0.5) then
						{
							_wp = _grp addwaypoint [[dfk_map_x,dfk_map_y],random (dfk_map_raduis / 3)];
						}
						else
						{
							_wp = _grp addwaypoint [getpos (selectrandom allplayers),500];
						};
					};
				
					_wp = _grp addwaypoint [[selectrandom [(dfk_map_x - random dfk_map_raduis),(dfk_map_x + random dfk_map_raduis)],selectrandom [(dfk_map_y - random dfk_map_raduis),(dfk_map_y + random dfk_map_raduis)]],0];
					_wp setwaypointstatements ["true","[group this,getpos this] spawn dfk_removegroup"];
				};
			}
			else
			{
				for "_wp_x" from 0 to random 5 do
				{
					if (random 1 > 0.5) then
					{
						_wp = _grp addwaypoint [[dfk_map_x,dfk_map_y],random (dfk_map_raduis / 3)];
					}
					else
					{
						_wp = _grp addwaypoint [getpos (selectrandom allplayers),500];
					};
				};				
			
				_wp = _grp addwaypoint [[selectrandom [(dfk_map_x - random dfk_map_raduis),(dfk_map_x + random dfk_map_raduis)],selectrandom [(dfk_map_y random - dfk_map_raduis),(dfk_map_y + random dfk_map_raduis)]],0];
				_wp setwaypointstatements ["true","[group this,getpos this] spawn dfk_removegroup"];
			};	
		};
	};
	
	//eventhandlers are a pain...
	//GOFW it is then...
	//loops over logistical target buildings and checks their status
	//removes them from the list and removes logistics if dead
	if (_tick mod 60 == 0 && time > 0) then
	{
		{
			if (!alive _x) then
			{
				[_x] remoteExec ["DFK_fnc_remove_logistics", 2]; 
				_logisticstargetsalivetrackingarray deleteat _foreachindex;
				_dfk_record_destruction1 = _dfk_record_destruction1 + 1;
				_dfk_record_destruction2 = _dfk_record_destruction2 + 1;
			};
		} foreach _logisticstargetsalivetrackingarray;
	};
	
	if (time > 0 && (daytime % 24 > 0) && (daytime % 24 <= 0.1)) then
	{
		{
			_bombkind = selectrandom ["IEDUrbanBig_Remote_Mag","IEDLandBig_Remote_Mag","IEDUrbanSmall_Remote_Mag","IEDLandSmall_Remote_Mag"];
			_x addMagazineCargoGlobal [_bombkind,1];
		} foreach ammoboxarray;
	};
	
	
	//print status 4 times each 24 ingame hours
	if (
			time > 0 && (daytime % 3 > 0 && daytime % 3 <= 0.2) || dfk_printstatus
			// (daytime >= 6 && daytime < 6.025) OR 
			// (daytime >= 12 && daytime < 12.025) OR 
			// (daytime >= 18 && daytime < 18.025) OR 
			// (daytime >= 0 && daytime < 0.025)
			
		) then
	{
		_hintstr = format ["STATUS: %12\nMorale: %1\nLogistics: %2\nPolitics: %3\nAlive enemies: %4\n\nWeatherforcast:\nCurrent Fog: %6 Cloudcover: %7 Rainchanse: %13 Until %8 hours\nThen fog: %9 and cloudcover %10\n\nRespawn tickets: %11",sidemorale,logistics,politics,aliveenemies,_groundresupply,fog,overcast,nextweatherchange/3600,fogForecast,overcastforecast,_airresupply,date,rain,_currentlives];
		
		private _killstr = "";
		
		if (dfk_storekind_type == 4) then 
		{
			private _kills = 0;
			_kills = _kills + (floor (funds /10000));
			private _lvl = 0;
			
			_lvl = (selectrandom allplayers) getvariable ["gunlvl",0];
			
			if (count allplayers > 0) then
			{	
				{
					private _arr = getPlayerScores _x;
					
					private _inf = (_arr select 0);
					private _soft = ((_arr select 1) * 1.25);
					private _hard = (_arr select 2) * 1.5;
					private _air = (_arr select 3) * 2;
					private _score = (_arr select 5) - (_arr select 0) - (_arr select 1) - (_arr select 2) - (_arr select 3);
					
					_kills = _kills + _inf + _soft + _hard + _air + _score;
					
					private _derp = _x getvariable ["dfk_ai_kills",0];
					diag_log format ["dfk_ai_kills (main.sqf) %1",_derp];
					_kills = _kills + _derp;
				} foreach allplayers;
			};
			
			_hintstr = format ["%1\nKill-Score: %2\nLevel: %3",_hintstr,_kills,_lvl];
		};
			
		if (dfk_printstatus) then {dfk_printstatus = false;publicvariable "dfk_printstatus"};
	
		[_hintstr] remoteExec ["hintsilent"]; 
		// diag_log format ["time %1",time];
		// diag_log format ["Allgroups %1", count allgroups];
		// diag_log format ["allunits %1", count allunits];
		// diag_log format ["vehicles %1", count vehicles];
		// diag_log format ["mor %1 pol %2 log %3",sidemorale,politics,logistics];
		// diag_log format ["funds %1 secrets %2 rabatt %3", funds,secrets,rabatt];
		// diag_log format ["qrf pool %1",count dfk_qrf_pool_array];
		// diag_log format ["drugrunner level %1 delivery %2 target %3",dfk_drugrunnerlevel,dfk_drugrunnerdelivery,dfk_drugrunnertarget];
		// diag_log format ["artilleryarray %1",artilleryarray];	
	};	
	
	// if (_tick mod 120 == 0 && time > 1 && debug) then
	// {
		// ["dfk_debugstatus.sqf"] remoteexec ["execvm",2];
	// };
	
	if (_tick mod 30 == 0 && time > 1) then
	{
		[] remoteExec ["dfk_artyradar",2];
	};	
	
	if (_tick mod 60 == 0 && time > 0) then
	{
		{deletegroup _x} foreach allgroups;
		
		//diag_log dfk_respawnMarkersArray;
		{
			//diag_log format ["main dfk_jingleJangleRespawnPoints %1", _x];
			[_x] call dfk_jingleJangleRespawnPoints;
		} foreach dfk_respawnMarkersArray;	
	};
	
	//if (_tick % 60 == 0) then
	//{
		//private _allgrps = count allgroups;
		//private _allunits = count allunits;
		//private _allvehicles = count vehicles;
		//private _qrfpool = count dfk_qrf_pool_array;
		
		//[format ["Daytime: %1\nAllgroups: %2\nAllunits: %3\nVehicles: %4\nMor %5 Pol %6 Log %7\nFunds %8 Secrets %9 Rabatt %10\nQRF: %11\n\nViewdistance %12 Town spawndistance %13\nTrummer %14 Campsize %15\nRoaminglimit %16/%17",daytime,_allgrps,_allunits,_allvehicles,sidemorale,politics,logistics,funds,secrets,rabatt,_qrfpool,viewdistance,spawn_distance,max_trummer,campsize,_alivecars,_alivecarstarget]] remoteExec ["hintsilent"];
		//diag_log format ["SUPERTICKDEBUG Daytime: %1 Allgroups: %2 Allunits: %3 Vehicles: %4 Mor: %5 Pol: %6 Log: %7 Funds: %8 Secrets: %9 Rabatt: %10 QRF: %11 Viewdistance: %12 Townspawndistance: %13 Trummer: %14 Campsize: %15 Roaminglimit: %16/%17",daytime,_allgrps,_allunits,_allvehicles,sidemorale,politics,logistics,funds,secrets,rabatt,_qrfpool,viewdistance,spawn_distance,max_trummer,campsize,_alivecars,_alivecarstarget]	
	//};
	
	sleep 1;
	_tick = _tick + 1;
};

private _civkillstr = "<br />";
{
	_civkillstr = _civkillstr + "        "  + _x + "<br />";
} foreach res_civkills_array;

private _leavequotes = "";
//kill qoutes
_killquotes =
[
	"Man... sometime you just wonder... why did I make it out? No-one else did? ... - Lone Survivor of Talon Squad",
	"They were commin' outta the walls! THE GODDAMN WALLS!",
	"It was like... you got 5,6,7,8- ... 10! But somehow, there was always an 11th guy and a 12th guy... Man... I was lucky to get out the way I did...",
	"They were everywhere... and nowhere... man..."
];
//logistics quotes
_logisticsquotes =
[
	"That place was just a nightmare... you had to go on patrol in yo briefs man!... - Unkown Pvt.",
	"No ammo, no fuel, hardly any guns, no support... we ate fucking dirt! - Unkown",
	"At one time I found an old candybar... someone had stepped on it... it was the best... best thing that happened that week",
	"The WiFi went down... then the showers stopped working... then the power... we were... I mean... uuughh - Sgt 1 Platoon",
	"I ate cardboard...",
	"Man, I didn't watch porn for a whole month! A MONTH! Do you have any idea what that does to a man?",
	"... we made dirt angels on the downtime"
];
//political quotes
_politicalquotes =
[
	"... unknown sources cite growing cost and manpower issues, [REDACTED] declined to comment, instead refering to it as refocusing of strategic assets",
	"... citing leaked images and documents proving the indiscrimate use of cluster-munitions on civilian villages Gen. [REDACTED] declined to comment...",
	"... as scandals regard officers being caught in alleged trafficing in the conflict area...",
	"... a video of troops of the 'Peace keeping force' shooting stray dogs, laughing, all to a heavy metal soundtrack",
	"... images of indiscriminate violence against unarmed civilians and aidworkers... the department declined to comment citing an ongoing investigation",
	"... from the Hauge today as the final cases of the ... war tribinal adjoured, all were sentenced to life in jail while pleading innocense or claiming to just following orders..."
];
//morale quotes
_moraleqoutes =
[
	"Man, it was just... just... They thretend to execute me... for disobeying... refusing to go on patrol... I was like... 'whatever man'!",
	"What were we doin' over there anyway?",
	"What did I do with my life? I dunno man..."
];

//_victoryconditions
//1"Any",2"All",3"Kill 'em all", 4"Logistical",5Morale",6"Political",7"Log+Mor",8"Log+Pol",9"Mor+Pol"

//add kill qoutes
if (_victoryconditions == 1 or _victoryconditions == 2 or _victoryconditions == 3) then
{
	_leavequotes = _leavequotes + "   " + (selectrandom _killquotes) + "<br /><br />";
};

//log
if (_victoryconditions == 1 or _victoryconditions == 2 or _victoryconditions == 4 or _victoryconditions == 7 or _victoryconditions == 8) then
{
	_leavequotes = _leavequotes + "   " + (selectrandom _logisticsquotes) + "<br /><br />";
};

//pol
if (_victoryconditions == 1 or _victoryconditions == 2 or _victoryconditions == 6 or _victoryconditions == 8 or _victoryconditions == 9) then
{
	_leavequotes = _leavequotes + "   " + (selectrandom _politicalquotes) + "<br /><br />";
};

//mor
if (_victoryconditions == 1 or _victoryconditions == 2 or _victoryconditions == 5 or _victoryconditions == 7 or _victoryconditions == 9) then
{
	_leavequotes = _leavequotes + "   " + (selectrandom _moraleqoutes) + "<br /><br />";
};

_livesatendofgame = [missionNamespace] call BIS_fnc_respawnTickets;

_destroyedfarmland = 1;
_minesremaining = 0;
if (globalminevar > 0 && _globaldestruction > 0) then
{
	_destroyedfarmland = round (random (globalminevar * _globaldestruction));
	_minesremaining = (((globalminevar/10) * _globaldestruction * 50) * (count locationarray));
}
else
{
	_destroyedfarmland = round (random 1 * (count locationarray));
	_minesremaining = ((random 1 * random 1 * 50) * (count locationarray));
};

_recoverytime = ((_minesremaining * 3) + (_destroyedfarmland * count precompiledhideoutpositions) + (_dfk_record_destruction2 * 3000) + (_dfk_record_destruction1 * 1000) + (res_civkills_vehcost/100) + (opfor_civkills_vehcost/100) +(1000 * ((count res_civkills_array) + opfor_civkills)))/8760;

_debriefstr = format [
	"Debriefing: <br />" + 
	"-----------------------------<br />" + 
	"RESISTANCE:<br /> " + 
	"Losses: %1<br /> " +
	"Loot gathered: $ %2<br /> " +
	"Goods purchased: $ %3<br /> " +
	"Secrets gathered: %4<br /> " +
	"Secrets used: %5<br /> " +
	"Cloak and dagger missions compleated: %6<br /> " +
	"Innocent civilians murdered: %7 <br /> " + 
	"Cost of destroyed civilian property: $ %8<br /> " + 	
	"Commandered vehicles: %9<br />" + 
	"<br />" + 
	"OPFOR: <br /> " + 
	"-----------------------------<br />" + 
	"%10" + 
	"<br />" + 
	"KIA/MIA: %11<br /> " + 
	"Estimated material losses: $ %12<br />" +
	"Civilians caught in insurgency crossfire: %13<br /> " + 
	"Insurance claims of traffic incidents: $ %14<br /> " +
	"-----------------------------<br /><br />" + 
	"%15: <br />" +
	"%16 percent Infrastructure damaged<br />" + 
	"%17 percent Homes destroyed<br />" + 
	"Remaining Mines, UXO: %18 metric tons<br />" +
	"Estimated time for recovery: %19 years<br />" +
	"Estimated cost: $ %20 million" +
	"<br /> <br /> Debrief end.",
	dfk_playerdeaths,
	funds_all,
	funds_spent,
	secrets_all,
	secrets_used,
	dfk_drugrunnerlevel,
	_civkillstr,
	res_civkills_vehcost,
	dfk_spawned_vehicles,
	_leavequotes,
	originalnumberofdudes - aliveenemies,
	dfk_opfor_vehcost,
	opfor_civkills,
	opfor_civkills_vehcost,
	worldName,
	round ((_dfk_record_destruction2 / _log_logisticstargetsalivetrackingarray) * 100),
	round (((_dfk_record_destruction1 + dfk_record_house2) / _dfk_record_house) * 100),
	round (_minesremaining/10),
	round (_recoverytime/(8 + random 5)),
	_recoverytime
	];
	
//missionNamespace setVariable ["dfk_debrief",_debriefstr];
[missionNamespace,["dfk_debrief",_debriefstr]] remoteexec ["setvariable"];