_arr = [];


while {true} do
{
	{
		_dude = _x;
		_alreadythere = false;
		{
			if (_dude == _x select 0) then
			{
				_alreadythere = true;
			};
		} foreach _arr;
		
		if (!_alreadythere) then
		{
			_m = [getpos _x,"colorred","","hd_dot",0.5] call dfk_generate_marker;
			_arr append [[_x,_m]];		
		};
		
	} foreach allunits;
	
	{
		_dude = _x;
		_alreadythere = false;
		{
			if (_dude == _x select 0) then
			{
				_alreadythere = true;
			};
		} foreach _arr;
		
		if (!_alreadythere) then
		{
			_m = [getpos _x,"colorblack","","hd_dot",0.5] call dfk_generate_marker;
			_arr append [[_x,_m]];		
		};
		
	} foreach vehicles;	
	
	{
		_m = _x select 1;
		_dude = _x select 0;
		_m setmarkerpos getpos _dude;
		
		if (!alive _dude) then
		{
			deletemarker _m;
			_arr deleteat _foreachindex;
		};
	} foreach _arr;	
	sleep 5;
};