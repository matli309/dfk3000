diag_log "DFK 3000 INIT.SQF";

#include "defines.h";

_debug = ["debug"] call BIS_fnc_getParamValue;
if (_debug == 0) then
{
	debug = false;
}
else
{
	debug = true;
};

enableDynamicSimulationSystem true;

player createDiaryRecord ["Diary", ["Situation", 
"Out strategic predictions fell through. CSAT steamrolled us over teatime two weeks ago.<br />
In the meantime we've been twiddling our thumbs, gathering intel,<br />
establishing covert bases/safehouses and making pine needle tea.<br>
All our logistical, intelligence and communication functions and systems where<br />
promptly destroyed. We have no contact with headquarters or<br />
any other unit in the batalion. We've had fumbly contacts with some<br />
sort of friendly-ish intelligence operator/arms dealer/drug runner.<br />
They are offering some sort of exchange of goods and services.<br />
It's an opportunity to accuire less rusty shity guns.<br />
<br />

All in all, buissnes as usual:<br />
-All communications regard surrenduring, laying down arms, cessasing of hostilites<br />
or that all resistance must cease are false and enemy psyops.<br />
<br />
Expell the enemy by all and any means possible!<br />
-Limit, disrupt and/or destroy enemy logisitical functions<br />
(fuel depots, communicationssystems, radio masts, logistical vehicles<br />
(supplytrucks), transport helicopters, aircrafts)<br />
(Bridges are indestrucible so don't bother – FEATURE)<br />
-Disrupt enemy morale with gun/ied based harrsment and/or hazing<br />
-Find evidence of apparent warcrimes and misconduct, expose said evidence<br />
to the world hence further lower the enemys political willpower to further fund this occupation.<br />
-The more logistical functions destroyed, the more needs replacing, this is ofcourse<br />
expensive and will over time lower the political willingness to fight.<br />
-A truckload of bodybags will also dissuade the enemys fighting will / political willpower.<br />
-Or just KILL 'EM ALL!<br />
<br/>
*Think for yourself<br/>
*Hesitation kills!<br/>
*Solve the problem!<br/>
*ACT!<br/>
<br/>
That will be all. Forward!<br />"
]];

// "I enlighet med vår strategiska doktrin... lel. Så blev vi promt överkörda av CSAT för två veckor sedan.<br>
// Under dessa två veckor har vi etablerat hyffsat säkra baser, dryckit granbarrsté och samlat in underättelser om fienden<br>
// All våra logistiska- och lednings-system är promt jävla bortsprängda (i enlighet med prognos)<br>
// Vi har ingen kontakt med högkvarteret, eller övriga delar av bataljonen<br>
// Vi har gjort vissa trevande kontakter med vänligt sinnad utländska underrättelseoperatörer/vapenkrängande profitörer som mot en 'mindre' summa och/eller utbytande av vissa 'tjänster' kan facilitera viss utrustning (rostigt skrot som är marginellt mindre rostigt och skrotigt än det som vi har kvar)<br>
// Alla meddelanden om att mobilisering är inställd är falska<br>
// Alla meddelanden om att vi ska ge upp är falska<br>
// Alla meddeladen om fred eller att ge upp är falska<br>
// Fördriv fienden<br>
// Begränsa deras logistiska förmåga och (moralisk/politisk) vilja att bedriva olalig ockupation av vårt hemland<br>
// Bedriv det fria kriget.<br>
// FRAMÅT!"

createcenter east;
createcenter west;
createcenter resistance;
createcenter civilian;
createcenter sidelogic;

//enableteamswitch true;

// [resistance,"respawn1"] call BIS_fnc_addRespawnPosition;
// [resistance,"respawn2"] call BIS_fnc_addRespawnPosition;
// [resistance,"respawn3"] call BIS_fnc_addRespawnPosition;
// [resistance,"respawn4"] call BIS_fnc_addRespawnPosition;
// [resistance,"respawn5"] call BIS_fnc_addRespawnPosition;

[resistance, "GUER1"] call BIS_fnc_addRespawnInventory;
[resistance, "GUER2"] call BIS_fnc_addRespawnInventory;

// storearray = [];
// vipstorearray = [];
// sidemorale = 0;
// logistics = 0;
// politics = 0;
// aliveenemies = 0;
// originalnumberofdudes = 0;
// secrets = 0;
// funds = 0;
// BADFACTION = [];
// GOODFACTION = [];
// CIVFACTION = [];
// artillyarray = [];
// artblacklist = [];
// artcooldown = 0;
// base = "";
// base_kp = "";
// base_h = "";
// artcooldown = 0;
// qrfcooldown = 0;
// lastqrf = 0;
// ammoboxarray = []; 
// artillyarray = [];
// artblacklist = [];
// islandblacklist = [];
// dfk_drugrunnerlevel = 0;
// dfk_drugrunnerdelivery = 0;
// dfk_drugrunnertarget = 0;
// rabatt = 0;

//jip, transmit shitload of variables :)

["1337", "OnPlayerConnected",
{
	//if jip, update variables
		publicvariable "storearray";
		publicvariable "vipstorearray" ;
		publicvariable "sidemorale";
		publicvariable "logistics";
		publicvariable "politics";
		publicvariable "aliveenemies";
		publicvariable "originalnumberofdudes";
		publicvariable "secrets";
		publicvariable "funds";
		publicvariable "BADFACTION";
		publicvariable "GOODFACTION";
		publicvariable "CIVFACTION";
		publicvariable "artillyarray";
		publicvariable "artblacklist";
		publicvariable "artcooldown";
		publicvariable "base";
		publicvariable "base_kp";
		publicvariable "base_h";
		publicvariable "artcooldown";
		publicvariable "qrfcooldown";
		publicvariable "lastqrf";
		publicvariable "ammoboxarray"; 
		publicvariable "artillyarray";
		publicvariable "artblacklist";
		publicvariable "islandblacklist";
		publicvariable "dfk_drugrunnerlevel";
		publicvariable "dfk_drugrunnerdelivery";
		publicvariable "dfk_drugrunnertarget";
		publicvariable "rabatt";	
		publicvariable "nerfgear";		
		diag_log "updated JIP variables, if you survive, please come again :)"

}] call BIS_fnc_addStackedEventHandler;

if (isserver) then {["dfk_main.sqf"] remoteExec ["execvm",2];};

//[_this select 0, [missionnamespace, "virtualinventory"]
waituntil {time > 1};
addMissionEventHandler ["EntityRespawned",{[_this select 0,_this select 1] execVM "dfk_bomberman.sqf"}];




