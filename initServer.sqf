["Initialize", [true]] call BIS_fnc_dynamicGroups;

private _lastpurge = time;

dfk_playerdeaths = 0;
publicvariable "dfk_playerdeaths";

[resistance,["task1"],["Simply kill all of the enemy units...","Kill 'em all","attack"],Objnull,"CREATED",1,true] call BIS_fnc_taskCreate;
[resistance,["task2"],["Find enemy shortwave radio transmittor station. Likely located on suitable hilltops. Sabotage and/or Hack enemy shortwave radio transmitters. (Or just shoot the operators)","Sabotage/Hack enemy Comms",""],Objnull,"CREATED",7,true] call BIS_fnc_taskCreate;
[resistance,["task3"],["Disrupt, destroy or hinder any logistical appartus such as (but not limited to): fuel dumps, supply convoys (air and ground), logistical vehicles (trucks)","Destroy logistics",""],Objnull,"CREATED",8,true] call BIS_fnc_taskCreate;
[resistance,["task4"],["We have heard rumors of numerous violations of the LOAC (Laws of Armed Conflict). Gather tangible intel on the matter to expose the enemy politicians to lessen their will to continue the occupation","Gather intel",""],Objnull,"CREATED",6,true] call BIS_fnc_taskCreate;
[resistance,["task5"],["Be on the lookout for abandonned useful weapons and/or other equipment","Get loot",""],Objnull,"CREATED",2,true] call BIS_fnc_taskCreate;
[resistance,["task6"],["Disrupt enemy routines with hit-and-run tactics to grind down their morale and will to fight","Harrass",""],Objnull,"CREATED",5,true] call BIS_fnc_taskCreate;
[resistance,["task7"],["Think outside the box, do not hesitate to apply any unconventional method to solve our current 'problem' ... (but beware of political fallout if you violate the LOAC (Laws of Armed Conflict))","Think for yourself!",""],Objnull,"CREATED",9,true] call BIS_fnc_taskCreate;
[resistance,["task8"],["Do not target civilians. Do not shoot prisoneers. Avoid damaging civilian property. Do not steal civilian property","LOAC",""],Objnull,"CREATED",10,true] call BIS_fnc_taskCreate;


sleep 5;
//
//[owner, taskID, description, destination, state, priority, showNotification, type, visibleIn3D] call BIS_fnc_taskCreate

while {true} do
{
	if (isserver && diag_fps <= 15 && viewdistance <= 600 && _lastpurge + 10 > time) then
	{
		_lastpurge = time;
		//[] remoteexec ["dfk_purgeDeadUnits",0];
		private _count = count alldead;
		diag_log format ["initserver purge %1 %2",_count,alldead];
		if (_count > 0) then
		{
			{
				//[_x] call dfk_deletevehicle
				deletevehicle _x;
			} foreach alldead;
			
			[format ["Removed %1 dead things",_count]] remoteexec ["systemchat"];
		};
	};	
	sleep 15;
};