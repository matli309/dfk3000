_vehicle = _this select 0;
_killer = _this select 1;

//diag_log format ["dfk_setFireToTheVehicle %1",_this];

//["derpscript"] remoteexec ["systemchat"];

//insert cool burning efx here
//faux random damage until destruction

//smallwrecksmoke
//smallfirebarrel

//old: ObjectDestructionFire1Smallx

//[format ["FIREFIREFIRE %1 %2 %3",_vehicle,_killer,player]] remoteexec ["systemchat",0];

_source01 = "#particlesource" createVehicle position _vehicle;
_source01 setParticleClass "smallfirebarrel";


_source02 = "#particlesource" createVehicle position _vehicle;
_source02 setParticleClass "smallwrecksmoke";
	

if (_vehicle iskindof "allVehicles") then
{
	_source01 attachTo [_vehicle,[0,0,0],"palivo"];	
	_source02 attachTo [_vehicle,[0,0,0],"palivo"];	
}
else
{
	_source01 attachTo [_vehicle,[0,0,0]];	
	_source02 attachTo [_vehicle,[0,0,0]];	
};


while {alive _vehicle} do
{
	_vehicle setdamage ((damage _vehicle) + 0.1);
	
	if (_vehicle iskindof "AllVehicles" && count crew _vehicle > 0) then
	{
		private _dude = selectrandom (crew _vehicle);
		
		if (alive _dude) then
		{
			unassignvehicle _dude;
			_dude globalchat "AAAAAH!!! IT BUUrRRrrrnns!!!!";
			_dude domove [(getpos _dude select 0) + (random 100) - (random 100),(getpos _dude select 0) + (random 100) - (random 100)];
			_dude setspeedmode "FULL";
			_dude setcombatmode "BLUE";
			[_dude,_killer] execvm "dfk_setFireToTheVehicle.sqf";
		};
	};

	sleep 4;
};

deletevehicle _source01;
deletevehicle _source02;
if (_vehicle iskindof "Car") then {_killer addscore 2;};
if (_vehicle iskindof "Air") then {_killer addscore 5;};
if (_vehicle iskindof "Tank") then {_killer addscore 4;};
if (_vehicle iskindof "Man") then {_killer addscore 1;};
