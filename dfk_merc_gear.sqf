dfk_merc_gear_1 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_I_G_Story_Protagonist_F";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "MiniGrenade";
	for "_i" from 1 to 3 do {_dude addItemToUniform "10Rnd_9x21_Mag";};
	_dude addItemToUniform "50Rnd_570x28_SMG_03";
	_dude addVest "V_TacVestIR_blk";
	for "_i" from 1 to 3 do {_dude addItemToVest "HandGrenade";};
	_dude addItemToVest "SmokeShell";
	_dude addItemToVest "SmokeShellGreen";
	for "_i" from 1 to 2 do {_dude addItemToVest "Chemlight_blue";};
	for "_i" from 1 to 6 do {_dude addItemToVest "50Rnd_570x28_SMG_03";};
	for "_i" from 1 to 4 do {_dude addItemToVest "SmokeShellRed";};
	_dude addHeadgear "H_MilCap_ocamo";
	_dude addGoggles "G_Balaclava_combat";

	_dude addWeapon "SMG_03C_TR_black";
	_dude addPrimaryWeaponItem "muzzle_snds_570";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_ACO_grn_smg";
	_dude addWeapon "hgun_Pistol_01_F";
	_dude addWeapon "Binocular";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "NVGoggles_OPFOR";
};

dfk_merc_gear_2 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	comment "Add containers";
	_dude forceAddUniform "U_OrestesBody";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "MiniGrenade";
	_dude addVest "V_BandollierB_blk";
	for "_i" from 1 to 2 do {_dude addItemToVest "HandGrenade";};
	_dude addItemToVest "SmokeShell";
	for "_i" from 1 to 2 do {_dude addItemToVest "Chemlight_blue";};
	for "_i" from 1 to 5 do {_dude addItemToVest "30Rnd_9x21_Mag_SMG_02";};
	_dude addHeadgear "H_CrewHelmetHeli_B";
	_dude addGoggles "G_Aviator";

	comment "Add weapons";
	_dude addWeapon "SMG_05_F";
	_dude addPrimaryWeaponItem "muzzle_snds_L";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_DMS";
	_dude addWeapon "hgun_Pistol_heavy_02_F";
	_dude addHandgunItem "muzzle_snds_L";
	_dude addWeapon "Binocular";

	comment "Add items";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "NVGoggles_OPFOR";
};

dfk_merc_gear_3 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_Rangemaster";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "MiniGrenade";
	_dude addItemToUniform "16Rnd_9x21_Mag";
	_dude addVest "V_TacVestIR_blk";
	for "_i" from 1 to 3 do {_dude addItemToVest "HandGrenade";};
	_dude addItemToVest "SmokeShell";
	_dude addItemToVest "SmokeShellGreen";
	for "_i" from 1 to 2 do {_dude addItemToVest "Chemlight_blue";};
	_dude addItemToVest "SmokeShellRed";
	for "_i" from 1 to 6 do {_dude addItemToVest "10Rnd_127x54_Mag";};
	_dude addHeadgear "H_Shemag_olive_hs";
	_dude addGoggles "G_Aviator";

	_dude addWeapon "srifle_DMR_04_F";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_DMS";
	_dude addWeapon "hgun_P07_F";
	_dude addHandgunItem "muzzle_snds_L";
	_dude addWeapon "Binocular";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "NVGoggles_OPFOR";
};

dfk_merc_gear_4 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_C_HunterBody_grn";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "MiniGrenade";
	for "_i" from 1 to 2 do {_dude addItemToUniform "10Rnd_762x54_Mag";};
	_dude addVest "V_LegStrapBag_black_F";
	_dude addItemToVest "SmokeShell";
	_dude addItemToVest "SmokeShellGreen";
	for "_i" from 1 to 2 do {_dude addItemToVest "Chemlight_blue";};
	_dude addItemToVest "SmokeShellRed";
	for "_i" from 1 to 8 do {_dude addItemToVest "10Rnd_762x54_Mag";};
	_dude addHeadgear "H_ShemagOpen_tan";
	_dude addGoggles "G_Aviator";

	_dude addWeapon "srifle_DMR_01_F";
	_dude addPrimaryWeaponItem "muzzle_snds_B_snd_F";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_DMS";
	_dude addPrimaryWeaponItem "bipod_02_F_hex";
	_dude addWeapon "Rangefinder";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "NVGoggles_OPFOR";
};

dfk_merc_gear_5 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_BG_Guerilla2_3";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "MiniGrenade";
	_dude addItemToUniform "6Rnd_GreenSignal_F";
	_dude addVest "V_Chestrig_blk";
	_dude addItemToVest "SmokeShell";
	_dude addItemToVest "SmokeShellGreen";
	for "_i" from 1 to 2 do {_dude addItemToVest "Chemlight_blue";};
	_dude addItemToVest "SmokeShellRed";
	for "_i" from 1 to 3 do {_dude addItemToVest "200Rnd_556x45_Box_F";};
	_dude addBackpack "B_AssaultPack_blk";
	_dude addItemToBackpack "200Rnd_556x45_Box_F";
	for "_i" from 1 to 3 do {_dude addItemToBackpack "6Rnd_GreenSignal_F";};
	_dude addHeadgear "H_Cap_brn_SPECOPS";
	_dude addGoggles "G_Balaclava_lowprofile";

	_dude addWeapon "LMG_03_F";
	_dude addPrimaryWeaponItem "muzzle_snds_M";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addWeapon "hgun_Pistol_Signal_F";
	_dude addWeapon "Rangefinder";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "NVGoggles_OPFOR";
};

dfk_merc_gear_6 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_C_ConstructionCoverall_Black_F";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "MiniGrenade";
	_dude addItemToUniform "30Rnd_9x21_Mag";
	_dude addVest "V_LegStrapBag_black_F";
	_dude addItemToVest "SmokeShell";
	_dude addItemToVest "SmokeShellGreen";
	for "_i" from 1 to 2 do {_dude addItemToVest "Chemlight_blue";};
	_dude addItemToVest "SmokeShellRed";
	for "_i" from 1 to 6 do {_dude addItemToVest "30Rnd_9x21_Mag";};
	_dude addGoggles "G_Balaclava_blk";

	_dude addWeapon "hgun_PDW2000_F";
	_dude addPrimaryWeaponItem "muzzle_snds_L";
	_dude addPrimaryWeaponItem "optic_Holosight_blk_F";
	_dude addWeapon "Rangefinder";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "NVGoggles_OPFOR";
};


dfk_merc_gear_7 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_I_C_Soldier_Bandit_2_F";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "MiniGrenade";
	_dude addItemToUniform "30Rnd_580x42_Mag_F";
	_dude addVest "V_TacVestIR_blk";
	for "_i" from 1 to 10 do {_dude addItemToVest "30Rnd_580x42_Mag_F";};
	for "_i" from 1 to 14 do {_dude addItemToVest "1Rnd_HE_Grenade_shell";};
	_dude addHeadgear "H_Cap_headphones";
	_dude addGoggles "G_Balaclava_oli";

	_dude addWeapon "arifle_CTAR_GL_blk_F";
	_dude addPrimaryWeaponItem "muzzle_snds_58_blk_F";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_Hamr";
	_dude addWeapon "hgun_Rook40_F";
	_dude addWeapon "Rangefinder";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "NVGoggles_OPFOR";
};

dfk_merc_gear_8 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_BG_Guerilla2_1";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "SmokeShell";
	_dude addItemToUniform "SmokeShellGreen";
	for "_i" from 1 to 2 do {_dude addItemToUniform "11Rnd_45ACP_Mag";};
	_dude addVest "V_BandollierB_oli";
	for "_i" from 1 to 6 do {_dude addItemToVest "20Rnd_762x51_Mag";};
	for "_i" from 1 to 2 do {_dude addItemToVest "Chemlight_blue";};
	_dude addHeadgear "H_Hat_Safari_olive_F";
	_dude addGoggles "G_Aviator";

	_dude addWeapon "srifle_DMR_06_camo_F";
	_dude addPrimaryWeaponItem "muzzle_snds_B";
	_dude addPrimaryWeaponItem "optic_KHS_old";
	_dude addPrimaryWeaponItem "bipod_01_F_blk";
	_dude addWeapon "hgun_Pistol_heavy_01_F";
	_dude addWeapon "Binocular";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
};

dfk_merc_gear_9 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_I_C_Soldier_Bandit_4_F";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "SmokeShell";
	_dude addItemToUniform "SmokeShellGreen";
	_dude addItemToUniform "11Rnd_45ACP_Mag";
	_dude addVest "V_PlateCarrier1_blk";
	for "_i" from 1 to 2 do {_dude addItemToVest "Chemlight_blue";};
	for "_i" from 1 to 8 do {_dude addItemToVest "30Rnd_556x45_Stanag_Tracer_Red";};
	for "_i" from 1 to 4 do {_dude addItemToVest "HandGrenade";};
	for "_i" from 1 to 2 do {_dude addItemToVest "11Rnd_45ACP_Mag";};
	_dude addHeadgear "H_MilCap_blue";

	_dude addWeapon "arifle_SPAR_01_blk_F";
	_dude addPrimaryWeaponItem "muzzle_snds_M";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_ERCO_blk_F";
	_dude addWeapon "hgun_Pistol_heavy_01_F";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "NVGoggles_OPFOR";	
};

dfk_merc_gear_10 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_Marshal";
	for "_i" from 1 to 2 do {_dude addItemToUniform "11Rnd_45ACP_Mag";};
	_dude addVest "V_LegStrapBag_black_F";
	for "_i" from 1 to 5 do {_dude addItemToVest "11Rnd_45ACP_Mag";};
	for "_i" from 1 to 4 do {_dude addItemToVest "HandGrenade";};
	_dude addHeadgear "H_Beret_blk";
	_dude addGoggles "G_Spectacles_Tinted";

	_dude addWeapon "hgun_Pistol_heavy_01_F";
	_dude addHandgunItem "muzzle_snds_acp";
	_dude addHandgunItem "acc_flashlight_pistol";
	_dude addHandgunItem "optic_MRD";	
};

dfk_merc_gear_11 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_Rangemaster";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "30Rnd_45ACP_Mag_SMG_01";
	_dude addVest "V_LegStrapBag_black_F";
	for "_i" from 1 to 3 do {_dude addItemToVest "HandGrenade";};
	_dude addItemToVest "MiniGrenade";
	for "_i" from 1 to 2 do {_dude addItemToVest "1Rnd_HE_Grenade_shell";};
	_dude addItemToVest "SmokeShell";
	_dude addItemToVest "SmokeShellGreen";
	for "_i" from 1 to 2 do {_dude addItemToVest "Chemlight_blue";};
	_dude addItemToVest "1Rnd_Smoke_Grenade_shell";
	_dude addItemToVest "APERSTripMine_Wire_Mag";
	_dude addBackpack "B_Messenger_Black_F";
	for "_i" from 1 to 5 do {_dude addItemToBackpack "11Rnd_45ACP_Mag";};
	for "_i" from 1 to 8 do {_dude addItemToBackpack "30Rnd_45ACP_Mag_SMG_01_Tracer_Red";};
	_dude addItemToBackpack "DemoCharge_Remote_Mag";
	_dude addHeadgear "H_Cap_usblack";
	_dude addGoggles "G_Spectacles_Tinted";

	_dude addWeapon "SMG_01_F";
	_dude addPrimaryWeaponItem "muzzle_snds_acp";
	_dude addPrimaryWeaponItem "acc_flashlight_smg_01";
	_dude addPrimaryWeaponItem "optic_AMS";
	_dude addWeapon "hgun_Pistol_heavy_01_F";
	_dude addHandgunItem "muzzle_snds_acp";
	_dude addHandgunItem "acc_flashlight_pistol";
	_dude addHandgunItem "optic_MRD";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";	
};

dfk_merc_renegade1 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_BG_Guerilla1_2_F";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "MiniGrenade";
	_dude addItemToUniform "30Rnd_9x21_Red_Mag";
	_dude addVest "V_Pocketed_coyote_F";
	for "_i" from 1 to 7 do {_dude addItemToVest "30Rnd_9x21_Red_Mag";};
	_dude addBackpack "B_Messenger_Olive_F";
	for "_i" from 1 to 2 do {_dude addItemToBackpack "FirstAidKit";};
	for "_i" from 1 to 3 do {_dude addItemToBackpack "HandGrenade";};
	_dude addItemToBackpack "SmokeShellOrange";
	_dude addItemToBackpack "SmokeShellGreen";
	_dude addItemToBackpack "SmokeShellBlue";
	_dude addHeadgear "H_Hat_Safari_sand_F";

	_dude addWeapon "hgun_PDW2000_F";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
};

dfk_merc_renegade2 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_BG_Guerilla1_2_F";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 3 do {_dude addItemToUniform "SmokeShellBlue";};
	_dude addVest "V_LegStrapBag_black_F";
	for "_i" from 1 to 6 do {_dude addItemToVest "50Rnd_570x28_SMG_03";};
	_dude addHeadgear "H_Shemag_olive";
	_dude addGoggles "G_Bandanna_oli";

	_dude addWeapon "SMG_03C_camo";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
};

dfk_merc_renegade3 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	comment "Add containers";
	_dude forceAddUniform "U_BG_Guerilla1_2_F";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 3 do {_dude addItemToUniform "SmokeShellBlue";};
	_dude addVest "V_HarnessOGL_brn";
	for "_i" from 1 to 8 do {_dude addItemToVest "30Rnd_545x39_Mag_Tracer_F";};
	_dude addHeadgear "H_StrawHat";
	_dude addGoggles "G_Combat";

	comment "Add weapons";
	_dude addWeapon "arifle_AKS_F";

	comment "Add items";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
};

dfk_merc_renegade4 = 
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	comment "Add containers";
	_dude forceAddUniform "U_I_G_Story_Protagonist_F";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 3 do {_dude addItemToUniform "SmokeShellBlue";};
	_dude addVest "V_Chestrig_blk";
	for "_i" from 1 to 8 do {_dude addItemToVest "30Rnd_556x45_Stanag_Tracer_Red";};
	_dude addHeadgear "H_MilCap_gry";
	_dude addGoggles "G_Combat";

	comment "Add weapons";
	_dude addWeapon "arifle_Mk20_F";

	comment "Add items";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
};

dfk_merc_snapr1 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_FullGhillie_sard";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 3 do {_dude addItemToUniform "SmokeShellBlue";};
	_dude addItemToUniform "11Rnd_45ACP_Mag";
	_dude addVest "V_Chestrig_blk";
	for "_i" from 1 to 9 do {_dude addItemToVest "7Rnd_408_Mag";};
	for "_i" from 1 to 2 do {_dude addItemToVest "11Rnd_45ACP_Mag";};
	_dude addGoggles "G_Balaclava_TI_G_tna_F";

	_dude addWeapon "srifle_LRR_camo_F";
	_dude addPrimaryWeaponItem "optic_AMS_khk";
	_dude addWeapon "hgun_Pistol_heavy_01_F";
	_dude addHandgunItem "muzzle_snds_acp";
	_dude addWeapon "Rangefinder";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
	_dude linkItem "NVGoggles_INDEP";
};


dfk_merc_snapr2 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_FullGhillie_sard";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 3 do {_dude addItemToUniform "SmokeShellBlue";};
	_dude addItemToUniform "10Rnd_338_Mag";
	_dude addVest "V_Chestrig_blk";
	for "_i" from 1 to 9 do {_dude addItemToVest "10Rnd_338_Mag";};
	_dude addGoggles "G_Balaclava_TI_G_tna_F";

	_dude addWeapon "srifle_DMR_02_camo_F";
	_dude addPrimaryWeaponItem "muzzle_snds_338_green";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_AMS_khk";
	_dude addPrimaryWeaponItem "bipod_01_F_khk";
	_dude addWeapon "Rangefinder";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
	_dude linkItem "NVGoggles_INDEP";
};

dfk_merc_snapr3 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_FullGhillie_sard";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 3 do {_dude addItemToUniform "SmokeShellBlue";};
	for "_i" from 1 to 2 do {_dude addItemToUniform "20Rnd_762x51_Mag";};
	_dude addVest "V_Chestrig_blk";
	for "_i" from 1 to 11 do {_dude addItemToVest "20Rnd_762x51_Mag";};
	_dude addGoggles "G_Balaclava_TI_G_tna_F";

	_dude addWeapon "srifle_DMR_03_multicam_F";
	_dude addPrimaryWeaponItem "muzzle_snds_B_khk_F";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_MRCO";
	_dude addPrimaryWeaponItem "bipod_01_F_khk";
	_dude addWeapon "Rangefinder";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
	_dude linkItem "NVGoggles_INDEP";
};

dfk_merc_snapr4 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_C_HunterBody_grn";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 3 do {_dude addItemToUniform "SmokeShellBlue";};
	_dude addVest "V_Chestrig_blk";
	for "_i" from 1 to 11 do {_dude addItemToVest "20Rnd_762x51_Mag";};
	_dude addHeadgear "H_Shemag_olive_hs";
	_dude addGoggles "G_Balaclava_TI_G_tna_F";

	_dude addWeapon "srifle_DMR_06_olive_F";
	_dude addPrimaryWeaponItem "optic_KHS_old";
	_dude addPrimaryWeaponItem "bipod_01_F_khk";
	_dude addWeapon "Rangefinder";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
	_dude linkItem "NVGoggles_INDEP";
};

dfk_merc_snapr5 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_GhillieSuit";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 3 do {_dude addItemToUniform "SmokeShellBlue";};
	for "_i" from 1 to 2 do {_dude addItemToUniform "20Rnd_762x51_Mag";};
	_dude addVest "V_Chestrig_blk";
	for "_i" from 1 to 11 do {_dude addItemToVest "20Rnd_762x51_Mag";};
	_dude addHeadgear "H_Shemag_olive_hs";
	_dude addGoggles "G_Balaclava_TI_G_tna_F";

	_dude addWeapon "srifle_EBR_F";
	_dude addPrimaryWeaponItem "muzzle_snds_B";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_tws";
	_dude addPrimaryWeaponItem "bipod_02_F_blk";
	_dude addWeapon "Rangefinder";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
	_dude linkItem "NVGoggles_INDEP";
};

dfk_merc_snapr6 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_I_FullGhillie_sard";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 3 do {_dude addItemToUniform "SmokeShellBlue";};
	for "_i" from 1 to 2 do {_dude addItemToUniform "10Rnd_127x54_Mag";};
	_dude addVest "V_Chestrig_blk";
	for "_i" from 1 to 11 do {_dude addItemToVest "10Rnd_127x54_Mag";};
	_dude addHeadgear "H_Shemag_olive_hs";
	_dude addGoggles "G_Balaclava_TI_G_tna_F";

	_dude addWeapon "srifle_DMR_04_F";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_Hamr";
	_dude addPrimaryWeaponItem "bipod_02_F_blk";
	_dude addWeapon "Rangefinder";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
	_dude linkItem "NVGoggles_INDEP";
};

dfk_merc_snapr7 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_I_FullGhillie_sard";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 3 do {_dude addItemToUniform "SmokeShellBlue";};
	for "_i" from 1 to 3 do {_dude addItemToUniform "20Rnd_650x39_Cased_Mag_F";};
	_dude addVest "V_Chestrig_blk";
	for "_i" from 1 to 16 do {_dude addItemToVest "20Rnd_650x39_Cased_Mag_F";};
	_dude addHeadgear "H_Shemag_olive_hs";
	_dude addGoggles "G_Balaclava_TI_G_tna_F";

	_dude addWeapon "srifle_DMR_07_ghex_F";
	_dude addPrimaryWeaponItem "muzzle_snds_65_TI_ghex_F";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_SOS";
	_dude addWeapon "Rangefinder";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
	_dude linkItem "NVGoggles_INDEP";	
	
};

dfk_merc_snapr8 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_I_FullGhillie_sard";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 3 do {_dude addItemToUniform "SmokeShellBlue";};
	_dude addItemToUniform "10Rnd_93x64_DMR_05_Mag";
	_dude addVest "V_Chestrig_blk";
	for "_i" from 1 to 8 do {_dude addItemToVest "10Rnd_93x64_DMR_05_Mag";};
	_dude addHeadgear "H_Shemag_olive_hs";
	_dude addGoggles "G_Balaclava_TI_G_tna_F";

	_dude addWeapon "srifle_DMR_05_hex_F";
	_dude addPrimaryWeaponItem "muzzle_snds_93mmg";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_SOS";
	_dude addPrimaryWeaponItem "bipod_02_F_blk";
	_dude addWeapon "Rangefinder";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
	_dude linkItem "NVGoggles_INDEP";
};

dfk_merc_snapr9 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_I_FullGhillie_sard";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 3 do {_dude addItemToUniform "SmokeShellBlue";};
	_dude addItemToUniform "5Rnd_127x108_Mag";
	_dude addVest "V_Chestrig_blk";
	for "_i" from 1 to 8 do {_dude addItemToVest "5Rnd_127x108_Mag";};
	_dude addHeadgear "H_Shemag_olive_hs";
	_dude addGoggles "G_Balaclava_TI_G_tna_F";

	_dude addWeapon "srifle_GM6_camo_F";
	_dude addPrimaryWeaponItem "optic_KHS_hex";
	_dude addWeapon "Rangefinder";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
	_dude linkItem "NVGoggles_INDEP";
};

dfk_merc_snapr10 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_I_FullGhillie_sard";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 3 do {_dude addItemToUniform "SmokeShellBlue";};
	for "_i" from 1 to 3 do {_dude addItemToUniform "10Rnd_762x54_Mag";};
	_dude addVest "V_Chestrig_blk";
	for "_i" from 1 to 17 do {_dude addItemToVest "10Rnd_762x54_Mag";};
	_dude addHeadgear "H_Shemag_olive_hs";
	_dude addGoggles "G_Balaclava_TI_G_tna_F";

	_dude addWeapon "srifle_DMR_01_F";
	_dude addPrimaryWeaponItem "muzzle_snds_B";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_KHS_hex";
	_dude addPrimaryWeaponItem "bipod_02_F_blk";
	_dude addWeapon "Rangefinder";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
	_dude linkItem "NVGoggles_INDEP";
};

dfk_merc_simple =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform selectrandom 	["U_C_Poor1","U_C_Poor2","U_C_casual_4_F","U_C_casual_5_F","U_C_casual_6_F","U_rangemaster",
		"U_C_Mechanic_01_F",
		"U_C_Poloshirt_blue",
		"U_C_Poloshirt_burgundy",
		"U_C_Poloshirt_stripped",
		"U_C_Poloshirt_tricolour",
		"U_C_Poloshirt_salmon",
		"U_C_Poloshirt_redwhite",
		"U_C_Poor_shorts_1",
		"U_NikosBody",
		"U_NikosAgedBody",
		"U_C_Man_casual_1_F",
		"U_C_Man_casual_2_F",
		"U_C_Man_casual_3_F",
		"U_Marshal",
		"U_C_Journalist",
		"U_BG_Guerilla2_3",
		"U_BG_Guerilla2_1",
		"U_BG_Guerilla2_2",
		"U_BG_Guerilla2_1",
		"U_C_ConstructionCoverall_Red_F",
		"U_C_ConstructionCoverall_vrana_F",
		"U_C_ConstructionCoverall_Black_F",
		"U_C_ConstructionCoverall_Blue_F",
		"U_I_C_Soldier_Bandit_3_F",
		"U_I_C_Soldier_Bandit_5_F",
		"U_I_C_Soldier_Bandit_2_F",
		"U_I_C_Soldier_Bandit_1_F",
		"U_I_C_Soldier_Bandit_4_F"
	];
	
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "SmokeShellBlue";
	_dude addItemToUniform "SmokeShellGreen";
	_dude addItemToUniform "HandGrenade";
	_dude addVest "V_Pocketed_black_F";
	for "_i" from 1 to 8 do {_dude addItemToVest "30Rnd_545x39_Mag_Tracer_F";};

	_dude addWeapon "arifle_AKS_F";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
};

dfk_merc_stealthcop1 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_B_GEN_Commander_F";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "30Rnd_9x21_Mag_SMG_02";
	_dude addVest "V_TacVest_blk_POLICE";
	for "_i" from 1 to 6 do {_dude addItemToVest "30Rnd_9x21_Mag_SMG_02";};
	for "_i" from 1 to 2 do {_dude addItemToVest "16Rnd_9x21_Mag";};
	_dude addItemToVest "HandGrenade";
	for "_i" from 1 to 2 do {_dude addItemToVest "SmokeShell";};
	_dude addItemToVest "SmokeShellYellow";
	_dude addItemToVest "MiniGrenade";
	_dude addHeadgear "H_HelmetB_light_black";
	_dude addGoggles "G_Aviator";

	_dude addWeapon "SMG_05_F";
	_dude addPrimaryWeaponItem "muzzle_snds_L";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_Aco_smg";
	_dude addWeapon "hgun_P07_F";
	_dude addHandgunItem "muzzle_snds_L";
	_dude addWeapon "Binocular";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "NVGoggles_OPFOR";	
};

dfk_merc_stealthcop2 = 
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_B_GEN_Commander_F";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 2 do {_dude addItemToUniform "16Rnd_9x21_Mag";};
	_dude addItemToUniform "MiniGrenade";
	_dude addVest "V_TacVest_blk_POLICE";
	_dude addItemToVest "HandGrenade";
	for "_i" from 1 to 2 do {_dude addItemToVest "SmokeShell";};
	_dude addItemToVest "SmokeShellYellow";
	_dude addItemToVest "MiniGrenade";
	for "_i" from 1 to 6 do {_dude addItemToVest "50Rnd_570x28_SMG_03";};
	_dude addHeadgear "H_HelmetB_light_black";
	_dude addGoggles "G_Aviator";

	_dude addWeapon "SMG_03C_TR_black";
	_dude addPrimaryWeaponItem "muzzle_snds_570";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_Aco_smg";
	_dude addWeapon "hgun_P07_F";
	_dude addHandgunItem "muzzle_snds_L";
	_dude addWeapon "Binocular";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "NVGoggles_OPFOR";	
};

dfk_merc_stealthcop3 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_B_GEN_Commander_F";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 2 do {_dude addItemToUniform "16Rnd_9x21_Mag";};
	_dude addItemToUniform "MiniGrenade";
	_dude addVest "V_TacVest_blk_POLICE";
	_dude addItemToVest "HandGrenade";
	for "_i" from 1 to 2 do {_dude addItemToVest "SmokeShell";};
	_dude addItemToVest "SmokeShellYellow";
	_dude addItemToVest "MiniGrenade";
	for "_i" from 1 to 6 do {_dude addItemToVest "10Rnd_127x54_Mag";};
	_dude addHeadgear "H_HelmetB_light_black";
	_dude addGoggles "G_Aviator";

	_dude addWeapon "srifle_DMR_04_F";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_Arco_blk_F";
	_dude addPrimaryWeaponItem "bipod_02_F_blk";
	_dude addWeapon "hgun_P07_F";
	_dude addHandgunItem "muzzle_snds_L";
	_dude addWeapon "Binocular";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "NVGoggles_OPFOR";
};

dfk_merc_stealthcop4 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_B_GEN_Commander_F";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 2 do {_dude addItemToUniform "16Rnd_9x21_Mag";};
	_dude addItemToUniform "MiniGrenade";
	_dude addVest "V_TacVest_blk_POLICE";
	_dude addItemToVest "HandGrenade";
	for "_i" from 1 to 2 do {_dude addItemToVest "SmokeShell";};
	_dude addItemToVest "SmokeShellYellow";
	_dude addItemToVest "MiniGrenade";
	for "_i" from 1 to 6 do {_dude addItemToVest "30Rnd_65x39_caseless_green";};
	_dude addHeadgear "H_HelmetB_light_black";
	_dude addGoggles "G_Aviator";

	_dude addWeapon "arifle_Katiba_C_F";
	_dude addPrimaryWeaponItem "muzzle_snds_65_TI_blk_F";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_Arco_blk_F";
	_dude addWeapon "hgun_P07_F";
	_dude addHandgunItem "muzzle_snds_L";
	_dude addWeapon "Binocular";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "NVGoggles_OPFOR";
};

dfk_merc_stealthcop5 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_B_GEN_Commander_F";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 2 do {_dude addItemToUniform "16Rnd_9x21_Mag";};
	_dude addItemToUniform "MiniGrenade";
	_dude addVest "V_TacVest_blk_POLICE";
	_dude addItemToVest "HandGrenade";
	for "_i" from 1 to 2 do {_dude addItemToVest "SmokeShell";};
	_dude addItemToVest "SmokeShellYellow";
	_dude addItemToVest "MiniGrenade";
	for "_i" from 1 to 10 do {_dude addItemToVest "20Rnd_650x39_Cased_Mag_F";};
	_dude addHeadgear "H_HelmetB_light_black";
	_dude addGoggles "G_Aviator";

	_dude addWeapon "srifle_DMR_07_blk_F";
	_dude addPrimaryWeaponItem "muzzle_snds_65_TI_blk_F";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_Arco_blk_F";
	_dude addWeapon "hgun_P07_F";
	_dude addHandgunItem "muzzle_snds_L";
	_dude addWeapon "Binocular";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "NVGoggles_OPFOR";	
};

dfk_merc_urban1 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_CombatUniform_oucamo";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	for "_i" from 1 to 2 do {_dude addItemToUniform "20Rnd_762x51_Mag";};
	_dude addVest "V_HarnessO_gry";
	for "_i" from 1 to 10 do {_dude addItemToVest "20Rnd_762x51_Mag";};
	for "_i" from 1 to 3 do {_dude addItemToVest "HandGrenade";};
	for "_i" from 1 to 2 do {_dude addItemToVest "SmokeShellRed";};
	_dude addHeadgear "H_HelmetLeaderO_oucamo";
	_dude addGoggles "G_Combat";

	_dude addWeapon "srifle_DMR_03_F";
	_dude addPrimaryWeaponItem "muzzle_snds_B";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_Hamr";
	_dude addPrimaryWeaponItem "bipod_02_F_blk";
	_dude addWeapon "hgun_Pistol_heavy_02_F";
	_dude addHandgunItem "acc_flashlight_pistol";
	_dude addHandgunItem "optic_Yorris";
	_dude addWeapon "Laserdesignator_02";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "O_UavTerminal";
	_dude linkItem "O_NVGoggles_urb_F";
};

dfk_merc_urban2 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_CombatUniform_oucamo";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	_dude addItemToUniform "30Rnd_65x39_caseless_green";
	_dude addVest "V_HarnessO_gry";
	for "_i" from 1 to 3 do {_dude addItemToVest "HandGrenade";};
	for "_i" from 1 to 2 do {_dude addItemToVest "SmokeShellRed";};
	for "_i" from 1 to 12 do {_dude addItemToVest "30Rnd_65x39_caseless_green_mag_Tracer";};
	_dude addHeadgear "H_HelmetLeaderO_oucamo";
	_dude addGoggles "G_Combat";

	_dude addWeapon "arifle_Katiba_C_F";
	_dude addPrimaryWeaponItem "muzzle_snds_65_TI_blk_F";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_Hamr";
	_dude addWeapon "hgun_Pistol_heavy_02_F";
	_dude addHandgunItem "acc_flashlight_pistol";
	_dude addHandgunItem "optic_Yorris";
	_dude addWeapon "Laserdesignator_02";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "O_UavTerminal";
	_dude linkItem "O_NVGoggles_urb_F";
};

dfk_merc_urban3=
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_CombatUniform_oucamo";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	_dude addItemToUniform "30Rnd_45ACP_Mag_SMG_01";
	_dude addVest "V_HarnessO_gry";
	for "_i" from 1 to 3 do {_dude addItemToVest "HandGrenade";};
	for "_i" from 1 to 2 do {_dude addItemToVest "SmokeShellRed";};
	for "_i" from 1 to 12 do {_dude addItemToVest "30Rnd_45ACP_Mag_SMG_01_Tracer_Red";};
	_dude addHeadgear "H_HelmetLeaderO_oucamo";
	_dude addGoggles "G_Combat";

	_dude addWeapon "SMG_01_F";
	_dude addPrimaryWeaponItem "muzzle_snds_acp";
	_dude addPrimaryWeaponItem "optic_Aco_smg";
	_dude addWeapon "hgun_Pistol_heavy_02_F";
	_dude addHandgunItem "acc_flashlight_pistol";
	_dude addHandgunItem "optic_Yorris";
	_dude addWeapon "Laserdesignator_02";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "O_UavTerminal";
	_dude linkItem "O_NVGoggles_urb_F";
};

dfk_merc_urban4 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_CombatUniform_oucamo";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	_dude addVest "V_HarnessO_gry";
	_dude addItemToVest "FirstAidKit";
	for "_i" from 1 to 3 do {_dude addItemToVest "HandGrenade";};
	for "_i" from 1 to 3 do {_dude addItemToVest "SmokeShellRed";};
	_dude addItemToVest "150Rnd_93x64_Mag";
	_dude addHeadgear "H_HelmetLeaderO_oucamo";
	_dude addGoggles "G_Combat";

	_dude addWeapon "MMG_01_tan_F";
	_dude addPrimaryWeaponItem "muzzle_snds_93mmg";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_tws_mg";
	_dude addPrimaryWeaponItem "bipod_02_F_tan";
	_dude addWeapon "hgun_Pistol_heavy_02_F";
	_dude addHandgunItem "acc_flashlight_pistol";
	_dude addHandgunItem "optic_Yorris";
	_dude addWeapon "Laserdesignator_02";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "O_UavTerminal";
	_dude linkItem "O_NVGoggles_urb_F";
};

dfk_merc_urban5 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_CombatUniform_oucamo";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	for "_i" from 1 to 2 do {_dude addItemToUniform "20Rnd_650x39_Cased_Mag_F";};
	_dude addVest "V_HarnessO_gry";
	_dude addItemToVest "FirstAidKit";
	for "_i" from 1 to 3 do {_dude addItemToVest "HandGrenade";};
	for "_i" from 1 to 3 do {_dude addItemToVest "SmokeShellRed";};
	for "_i" from 1 to 11 do {_dude addItemToVest "20Rnd_650x39_Cased_Mag_F";};
	_dude addHeadgear "H_HelmetLeaderO_oucamo";
	_dude addGoggles "G_Combat";

	_dude addWeapon "srifle_DMR_07_blk_F";
	_dude addPrimaryWeaponItem "muzzle_snds_65_TI_blk_F";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_ERCO_blk_F";
	_dude addWeapon "hgun_Pistol_heavy_02_F";
	_dude addHandgunItem "acc_flashlight_pistol";
	_dude addHandgunItem "optic_Yorris";
	_dude addWeapon "Laserdesignator_02";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "O_UavTerminal";
	_dude linkItem "O_NVGoggles_urb_F";
};

dfk_merc_urban6 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_CombatUniform_oucamo";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	_dude addItemToUniform "30Rnd_9x21_Mag_SMG_02";
	_dude addVest "V_PlateCarrier1_blk";
	_dude addItemToVest "FirstAidKit";
	for "_i" from 1 to 3 do {_dude addItemToVest "HandGrenade";};
	for "_i" from 1 to 3 do {_dude addItemToVest "SmokeShellRed";};
	for "_i" from 1 to 5 do {_dude addItemToVest "30Rnd_9x21_Mag_SMG_02";};
	for "_i" from 1 to 2 do {_dude addItemToVest "APERSBoundingMine_Range_Mag";};
	_dude addBackpack "B_FieldPack_oucamo";
	_dude addItemToBackpack "ToolKit";
	for "_i" from 1 to 3 do {_dude addItemToBackpack "APERSMine_Range_Mag";};
	for "_i" from 1 to 2 do {_dude addItemToBackpack "DemoCharge_Remote_Mag";};
	_dude addItemToBackpack "SLAMDirectionalMine_Wire_Mag";
	_dude addItemToBackpack "APERSTripMine_Wire_Mag";
	_dude addHeadgear "H_HelmetLeaderO_oucamo";
	_dude addGoggles "G_Combat";

	_dude addWeapon "SMG_05_F";
	_dude addPrimaryWeaponItem "muzzle_snds_L";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_Aco_smg";
	_dude addWeapon "hgun_Pistol_heavy_02_F";
	_dude addHandgunItem "acc_flashlight_pistol";
	_dude addHandgunItem "optic_Yorris";
	_dude addWeapon "Laserdesignator_02";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "O_UavTerminal";
	_dude linkItem "O_NVGoggles_urb_F";
};