#include "defines.h"

dfk_addgunlvl =
{
	_level = _this select 0;
	
	diag_log format ["dfk_addgunlvl %1",_level];

	private _weapons = [];
	private _magazines = [];
	private _items = [];
	private _bags = [];
	private _debugtype = "";

	{
		if ((_x iskindof ["Rifle", configFile >> "CfgWeapons"]) or (_x iskindof ["Pistol", configFile >> "CfgWeapons"]) or (_x iskindof ["Launcher", configFile >> "CfgWeapons"]) or (_x iskindof ["Mgun", configFile >> "CfgWeapons"])) then {_debugtype = "gun";_weapons pushbackunique _x};
		if (_x iskindof ["Default", configFile >> "CfgMagazines"]) then {_magazines pushbackunique _x;_debugtype = "mag";};
		if (_x iskindof ["ItemCore", configFile >> "CfgWeapons"]) then {_items pushbackunique _x;_debugtype = "item";};
		if (_x iskindof ["NVGoggles", configFile >> "CfgWeapons"]) then {_items pushbackunique _x;_debugtype = "item";};
		if (_x iskindof ["Binocular", configFile >> "CfgWeapons"]) then {_items pushbackunique _x;_debugtype = "item";};
		if (_x iskindof ["DetectorCore", configFile >> "CfgWeapons"]) then {_items pushbackunique _x;_debugtype = "item";};
		if (_x iskindof "bag_base") then {_bags pushbackunique _x;_debugtype = "bag ";};
		diag_log format ["dfk_addgunlvl %1 %2",_x,_debugtype];
	} foreach _level;
	
	
	
	//also add magazines/ammo to weapons... lol
	{
		private _tempmags = getarray(configfile >> "CfgWeapons" >> _x >> "magazines");
		
		private _mz = getarray(configfile >> "CfgWeapons" >> _x >> "muzzles");
		if (count _mz >= 2) then
		{
			if ("EGLM" in _mz or "GL_3GL_F" in _mz) then
			{
				private _derp = [
							"1Rnd_HE_Grenade_shell",
							"3Rnd_HE_Grenade_shell",
							"1Rnd_Smoke_Grenade_shell",
							"3Rnd_Smoke_Grenade_shell",
							"1Rnd_SmokeRed_Grenade_shell",
							"3Rnd_SmokeRed_Grenade_shell",
							"1Rnd_SmokeGreen_Grenade_shell",
							"3Rnd_SmokeGreen_Grenade_shell",
							"1Rnd_SmokeYellow_Grenade_shell",
							"3Rnd_SmokeYellow_Grenade_shell",
							"1Rnd_SmokePurple_Grenade_shell",
							"3Rnd_SmokePurple_Grenade_shell",
							"1Rnd_SmokeBlue_Grenade_shell",
							"3Rnd_SmokeBlue_Grenade_shell",
							"1Rnd_SmokeOrange_Grenade_shell",
							"3Rnd_SmokeOrange_Grenade_shell",
							"UGL_FlareWhite_F",
							"3Rnd_UGL_FlareWhite_F",
							"UGL_FlareGreen_F",
							"3Rnd_UGL_FlareGreen_F",
							"UGL_FlareRed_F",
							"3Rnd_UGL_FlareRed_F",
							"UGL_FlareYellow_F",
							"3Rnd_UGL_FlareYellow_F",
							"UGL_FlareCIR_F",
							"3Rnd_UGL_FlareCIR_F",
							"FlareWhite_F",
							"FlareGreen_F",
							"FlareRed_F",
							"FlareYellow_F"							
						];
						
				_magazines append _derp;
			};
			
			if ("Secondary" in _mz) then
			{
				_derp = [
								"10Rnd_50BW_Mag_F"
						];
						
				_magazines append _derp;
			};			
		};
		
		{
			if (_x in sanctionedmagsarray) then {_magazines pushbackunique _x};
		} foreach _tempmags;		
	} foreach _weapons;

	diag_log format ["%1 %2 %3 %4",_weapons,_items,_magazines,_bags];
	[_weapons,_items,_magazines,_bags] remoteExec ["dfk_blip_ammoboxes"];
};

#include "dfk_gun_lvl.h";

// 1
// 2
// 4
// 8
// 14
// 22
// 32
// 44
// 58
// 74
// 92
// 112
// 134
// 158
// 184
// 212
// 242
// 274
// 308

dfk_cashLevelBasedGunProgressionStuff =
[
	[1000,dfk_gun_lvl1,1],
	[2000,dfk_gun_lvl2,2],
	[4000,dfk_gun_lvl3,3],
	[8000,dfk_gun_lvl4,4],
	[14000,dfk_gun_lvl5,5],
	[22000,dfk_gun_lvl6,6],
	[32000,dfk_gun_lvl7,7],
	[44000,dfk_gun_lvl8,8],
	[58000,dfk_gun_lvl9,9],
	[74000,dfk_gun_lvl10,10],
	[92000,dfk_gun_lvl11,11],
	[112000,dfk_gun_lvl12,12],
	[134000,dfk_gun_lvl13,13],
	[158000,dfk_gun_lvl14,14],
	[184000,dfk_gun_lvl15,15],
	[212000,dfk_gun_lvl16,16],
	[242000,dfk_gun_lvl17,17],
	[274000,dfk_gun_lvl18,18],
	[308000,dfk_gun_lvl19,19]
];

publicvariable "funds";
private _oldcash = 0;
_oldcas = _oldcash + funds;
private _added = [];

_cashmode = ["dfk_storekind"] call BIS_fnc_getParamValue;
//systemchat format ["economy: %1",_cashmode];
sleep 1;

if (_cashmode == 4) then
{
	private _oldkills = 0;
	
	private _cash_kills = 0;
	systemchat "Blood based economy enabled";
	while {true} do
	{
		private _update = false;
		private _str_arr = [];
		private _max_lvl = 0;

		_kills = 0;
		_kills = _kills + (floor (funds /10000));
		
		if (count allplayers > 0) then
		{	
			{
				private _arr = getPlayerScores _x;
				
				private _inf = (_arr select 0);
				private _soft = ((_arr select 1) * 1.25);
				private _hard = (_arr select 2) * 1.5;
				private _air = (_arr select 3) * 2;
				private _score = (_arr select 5) - (_arr select 0) - (_arr select 1) - (_arr select 2) - (_arr select 3);
				
				_kills = _kills + _inf + _soft + _hard + _air + _score;
				
				private _derp = _x getvariable ["dfk_ai_kills",0];
				diag_log format ["dfk_ai_kills (gun_lvl_loop.sqf) %1",_derp];
				_kills = _kills + _derp;
			} foreach allplayers;
		};
		
		//systemchat format ["Kills: %1",_kills];
		
		if (_kills != _oldkills) then
		{
			//[format ["Kills:\n%1",_kills]] remoteexec ["hintsilent"];
			_cash_kills = (_kills * 1000);
			
			{
				//systemchat format ["%1 | %2 | %3",_cash_kills,_x select 0, (_x select 0) in _added];
				if (_cash_kills >= (_x select 0) && !((_x select 0) in _added)) then 
				{
					[(_x select 1)] call dfk_addgunlvl;
					_added pushbackunique (_x select 0);
					_update = true;
					{_str_arr pushbackunique _x} foreach (_x select 1);
					
					_lvl = _x select 2;
					_max_lvl = _lvl;
					{_x setvariable ["gunlvl",_lvl,true]} foreach allplayers;
				};
			} foreach dfk_cashLevelBasedGunProgressionStuff;
			_oldkills = _kills;
		};
		
		if (_update) then 
		{
			private _str = "";
			
			{
				if ((_x iskindof ["Default", configFile >> "CfgWeapons"])) then
				{
					_str = _str + format ["%1\n",(getText (configFile >> "CfgWeapons" >> _x >> "displayname"))];
				};
				if ((_x iskindof ["Default", configFile >> "CfgMagazines"])) then
				{
					_str = _str + format ["%1\n",(getText (configFile >> "CfgMagazines" >> _x >> "displayname"))];
				};			
				
			} foreach _str_arr;
		
			
			[format ["Arsenal @ Level %2 updated with:\n%1",_str]] remoteexec ["hintsilent"];
			diag_log format ["Arsenal updated with: %1",_str];
			_update = false;
		};
		sleep 15;
	};
}
else
{
	while {true} do
	{
		private _update = false;
		private _str_arr = [];
		private _max_lvl = 0;
		
		if (funds != _oldcash) then
		{
			{
				if (funds >= (_x select 0) && !((_x select 0) in _added)) then 
				{
					[(_x select 1)] call dfk_addgunlvl;
					_added pushbackunique (_x select 0);
					_update = true;
					{_str_arr pushbackunique _x} foreach (_x select 1);
					
					_lvl = _x select 2;
					_max_lvl = _lvl;
					{_x setvariable ["gunlvl",_lvl,true]} foreach allplayers;
				};
			} foreach dfk_cashLevelBasedGunProgressionStuff;
			_oldcash = funds;
		};
		
		if (_update) then 
		{
			private _str = "";
			
			{
				if ((_x iskindof ["Default", configFile >> "CfgWeapons"])) then
				{
					_str = _str + format ["%1\n",(getText (configFile >> "CfgWeapons" >> _x >> "displayname"))];
				};
				if ((_x iskindof ["Default", configFile >> "CfgMagazines"])) then
				{
					_str = _str + format ["%1\n",(getText (configFile >> "CfgMagazines" >> _x >> "displayname"))];
				};			
				
			} foreach _str_arr;
		
			hint format ["Arsenal @ Level %2 updated with:\n%1",_str,_max_lvl];
			_update = false;
		};
		sleep 5;
	};
};