_unit = _this select 0;
_killer = _this select 1;
_instigator = _this select 2;
_type = _this select 3;

//[format ["Killer0| %1 %5 %2 %6 %3  %4 %7 %8 %9",_unit,_killer,_instigator,_type,faction _unit,faction _killer,typeof _unit]] remoteExec ["systemchat"]; 
//[format ["veh %1 truck %2 air %3",_unit iskindof "AllVehicles",_unit iskindof "Car",_unit iskindof "Air"]] remoteExec ["systemchat"]; 

if (isserver) then
{
	//["Killer1| Running on server"] remoteExec ["systemchat"]; 
	if (_unit iskindof "Man") then
	{
		if (nerfgear) then {[_unit] remoteExec ["dfk_nerf_gear",2]};
		
		//["Killer2| Is Man"] remoteExec ["systemchat"]; 
		//[format ["Killer0| %1 %5 %2 %6 %3  %4 %7 %8 %9",_unit,_killer,_instigator,_type,faction _unit,faction _killer,typeof _unit]] remoteExec ["systemchat"]; 
		//increase enemy morale
		if (faction _unit in CIVFACTION && (faction _killer in GOODFACTION OR faction _instigator in GOODFACTION) OR (side _killer == sideEnemy OR side _instigator == sideEnemy)) then 
		{
			//["Killer2-1| Warcrime"] remoteExec ["systemchat"]; 
			//increase morale, as resistance are killing civs
			[_type] remoteExecCall ["DFK_fnc_add_morale", 2]; 
			
			//increase pol as resistance commits warcrime
			["WARCRIME"] remoteExecCall ["DFK_fnc_add_political", 2]; 
			
			res_civkills_array pushback (name _unit);
			publicvariable "res_civkills_array";
		};
		
		//decrease east morale
		//if ((faction _unit in BADFACTION) && ((faction _killer in GOODFACTION) OR (faction _instigator in GOODFACTION))) then 
		if (faction _unit in BADFACTION && (faction _killer in GOODFACTION OR faction _instigator in GOODFACTION)) then 
		{
			//["Killer2-2| Killed badguy"] remoteExec ["systemchat"]; 
			//lower morale with losses
			private _derp = _killer getvariable ["retard",false];
			//systemchat format ["%1",_derp];
			if (_derp) then
			{
				//(leader _killer) addscore 1;
				_aikills = (leader _killer) getvariable ["dfk_ai_kills",0];
				_aikills = _aikills + 1;
				(leader _killer) setvariable ["dfk_ai_kills",_aikills,true];
				diag_log format ["dfk_ai_kills (killer.sqf) %1",_aikills];
				
				//systemchat "kill";
			};
			
			[1] remoteExecCall ["DFK_deaddude", 2]; 
			if (!captive _unit) then
			{
				[_type] remoteExecCall ["DFK_fnc_remove_political", 2]; 
				[_type] remoteExecCall ["DFK_fnc_remove_morale", 2]; 
			}
			else
			{
				["WARCRIME"] remoteexeccall ["DFK_fnc_add_political",2];
				["KILLEDPRISONER"] remoteexeccall ["DFK_fnc_add_morale",2];
			};
		};	
		
		if ((faction _unit in CIVFACTION) && ((faction _killer in BADFACTION) OR (faction _instigator in BADFACTION))) then 
		{
			//["Killer2-2| Warcrime!"] remoteExec ["systemchat"]; 
			//if east commits warcrime, lower pol
			["WARCRIME"] remoteExecCall ["DFK_fnc_remove_political", 2]; 
			opfor_civkills = opfor_civkills + 1;
			publicvariable "opfor_civkills";
		};	
		
		//"friendly fire" / darwin award
		if (!(_unit in playableunits) && (faction _unit in BADFACTION) && ((faction _killer in BADFACTION) OR (faction _instigator in BADFACTION))) then 
		{
			//["Killer2-2| Warcrime!"] remoteExec ["systemchat"]; 
			//if east commits warcrime, lower pol
			[1] remoteExecCall ["DFK_deaddude", 2];
			//["FF"] remoteExecCall ["DFK_fnc_remove_morale", 2]; 
			//diag_log format ["killer.sqf FF %1 %2 %3 %4",_unit,group _unit,_killer,group _killer];
			//systemchat format ["killer.sqf FF %1 %2 %3 %4",_unit,group _unit,_killer,group _killer];
		};			
	};
	
	//calc logistical stuff
	//if (_unit iskindof "LandVehicle" OR _unit iskindof "Air" OR  _type == "Vehicle") then
	if (not(_unit iskindof "Man") && (_unit iskindof "AllVehicles" OR _unit iskindof "ReammoBox_F" OR _unit iskindof "FlexibleTank_base_F")) then
	{
		//["Killer3| Not man, is vehicle or ammobox"] remoteExec ["systemchat"]; 
		//[format ["Killer2| %1 %2 %3",_unit iskindof "AllVEhicles",_unit iskindof "ReammoBox_F",_unit iskindof "FlexibleTank_base_F"]] remoteExec ["systemchat"]; 
		if (faction _unit in BADFACTION) then 
		{
			//["Killer3-1| Badguys lost a vehicle"] remoteExec ["systemchat"]; 
			[_unit] remoteExec ["DFK_fnc_remove_logistics", 2]; 
			//[format ["Killer1| Vehicle! %1",typeof _unit]] remoteExec ["systemchat"]; 
		};
		
		if (faction _unit in GOODFACTION) then 
		{
			//["Killer3-2| Good guys lost a vehicle"] remoteExec ["systemchat"]; 
			["ENEMYDESTROYEDVEHICLE"] remoteExec ["DFK_fnc_add_morale", 2]; 
		};	
		
		if (faction _unit in CIVFACTION && (faction _killer in GOODFACTION OR faction _instigator in GOODFACTION) OR (side _killer == sideEnemy OR side _instigator == sideEnemy)) then 
		{
			//["Killer3-3| Warcriming, killing civ vehicles"] remoteExec ["systemchat"]; 
			//increase morale, as resistance are killing civs
			if (_type == "") then
			{
				[_type] remoteExec ["DFK_fnc_add_morale", 2]; 
			}
			else
			{
				["ENEMYDESTROYEDVEHICLE"] remoteExec ["DFK_fnc_add_morale", 2]; 
			};

			//increase pol as resistance commits warcrime
			["WARCRIME"] remoteExec ["DFK_fnc_add_political", 2]; 	

			res_civkills_vehcost = res_civkills_vehcost + (getNumber (configFile >> "CfgVehicles" >> typeof _unit >> "cost"));
			publicvariable "res_civkills_vehcost";
		};		
		
		if (faction _unit in CIVFACTION && (faction _killer in BADFACTION OR faction _instigator in BADFACTION)) then 
		{
			//["Killer3-4| Warcriming badguys killing civ vehicles"] remoteExec ["systemchat"]; 
			["WARCRIME"] remoteExec ["DFK_fnc_remove_political", 2];		

			opfor_civkills_vehcost = opfor_civkills_vehcost + (getNumber (configFile >> "CfgVehicles" >> typeof _unit >> "cost"));
			publicvariable "opfor_civkills_vehcost";			
		};	
	};
	
	// if (_unit iskindof "Building" or _type == "building") then
	// {
		// diag_log _this;
		// dfk_record_house2 = dfk_record_house2 + 1;
		// if (faction _killer in GOODFACTION or faction _instigator in GOODFACTION) then
		// {
			// res_civkills_vehcost = res_civkills_vehcost + (getNumber (configFile >> "CfgVehicles" >> typeof _unit >> "cost"));
		// };
		
		// if (faction _killer in BADFACTION or faction _instigator in BADFACTION) then
		// {
			// opfor_civkills_vehcost = opfor_civkills_vehcost + (getNumber (configFile >> "CfgVehicles" >> typeof _unit >> "cost"));
		// };		
	// };
	
};