_gen = _this select 0;
_caller = _this select 1;
_id = _this select 2;
_arr1 = _this select 3;

#include "defines.h";

_scarecrow_pos = _arr1 select 0;
_scarecrowname = _arr1 select 1;
_scarecrow_tickrate = _arr1 select 2;

_markarr = [];
_tick = 0;
publicvariable "funds";

_reporttargets = [];
_oldtargets = [];

if (funds > 1000) then
{
	[3000] call dfk_remove_funds;

	
	//_civgrp = creategroup civilian;
	//_civgrp setGroupIdGlobal [_scarecrowname,"GroupColor6"];

	_p = [_scarecrow_pos, 1, 50, 3, 0, 0.5, 0] call BIS_fnc_findSafePos;
	_scarecrow = createVehicle [dfk_scarecrow_dronekind, [0,0,1000], [], 10, "NONE"];
	// _ai = _civgrp createUnit ["C_UAV_AI_F", _scarecrow_pos, [], 0, "NONE"];
	// _ai moveindriver _scarecrow;
	createvehiclecrew _scarecrow;
	_civgrp = group driver _scarecrow;
	_scarecrow setpos _p;

	if (_scarecrow_tickrate < 1) then {_scarecrow_tickrate = 1};
	_scarecrow_marker = [getpos _scarecrow,"colorGUER",_scarecrowname,"hd_flag",1] call dfk_generate_marker;
	[format ["Scarecrow %1 active at %2",_scarecrowname,nearestlocationwithdubbing [_scarecrow_pos select 0,_scarecrow_pos select 1]]] remoteExec ["systemchat"];
	_str = str format ["This is %1 reporting in!",_scarecrowname];
	[_scarecrow, _str] remoteexec ["globalchat"];
	
	sleep 9;
	
	 [_civgrp, _p, 100,blacklist_water] call bis_fnc_taskPatrol;
	 
	 _scarecrow setbehaviour "CARELESS";
	 _scarecrow setcombatmode "BLUE";
	 _scarecrow setspeedmode "LIMITED";
	 _scarecrow setunitpos "UP";
	 
	_scarecrow setskill ["aimingAccuracy",1];
	_scarecrow setskill ["aimingShake",0];
	_scarecrow setskill ["aimingSpeed",1];
	_scarecrow setskill ["spotDistance",1];
	_scarecrow setskill ["spotTime",1];
	_scarecrow setskill ["courage",1];
	_scarecrow setskill ["reloadSpeed",1];
	_scarecrow setskill ["commanding",1];
	_scarecrow setskill ["general",1];		
	
	
	while {alive _scarecrow} do
	{	
		if (_tick mod _scarecrow_tickrate == 0) then
		{
			{
				deleteMarker _x;
				if (markeralpha _x <= 0) then
				{
					deletemarker _x;
					//scarecrow globalchat format ["DELETE %1",_x];
				};
				//scarecrow globalchat format ["DELETEDERP %1",_x];
				_markarr deleteat _foreachindex;
			} foreach _markarr;
			
			_report = false;
			{
				if (_x select 3 > 0) then 
				{
					_displayname = "ERROR";
					_displayname = gettext(configfile >> "Cfgvehicles" >> (_x select 1) >> "displayname");
					_marker = [(_x select 0),"colorOPFOR",_displayname,"hd_unknown",1] call dfk_generate_marker;
					_markarr pushback _marker;	
					
					if (!(_x select 4 in _oldtargets)) then
					{
						_oldtargets pushbackunique (_x select 4);
						_reporttargets pushback _displayname;
						_report = true;
					};
					
				};
				
			} foreach (_scarecrow neartargets 800);
			
			if (_report) then
			{
				_reportarr = [];
				
				{
					_strdamptype = _x;
					if (!(_strdamptype in _reportarr)) then
					{
						_nr = {_x == _strdamptype} count _reporttargets;
						_reportarr pushbackunique [_nr,_strdamptype];
					};
				} foreach _reporttargets;
				
				_str2 = "";
				
				{
					_damp = "";
					_damp = _damp + str (_x select 0) + " ";
					_damp = _damp + str (_x select 1) + " ";
					_str2 = _str2 + _damp;
				} foreach _reportarr;
				
				_str = format ["From %1 reporting activity! %2",_scarecrowname,_str2];
				[_scarecrow, _str] remoteexec ["globalchat"];
				_reporttargets = [];
				
				{
					if (_x distance _scarecrow > 800) then
					{
						_oldtargets deleteat _foreachindex;
					};
				} foreach _oldtargets;
			};
			//hint format ["%1\n\n%2 %3",_markarr,count _markarr,count allmapmarkers];
		};
		
		{
			_x setmarkeralpha ((markeralpha _x) - (1/_scarecrow_tickrate));
			if (markeralpha _x <= 0) then
			{
				deletemarker _x;
				//scarecrow globalchat format ["DELETEDAMP %1",_x];
				_markarr deleteat _foreachindex;
			};		
		} foreach _markarr;
		
		_scarecrow_marker setmarkerpos getpos _scarecrow;		
		sleep 1;
		_tick = _tick + 1;
	};

	{
		_x setmarkeralpha 0;
		deleteMarker _x;
		
		if (markeralpha _x <= 0) then
		{
			deletemarker _x;
			//scarecrow globalchat format ["DELETEDAMP %1",_x];
		};		
	} foreach _markarr;
	//OH NO! SCARECROW IS DOWN!
	deletemarker _scarecrow_marker;
	//hint format ["END \n%1\n\n%2 %3\nEND",_markarr,count _markarr,count allmapmarkers];
}
else
{
	["Insufficient funds to hire scarecrow"] remoteExec ["systemchat"];
};