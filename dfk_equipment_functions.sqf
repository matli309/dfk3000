dfk_random_cophat = ["Headgear_H_Bandanna_gry","Headgear_H_Bandanna_blu","Headgear_H_Beret_blk","Headgear_H_Cap_blu","Headgear_H_Cap_oli","Headgear_H_Cap_tan","Headgear_H_Cap_blk","Headgear_H_Cap_grn","H_MilCap_blue","H_MilCap_gry"];

dfk_equipment_cop1 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	removeUniform _dude;
	_dude forceAddUniform "U_B_GEN_Soldier_F";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 2 do {_dude addItemToUniform "30Rnd_9x21_Mag_SMG_02_Tracer_Red";};
	_dude addVest "V_TacVest_blk";
	for "_i" from 1 to 3 do {_dude addItemToVest "16Rnd_9x21_red_Mag";};
	for "_i" from 1 to 5 do {_dude addItemToVest "30Rnd_9x21_Mag_SMG_02_Tracer_Red";};
	_dude addHeadgear selectrandom dfk_random_cophat;
	_dude addWeapon "SMG_05_F";
	_dude addPrimaryWeaponItem "acc_flashlight";
	_dude addPrimaryWeaponItem "optic_Aco_smg";
	_dude addWeapon "hgun_Rook40_F";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
};

dfk_equipment_cop2 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	_dude forceAddUniform "U_B_GEN_Soldier_F";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 2 do {_dude addItemToUniform "Chemlight_red";};
	for "_i" from 1 to 4 do {_dude addItemToUniform "6Rnd_45ACP_Cylinder";};
	_dude addVest "V_LegStrapBag_black_F";
	for "_i" from 1 to 2 do {_dude addItemToVest "FirstAidKit";};
	for "_i" from 1 to 4 do {_dude addItemToVest "SmokeShell";};
	for "_i" from 1 to 4 do {_dude addItemToVest "6Rnd_45ACP_Cylinder";};
	_dude addHeadgear selectrandom dfk_random_cophat;
	_dude addWeapon "hgun_Pistol_heavy_02_F";
	_dude addHandgunItem "acc_flashlight_pistol";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
};

dfk_equipment_cop2_1 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	_dude forceAddUniform "U_B_GEN_Soldier_F";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 2 do {_dude addItemToUniform "Chemlight_red";};
	for "_i" from 1 to 3 do {_dude addItemToUniform "16Rnd_9x21_red_Mag";};
	_dude addVest "V_LegStrapBag_black_F";
	for "_i" from 1 to 2 do {_dude addItemToVest "FirstAidKit";};
	for "_i" from 1 to 4 do {_dude addItemToVest "SmokeShell";};
	for "_i" from 1 to 5 do {_dude addItemToVest "16Rnd_9x21_red_Mag";};
	_dude addHeadgear selectrandom dfk_random_cophat;
	_dude addWeapon "hgun_Rook40_F";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	
	if (0.5 > random 1) then
	{
		_dude linkItem "SAN_Headlamp_v2";
	};
};

dfk_equipment_cop2_2 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	_dude forceAddUniform "U_B_GEN_Soldier_F";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	for "_i" from 1 to 2 do {_dude addItemToUniform "Chemlight_red";};
	for "_i" from 1 to 3 do {_dude addItemToUniform "9Rnd_45ACP_Mag";};
	_dude addVest "V_LegStrapBag_black_F";
	for "_i" from 1 to 2 do {_dude addItemToVest "FirstAidKit";};
	for "_i" from 1 to 4 do {_dude addItemToVest "SmokeShell";};
	for "_i" from 1 to 3 do {_dude addItemToVest "9Rnd_45ACP_Mag";};
	for "_i" from 1 to 6 do {_dude addItemToVest "Chemlight_red";};
	_dude addHeadgear selectrandom dfk_random_cophat;
	_dude addWeapon "hgun_ACPC2_F";
	_dude addHandgunItem "acc_flashlight_pistol";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
};

dfk_equipment_cop1_2 =
{
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_B_GEN_Soldier_F";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	for "_i" from 1 to 2 do {_dude addItemToUniform "SmokeShellRed";};
	_dude addItemToUniform "30Rnd_9x21_Mag_SMG_02_Tracer_Red";
	_dude addVest "V_TacVest_blk";
	for "_i" from 1 to 2 do {_dude addItemToVest "HandGrenade";};
	_dude addItemToVest "SmokeShell";
	_dude addItemToVest "SmokeShellRed";
	_dude addItemToVest "Chemlight_red";
	for "_i" from 1 to 7 do {_dude addItemToVest "30Rnd_9x21_Mag_SMG_02_Tracer_Red";};

	_dude addWeapon "SMG_05_F";
	_dude addPrimaryWeaponItem "acc_flashlight";
	_dude addPrimaryWeaponItem "optic_Aco_smg";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	
	if (0.5 > random 1) then
	{
		_dude linkItem "SAN_Headlamp_v2";
	};	
};

dfk_equipment_cop1_3 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	removeUniform _dude;
	_dude forceAddUniform "U_B_GEN_Soldier_F";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "FirstAidKit";	
	for "_i" from 1 to 2 do {_dude addItemToUniform "30Rnd_9x21_Red_Mag";};
	_dude addBackpack "B_LegStrapBag_black_F";
	for "_i" from 1 to 6 do {_dude addItemToVest "30Rnd_9x21_Red_Mag";};
	for "_i" from 1 to 3 do {_dude addItemToBackpack "Chemlight_red";};
	for "_i" from 1 to 2 do {_dude addItemToBackpack "SmokeShellYellow";};	
	_dude addHeadgear selectrandom dfk_random_cophat;
	_dude addWeapon "hgun_PDW2000_F";
	_dude addPrimaryWeaponItem "optic_Aco_smg";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	
	if (0.5 > random 1) then
	{
		_dude linkItem "SAN_Headlamp_v2";
	};	
};

dfk_equipment_cop3 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	_dude forceAddUniform "U_B_GEN_Commander_F";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 2 do {_dude addItemToUniform "Chemlight_red";};
	for "_i" from 1 to 4 do {_dude addItemToUniform "6Rnd_45ACP_Cylinder";};
	_dude addVest "V_TacVest_blk";
	for "_i" from 1 to 6 do {_dude addItemToVest "50Rnd_570x28_SMG_03";};
	for "_i" from 1 to 3 do {_dude addItemToVest "SmokeShell";};
	_dude addHeadgear "H_PASGT_basic_black_F";
	_dude addGoggles "G_Balaclava_blk";
	_dude addWeapon "SMG_03C_TR_black";
	_dude addPrimaryWeaponItem "acc_flashlight";
	_dude addPrimaryWeaponItem "optic_Aco_smg";
	_dude addWeapon "hgun_Pistol_heavy_02_F";
	_dude addHandgunItem "acc_flashlight_pistol";
	_dude addHandgunItem "optic_Yorris";
	_dude addWeapon "Binocular";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
};

dfk_equipment_cop4 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	_dude forceAddUniform "U_B_GEN_Commander_F";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 2 do {_dude addItemToUniform "Chemlight_red";};
	for "_i" from 1 to 4 do {_dude addItemToUniform "6Rnd_45ACP_Cylinder";};
	_dude addVest "V_TacVest_blk";
	for "_i" from 1 to 3 do {_dude addItemToVest "SmokeShell";};
	for "_i" from 1 to 6 do {_dude addItemToVest "20Rnd_762x51_Mag";};
	_dude addHeadgear "H_PASGT_basic_black_F";
	_dude addGoggles "G_Balaclava_blk";
	_dude addWeapon "srifle_DMR_03_F";
	_dude addPrimaryWeaponItem "acc_flashlight";
	_dude addPrimaryWeaponItem "optic_Yorris";
	_dude addWeapon "hgun_Pistol_heavy_02_F";
	_dude addHandgunItem "acc_flashlight_pistol";
	_dude addHandgunItem "optic_Yorris";
	_dude addWeapon "Binocular";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
};

dfk_equipment_cop5 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	_dude forceAddUniform "U_B_GEN_Commander_F";
	_dude addItemToUniform "FirstAidKit";
	for "_i" from 1 to 2 do {_dude addItemToUniform "Chemlight_red";};
	for "_i" from 1 to 4 do {_dude addItemToUniform "6Rnd_45ACP_Cylinder";};
	_dude addVest "V_TacVest_blk";
	for "_i" from 1 to 3 do {_dude addItemToVest "SmokeShell";};
	for "_i" from 1 to 7 do {_dude addItemToVest "30Rnd_556x45_Stanag_Tracer_Red";};
	_dude addHeadgear "H_PASGT_basic_black_F";
	_dude addGoggles "G_Balaclava_blk";
	_dude addWeapon "arifle_SPAR_01_blk_F";
	_dude addPrimaryWeaponItem "acc_flashlight";
	_dude addPrimaryWeaponItem "optic_Yorris";
	_dude addWeapon "hgun_Pistol_heavy_02_F";
	_dude addHandgunItem "acc_flashlight_pistol";
	_dude addHandgunItem "optic_Yorris";
	_dude addWeapon "Binocular";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
};

dfk_equipment_cop6 =
{
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_B_GEN_Commander_F";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	for "_i" from 1 to 2 do {_dude addItemToUniform "Chemlight_red";};
	_dude addItemToUniform "11Rnd_45ACP_Mag";
	_dude addVest "V_TacVest_blk";
	for "_i" from 1 to 16 do {_dude addItemToVest "CUP_8Rnd_B_Beneli_74Pellets";};
	_dude addBackpack "B_LegStrapBag_black_F";
	_dude addItemToBackpack "FirstAidKit";
	for "_i" from 1 to 4 do {_dude addItemToBackpack "11Rnd_45ACP_Mag";};
	for "_i" from 1 to 2 do {_dude addItemToBackpack "HandGrenade";};
	for "_i" from 1 to 2 do {_dude addItemToBackpack "SmokeShell";};
	for "_i" from 1 to 5 do {_dude addItemToBackpack "Chemlight_red";};
	_dude addHeadgear "H_PASGT_basic_black_F";
	_dude addGoggles "G_Balaclava_blk";

	_dude addWeapon "CUP_sgun_M1014";
	_dude addPrimaryWeaponItem "optic_Aco";
	_dude addWeapon "hgun_Pistol_heavy_01_F";
	_dude addHandgunItem "acc_flashlight_pistol";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
};


dfk_equipment_driver =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	_dude forceAddUniform "U_O_officer_noInsignia_hex_F";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	_dude addItemToUniform "30Rnd_45ACP_Mag_SMG_01_Tracer_Red";
	_dude addVest "V_TacVest_khk";
	for "_i" from 1 to 2 do {_dude addItemToVest "FirstAidKit";};
	for "_i" from 1 to 5 do {_dude addItemToVest "Chemlight_red";};
	for "_i" from 1 to 4 do {_dude addItemToVest "SmokeShellRed";};
	_dude addItemToVest "SmokeShell";
	for "_i" from 1 to 5 do {_dude addItemToVest "30Rnd_45ACP_Mag_SMG_01_Tracer_Red";};
	_dude addBackpack "B_LegStrapBag_black_F";
	_dude addItemToBackpack "ToolKit";	
	_dude addHeadgear "H_HelmetCrew_O";
	_dude addWeapon "SMG_01_F";
	_dude addPrimaryWeaponItem "acc_flashlight_smg_01";
	_dude addPrimaryWeaponItem "optic_ACO_grn_smg";
	_dude addWeapon "hgun_Rook40_F";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	
	_dude setUnitTrait ["engineer",true];
};

dfk_equipment_crew =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	_dude forceAddUniform "U_O_CombatUniform_ocamo";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	_dude addItemToUniform "30Rnd_45ACP_Mag_SMG_01_Tracer_Red";
	_dude addVest "V_TacVest_khk";
	for "_i" from 1 to 2 do {_dude addItemToVest "FirstAidKit";};
	for "_i" from 1 to 5 do {_dude addItemToVest "Chemlight_red";};
	for "_i" from 1 to 4 do {_dude addItemToVest "SmokeShellRed";};
	_dude addItemToVest "SmokeShell";
	for "_i" from 1 to 5 do {_dude addItemToVest "30Rnd_45ACP_Mag_SMG_01_Tracer_Red";};
	_dude addHeadgear "H_HelmetCrew_O";
	_dude addWeapon "SMG_01_F";
	_dude addPrimaryWeaponItem "acc_flashlight_smg_01";
	_dude addPrimaryWeaponItem "optic_ACO_grn_smg";
	_dude addWeapon "hgun_Rook40_F";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
};

dfk_equipment_low1 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_officer_noInsignia_hex_F";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	for "_i" from 1 to 2 do {_dude addItemToUniform "30Rnd_9x21_Mag_SMG_02_Tracer_Red";};
	_dude addVest "V_BandollierB_khk";
	for "_i" from 1 to 5 do {_dude addItemToVest "30Rnd_9x21_Mag_SMG_02_Tracer_Red";};
	for "_i" from 1 to 4 do {_dude addItemToVest "Chemlight_red";};
	_dude addWeapon "SMG_02_F";
	_dude addPrimaryWeaponItem "acc_flashlight";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemWatch";
};

dfk_equipment_low2 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	_dude forceAddUniform "U_O_officer_noInsignia_hex_F";
	for "_i" from 1 to 4 do {_dude addItemToUniform "30Rnd_65x39_caseless_green_mag_Tracer";};
	_dude addVest "V_BandollierB_khk";
	for "_i" from 1 to 2 do {_dude addItemToVest "FirstAidKit";};
	for "_i" from 1 to 4 do {_dude addItemToVest "Chemlight_red";};
	for "_i" from 1 to 2 do {_dude addItemToVest "30Rnd_65x39_caseless_green_mag_Tracer";};
	_dude addWeapon "arifle_Katiba_C_F";
	_dude addPrimaryWeaponItem "acc_flashlight";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemWatch";
};

dfk_equipment_simulate_hat =
{
	_dude = _this select 0;
	
	_hats = ["H_Cap_oli","H_Cap_blk","H_Cap_tan","H_Cap_oli_hs","H_Cap_brn_SPECOPS","H_BandMask_blk","H_Bandanna_khk","H_Bandanna_surfer_blk","H_Bandanna_camo","H_Shemag_olive","H_ShemagOpen_tan"];
	
	removeheadgear _dude;
	_dude addHeadgear (selectrandom _hats);
};

dfk_equipment_normal1 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	_dude forceAddUniform "U_O_CombatUniform_ocamo";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	_dude addItemToUniform "Chemlight_red";
	for "_i" from 1 to 2 do {_dude addItemToUniform "30Rnd_65x39_caseless_green_mag_Tracer";};
	_dude addVest "V_BandollierB_khk";
	_dude addItemToVest "SmokeShell";
	_dude addItemToVest "SmokeShellRed";
	_dude addItemToVest "Chemlight_red";
	for "_i" from 1 to 7 do {_dude addItemToVest "30Rnd_65x39_caseless_green_mag_Tracer";};
	_dude addHeadgear "H_HelmetLeaderO_ocamo";
	_dude addWeapon "arifle_Katiba_F";
	_dude addPrimaryWeaponItem "acc_flashlight";
	_dude addPrimaryWeaponItem "optic_ACO_grn";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemWatch";
};

dfk_equipment_normal2 =
{
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	_dude forceAddUniform "U_O_CombatUniform_ocamo";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "Chemlight_red";
	for "_i" from 1 to 2 do {_dude addItemToUniform "30Rnd_65x39_caseless_green_mag_Tracer";};
	_dude addItemToUniform "16Rnd_9x21_Mag";
	_dude addVest "V_TacVest_khk";
	for "_i" from 1 to 7 do {_dude addItemToVest "30Rnd_65x39_caseless_green_mag_Tracer";};
	for "_i" from 1 to 2 do {_dude addItemToVest "MiniGrenade";};
	_dude addItemToVest "HandGrenade";
	_dude addItemToVest "SmokeShell";
	_dude addHeadgear "H_HelmetLeaderO_ocamo";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addWeapon "arifle_Katiba_F";
	_dude addPrimaryWeaponItem "optic_Arco";
	_dude addWeapon "hgun_Rook40_F";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
};

dfk_equipment_normal3 =
{
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	_dude forceAddUniform "U_O_CombatUniform_ocamo";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	_dude addItemToUniform "Chemlight_red";
	for "_i" from 1 to 3 do {_dude addItemToUniform "16Rnd_9x21_Mag";};
	_dude addVest "V_TacVest_khk";
	for "_i" from 1 to 7 do {_dude addItemToVest "30Rnd_65x39_caseless_green_mag_Tracer";};
	for "_i" from 1 to 2 do {_dude addItemToVest "MiniGrenade";};
	_dude addItemToVest "HandGrenade";
	_dude addItemToVest "SmokeShell";
	_dude addHeadgear "H_HelmetLeaderO_ocamo";
	_dude addWeapon "arifle_Katiba_F";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_Arco";
	_dude addWeapon "hgun_Rook40_F";
	_dude addWeapon "Laserdesignator_02";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
	_dude linkItem "O_NVGoggles_OPFOR";
};

dfk_equipment_iwokeuplikethis =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	_dude addVest "V_HarnessO_brn";
	for "_i" from 1 to 2 do {_dude addItemToVest "FirstAidKit";};
	for "_i" from 1 to 6 do {_dude addItemToVest "30Rnd_65x39_caseless_green_mag_Tracer";};
	_dude addWeapon "arifle_Katiba_F";
};

dfk_equipment_sqdld =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	_dude forceAddUniform "U_O_CombatUniform_ocamo";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "Chemlight_red";
	for "_i" from 1 to 3 do {_dude addItemToUniform "30Rnd_65x39_caseless_green_mag_Tracer";};
	_dude addVest "V_TacVest_khk";
	_dude addItemToVest "SmokeShell";
	_dude addItemToVest "SmokeShellRed";
	_dude addItemToVest "Chemlight_red";
	for "_i" from 1 to 3 do {_dude addItemToVest "30Rnd_65x39_caseless_green_mag_Tracer";};
	for "_i" from 1 to 3 do {_dude addItemToVest "16Rnd_9x21_red_Mag";};
	for "_i" from 1 to 5 do {_dude addItemToVest "1Rnd_HE_Grenade_shell";};
	for "_i" from 1 to 2 do {_dude addItemToVest "UGL_FlareWhite_F";};
	for "_i" from 1 to 3 do {_dude addItemToVest "1Rnd_Smoke_Grenade_shell";};
	_dude addHeadgear "H_MilCap_ocamo";
	_dude addGoggles "G_Aviator";	
	_dude addWeapon "arifle_Katiba_GL_F";
	_dude addPrimaryWeaponItem "acc_flashlight";
	_dude addPrimaryWeaponItem "optic_Arco";
	_dude addWeapon "hgun_Rook40_F";
	_dude addWeapon "Binocular";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
};

dfk_equipment_boss = 
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	_dude forceAddUniform "U_O_OfficerUniform_ocamo";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "Chemlight_red";
	for "_i" from 1 to 5 do {_dude addItemToUniform "6Rnd_45ACP_Cylinder";};
	_dude addVest "V_Rangemaster_belt";
	_dude addHeadgear "H_Beret_ocamo";
	_dude addGoggles "G_Aviator";
	_dude addWeapon "hgun_Pistol_heavy_02_F";
	_dude addHandgunItem "acc_flashlight_pistol";
	_dude addHandgunItem "optic_Yorris";
	_dude addWeapon "Binocular";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
};

dfk_equipment_compch =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	_dude forceAddUniform "U_O_CombatUniform_ocamo";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "Chemlight_red";
	for "_i" from 1 to 2 do {_dude addItemToUniform "6Rnd_45ACP_Cylinder";};
	for "_i" from 1 to 2 do {_dude addItemToUniform "30Rnd_65x39_caseless_green_mag_Tracer";};
	_dude addVest "V_TacVest_khk";
	_dude addItemToVest "SmokeShell";
	_dude addItemToVest "SmokeShellRed";
	_dude addItemToVest "Chemlight_red";
	for "_i" from 1 to 4 do {_dude addItemToVest "1Rnd_HE_Grenade_shell";};
	for "_i" from 1 to 2 do {_dude addItemToVest "UGL_FlareWhite_F";};
	for "_i" from 1 to 3 do {_dude addItemToVest "1Rnd_Smoke_Grenade_shell";};
	for "_i" from 1 to 2 do {_dude addItemToVest "6Rnd_45ACP_Cylinder";};
	for "_i" from 1 to 4 do {_dude addItemToVest "30Rnd_65x39_caseless_green_mag_Tracer";};
	_dude addHeadgear "H_MilCap_ocamo";
	_dude addGoggles "G_Aviator";
	_dude addWeapon "arifle_Katiba_C_F";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_Arco";
	_dude addWeapon "hgun_Pistol_heavy_02_F";
	_dude addHandgunItem "acc_flashlight_pistol";
	_dude addHandgunItem "optic_Yorris";
	_dude addWeapon "Laserdesignator_02";
	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "ItemGPS";
	_dude linkItem "O_NVGoggles_OPFOR";
};

dfk_equipment_mg_low =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_officer_noInsignia_hex_F";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "FirstAidKit";
	_dude addItemToUniform "Chemlight_red";
	_dude addVest "V_BandollierB_khk";
	_dude addItemToVest "SmokeShell";
	_dude addItemToVest "SmokeShellRed";
	_dude addItemToVest "Chemlight_red";
	_dude addItemToVest "150Rnd_762x54_Box_Tracer";
	_dude addBackpack "B_AssaultPack_ocamo";
	for "_i" from 1 to 2 do {_dude addItemToBackpack "150Rnd_762x54_Box_Tracer";};
	for "_i" from 1 to 2 do {_dude addItemToBackpack "SmokeShellRed";};

	_dude addWeapon "LMG_Zafir_F";
	_dude addPrimaryWeaponItem "acc_flashlight";
	_dude addPrimaryWeaponItem "optic_Arco";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
};

dfk_equipment_mg_normal1 =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_CombatUniform_ocamo";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	_dude addItemToUniform "Chemlight_red";
	for "_i" from 1 to 3 do {_dude addItemToUniform "16Rnd_9x21_red_Mag";};
	_dude addVest "V_TacVest_khk";
	_dude addItemToVest "SmokeShell";
	_dude addItemToVest "SmokeShellRed";
	_dude addItemToVest "Chemlight_red";
	_dude addItemToVest "150Rnd_762x54_Box_Tracer";
	for "_i" from 1 to 2 do {_dude addItemToVest "HandGrenade";};
	_dude addBackpack "B_AssaultPack_ocamo";
	for "_i" from 1 to 3 do {_dude addItemToBackpack "150Rnd_762x54_Box_Tracer";};
	for "_i" from 1 to 2 do {_dude addItemToBackpack "SmokeShellRed";};
	_dude addHeadgear "H_HelmetO_ocamo";

	_dude addWeapon "LMG_Zafir_F";
	_dude addPrimaryWeaponItem "acc_flashlight";
	_dude addPrimaryWeaponItem "optic_Arco";
	_dude addWeapon "hgun_Rook40_F";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
};

dfk_equipment_mg_normal2 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_CombatUniform_ocamo";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	_dude addItemToUniform "Chemlight_red";
	for "_i" from 1 to 3 do {_dude addItemToUniform "16Rnd_9x21_red_Mag";};
	_dude addVest "V_TacVest_khk";
	_dude addItemToVest "SmokeShell";
	_dude addItemToVest "SmokeShellRed";
	_dude addItemToVest "Chemlight_red";
	for "_i" from 1 to 2 do {_dude addItemToVest "HandGrenade";};
	_dude addItemToVest "150Rnd_93x64_Mag";
	_dude addBackpack "B_AssaultPack_ocamo";
	for "_i" from 1 to 2 do {_dude addItemToBackpack "SmokeShellRed";};
	for "_i" from 1 to 2 do {_dude addItemToBackpack "150Rnd_93x64_Mag";};
	_dude addHeadgear "H_HelmetO_ocamo";

	_dude addWeapon "MMG_01_hex_F";
	_dude addPrimaryWeaponItem "acc_flashlight";
	_dude addPrimaryWeaponItem "optic_Arco";
	_dude addPrimaryWeaponItem "bipod_02_F_hex";
	_dude addWeapon "hgun_Rook40_F";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
};

dfk_equipment_mg_hightech =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_CombatUniform_ocamo";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	_dude addItemToUniform "Chemlight_red";
	for "_i" from 1 to 3 do {_dude addItemToUniform "16Rnd_9x21_red_Mag";};
	_dude addVest "V_TacVest_khk";
	_dude addItemToVest "SmokeShell";
	_dude addItemToVest "SmokeShellRed";
	_dude addItemToVest "Chemlight_red";
	for "_i" from 1 to 2 do {_dude addItemToVest "HandGrenade";};
	_dude addItemToVest "150Rnd_93x64_Mag";
	_dude addBackpack "B_AssaultPack_ocamo";
	for "_i" from 1 to 2 do {_dude addItemToBackpack "SmokeShellRed";};
	for "_i" from 1 to 2 do {_dude addItemToBackpack "150Rnd_93x64_Mag";};
	_dude addHeadgear "H_HelmetO_ocamo";

	_dude addWeapon "MMG_01_hex_F";
	_dude addPrimaryWeaponItem "acc_pointer_IR";
	_dude addPrimaryWeaponItem "optic_tws_mg";
	_dude addPrimaryWeaponItem "bipod_02_F_hex";
	_dude addWeapon "hgun_Rook40_F";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	_dude linkItem "NVGoggles_OPFOR";
};

dfk_equipment_at1 =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_CombatUniform_ocamo";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	_dude addItemToUniform "Chemlight_red";
	for "_i" from 1 to 3 do {_dude addItemToUniform "16Rnd_9x21_red_Mag";};
	_dude addVest "V_TacVest_khk";
	_dude addItemToVest "SmokeShell";
	_dude addItemToVest "SmokeShellRed";
	_dude addItemToVest "Chemlight_red";
	for "_i" from 1 to 2 do {_dude addItemToVest "HandGrenade";};
	for "_i" from 1 to 5 do {_dude addItemToVest "30Rnd_65x39_caseless_green";};
	_dude addBackpack "B_AssaultPack_ocamo";
	for "_i" from 1 to 2 do {_dude addItemToBackpack "SmokeShellRed";};
	_dude addItemToBackpack "RPG32_F";
	for "_i" from 1 to 2 do {_dude addItemToBackpack "RPG32_HE_F";};
	_dude addHeadgear "H_HelmetO_ocamo";

	_dude addWeapon "arifle_Katiba_C_F";
	_dude addPrimaryWeaponItem "acc_flashlight";
	_dude addPrimaryWeaponItem "optic_Aco_smg";
	_dude addWeapon "launch_RPG32_F";
	_dude addWeapon "hgun_Rook40_F";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
};

dfk_opfor_unarmedmedic =
{
	_dude = _this select 0;
	
	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;

	_dude forceAddUniform "U_O_officer_noInsignia_hex_F";
	for "_i" from 1 to 2 do {_dude addItemToUniform "FirstAidKit";};
	for "_i" from 1 to 2 do {_dude addItemToUniform "Chemlight_red";};
	_dude addVest "V_Plain_crystal_F";
	_dude addHeadgear "H_Cap_brn_SPECOPS";

	_dude linkItem "ItemMap";
	_dude linkItem "ItemCompass";
	_dude linkItem "ItemWatch";
	_dude linkItem "ItemRadio";
	
	if (0.5 > random 1) then
	{
		_dude linkItem "SAN_Headlamp_v2";
	};	
};

dfk_equipment_random_civ =
{
	_dude = _this select 0;

	removeAllWeapons _dude;
	removeAllItems _dude;
	removeAllAssignedItems _dude;
	removeUniform _dude;
	removeVest _dude;
	removeBackpack _dude;
	removeHeadgear _dude;
	removeGoggles _dude;
	
	_civclothes =
	[
		"U_OrestesBody",
		"U_C_Poor_1",
		"U_C_Poor_2",
		"U_C_Man_casual_4_F",
		"U_C_Man_casual_5_F",
		"U_C_Man_casual_6_F",
		"U_rangemaster",
		"U_C_Mechanic_01_F",
		"U_C_Poloshirt_blue",
		"U_C_Poloshirt_burgundy",
		"U_C_Poloshirt_stripped",
		"U_C_Poloshirt_tricolour",
		"U_C_Poloshirt_salmon",
		"U_C_Poloshirt_redwhite",
		"U_C_Poor_shorts_1",
		"U_NikosBody",
		"U_NikosAgedBody",
		"U_C_Man_casual_1_F",
		"U_C_Man_casual_2_F",
		"U_C_Man_casual_3_F",
		"U_Marshal",
		"U_C_Journalist",
		"U_BG_Guerilla2_3",
		"U_BG_Guerilla2_1",
		"U_BG_Guerilla2_2",
		"U_BG_Guerilla2_1",
		"U_C_ConstructionCoverall_Red_F",
		"U_C_ConstructionCoverall_vrana_F",
		"U_C_ConstructionCoverall_Black_F",
		"U_C_ConstructionCoverall_Blue_F",
		"U_I_C_Soldier_Bandit_3_F",
		"U_I_C_Soldier_Bandit_5_F",
		"U_I_C_Soldier_Bandit_2_F",
		"U_I_C_Soldier_Bandit_1_F",
		"U_I_C_Soldier_Bandit_4_F"
	];
	
	_civvests = [
		"V_LegStrapBag_coyote_F",
		"V_LegStrapBag_black_F",
		"V_LegStrapBag_olive_F",
		"V_Pocketed_coyote_F",
		"V_Pocketed_black_F",
		"V_Pocketed_olive_F"
	];
	
	_civbackpacks = 
	[
		"B_AssaultPack_blk",
		"B_Respawn_TentDome_F",
		"B_Respawn_TentA_F",
		"B_Respawn_Sleeping_bag_F",
		"B_Respawn_Sleeping_bag_blue_F",
		"B_Respawn_Sleeping_bag_brown_F",
		"B_FieldPack_oli",
		"B_Kitbag_sgg",
		"B_Messenger_Coyote_F",
		"B_Messenger_Olive_F",
		"B_Messenger_Black_F",
		"B_Messenger_Gray_F",
		"B_TacticalPack_oli"
	];
	
	_civhats =
	[
		"Headgear_H_HeadBandage_stained_F",
		"Headgear_H_HeadBandage_bloody_F",
		"Headgear_H_HeadBandage_clean_F",
		"Headgear_H_Hat_Safari_sand_F",
		"Headgear_H_Hat_Safari_olive_F",
		"Headgear_H_Bandanna_surfer",
		"Headgear_H_Bandanna_khk",
		"Headgear_H_Bandanna_khk_hs",
		"Headgear_H_Bandanna_cbr",
		"Headgear_H_Bandanna_sgg",
		"Headgear_H_Bandanna_sand",
		"Headgear_H_Bandanna_gry",
		"Headgear_H_Bandanna_surfer_blk",
		"Headgear_H_Bandanna_surfer_grn",
		"Headgear_H_Bandanna_blu",
		"Headgear_H_Shemag_olive",
		"Headgear_H_ShemagOpen_khk",
		"Headgear_H_ShemagOpen_tan",
		"Headgear_H_Beret_blk",
		"Headgear_H_StrawHat",
		"Headgear_H_StrawHat_dark",
		"Headgear_H_Hat_blue",
		"Headgear_H_Hat_brown",
		"Headgear_H_Hat_camo",
		"Headgear_H_Hat_grey",
		"Headgear_H_Hat_checker",
		"Headgear_H_Hat_tan",
		"Headgear_H_Cap_red",
		"Headgear_H_Cap_blu",
		"Headgear_H_Cap_oli",
		"Headgear_H_Cap_tan",
		"Headgear_H_Cap_blk",
		"Headgear_H_Cap_grn",
		"Headgear_H_Cap_grn_BI",
		"Headgear_H_Cap_blk_ION",
		"Headgear_H_Cap_oli_hs",
		"Headgear_H_Cap_surfer"
	];
	
	_civcargostuff =
	[
		"6Rnd_GreenSignal_F",
		"6Rnd_RedSignal_F",
		"UGL_FlareWhite_F",
		"UGL_FlareGreen_F",
		"UGL_FlareRed_F",
		"UGL_FlareYellow_F",
		"Laserbatteries",
		"1Rnd_Smoke_Grenade_shell",
		"1Rnd_SmokeRed_Grenade_shell",
		"1Rnd_SmokeGreen_Grenade_shell",
		"1Rnd_SmokeYellow_Grenade_shell",
		"1Rnd_SmokePurple_Grenade_shell",
		"1Rnd_SmokeBlue_Grenade_shell",
		"1Rnd_SmokeOrange_Grenade_shell",
		"SmokeShell",
		"SmokeShellGreen",
		"SmokeShellRed",
		"SmokeShellYellow",
		"SmokeShellPurple",
		"SmokeShellBlue",
		"SmokeShellOrange",
		"Chemlight_green",
		"Chemlight_red",
		"Chemlight_yellow",
		"Chemlight_blue",
		"ItemWatch",
		"ItemCompass",
		"ItemGPS",
		"ItemRadio",
		"ItemMap",
		"MineDetector",
		"Binocular",
		"Rangefinder",
		"NVGoggles_INDEP",
		"Laserdesignator",
		"FirstAidKit",
		"Medikit",
		"ToolKit",
		"I_UavTerminal",
		"acc_flashlight",
		"acc_flashlight_pistol"	
	];
	
	_dude forceAddUniform selectrandom _civclothes;

	if (random 1 < 0.8) then 
	{
		_dude addVest selectrandom _civvests;
		if (random 1 > 0.5) then
		{
			for "_derp" from 0 to random 10 do
			{
				_dude addItemToVest selectrandom _civcargostuff;
			};		
		};
	};
	
	if (random 1 < 0.5) then 
	{
		_dude addBackpack selectrandom _civbackpacks;
		if (random 1 > 0.5) then
		{
			for "_derp" from 0 to random 10 do
			{
				_dude addItemToBackpack selectrandom _civcargostuff;
			};
		};		
	};
	
	_dude addHeadgear selectrandom _civhats;	
	
	if (0.5 > random 1) then
	{
		_dude linkItem "SAN_Headlamp_v2";
	};	
};