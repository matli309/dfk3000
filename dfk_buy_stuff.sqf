//dfk consume
//disableserialization;
// _actionarray = _this select 0;

// #ifndef sanctionedmagsarray
	// #define sanctionedmagsarray sanctionedmagsarray_def
// #endif

#include "defines.h";

// _gen = _actionarray select 0;
// _caller = _actionarray select 1;
// _id = _actionarray select 2;
_selectedindex = _this select 0;

_gunarr = storearray select _selectedindex;
_gunclass = _gunarr select 0;
_gunprice = _gunarr select 1;
_type = "";

_gunprice = _gunprice * rabatt;

//player globalchat format ["bs %1",_this];
//player globalchat ;
//[format ["bs %1 %2 %3 %4",_gunarr,_gunclass,_gunprice]] remoteExec ["systemchat"]; 

//[format []] remoteExec ["hint"]; 

_ammoboxriflearray = [];
_ammoboxitemarray = [];
_ammoboxmagazinearray = [];
_ammoboxbackpackarray = [];

if (_gunclass iskindof ["Rifle", configFile >> "CfgWeapons"] or _gunclass iskindof ["Pistol", configFile >> "CfgWeapons"] or _gunclass iskindof ["Launcher", configFile >> "CfgWeapons"] or _gunclass iskindof ["Mgun", configFile >> "CfgWeapons"]) then
{
	_type = "rifle";
};
_derparr = ["MineDetector","Binocular","NVGoggles_INDEP","Rangefinder"];
if (_gunclass iskindof ["ItemCore", configFile >> "CfgWeapons"] or _gunclass in _derparr) then
{
	_type = "item";
};

if (_gunclass iskindof "Bag_Base") then
{
	_type = "backpack";
};	

//Smokes-let'sgo!
if (_gunclass == "Smokes") then
{
	_type = "smokes";
};	

if (_gunclass == "Grenades") then
{
	_type = "Grenades";
};		

if (_gunclass == "Chemlights") then
{
	_type = "Chemlights";
};	

_bombsarr = ["IEDUrbanSmall_Remote_Mag","IEDLandSmall_Remote_Mag","ATMine_Range_Mag","APERSMine_Range_Mag","APERSBoundingMine_Range_Mag","SLAMDirectionalMine_Wire_Mag","APERSTripMine_Wire_Mag","ClaymoreDirectionalMine_Remote_Mag","SatchelCharge_Remote_Mag","DemoCharge_Remote_Mag","IEDUrbanBig_Remote_Mag","IEDLandBig_Remote_Mag"];
if (_gunclass in _bombsarr or (_gunarr select 0) in _bombsarr) then
{
	_type = "bomb";
	//[format ["bs %",_type]] remoteExec ["systemchat"]; 
};	

publicvariable "funds";
if (_gunprice <= funds) then
{
	if (_type == "bomb") then
	{
		_ammoboxmagazinearray append [_gunclass];	
		//[format ["bs1 %1",_gunclass]] remoteExec ["systemchat"]; 
		//[format ["bs2 %1",_ammoboxmagazinearray]] remoteExec ["systemchat"]; 		
	};

	if (_gunclass == "IED") then
	{
		_ammoboxmagazinearray append ["IEDUrbanSmall_Remote_Mag","IEDLandSmall_Remote_Mag"];	
	};

	if (_gunclass == "glasses") then
	{
		_ammoboxitemarray append 
		[
			"G_Spectacles","G_Spectacles_Tinted","G_Combat","G_Lowprofile","G_Shades_Black","G_Shades_Green","G_Shades_Red",
			"G_Squares","G_Squares_Tinted","G_Sport_BlackWhite","G_Sport_Blackyellow","G_Sport_Greenblack","G_Sport_Checkered",
			"G_Sport_Red","G_Tactical_Black","G_Aviator","G_Lady_Mirror","G_Lady_Dark","G_Lady_Red","G_Lady_Blue",
			"G_Shades_Blue","G_Sport_Blackred","G_Tactical_Clear","G_Combat_Goggles_tna_F"		
		];
	};

	if (_gunclass == "Diving googles") then
	{
		_ammoboxitemarray append ["G_Diving","G_I_Diving"];
	};

	if (_gunclass == "Balacalavas") then
	{
		_ammoboxitemarray append ["G_Balaclava_blk","G_Balaclava_oli","G_Balaclava_combat","G_Balaclava_lowprofile"];
	};

	if (_gunclass == "Bandanas") then
	{
		_ammoboxitemarray append ["G_Bandanna_blk","G_Bandanna_oli","G_Bandanna_khk","G_Bandanna_tan","G_Bandanna_beast","G_Bandanna_shades","G_Bandanna_sport","G_Bandanna_aviator"];
	};

	if (_type == "Chemlights") then
	{
		_ammoboxmagazinearray append ["Chemlight_green","Chemlight_red","Chemlight_yellow","Chemlight_blue"];	
	};
	
	if (_type == "Grenades") then
	{
		_ammoboxmagazinearray append ["HandGrenade","MiniGrenade","I_IR_Grenade"];	
	};

	if (_type == "Smokes") then
	{
		_ammoboxmagazinearray append ["SmokeShell","SmokeShellRed","SmokeShellGreen","SmokeShellYellow","SmokeShellPurple","SmokeShellBlue","SmokeShellOrange"];	
	};	

	if (_type == "rifle") then
	{
		//player globalchat "rifle";
		_ammoboxriflearray append [_gunclass];
		//_nomags = true;
		//_mags = [];
		
		//bother figuring out all the exceptions
		//just add all mags
		_tempmags = getarray(configfile >> "CfgWeapons" >> _gunclass >> "magazines");
		_sanctionedmags = [];
		
		_mz = getarray(configfile >> "CfgWeapons" >> _gunclass >> "muzzles");
		if (count _mz >= 2) then
		{
			if ("EGLM" in _mz or "GL_3GL_F" in _mz) then
			{
				_derp = [
							"1Rnd_HE_Grenade_shell",
							"3Rnd_HE_Grenade_shell",
							"1Rnd_Smoke_Grenade_shell",
							"3Rnd_Smoke_Grenade_shell",
							"1Rnd_SmokeRed_Grenade_shell",
							"3Rnd_SmokeRed_Grenade_shell",
							"1Rnd_SmokeGreen_Grenade_shell",
							"3Rnd_SmokeGreen_Grenade_shell",
							"1Rnd_SmokeYellow_Grenade_shell",
							"3Rnd_SmokeYellow_Grenade_shell",
							"1Rnd_SmokePurple_Grenade_shell",
							"3Rnd_SmokePurple_Grenade_shell",
							"1Rnd_SmokeBlue_Grenade_shell",
							"3Rnd_SmokeBlue_Grenade_shell",
							"1Rnd_SmokeOrange_Grenade_shell",
							"3Rnd_SmokeOrange_Grenade_shell",
							"UGL_FlareWhite_F",
							"3Rnd_UGL_FlareWhite_F",
							"UGL_FlareGreen_F",
							"3Rnd_UGL_FlareGreen_F",
							"UGL_FlareRed_F",
							"3Rnd_UGL_FlareRed_F",
							"UGL_FlareYellow_F",
							"3Rnd_UGL_FlareYellow_F",
							"UGL_FlareCIR_F",
							"3Rnd_UGL_FlareCIR_F",
							"FlareWhite_F",
							"FlareGreen_F",
							"FlareRed_F",
							"FlareYellow_F"							
						];
						
				_sanctionedmags append _derp;
			};
			
			if ("Secondary" in _mz) then
			{
				_derp = [
								"10Rnd_50BW_Mag_F"
						];
						
				_sanctionedmags append _derp;
			};			
		};
		
		//sanctioned mags only
		//tracers only
		//(ish)

		{
			if (_x in sanctionedmagsarray) then {_sanctionedmags append [_x]};
		} foreach _tempmags;
		// {
			// if ((getnumber(configfile >> "CfgMagazines" >> _x >> "tracersEvery")) == 1) then
			// {
				// player globalchat format ["1.2 %1",_x];
				// _mags append [_x];
				// _nomags = false;
			// };
		// } foreach _tempmags;
		
		// if (_nomags) then
		// {
			// _ammoboxmagazinearray append _tempmags;
		// }
		// else
		// {
			// _ammoboxmagazinearray append _mags;		
		// };
		
		_ammoboxmagazinearray append _sanctionedmags;
	};
	
	if (_type == "item") then
	{
		//player globalchat "item";
		_ammoboxitemarray append [_gunclass];
	};	
		
	if (_type == "backpack") then
	{
		//player globalchat "backpack";
		_ammoboxbackpackarray append [_gunclass];
	};
	
	//player globalchat format ["1 %1 %2 %3 %4",_ammoboxriflearray,_ammoboxmagazinearray,_ammoboxitemarray,_ammoboxbackpackarray];
	[_ammoboxriflearray,_ammoboxitemarray,_ammoboxmagazinearray,_ammoboxbackpackarray] remoteExec ["dfk_blip_ammoboxes"];
	// [ammobox,_ammoboxriflearray,true] call xla_fnc_addVirtualWeaponCargo;
	// [ammobox,_ammoboxitemarray,true] call xla_fnc_addVirtuaItemCargo;
	// [ammobox,_ammoboxmagazinearray,true] call xla_fnc_addVirtualMagazineCargo;
	// [ammobox,_ammoboxbackpackarray,true] call xla_fnc_addVirtuaBackpackCargo;
	
	//fix later
	[_gunprice] call dfk_remove_funds;

	//_gen removeAction _id;

	storearray deleteAt _selectedindex;	
	publicvariable "storearray";
}
else
{
	hint "insufficient funds!";
};