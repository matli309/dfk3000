//dfk parachute cargocrate script

_crate = _this select 0;
_mode = _this select 1;

clearBackpackCargoGlobal _crate;
clearItemCargoGlobal _crate;
clearMagazineCargoGlobal _crate;
clearWeaponCargoGlobal _crate;

for "_x" from 0 to (10 + random 10) do
{
	[_crate,(selectrandom gunlootarray) select 0,random 10] call dfk_addRandomEquipmentToBox;
};

if (!(_crate getvariable ["dontbother",false])) then
{
	sleep 2;

	//if drop from great height, wait until 500 meters to deploy
	//other, wait 2 secs to clear helo, then open
	while {((getpos _crate) select 2) > 150} do
	{
		sleep 1;
	};

	_chute = createVehicle ["O_Parachute_02_F", [0,0,100], [], 0, "FLY"];
	_chute setpos getpos _crate;
	_crate attachto [_chute,[0,0,0]];

	_crate allowdamage false;

	while {((getpos _crate) select 2) > 8} do
	{
		sleep 1;
	};
	detach _crate;
	detach _chute;

	sleep 5;
	deletevehicle _chute;
	_crate allowdamage true;
	_crate setvariable ["important",true,true];
	_crate setvariable ["dontbother",true,true];

	//if delivery, add logistics and remove, else, let resistance pilfer crate
	if (_mode == "unload") then
	{
		sleep 1;
		["AIRDROP"] remoteExec ["DFK_fnc_add_logistics",2];
		deletevehicle _crate;
	};

	if (_mode == "unloadsmall") then
	{
		sleep 1;
		["AIRDELIVERYSMALL"] remoteExec ["DFK_fnc_add_logistics",2];
		deletevehicle _crate;
	};

	if (_mode == "panic") then
	{
		_smoke = createvehicle [selectrandom ["SmokeShellBlue","SmokeShellGreen","SmokeShellRed","SmokeShellYellow"], getpos _crate,[],20,"NONE"];
		_smoke attachto [_crate,[0,0,0]];
	
		waituntil {(getpos _crate select 2) < 1};
		_spawnpos = getpos _crate;

		[_spawnpos,10,"cratedrop"] call dfk_drop_loot;
		
		for "_x" from 0 to 9 do
		{
			_derp = createvehicle [selectrandom ["Chemlight_red","Chemlight_green","Chemlight_blue","F_40mm_Red","F_40mm_Green","F_40mm_Yellow","F_40mm_Red","SmokeShellBlue","SmokeShellGreen","SmokeShellRed","SmokeShellYellow"], getpos _crate,[],20,"NONE"];
		};
		
	};
};