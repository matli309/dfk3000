_drone = _this select 0;

_dir = 0;

while {alive _drone} do
{
	if (count (_drone targets [true, 16000, [west,resistance,sideEnemy]]) <= 0) then
	{
		_drone dowatch (_drone getpos [1000,_dir]);
	};
	sleep 2;
	_dir = _dir + 20;
	if (_dir > 360) then {_dir = 0};
};
