_bomberman = _this select 0;
_corpse = _this select 1;

#ifndef bombkindarray
	//bombkindarray = ["DemoCharge_Remote_Mag","IEDLandBig_Remote_Mag","IEDUrbanBig_Remote_Mag","IEDLandSmall_Remote_Mag","IEDUrbanSmall_Remote_Mag","SatchelCharge_Remote_Mag"];
	#define bombkindarray ["DemoCharge_Remote_Mag","IEDLandBig_Remote_Mag","IEDUrbanBig_Remote_Mag","IEDLandSmall_Remote_Mag","IEDUrbanSmall_Remote_Mag","SatchelCharge_Remote_Mag"]
#endif

_bombactionarray = [];
_bombvehiclearray = [];

_bombininventory = true;

// publicvariable "bombkindarray";
// publicvariable "artillerykindsarray";

_bomberman setvariable ["lastbomb",objNull,true];
_bomberman setvariable ["bombed",[false,"",""],true];

_bomberman addEventHandler 
	[
		"fired",
		{
			_bomberman = (_this select 0);
			
			//if (debug) then {[format ["%1",_this]] remoteexec ["systemchat"]};
			
			if ((_this select 5) in bombkindarray) then
			{
				_bomberman setvariable ["lastbomb",(_this select 6),true];
			};
			
			//hijacking this EH to add counter-art functions, unnecessary to use more scripts for that IMO
			// diag_log _this;
			// {
				// if ((_this select 1) iskindof _x) then
				// {
					// [getpos _bomberman] remoteexec ["dfk_artyradar_addtarget",2];
				// };
			// } foreach artillerykindsarray;
		}
	];
	
//hijacking this script to add counter-art functions, unnecessary to use more scripts for that IMO	
_bomberman addEventHandler 
	[
		"WeaponAssembled",
		{
			_bomberman = _this select 0;
			_arty = _this select 1;
			if (_arty iskindof "StaticMortar") then
			{
				_arty addeventhandler
				[
					"fired",
					{
						//[getpos (_this select 0)] remoteexec ["dfk_request_arty",2];
						[getpos (_this select 0)] remoteexec ["dfk_artyradar_addtarget",2];
					}				
				];
				
			};
		}
	];	

{_bomberman addOwnedMine _x} foreach getAllOwnedMines _corpse;

	
while {alive _bomberman} do
{
	_magtypes = [];
	{
		if ((_x in bombkindarray) && !(_x in _magtypes)) then
		{
			_magtypes append [_x];
		};
	} foreach (magazines _bomberman);

	_removeindex = [];
	_near = ((getpos _bomberman) nearentities [["LandVehicle","C_Boat_Civil_04_F","Ship"],8]);
	{
		if (!((_x select 1) in _near) or !((_x select 2) in _magtypes) or vehicle _bomberman != _bomberman) then
		{
			_bomberman removeaction (_x select 0);
			_bombvehiclearray deleteat (_bombvehiclearray find (_x select 1));
			_bombactionarray deleteat _foreachindex;
		};
	} foreach _bombactionarray;

	if (vehicle _bomberman == _bomberman) then
	{
		{
			_veh = _x;
			if (!(_veh in _bombvehiclearray)) then
			{		
				{
					_magtype = _x;				
					_str = format ["Attach %1 to %2",gettext (configfile >> "CfgMagazines" >> _magtype >> "displayName"),gettext (configfile >> "CfgVehicles" >> (typeof _veh) >> "displayName")];
					
					_bombaction = _bomberman addAction
					[
						_str,
						{
							_veh = _this select 3 select 0;
							_magtype = _this select 3 select 1;
							_bomberman = _this select 3 select 2;
							
							_bomberman playActionNow "PutDown";
							if (_magtype in ["DemoCharge_Remote_Mag","IEDLandSmall_Remote_Mag","IEDUrbanSmall_Remote_Mag"]) then
							{
								_currwp = currentweapon _bomberman;
								_bomberman selectWeapon "DemoChargeMuzzle";
								_bomberman fire ["DemoChargeMuzzle", "DemoChargeMuzzle", _magtype];
								_bomberman setWeaponReloadingTime [_bomberman, "DemoChargeMuzzle", 0];
								sleep 0.5;
								_bomberman selectweapon _currwp;
							}
							else
							{
								_currwp = currentweapon _bomberman;
								_bomberman selectWeapon "PipeBombMuzzle";
								_bomberman fire ["PipeBombMuzzle", "PipeBombMuzzle", _magtype];
								_bomberman setWeaponReloadingTime [_bomberman, "PipeBombMuzzle", 0];
								sleep 0.5;
								_bomberman selectweapon _currwp;
							};

							_bomberman setvariable ["bombed",[true,_veh,_magtype],true];
						},
						[_veh,_magtype,_bomberman]
					];

					_bombactionarray append [[_bombaction,_veh,_magtype]];
					_bombvehiclearray append [_veh];
				} foreach _magtypes;
			};
		} foreach _near;
		sleep 0.1;
	};
	_bombed =[];
	_lastbomb = objNull;
	isNil {_lastbomb = _bomberman getVariable "lastbomb"};
	isNil {_bombed = _bomberman getVariable "bombed"};

	
	if ((_bombed select 0) && !(isNull _lastbomb)) then
	{
		_lastbomb attachto [_bombed select 1,[(random 1) + (random -1),(random 1) + (random -1), -1.5 + (random -1)]];
		_lastbomb hideObjectGlobal true;
		_bomberman setvariable ["lastbomb",objNull,true];
		_bomberman setvariable ["bombed",[false,"",""],true];	

		{
			_bomberman removeaction (_x select 0);
			_bombvehiclearray deleteat (_bombvehiclearray find (_x select 1));
			_bombactionarray deleteat _foreachindex;
		} foreach _bombactionarray;
	};
};

//_bomberman removeAllMPEventHandlers "Fired"; 
_bomberman removeAllEventHandlers "Fired"; 
{
	_bomberman removeaction (_x select 0);
	_bombvehiclearray deleteat (_bombvehiclearray find (_x select 1));
	_bombactionarray deleteat _foreachindex;
} foreach _bombactionarray;
removeAllActions _bomberman;